/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.test.basic;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.nexala.spectrum.channel.Binary;

public class TestBinaryUtil {
    @Test
    public void test1() {
        int value1 = Binary.binToInt(0);
        assertTrue(value1 == 0);

        int value2 = Binary.binToInt(1);
        assertTrue(value2 == 1);
        
        int value3 = Binary.binToInt(1, 0);
        assertTrue(value3 == 2);
        
        int value4 = Binary.binToInt(1, 1);
        assertTrue(value4 == 3);
        
        int value5 = Binary.binToInt(1, 0, 0);
        assertTrue(value5 == 4);
        
        int value6 = Binary.binToInt(
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1);
        
        assertTrue(value6 == Integer.MAX_VALUE);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void test2() {
        Binary.binToInt(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
    }
    
    @Test
    public void test3() {
        int value1 = Binary.binToInt(false);
        assertTrue(value1 == 0);

        int value2 = Binary.binToInt(true);
        assertTrue(value2 == 1);
        
        int value3 = Binary.binToInt(true, false);
        assertTrue(value3 == 2);
        
        int value4 = Binary.binToInt(true, true);
        assertTrue(value4 == 3);
        
        int value5 = Binary.binToInt(true, false, false);
        assertTrue(value5 == 4);
        
        int value6 = Binary.binToInt(
                true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true);
        
        assertTrue(value6 == Integer.MAX_VALUE);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void test4() {
        Binary.binToInt(true, true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true, true, true,
                true, true, true, true);
    }
}