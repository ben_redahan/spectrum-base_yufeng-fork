/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.test.loader;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ NumberAlgorithmTest.class, DotNetTicksAlgorithmTest.class })
public class AllLoaderTests {}