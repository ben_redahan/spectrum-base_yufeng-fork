/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.test.web.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -280201646776929572L;

    public void doPost(HttpServletRequest req, HttpServletResponse resp) {

        resp.setContentType("text");

        PrintWriter writer = null;

        try {
            writer = resp.getWriter();

            for (int i = 0; i < 1024; i++) {
                writer.write("A");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        doPost(req, resp);
    }
}
