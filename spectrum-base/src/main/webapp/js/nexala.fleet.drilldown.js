var nx = nx || {};
nx.fleet = nx.fleet || {};

nx.fleet.DrillDown = (function() {
    function ctor(group, unitIds, fleetId, refreshRate) {
        this.__group = group;
        this.__unitIds = unitIds;
        this.__dialog = $('#drilldown_dialog');
        this.__grid = null;
        this.__table = null;
        this.__formationObj = null;
        this.__intervalId = null;
        this.__fleetId = fleetId;
        this.__refreshRate = refreshRate

        var me = this;

        nx.rest.fleet.drilldown(function(response) {
            me.buildTable(me.__group.header, response.columns, 
                    response.activeVehicle, response.data.length);
            
            me.__applyFilter(response);
            me.updateData(response.data);
            // We only display always visible channels 
            //me.__hideChannels();
            me.__resizeDialog();

            me.__dialog.dialog('option', 'position', 'center');
            
            $("body").removeClass("wait");
            
            me.__intervalId = window.setInterval(function() {
                nx.rest.fleet.drilldown(function(updated) {
                    me.updateData(updated.data);
                    // We only display always visible channels 
                    //me.__hideChannels();
                }, me.__unitIds, me.__group.name, true, me.__fleetId);
            }, me.__refreshRate);
            
        }, this.__unitIds, this.__group.name, true, this.__fleetId);
    };
    
    ctor.prototype.__applyFilter = function(response) {
    };
    
    ctor.prototype.__resizeDialog = function() {
        this.__table = $('.drilldownTable');
        var height = this.__table.height();
        var bodyHeight = $('body').height();
        if (height >= bodyHeight) {
            height = (bodyHeight - 100);
            this.__table.css('height', height);
        }
        var containerHeight = $('.drilldownTable .tableContainer').height();
        if (height > containerHeight) {
            this.__table.css('height', containerHeight);
        }
    };
    
    ctor.prototype.updateData = function(data) {
        var gridData = [];
        
        for (var i = 0, l = data.length; i < l; i++) {
            var row = data[i].channels;
            var rowArr = [];
            var rowChanged =[];
            rowChanged.push ($.i18n(row['channelName'][0]));
            rowChanged.push (row['channelName'][1]);
            rowArr.push(rowChanged);
            rowArr.push(row.alwaysDisplayed);
            
            for (var j = 0; j < this.__formationObj.length; j++) {
                // Get the value for unit[j] from the response..
                rowArr.push(row[this.__formationObj[j].name]);
            }
            rowArr.push(rowChanged);
            gridData.push({id: i, data: rowArr});
        }
        
        this.__grid.setData(gridData);
    };
    
    ctor.prototype.buildTable = function(header, columns, activeVehicle, dataLength) {
        var me = this;
        
        this.__dialog.html('<div class = "drilldownTable"><table></table></div>');
        
        var tableColumns = [];
        var unitRow = [];
        var vehicleRow = [];

        unitRow.push({
            name: columns[0].name,
            width: columns[0].width,
            text: '', // XXX
            rowspan: 2,
            sortable: false
        });

        unitRow.push({
            name: columns[1].name,
            width: columns[1].width,
            text: '', // XXX
            rowspan: 2,
            sortable: false,
            visible: columns[1].visible
        });

        this.__formationObj = [];

        for (var i = 2, l = columns.length; i < l; i++) {
        	if (columns[i].columns === undefined) {
        		var col = columns[i];

        		this.__formationObj.push(col);

        		vehicleRow.push({
                    name: col.name,
                    text: col.header,
                    width: col.width || 60,
                    sortable: false,
                    visible: col.visible,
                    styles: col.styles,
                    formatter: function(el, value) {
                        var value = me.__columnFormatter(el, value);
                        return value;
                    }
                });
        	} else {
        		var columnGroup = columns[i];
                unitRow.push({
                    name: columnGroup.name,
                    text: columnGroup.header,
                    colspan: columnGroup.columns.length,
                    sortable: false
                });

                for (var x = 0, y = columnGroup.columns.length; x < y; x++) {
                    var col = columnGroup.columns[x];
                    
                    this.__formationObj.push(col);
                    
                    vehicleRow.push({
                        name: col.name,
                        text: col.header,
                        width: col.width || 60,
                        sortable: false,
                        visible: col.visible,
                        styles: col.styles,
                        formatter: function(el, value) {
                            var value = me.__columnFormatter(el, value);
                            return value;
                        }
                    });
                }
        	}
        }

        if (unitRow.length > 0) {
        	tableColumns.push(unitRow);
        }
        tableColumns.push(vehicleRow);

        this.__table = $('.drilldownTable');
        
        this.__grid = this.__table.tabular({
            fixedHeader: true,
            columns: tableColumns,
            cellMouseOverHandler: (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseoverHandler(cell, data, columnDef);
                };
            })(me)
        });
        
        //highlight the active vehicle column header
        // TODO :: Better way?
        if (!!activeVehicle && $.trim(activeVehicle) != '') {
            var columnHeaders = $("#drilldown_dialog").find('.headerContent > span');
            for (var index = 0; index < columnHeaders.length; index++) {
                var span = columnHeaders[index];
                
                if ($.trim($(span).text()) == $.trim(activeVehicle)) {
                    var parent = span.parentElement;
                    $(parent).addClass('activeVehicle');
                }
            }
        }
        
        this.openDialog(header);
    };

    ctor.prototype.__mouseoverHandler = function(cell, data, columnDef) {
        if (cell.context.cellIndex == 0) {
        	var titleIndex = data.data.length - 1;
            var title = data.data[titleIndex]['0'];
            cell.attr('title', title);
        }
    };

    ctor.prototype.openDialog = function(groupHeader) {
        this.__dialog.dialog({
            'title': $.i18n(groupHeader) + $.i18n(' channel values'),
            'autoOpen': true,
            'modal': true,
            'width': 'auto',
            'height': 'auto',
            'min-height': '0px',
            'resizable': false,
            'position': 'center'
        });
    
        var me = this;

        this.__dialog.dialog({
            'close' : function(event, ui) {
                if (me.__intervalId != null) {
                    window.clearInterval(me.__intervalId);
                    me.__intervalId = null;
                }
                
                me.__dialog.empty();
            }
        });
    };
    
    ctor.prototype.__hideChannels = function() {
        if (!this.__grid) {
            return;
        }
        
        var trs = this.__table.find('tbody tr');
        
        for (var i = 0, l = trs.length; i < l; i++) {
            var alwaysDisplayed = this.__grid.getRowData(i)[1][0];
            if (!alwaysDisplayed) {
                this.__grid.hideRow(i);
            }
        }
        
        this.__grid.updateStriping();
    };
    
    ctor.prototype.__columnFormatter = function(column, value) {
        var cellText = !!value ? value[0] || '&nbsp;' : '&nbsp;';
        var cellCat = !!value ? value[1] || null : null;
        
        var className = !!cellCat ? column.styles[nx.util.CATEGORIES[cellCat]] 
                : 'drill_default';
        
        return "<div class = '" + className + "'>" + cellText + "</div>";
    };

    return ctor;
}());
