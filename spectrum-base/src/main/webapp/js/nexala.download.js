var nx = nx || {};
nx.download = nx.download || {};
/**
 * nx.download
 * 
 * A collection of download utilities
 */
nx.download.Download = (function() {
    function ctor () {
        var me = this; 
        me.__downloadTable;
        me.__units;
        me.__unitsNumberList = [];
        // the datatable column list
        me.__columList = [];
        // the column descriptions as they are received from the server.
        me.__columns = [];
        me.__downloadConfig = [];
        me.__previousStartTime = 0;

        me.downloadTable = $("#downloadTable");
        me.__downloadHeader = $("#downloadHeader");
        me.__downloadDialog = $("#createDownloadDialog");
        me.__downloadDialog.dialog({ 
        	autoOpen: false,
        	resizable: false,
        	title: 'Request download',
            width: 360,
            modal: true
    	});

        //SearchInputs
        me.__fleet = $('#fleetField');
        me.__startDate = $('#startDateField');
        me.__endDate = $('#endDateField');
        me.__unitNumber = $('#unitNumberField');
        me.__vehicleNumber = $('#vehicleNumberField');
        me.__fileType = $('#fileTypeField');
        me.__fileTypeList = {};

        nx.rest.download.downloadConfig(function(response) {
        	me.__downloadConfig = response;
        	
        	me.__initDownloadColumns(response.columns, me);
        	
        	me.__initPopup(response.popupFilters, me);
        	
        	me.__init();
        	
        	me.__initFileTypes(response.fileTypes, me);
        	
        	me.__initTimepickers();

        });
        
        me.__eventFireObject = {
                flag: false,
                isAlreadyFired: function () {
                    var wasFired = me.__eventFireObject.flag;
                    me.__eventFireObject.flag = true;
                    setTimeout(function () { me.__eventFireObject.flag = false; }, 
                            200);
                    return wasFired;
                }
            };      

    };
          
    ctor.prototype.__initFileTypes = function(fileTypes, me) {
        for (var i = 0, l = fileTypes.length; i < l; i++) {
            var option = fileTypes[i];
            me.__fileType.append($("<option />")
                    .val(option.id).text(option.name));
           
            $(me.__fileTypeDialog).append($("<option style='background-color: white !important' />")
                    .val(option.id).text(option.name));
            
            $(me.__fileTypeDialog).uniform({selectAutoWidth : false});
            
            me.__fileTypeList[option.id] = option;
        }
        if(fileTypes.length == 0)
            me.__fileType.val(1);
        else
            me.__fileType.val(fileTypes[0].id);
        $.uniform.update();
    };

    ctor.prototype.__initDownloadColumns = function(columns, me) {
     me.__columns =  [];        
        for (var i = 0; i < columns.length; i++) {
            var column = columns[i];              
            me.__columns.push({
                'name': column.name,
                'text': column.header, // FIXME... if not for this property - conversion would not be needed
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'format': column.format,
                'handler': column.handler
            });
        }

         me.__downloadTable = $('#downloadTable').tabular({
            'columns': me.__columns,
            'scrollbar': true,
            'fixedHeader': true,
            'renderOnlyVisibleRows' : true
        });
    };
    
    ctor.prototype.__initPopup = function(filters, me) {
        me.__filters =  [];   //not used?     

        var rows = 0;
        
        var popupFilters = $("#popupFilters");
        
        for (var i = 0, l = filters.length; i < l; i++) {
            var filter = filters[i];
            var input = null;
            
            var div = $('<div>')
                .addClass('labelledItem');
            
            var label = $('<label>')
                .addClass('searchLabel100')
                .addClass('labelSelect')
                .attr('for', 'uniform-'+filter.name)
                .text($.i18n(filter.label));

            switch (filter.type)
            {
	            case "input_text" : { 
	            	input = $('<input>')
	            	.attr('id', me.__newTextFilter(filter.name))
	            	.attr('name', filter.name)
	                .attr('type', 'text')
	            	.addClass('inputText');
	            	rows ++;
	            }break;
	            case "select" : { 
	            	input = $('<select>')
	            	.attr('id', me.__newTextFilter(filter.name))
	    	        .attr('name', filter.name)
	    	        .attr('width', filter.width)
	    	        .addClass('selector')
	    	        .append('<option />');
	            	
	            	if(!!filter.values){
	            		var values = filter.values;

			    		$.each(values, function(i, value) {
			    			if(value.licence == null || value.licence == "" || licences[value.licence] == true) {
			    				input.append($("<option>", {
				    				value: value.value,
				    				text: $.i18n(value.label)
				    			}));
				    		}
			    		});
	            	}
		    		
	            	rows++;
	            }break;
	            case "multiple_select" : { 
	            	input = $('<select multiple>')
	    	        .attr('id', me.__newTextFilter(filter.name))
	    	        .attr('name', filter.name)
	    	        .attr('width', filter.width)
	    	        .attr('style', 'background-color: white !important; padding: 0px !important')
	            	.addClass('multiselect');
			        
	            	if(!!filter.values){
	            		var values = filter.values;

			    		$.each(values, function(i, value) {
			    			if(value.licence == null || value.licence == "" || licences[value.licence] == true) {
			    				input.append($('<option>', {
				    				value: value.value,
				    				text: $.i18n(value.label)
				    			}));
			    			}
			    		});
	            	}
	            	rows = rows + 3;
	            }break;
	            case "date" : { 
	            	input = $('<div>')
	    	        .attr('id', me.__newTextFilter(filter.name))
	    	        .attr('name', filter.name)
	    	        .attr('style', 'display:inline-block')
	    	        .addClass('timePicker date');
	            	
	            	var inputDiv = $('<div>')
	    	        .addClass('inputDiv');
	            	
	            	label.addClass('timeLabel');
	            	
	            	input.append(inputDiv);
	            	rows++;
	            }break;
	            case "time" : { 
	            	input = $('<div>')
	    	        .attr('id', me.__newTextFilter(filter.name))
	    	        .attr('name', filter.name)
	    	        .attr('style', 'display:inline-block')
	    	        .addClass('timePicker time');
	            	
	            	var inputDiv = $('<div>')
	    	        .addClass('inputDiv');
	            	
	            	label.addClass('timeLabel');
	            	
	            	input.append(inputDiv);
	            	rows++;
	            }break;
	            
            }
            
            if (!!filter.disabledConditions) {            	
            	$.each(filter.disabledConditions, function(i, disableCondition) {
            		
            		$(eval(me.__newTextFilter(disableCondition.filterName))).data('filterToBeDisabled', filter.name);
    				
            		$(eval(me.__newTextFilter(disableCondition.filterName))).change(function() {
    					
            			var operator = disableCondition.operator;
    					var selectedOption = $(eval(me.__newTextFilter(disableCondition.filterName))).find("option:selected").text().toString();
    					var filterToBeDisabled = $(eval(me.__newTextFilter(disableCondition.filterName))).data('filterToBeDisabled');
    					
    					if($(eval(me.__newTextFilter(filterToBeDisabled))).length > 0) {
    						if (eval('"' + selectedOption + '"' + operator + '"' + disableCondition.value.toString() + '"')) {
    							$(eval(me.__newTextFilter(filterToBeDisabled))).addClass("ui-state-disabled").attr("disabled", true);
        					} else {
        						$(eval(me.__newTextFilter(filterToBeDisabled))).removeClass("ui-state-disabled").attr("disabled", false);
        					}
    					}
    				});
	    		});
            }
            

            div.append(label).append(input); 
            popupFilters.append(div);
            // these must be uniformed after added to the div
            if(filter.type == "select" || filter.type == "multiple_select"){
            	$("#"+me.__newTextFilter(filter.name)).uniform({selectAutoWidth : false});
            }
        }
        
        me.__createErrorMessages().appendTo(popupFilters);   
        me.__createConfirmPanel().appendTo(popupFilters);       
        me.__downloadDialog.dialog( "option", "height", rows*75 +30 );
    };
    
    ctor.prototype.__createErrorMessages = function() {
    	var div = $('<div>')
    		.addClass('errorMessage');
    	
    	var insertErrorMessage = $('<label>')
	    	.attr('id', 'insertErrorMessage')
	    	.addClass('insertErrorMessage')
	        .appendTo(div);
    
	    var insertErrorMessage = $('<label>')
	    	.attr('id', 'insertTimeErrorMessage')
	    	.addClass('insertErrorMessage')
	        .appendTo(div);
	    
	    return div;
    }
    
    ctor.prototype.__createConfirmPanel = function() {
    	var me = this;
    	
    	var confirmPanel = $('<div>')
	        .attr('id', 'confirmPanel')
	        .addClass('confirmPanel');
	            
	    var saveDownloadButton = $('<button>')
	        .attr('title', 'Confirm Download')
	        .addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary')
	        .click(function(event) {
	            me.__startCreateDownload();
	        })
	        .html('<span class="ui-button-text">Confirm Download</span>')
	        .appendTo(confirmPanel);
		
		var cancelDownloadButton = $('<button>')
	        .attr('title', 'Cancel')
	        .addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary')
	        .click(function(event) {
	            me.__downloadDialog.dialog('close');
	        })
	        .html('<span class="ui-button-text">Cancel</span>')
	        .appendTo(confirmPanel);

        return confirmPanel;
    };
    
    ctor.prototype.__newTextFilter = function(filterName) {
        return 'dwnFilter' + filterName;
    };

    ctor.prototype.__init = function() {
        var me = this;
        
        //Dialog inputs
        me.__fleetDialog = $('#dwnFilterfleet');
        me.__unitNumberDialog = $('#dwnFilterunitNumber');
        me.__vehicleNumberDialog = $('#dwnFiltervehicleNumber');
        me.__fileTypeDialog = $('#dwnFilterfileTypeName');
        me.__dialogErrorMessage = $('#insertErrorMessage');
        me.__dialogTimeErrorMessage = $('#insertTimeErrorMessage');
        
        //Popup inputs
        me.__dateRequest = $('#dwnFilterdate');
        me.__startTimeRequest = $('#dwnFilterstartTime');
        me.__endTimeRequest = $('#dwnFilterendTime');
       
        $( ".downloadDate").datepicker();
        $( ".downloadDate").datepicker( "setDate" , new Date );
        var daysAgo7 = (new Date ).valueOf() - 604800000; //Less 7 days
        $( "#startDateField").datepicker( "setDate" , new Date(daysAgo7) );
        $( ".downloadDate").datepicker( "option", "dateFormat", "dd/mm/yy" );

        $('#searchDownloadButton').click(function(event) {
            me.__doSearch();
        });
        $('#resetSearchDownloadButton').click(function(event) {
            me.__resetParameters();
        });
        $('#createDialogDownloadButton').click(function(event) {
            me.__openDialog();
        });
        $(window).resize(function() {
            if (!!me.__eventFireObject.isAlreadyFired()) {
               return;
            }
        });
        
        nx.rest.fleet.getFleetList(function(me) {
            return function(response) {
                return me.__loadFleetCombo(response);
            }
        }(me));

        me.__fleet.focusout(function() {
            me.__loadFleetUnits(me.__fleet.val(), me.__unitNumber)
        });
        
        me.__fleetDialog.focusout(function() {
            me.__loadFleetUnits(me.__fleetDialog.val(), me.__unitNumberDialog)
        });
        
        me.__unitNumber.focusout(function() {
            me.__loadVehicles(me.__unitNumber.val(), me.__vehicleNumber)
        });
        
        me.__unitNumberDialog.focusout(function() {
            me.__loadVehicles(me.__unitNumberDialog.val(), me.__vehicleNumberDialog)
        });
        
        $(me.__vehicleNumberDialog).uniform({selectAutoWidth : false});
    };
    
    ctor.prototype.__initTimepickers = function() {
    	var me = this;
    	
    	var date = $.format('{date : YYYY/MM/DD}', new Date());
    	
        if (me.__dateRequest.length > 0) {
        	me.__startDatePopup = me.__dateRequest.timepicker().data('obj');
            me.__startDatePopup.setTime(new Date(date));
            
        }
        
        if (me.__startTimeRequest.length > 0) {
        	me.__startTimePopup = me.__startTimeRequest.timepicker().data('obj');
            me.__startTimePopup.setTime(new Date(date));
        }
        
        if (me.__endTimeRequest.length > 0) {
        	me.__endTimePopup = me.__endTimeRequest.timepicker().data('obj');
            me.__endTimePopup.setTime(new Date(me.__startTimePopup.getTime() + me.__downloadConfig.gapTime * 1000));
        }
    }
    
    /** Attaches time events **/
   ctor.prototype.__addTimeEvents = function() {
        var me = this;
        
        nx.hub.timeChanged.subscribe(function(e, timestamp) {
        	var startTimePopupTime = me.__startTimePopup.getTime();
            if(startTimePopupTime != me.__previousStartTime) {
            	me.__previousStartTime = startTimePopupTime;
            	var newTime = new Date(startTimePopupTime + me.__downloadConfig.gapTime * 1000);
            	if (newTime.getDay() == new Date(startTimePopupTime).getDay()) {
            		me.__endTimePopup.setTime(startTimePopupTime + me.__downloadConfig.gapTime * 1000);
            	} else {
                	var date = $.format('{date : YYYY/MM/DD}', new Date());
                    me.__endTimePopup.setTime(new Date(date + " 23:59:59"));
            	}
            }
        });
    };

    /**Open create download dialog*/
    ctor.prototype.__openDialog = function() {
        me = this;
        
        me.__initTimepickers();
        me.__addTimeEvents();
        me.__dialogErrorMessage.empty();
        me.__dialogTimeErrorMessage.empty();
        me.__fleetDialog.val(me.__fleet.val());
        me.__loadFleetUnits(me.__fleetDialog.val(), me.__unitNumberDialog)
        me.__unitNumberDialog.val(me.__unitNumber.val());
        me.__loadVehicles(me.__unitNumberDialog.val(), me.__vehicleNumberDialog);
        me.__vehicleNumberDialog.val(me.__vehicleNumber.val());
        me.__fileTypeDialog.val(me.__fileType.val());
        
        me.__downloadDialog.dialog('open');
    };

    /** Call the rest search method*/
    ctor.prototype.__doSearch = function() {
        var me = this;
        var parameters = me.__getSearchParameters();
        nx.rest.download.search((function(me) {
            return function(response) {
                return me.__loadData(response);
            };
        })(me),
                parameters[0],
                parameters[1],
                parameters[2],
                parameters[3],
                parameters[4],
                parameters[5]);
    };

     /** Load the data table from the response.data*/
    ctor.prototype.__loadData = function(data) {
        var me = this;
        var gridData = [];
        if (!!data && data.length > 0) {
            // We need the fleet id now, because the back end must know which fleet provider to use when retrieving
            var fleetId = me.__fleet.val();
            for (var id in data) {
            	var download = data[id].fields;
                download.fileType = me.__getFileType(download.fileType);
                
                if (download.status == "0") {
                    if (me.__downloadConfig.retrievable) {
                    	download.retrievable = '<button type="button"'
                            + 'fleetId="' + fleetId
                            + '" file="'+ download.retrieveUrl
                            + '" class="retrieveButton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="Retrieve" >'
                            + '<span class="ui-button-text">Retrieve</span>'
                            + '</button>';
                    }
                    else {
                    	download.retrievable = "<span>Complete</span>"
                    }
                }
                
                if (download.status != null) {
                	download.status = this.__setStatus(download.status);
                }
            
                var row  = {'id': download.id,
                        	'data': this.__getRow(download)};
                
                gridData.push(row);
            }
            me.__downloadTable.setData(gridData, true);
            $('.retrieveButton').click(function(event) {
                me.__downloadFile(event.currentTarget.attributes.fleetId.value, event.currentTarget.attributes.file.value);
            });
        } else {
            this.__downloadTable.setData(gridData, true);
            $('<div></div>').dialog({
                buttons: { 'Ok': function () { $(this).dialog('close'); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).text($.i18n('The search returned no results.'));
        }
    };
    
    ctor.prototype.__getRow = function(obj) {
        var row = [];
        
        for (var i in this.__columns) {
            var column = this.__columns[i];
            row.push(obj[column.name]);
        }
        
        return row;
    };


    /**Set the status for display*/
    ctor.prototype.__setStatus = function(downloadStatus) {
        var status = "";
        if (downloadStatus == "0") {
            status = "<div title='Success' class='successIcon'>&nbsp;</div>";
            }
        if (downloadStatus == "1") {
            status = "<div title='Requested' class='requestedIcon'></div>";
            }
        if (downloadStatus == "2") {
            status = "<div title='In Progress' class='progressIcon'></div>";
            }
        if (downloadStatus == "3") {
            status = "<div title='Failed' class='failedIcon'></div>";
            }
        return status;
    };

    /**Obtain de name of a fileType*/
    ctor.prototype.__getFileType = function(fileTypeId) {
        var type = this.__fileTypeList[fileTypeId];
        if (!!type) {
            return type.name;
        }
        return "";
    };

    /**Reset searh parameters*/
    ctor.prototype.__resetParameters = function() {
        me = this;
        var gridData = [];
        var date = new Date; 
        var daysAgo7 = date.valueOf() - 604800000; //Less 7 days
        me.__startDate.datepicker( "setDate" , new Date(daysAgo7) );
        me.__endDate.datepicker( "setDate" , date );
        me.__unitNumber.val("");
        me.__vehicleNumber.val("");
        me.__downloadTable.setData(gridData, true);
        me.__vehicleNumber.find('option').remove().end();
        me.__vehicleNumber.append($("<option />"));
        $.uniform.update();
    };

     /**Read the input necessaries to do the search */
    ctor.prototype.__getSearchParameters = function() {
        var me = this;
        var fleetId = me.__fleet.val();
        var startDate = me.__startDate.val();
        var endDate = me.__endDate.val();
        var unitNumber = me.__unitNumber.val();
        var vehicleNumber = null;
        if (me.__vehicleNumber.length > 0) {
            vehicleNumber = me.__vehicleNumber.val();
        } else {
            var vehicle = me.__getUniqueVehicle(unitNumber);
            if(!! vehicle) {
                vehicleNumber = vehicle.id;
            }
             
        }
        var fileType = me.__fileType.val();
        return [startDate, endDate, fleetId, unitNumber, vehicleNumber, fileType];
        
    };
    
    /**
     * Get a vehicle number from a unit.
     * Used when the vehicles selector is disable.
     */
    ctor.prototype.__getUniqueVehicle = function(unitNumber) {
        
        var result = null;
        
        var unit = null;
        $.each(this.__units, function( index, value ) {
            if(value.unitNumber == unitNumber) {
                unit = value;
                return false; // break the loop
            }
          });
        
        if(!!unit) {
            result = this.getUniqueVehicleByUnit(unit);
        }
       
        return result;   
    }
    
    /**
     * Return the vehicle number to use for this unit.
     * Could be overriden depending on the project.
     * @param unit the unit, could be null if not found
     */
    ctor.prototype.getUniqueVehicleByUnit = function(unit) {
        if (!! unit && unit.vehicles.length > 0) {
            return unit.vehicles[0];
        } else {
            return null;
        }
    }
    
    ctor.prototype.__loadFleetCombo = function(fleets) {
        var me = this;

        $.each(fleets, function (id, fleet) {
            $('<option />', {
                'val': fleet.code,
                'text': fleet.name
            }).appendTo(me.__fleet);
            
            $(me.__fleetDialog).append($("<option style='background-color: white !important' />")
                	.val(fleet.code).text(fleet.name));
            
            $(me.__fleetDialog).uniform({selectAutoWidth : false});
        });

        this.__fleet.prop("selectedIndex", '').change();
        this.__fleetDialog.prop("selectedIndex", '').change();
        
        this.__loadFleetUnits(this.__fleet.val(), this.__unitNumber);
        this.__loadFleetUnits(this.__fleetDialog.val(), this.__unitNumberDialog);
    };
    
    ctor.prototype.__loadFleetUnits = function(fleetId, unitField) {
        var me = this;
        
        if (!!fleetId && !!unitField) {
            nx.rest.unit.fleetUnits(fleetId, function(me) {
                return function(response) {
                    return me.__loadUnits(response, unitField);
                };
            }(me));
        }

    };

    ctor.prototype.__loadUnits = function(response, unitField) {
        var me = this;
        if (!!response && response.length > 0) {
            
            me.__units = response
            me.__unitsNumberList = [];
            for (var x in response) {
                var unit = response[x];
                me.__unitsNumberList.push(unit.unitNumber);
            }
            
            unitField.autocomplete({
                'source': me.__unitsNumberList
            });

        } else {
            alert("The units coludn't be loaded");
        }
    };
    
    /**Load a list of vehicles from a Unit*/
    ctor.prototype.__loadVehicles = function(value, vehicleField) {
        var fileType = this.__fileTypeList[this.__fileType.val()];
        
        var lastValue = vehicleField.val();
        vehicleField.find('option').remove().end();
        vehicleField.append($("<option />"));
        for (var i in this.__units) {
            var unit = this.__units[i];
            if (unit.unitNumber == value) {
                var vehicleList = unit.vehicles;
                for (var x in vehicleList) {
                    var option = vehicleList[x];
                    
                    if (this.__hasFileType(fileType, option.vehicleType)) {
                        vehicleField.append($("<option />")
                                .val(option.id).text(option.vehicleNumber));
                    }
                }
            }
        }
        vehicleField.val(lastValue);
    };

    ctor.prototype.__hasFileType = function(fileType, vehicleType) {
        if (!!fileType && !!vehicleType) {
            var types = fileType.vehicleTypes;
            for (var i = 0, l = types.length; i < l; i++) {
                var type = types[i];
                if (type == vehicleType) {
                    return true;
                }
            }
        }
        return false;
    };

    /**Start to create a download request*/
    ctor.prototype.__startCreateDownload = function() {
        me = this;
        
        var fleetId = me.__fleetDialog.val();
        var unitNumber = me.__unitNumberDialog.val();
        var vehicleId = "";
        var startDate = "";
        var startTime = "";
        var endTime = "";
        
        // TODO hardcoding two fields for Railhead. This should be done generically
        var requestTypeId = $('#dwnFilterrequestType').val();
        var deviceFilter = ( $('#dwnFilterdevice').val() == null || !!$('#dwnFilterdevice').attr("disabled")) ? "" : $('#dwnFilterdevice').val();
        var devices = "";
        
        if(deviceFilter.length > 0){
        	for(i in deviceFilter){
        		if(i == 0){
        			devices = devices + deviceFilter[i];
        		} else {
        			devices = devices + "," + deviceFilter[i];        			
        		}
        	}
        }
        
        if(me.__startTimeRequest.length > 0) {
        	startTime = me.__startTimePopup.getTime();
        }
        
        if(me.__dateRequest.length > 0 && me.__startTimeRequest.length > 0) {
        	if(new Date(me.__startTimePopup.getTime()).getUTCHours() == 23) {
        		startDate = me.__startDatePopup.getTime() - 86400;
        	} else {
        		startDate = me.__startDatePopup.getTime();
        	}
        }
        
        if(me.__endTimeRequest.length > 0 && !me.__endTimeRequest.attr("disabled")) {
        	endTime = me.__endTimePopup.getTime();
        } else {
        	endTime = "";
        }
        
        if (me.__vehicleNumberDialog.length > 0) {
            vehicleId = me.__vehicleNumberDialog.val();
        } else {
            var vehicle = me.__getUniqueVehicle(unitNumber);
            if (!! vehicle) {
                vehicleId = vehicle.id;
            }
        }
        var fileType = me.__fileTypeDialog.val();
        
        var error = false;
        var errorMsg = "";
        if (fleetId == "") {
            errorMsg += "Fleet";
            error = true;            
        }
        if (unitNumber == "" || (vehicleId == "" && me.__vehicleNumberDialog.length == 0)) {
            if (errorMsg != "") {
                errorMsg += ", "
            }
            errorMsg += "Unit Number";
            error = true;
        }
        if (vehicleId == "" && me.__vehicleNumberDialog.length > 0) {
            if (errorMsg != "") {
                errorMsg += ", "
            }
            errorMsg += "Vehicle Number";
            error = true;
        }
        if (fileType == "") {
            if (errorMsg != "") {
                errorMsg += ", "
            }
            errorMsg += "Type";
            error = true;
        }
        
        // TODO:  This should be done generically
		var incompError = false;
        var incompErrorMsg = "";
		
		if(requestTypeId == 2 && fileType == "1") {
			incompError = true;
			incompErrorMsg = "The type selected is not compatible with the request type.";
		}
		
        var timeError = false;
        var timeErrorMsg = "";
        if (!me.__endTimeRequest.attr("disabled")) {
            if (startTime - endTime > 0) {
                timeErrorMsg += "The End Time must be greater than the Start Time";
                timeError = true;
            } else if (endTime - startTime > me.__downloadConfig.thresholdTime*1000) {
            	timeErrorMsg += "The difference between the Start Time and the End Time must not exceed " + me.__downloadConfig.thresholdTime/60 + " minutes";
                timeError = true;
            }
        }

        me.__dialogErrorMessage.empty();
        me.__dialogTimeErrorMessage.empty();
        if (!error && !timeError && !incompError) {
            if (me.__checkValidUnitVehicle(unitNumber, vehicleId)) {
                nx.rest.download.checkCurrent((function(me) {
                    return function(response) {
                        return me.__insertDownload(response,
                                fleetId, vehicleId, fileType, startDate, startTime, endTime, requestTypeId, devices);
                    };})(me),
                    fleetId,
                    vehicleId,
                    fileType);
            } else {
                me.__dialogErrorMessage.text("The unit number or vehicle " +
                        "number which you have entered is invalid");
            }
        } else if (error) {
            me.__dialogErrorMessage.text("Please enter a valid " + errorMsg + ".");
        } else if (timeError) {
        	me.__dialogTimeErrorMessage.text(timeErrorMsg + ".");
        } else if (incompError) {
        	me.__dialogErrorMessage.text(incompErrorMsg);
        }
    };

    /**Check a vehicle is in a unit*/
    ctor.prototype.__checkValidUnitVehicle = function(unitNumber, vehicleId) {
        for (var i in me.__units) {
            var unit = me.__units[i];
            if (unit.unitNumber == unitNumber) {
                var vehicleList = unit.vehicles;
                for (var x in vehicleList) {
                    var vehicle = vehicleList[x];
                    if (vehicle.id == vehicleId) {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    /** Insert a new download request if it doesn't exist already*/
    ctor.prototype.__insertDownload = function(response, fleetId, vehicleId, fileType, startDate, startTime, endTime, requestTypeId, devices) {
        var me = this;
        if (!!response) {
            if (response.data == false) {
            	// TODO refactor - insert should be done generically. RequestTypeId and Devices are railhead specific
                nx.rest.download.insert((function(me) {
                    return function(response) {
                        return me.__finishInsertDownload(response);
                    };})(me),
                    fleetId,
                    vehicleId,
                    fileType,
                    startDate,
                    startTime,
                    endTime, requestTypeId, devices);

            } else {
                me.__dialogErrorMessage.text("A download has already been" +
                        " requested for this vehicle.");
            }
        }
    };

    /** Check if a download has been inserted*/
    ctor.prototype.__finishInsertDownload = function(response) {
        var me = this;
        if (!!response && !!response.data) {
            if (response.data == true) {
                me.__fleet.val(me.__fleetDialog.val());
                me.__loadFleetUnits(me.__fleet.val(), this.__unitNumber);
                me.__unitNumber.val(me.__unitNumberDialog.val());
                me.__loadVehicles(me.__unitNumberDialog.val(), me.__vehicleNumber);
                me.__vehicleNumber.val(me.__vehicleNumberDialog.val());
                me.__fileType.val(me.__fileTypeDialog.val());
                me.__downloadDialog.dialog('close');
                $.uniform.update();
                me.__doSearch();
            } else {
                me.__dialogErrorMessage.text("The download hasn't" +
                        " been inserted");
            }
        }
    };

    /** Call the rest service to download files*/
    ctor.prototype.__downloadFile = function(fleet, file) {  
        nx.rest.download.retrieve(fleet, file, function(url) {
        	nx.util.download(url);
		});       
    };

    return ctor;
}());
 //download access logging
nx.rest.logging.download();

$(document).ready(function() {                
    var download = new nx.download.Download();
});