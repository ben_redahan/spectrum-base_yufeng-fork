var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Arrow = (function () {
    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);

        this._value = null;
        this._settings = $.extend({
            cx: component.x+component.width / 2,
            cy: component.y+component.height / 2,
            radius: Math.min(component.width, component.height) / 2
        }, settings);

        this._paper = el;
        this._dial = this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();

    ctor.prototype._draw = function() {
        var cx = this._settings.cx;
        var cy = this._settings.cy;
        var radius = this._settings.radius;

        return this._paper.set(
            this._paper.path([
                'M', cx - radius + (radius / 10), cy ,
                'L', cx -(radius/3.2) , cy - radius / 3,
                'L', cx -(radius/3.2) , cy + radius / 3,
                'L', cx - radius + (radius / 20), cy ,
                //'L', cx + radius - (radius / 20), cy + radius / 16,
                //'L', cx, cy + radius / 3.8,
                //'L', cx - radius + (radius / 20), cy + radius / 16,
                'Z'
            ].join(',')).attr({
                fill: '0-#555-#333-#555',
                stroke: 'none'
            }),

            this._paper.rect(
                cx  + (radius / 20),
                cy - radius / 12,
                (radius - (radius / 20)) ,
                radius / 6,
                radius / 12).attr({
                fill: '0-#555-#333-#555',
                stroke: '#999',
                'stroke-width': 1
            }),

            this._paper.circle(cx, cy, radius / 3).attr({
                fill: '0-#999-#333-#999-#333-#333-#888',
                stroke: '#999'
            }),

            this._paper.circle(cx, cy, radius / 5).attr({
                fill: '#777'
            }),

            this._paper.circle(cx, cy, radius / 8).attr({
                fill: '#444'
            })
        );
    };

    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if ( channelValue != this._value) {
            var cx = this._settings.cx;
            var cy = this._settings.cy;
            this._dial.transform('R' + (channelValue*90) + ',' +  cx + ',' + cy);
            this._value = channelValue;
        }
    };

    return ctor;
}());
