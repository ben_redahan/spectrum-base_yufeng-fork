var nx = nx || {};
nx.maps = nx.maps || {};
nx.fl = nx.fl || {};
nx.fl.maps = nx.fl.maps || {};

/**
 * A frame to manipulate the objects in the map screens.
 * The frame has its own configuration and holds the inside objects:
 * a) Overview: provides the named views
 * b) Toolbar: provides the layers and searches
 * c) Map: provides the map
 * d) Detail: provides extra information about a selected formation
 * 
 * All access to the inside objects are done through the frame object.
 */
nx.maps.Frame = (function() {
    /**
     * Frame constructor
     *      The inside objects are created based on the presence of a valid configuration
     * @param frameConf
     *      The frame configuration object {}.
     *      Options:
     *          singleUnitMode: if true makes the detail panel always visible
     * @param overviewConf
     *      The overview configuration object
     * @param toolbarConf
     *      The toolbar configuration object
     * @param mapConf
     *      The map configuration object
     * @param detailConf
     *      The detail configuration object
     */
    function ctor(frameConf, toolbarConf, mapConf, detailConf, callback) {
        this.__config = frameConf;
        this.__map = null;
        this.__toolbar = null;
        this.__detail = null;
        
        this.__timestamp = null;
        this.__playPauseButton = null;
        this.__timeoutID = null;
        
        this.updateInProgress = false;
        
        this.__refreshRate = null;
        
        var me = this;
        
        var globalRefreshTimeDfd = $.Deferred()
        var globalRefreshTimeLoaded = globalRefreshTimeDfd.promise();
        nx.rest.system.globalRefreshRate(function(refreshRate) {
            me.__refreshRate = refreshRate;
            globalRefreshTimeDfd.resolve();
        });
        
        $.when(globalRefreshTimeLoaded).done(function() {
            if (!!mapConf) {
                me.__map = new nx.fl.maps.Map(me, mapConf);
                me.__addMapEvents();
            }
            
            if (!!toolbarConf) {
                me.__toolbar = new nx.maps.Toolbar(me, toolbarConf);
            }
            
            if (!!detailConf) {
                me.__detail = new nx.maps.Detail(me, detailConf, mapConf);
            }
            
            if (!!me.__config.timePicker) {
                me.__timePicker = me.__config.timePicker.timepicker();
                if (!!me.__config.playPauseButton) {
                    me.__playPauseButton = me.__config.playPauseButton.togglebutton(true);
                    me.__addTimeEvents();
                    me.refresh(true);
                }
            }
            
            if (!!callback) {
            	callback(me);
            }
            
        });
    };

    ctor.prototype.setExtraDropdown = function(dropdowns) {
        var me = this;

        for (var index in dropdowns) {
            var dropdown = dropdowns[index];

            // create a select field and add options elements containing
            // the locations

            var dropdownSelector = $('<select>')
            					       .addClass('js-location-selector');
        
            $('<option>')
                .attr('value', '0')
                .text($.i18n(dropdown.displayName))
                .appendTo(dropdownSelector);

            for (var itemIndex in dropdown.extraLocations) {
                var location = dropdown.extraLocations[itemIndex];

                $('<option>')
                    .attr('value', location.latitude + "," + location.longitude)
                    .text(location.name)
                    .appendTo(dropdownSelector);
            }

            // when user select an item form the dropdown menu this block
            // will be called. The coordinates are stored if value attribute of each
            // option html field these will be used to navigate on the map
            dropdownSelector.change(function(e){
            	// JQuery Framework Uniform trigger the onChange event
            	// when clicking on the <select> element which doesn't change anything yet
            	// so do nothing for that sort of event
            	if (e.hasOwnProperty('isTrigger')
            		&& e.isTrigger == true) {
            		return;
            	}
                var valueSelected = $(this).find('option:selected').val();
                
                // if user select the first element of the dropdown menu
                // display the whole map instead of the single location
                
                if (valueSelected == 0) {
                    view = { 'bounds': me.__map.__overviewBounds };
                } else {
                    // get the coordinates from the option html field
                    // "logitude,latitude"
                    var coordinates = valueSelected.split(",");

                    view = {
                        'center': new Microsoft.Maps.Location(coordinates[0], coordinates[1]),
                        'zoom': 17
                    };
                }
                
                nx.maps.utils.updateLocationDropdownLists(this);

                me.__map._getBingMap().setView(view);
            });

            $("#extraDropdown").append(dropdownSelector);
            dropdownSelector.uniform();
        }
    }

    /** Calls 
     * nx.maps.Toolbar._setStations
     * nx.fl.maps.Map._setStations
     * nx.maps.Detail._setStations
     */
    ctor.prototype.setStations = function(stations) {
        
        if (!!this.__toolbar) {
            this.__toolbar._setStations(stations);
        }

        if (!!this.__map) {
            var map = this.__map.__map;
            var stationProviders = map._markerProviderRegister.getAllMarkerProviderForDatasource("station");
            for (var stationProvider in stationProviders) {
            	stationProviders[stationProvider].setStations(stations);
                map._drawForProvider(stationProviders[stationProvider]._type);
            }
        }

        if (!!this.__detail) {
            var map = this.__detail.__map;
            var stationProvider = map._markerProviderRegister.getMarkerProvider("station");
            stationProvider.setStations(stations);
            map._drawForProvider("station");
            
        }
    }; 

    /** Calls nx.fl.maps.Map._setRoutes */
    ctor.prototype.setRoutes = function(routes) {
        if (!!this.__map) {
            var map = this.__map.__map;
            var routeProvider = map._markerProviderRegister.getMarkerProvider("route");
            routeProvider.setRoute(routes);
            map._drawForProvider("route");
        }
    };
    
    /** Calls nx.fl.maps.Map._setDetectors */
    ctor.prototype.setDetectors = function(detectors) {
        if (!!this.__map) {
            var map = this.__map.__map;
            var detectorProvider = map._markerProviderRegister.getMarkerProvider("detector");
            detectorProvider.setDetectors(detectors);
            map._drawForProvider("detector");
        }
    };
    
    ctor.prototype.setNamedViews = function(namedViews) {
        if (!!this.__toolbar) {
            this.__toolbar.__addNamedViews(namedViews);
        }
    }

    ctor.prototype.getMap = function() {
        return this.__map;
    };
    
    ctor.prototype._resetSelectedNamedView = function() {
        if (!this.__toolbar) {
            return;
        }
        this.__toolbar._resetSelectedNamedView();
    };

    ctor.prototype._setRoadTypeMode = function() {
        if (!this.__toolbar) {
            return;
        }
        this.__toolbar._setRoadTypeMode();
    };

    /** Calls nx.maps.Toolbar._isTrainLabelChecked */
    ctor.prototype._isTrainLabelChecked = function() {
        if (!this.__toolbar) {
            return false;
        }
        return this.__toolbar._isTrainLabelChecked();
    };

    /** Calls nx.fl.maps.Map._viewEvent */
    ctor.prototype._viewEvent = function(fleetId, eventCoordinates, zoom, eventId, eventIdList) {
        if (!this.__map) {
            return;
        }
        this.__map._viewEvent(fleetId, eventCoordinates, zoom, eventId, eventIdList);
    };

    /** Calls nx.fl.maps.Map._viewSet */
    ctor.prototype._viewSet = function(set, zoom) {
        if (!this.__map) {
            return;
        }
        this.__map._viewSet(set, zoom);
    };

    /** Calls nx.fl.maps.Map._searchSet */
    ctor.prototype._searchSet = function(filter, isIdSearch, hideRouteMap) {
        if (!this.__map) {
            return;
        }
        this.__map._searchSet(filter, isIdSearch, hideRouteMap);
    };
    
    /** Calls nx.fl.maps.Map._showTrainLabels */
    ctor.prototype._showTrainLabels = function() {
        if (!this.__map) {
            return;
        }
        this.__map._showTrainLabels();
    };
    
    /** Calls nx.fl.maps.Map._hideTrainLabels */
    ctor.prototype._hideTrainLabels = function() {
        if (!this.__map) {
            return;
        }
        this.__map._hideTrainLabels();
    };
    
    /** Calls nx.fl.maps.Map._setLayer */
    ctor.prototype._setLayer = function(name, status) {
        if (!this.__map) {
            return;
        }
        this.__map._setLayer(name, status);
    };

    /** Calls nx.fl.maps.Map._selectStation */
    ctor.prototype._selectStation = function(code) {
        if (!this.__map) {
            return;
        }
        this.__map._selectStation(code);
    };
    
    /** Calls nx.fl.maps.Map._setNamedView */
    ctor.prototype._setNamedView = function(view) {
        if (!this.__map) {
            return;
        }
        this.__map._setNamedView(view);
    };

    /** Calls nx.fl.maps.Map._clearSelectedSet */
    ctor.prototype._clearSelectedSet = function() {
        if (!this.__map) {
            return;
        }
        this.__map._clearSelectedSet();
    };

    /** Calls nx.maps.Detail._showDetail */
    ctor.prototype._showDetail = function(set) {
        if (!this.__detail) {
            return;
        }
        this.__detail._showDetail(set);
    };

    /** Calls nx.fl.maps.Map._clearSets */
    ctor.prototype._clearSets = function() {
        if (!this.__map) {
            return;
        }
        this.__map._clearSets();
    };
    
    /** Calls nx.fl.maps.Map._setFollowMode */
    ctor.prototype._setFollowMode = function(followMode) {
        if (!this.__map) {
            return;
        }
        this.__map._setFollowMode(followMode);
    };
    
    /** Calls nx.fl.maps.Map._isFollowMode */
    ctor.prototype._isFollowMode = function() {
        if (!this.__map) {
            return;
        }
        return this.__map._isFollowMode();
    };
    
    /** Returns true if the singleUnitMode property is active and false otherwise */
    ctor.prototype._isSingleUnitMode = function() {
        if (!!this.__config && !!this.__config.singleUnitMode && this.__config.singleUnitMode == true) {
            return true;
        }
        return false;
    };
    
    /** if the formations changed, removes the invalids units in the map */
    ctor.prototype.updateFormation = function(currentIdMap) {
        if (!this.__map) {
            return;
        }
        this.__map._updateFormation(currentIdMap);
    };
    
    /** Hide the routemap */
    ctor.prototype._hideRouteMap = function() {
        $('#routeMap').hide();
        $('#uniform-selectRoute').hide();
        $('#uniform-selComboStations').show();
    };
    
    /**
     * Calls nx.fl.maps.Map._update
     * Calls nx.maps.Detail._update
     * @param set
     *      The data set to be used to update the map and detail
     * @params unitChanged
     *      A flag indicating if the unit has changed (Unit Summary - Location)
     */
    ctor.prototype.update = function(set, unitChanged) {
        var me = this;
        
        if (this._isSingleUnitMode() && !!unitChanged) {
            this._clearSets();
            this._setFollowMode(true);
        }

        //TODO Only data is being used (position 0) the category is also available (position 1)
        if (!!this.__map) {
            var data = this.getTrainData(set);
            
            this.__map._update(data);
        }

        if (!!this.__detail) {
            var data = this.getDetailData(set);
            
            if (this._isSingleUnitMode()) {
                if(this.__detail.__set == null || !!unitChanged) {
                    if (!!this.__map.__sets[data.id]) {
                        this.__map.__selectSet(this.__map.__sets[data.id]);
                    }
                }
            }

            if (this._isFollowMode()) {
                if (!!this.__detail.__set) {
                    this._searchSet(this.__detail.__set._id, true, false);
                }
            }

            me.__detail._update(data);
        }
    };
    
    /**
     * Return the train data  
     */
    // TODO - Once map providers are generic, this needs to be made generic too or deleted
    ctor.prototype.getTrainData = function(set) {
        var data = {
                'sourceId': set.sourceId,
                'id': '' + set.channels.FleetId[0] +set.sourceId,
                'formation': set.channels.formation[0],
                'trainIcon': !!set.channels.TrainIcon? set.channels.TrainIcon[0] : '',
                'filterGroups': !!set.channels.MapFilterGroups? set.channels.MapFilterGroups[0] : '',
                'filterTextGroups': !!set.channels.MapFilterTextGroups? set.channels.MapFilterTextGroups[0] : '',
                // FIXME - this does not look generic.
                'lat': set.channels.GpsLatitude[0],
                'lng': set.channels.GpsLongitude[0],
                'fleetId': set.channels.FleetId[0],
                'fleetFormationID': set.channels.FleetFormationID[0]
            };
        return data;
    };
    
    /**
     * Return the detail data from the set. Should contain at least an id field.
     */
    // TODO - Once map providers are generic, this needs to be made generic too or deleted
    ctor.prototype.getDetailData = function(set) {
        var data = {
                'sourceId': set.sourceId,
                'id': '' + set.channels.FleetId[0] +set.sourceId,
                'timestamp': set.channels.TimeStamp[0],
                'headcode': set.channels.Headcode[0],
                'formation': set.channels.formation[0],
                'trainIcon': !!set.channels.TrainIcon? set.channels.TrainIcon[0] : '',
                'activeVehicle': set.channels.LeadingVehicle[0],
                'fleetFormationID' : set.channels.FleetFormationID[0],
                'lat': set.channels.GpsLatitude[0],
                'lng': set.channels.GpsLongitude[0],
                'gps': set.channels.GpsLatitude[0] + ', ' + set.channels.GpsLongitude[0],
                'location': set.channels.Location[0],
                'speed': set.channels.Speed[0],
                'diagram': set.channels.Diagram[0],
                'fleetId': set.channels.FleetId[0],
                'setNumber': !!set.channels.SetNumber? set.channels.SetNumber[0] : '',
                'timetableDivergence': !!set.channels.TimetableDivergence? set.channels.TimetableDivergence[0] : ''
            };
        
        return data;
    };
    
    /**
     * Return the code of each fleets that is marked as 'Shown' in the toolbar.
     */
    ctor.prototype.getDisplayedFleets = function() {
        
    	var me = this;
    	
        var fleetInfoList = this.__toolbar.__config.fleetInfoList;
        
        var displayedFleets = [];
        
        if (!this.__toolbar.__config.clearApplyButtons || me.applyFilters || !me.displayedFleets) {
	        if (fleetInfoList.length == 1) {
	            displayedFleets.push(fleetInfoList[0].code);
	        } else {
	            $.each(fleetInfoList, function(index, fleetInfo) {
	                if($('#'+fleetInfo.code).is(":checked")) {
	                    displayedFleets.push(fleetInfo.code);
	                }
	            });	
	        }
	        return displayedFleets;
        } else {
        	return me.displayedFleets;
        }
    };
    
    ctor.prototype.getDisplayedFilterGroups = function() {
    	
    	var me = this;
        
        var filtersList = this.__toolbar.__config.filters;
        
        var displayedFilterGroups = [];
        
        if (!this.__toolbar.__config.clearApplyButtons || me.applyFilters || !me.displayedFilterGroups) {
	    	if (filtersList.length == 1) {
	            displayedFilterGroups.push({name: filtersList[0].id,
                    	operator: filtersList[0].operator});
	        } else {
	            $.each(filtersList, function(index, filter) {
		                if($('#'+filter.id).is(":checked")) {
		                    displayedFilterGroups.push({
		                    	name: filter.id,
		                    	operator: filter.operator,
		                    	filterChecked: filter.filterChecked
		                    	}
		                    );   
		                }
	            }); 
	        }
	        return displayedFilterGroups;
        } else {
        	return me.displayedFilterGroups;
        }
    };
    
    
    ctor.prototype.getDisplayedFilterTextGroups = function() {
    	
    	var me = this;
    	
		var filtersList = this.__toolbar.__config.filters;
	     
	    var displayedFilterTextGroups = [];
	     
	    if (!this.__toolbar.__config.clearApplyButtons || me.applyFilters || !me.displayedFilterTextGroups) {
	    	this.applyFilters = false;
	        $.each(filtersList, function(index, filter) {
	            if(filter.type.toLowerCase() == "textbox" && $('#'+filter.id).val() != "") {
	            	displayedFilterTextGroups.push({
	            		id: filter.id,
	         			type: filter.type,
	         			value: $('#'+filter.id).val()			
	            	});
	             }
	        }); 
	        return displayedFilterTextGroups;
	    } else {
	    	return me.displayedFilterTextGroups;
	    }
	 };

    
    ctor.prototype.matchesAFilterGroup = function(displayedFilterGroups, filterGroups) {
    	/* The logic here is to display everything on default when no filter is checked
    	 * Then if any 'OR' filter is checked, loop through them and if none matched then return false
    	 * Then if any 'AND' filter is checked, loop through them and if one doesn't match, return false 
    	 */
    	
    	if(displayedFilterGroups.length == 0 || filterGroups == '') {
    		return true;
    	}
        var filterGroupsArr = filterGroups.split(',');
    	var orFiltersArray = [];
    	var andFiltersArray = [];
    	var hasOrMatches = false;
    	var hasAndMatches = false;
    	
    	for (var i = 0; i < displayedFilterGroups.length; i++) {
    		if (displayedFilterGroups[i].operator == "||") {
    			orFiltersArray.push(displayedFilterGroups[i]);
    		} 
    		if (displayedFilterGroups[i].operator == "&&" || typeof displayedFilterGroups[i].operator == "undefined" ) {
    			andFiltersArray.push(displayedFilterGroups[i]);
    		}
    	}
    	
    	if (orFiltersArray.length > 0) {
	    	for (var i = 0; i < orFiltersArray.length; i++) {
	            for (var j = 0; j < filterGroupsArr.length; j++) {
	                if (orFiltersArray[i].name.toUpperCase() == filterGroupsArr[j].toUpperCase()) {
	                    hasOrMatches = true;
	                    break;
	                }
	            }
	        }
	    	
	    	if (hasOrMatches==false) {
	    		return false;
	    	}
    	}
    	
    	if (andFiltersArray.length > 0) {
            for (var i = 0; i < andFiltersArray.length; i++) {
                if (andFiltersArray[i].filterChecked == 'true') {
                    hasAndMatches = false;
                    for (var j = 0; j < filterGroupsArr.length; j++) {
                        if (andFiltersArray[i].name.toUpperCase() == filterGroupsArr[j].toUpperCase()) {
                            hasAndMatches = true;
                        }
                    }
                    if (hasAndMatches == false) {
                        return false;
                    }
                }
            }
        }
	     
        return true;
    };
    
    ctor.prototype.matchesFilterTextGroup = function(displayedFilterTextGroups, filterTextGroups) {
        var filterTextGroupsArr = filterTextGroups.split(',');
        
        for (var i = 0; i < filterTextGroupsArr.length; i++) {
        	var filter = filterTextGroupsArr[i].split(':');
        	var id = filter[0];
        	var value = filter [1]
        	
        	if (!!displayedFilterTextGroups) {
	            for (var j = 0; j < displayedFilterTextGroups.length; j++) {
	                if (id.toUpperCase() == displayedFilterTextGroups[j].id.toUpperCase() && value.toUpperCase().includes(displayedFilterTextGroups[j].value.toUpperCase())) {
	                    return true;
	                } else {
	                	return false;
	                }
	            }
        	}
        }
        
        return true;
    };
    
    /**
     * Update the map with fresh data
     */
    ctor.prototype.refresh = function(schedule) {
        
        var me = this;
        
        if (!!me._isSingleUnitMode()) {
        	var fleetData = me.__detail.__fleetData[Object.keys(me.__detail.__fleetData)[0]];
        	var fleetId = fleetData.fleetId;
        	var unitId = fleetData.sourceId;
        	me.refreshUnit(fleetId, unitId);
        } else {
        	me.refreshFleet(schedule);
        }
    };
    
    ctor.prototype.refreshFleet = function(schedule) {
        
        var me = this;
        
        if (this.updateInProgress) return;
        
        this.updateInProgress = true;
        
        var refreshData = function(response) {
            var responseData = {};
            
            var data = response.data;
            
        	me.displayedFleets = me.getDisplayedFleets();
        	me.displayedFilterGroups = me.getDisplayedFilterGroups();
        	me.displayedFilterTextGroups = me.getDisplayedFilterTextGroups();
                        
            for (var id in data) {
                 
                var trainData = me.getTrainData(data[id]);
                
                if ($.inArray(trainData.fleetId, me.displayedFleets) != -1 && (me.matchesAFilterGroup(me.displayedFilterGroups, trainData.filterGroups) 
                		&& me.matchesFilterTextGroup(me.displayedFilterTextGroups, trainData.filterTextGroups))) {
                    responseData[trainData.id] = '1';
                }
                
                me.update(data[id]);
            }
            
            me.updateFormation(responseData);
            
            // Send an event to notify new data
            $.event.trigger({
                type: "mapUpdate",
                message: response
            });

            // if the cursor in the page is the loading one
            // switch it back to the default one
            if ($("body").hasClass("cursorWaiting"))
                $("body").removeClass("cursorWaiting");
            
            me.updateInProgress = false;
        };

        var onComplete = function() {
            if (schedule) {
                me.__timeoutID = setTimeout(function () { me.refresh(schedule); }, me.__refreshRate);
            }
        };

        if (this.__isLive()) {
            nx.rest.map.all(refreshData, onComplete);
        }
        else {
            var timestamp = this.__getTimestamp();
            nx.rest.map.allByTime(timestamp, refreshData, onComplete);
        }
    };
    
    ctor.prototype.refreshUnit = function(fleetId, unitId) {
        
        var me = this;
        
        if (this.updateInProgress) return;
        
        this.updateInProgress = true;
        
        var refreshData = function(response) {
            
             var obj = new Object();
             obj.data = [response];

            // Send an event to notify new data
            $.event.trigger({
                type: "mapUpdate",
                message: obj
            });
            
            // if the cursor in the page is the loading one
            // switch it back to the default one
            if ($("body").hasClass("cursorWaiting"))
                $("body").removeClass("cursorWaiting");
            
            me.updateInProgress = false;
        };
        
        if (this.__isLive()) {
            nx.rest.map.unitLive(fleetId, unitId, refreshData);
        }
        else {
            var timestamp = this.__getTimestamp();
            nx.rest.map.unit(fleetId, unitId, timestamp, refreshData);
        }
    };
    
    
    /** Attaches map events to the main Map */
    ctor.prototype.__addMapEvents = function() {
        var me = this;
        Microsoft.Maps.Events.addHandler(me.__map._getBingMap(), 'mouseup', function (mouseEvent) {
            me._setFollowMode(false);
        });
        Microsoft.Maps.Events.addHandler(me.__map._getBingMap(), 'mousewheel', function (mouseEvent) {
            me._setFollowMode(false);
        });
    };

    /** Attaches time events */
    ctor.prototype.__addTimeEvents = function() {
        var me = this;
        
        nx.hub.play.subscribe(function(e) {
            me.__play();
        });
        
        nx.hub.pause.subscribe(function(e) {
            me.__pause();
        });
        
        nx.hub.timeChanged.subscribe(function(e, timestamp) {
            me.__timeChanged(timestamp);
        });
        
        nx.hub.startLive.subscribe(function(e) {
            me.__startLive();
        });
        
        nx.hub.stopLive.subscribe(function(e) {
            me.__stopLive();
        });
    };
    
    ctor.prototype.__stopTimer = function() {
        clearTimeout(me.__timeoutID);
    };
    
    ctor.prototype.__play = function() {
        this.__stopTimer();
        if (this.__isLive()) {
            this.__setTimestamp(new Date().getTime());
        }
        this.refresh(true);
    };
    
    ctor.prototype.__pause = function() {
        this.__getPlayPauseButton().pause();
        this.__stopTimer();
        this.refresh();
    };
    
    ctor.prototype.__timeChanged = function(timestamp) {
        this.__stopLive();
        this.__timestamp = timestamp;
    };
    
    ctor.prototype.__startLive = function() {
        this.__getPlayPauseButton().play();
        this.__play();
    };
    
    ctor.prototype.__stopLive = function() {
        this.__setNotLive();
        this.__pause();
    };
    
    ctor.prototype.__getTimestamp = function() {
        return this.__getTimePicker().getTime();
    };
    
    ctor.prototype.__setTimestamp = function(timestamp) {
        this.__getTimePicker().setTime(timestamp);
    };
    
    ctor.prototype.__getTimePicker = function() {
        return this.__timePicker.data('obj');
    };
    
    ctor.prototype.__getPlayPauseButton = function() {
        return this.__playPauseButton.data('obj');
    };
    
    ctor.prototype.__isLive = function() {
        return this.__getTimePicker().isLive();
    };
    
    ctor.prototype.__setNotLive = function() {
        return this.__getTimePicker().setNotLive();
    };
    
    ctor.prototype.getRefreshRate = function() {
        return this.__refreshRate;
    };
    
    return ctor;
})();

/**
 * nx.maps.util is an object which consists of functions 
 * used by other objects 
 */
nx.maps.utils = (function() {
	function util() {}

	/**
	 * Update the location drop-down list(s)
	 */
	util.prototype.updateLocationDropdownLists = function(that) {
    	// if current select option is changed,
        // reset other drop-downs to the default option
        $('.js-location-selector').each(function() {
        	if (this != that) {
        		$(this).find('option[value="0"]').attr('selected', true);
        	}
        	$.uniform.update(this);
        });
        
        
    }
	
	return new util();
})();

nx.maps.Toolbar = (function() {
    /**
     * Toolbar constructor
     * @param frame
     *      The parent frame object.
     * @param conf
     *      The toolbar configuration object {}.
     *      Options:
     *          container: JQuery object to hold the toolbar content
     *          route: {
     *              visible: routes/tracks visibility status (true/false)
     *          },
     *          station: {
     *              visible: stations visibility status (true/false)
     *          },
     *          train: {
     *              visible: trains visibility status (true/false)
     *          },
     *          trainLabel: {
     *              visible: train labels visibility status (true/false)
     *          },
     *          stations: Array of station objects, if this parameter is not
     *              provided, it will be loaded asynchronous,
     *          searchUnit: visibility of search unit field (true / false)
     *
     *      A Station object:
     *      {
     *          name: 'station name',
     *          code: 'station code',
     *          coordinate : {
     *              latitude: 'station coordinate latitude',
     *              longitude': 'station coordinate longitude'
     *          }
     *      }
     */
    function ctor(frame, conf) {
        this.__frame = frame;
        this.__config = conf;
        
        this.addSettings(frame, this.__config, true);
        this.__addTable();
        this.__trainSearchField = $("#trainSearch");
        this.__comboStationsField = $('#selComboStations');
        this.__trainLabelField = $("#TrainLabels");
        this.__aerialTypeModeField = $("#Aerial");
        this.__unitSearch = $("#mapUnitSearchPanel");
        this.__unitSearch.css({'right': conf.mapUnitSearchPanelOffset});
        
        
        if (!!this.__config.stations) {
            this._setStations(this.__config.stations);
        }
        
        this.__addEvents();
        
    };
    
    ctor.prototype._addTimeControllerButton = function() {
        if ($('#mapHeader').length > 0) {
            this.__displayTimeController = false;
            var me = this;
            var button = $('<button id="timeCtrlBtn" >')
            .button({
                icons: {primary: 'ui-icon-clock'}})
            .click(function(e) {
                e.stopPropagation();
                if(me.__displayTimeController) {
                    $('#mapHeader').hide();
                    $('#mapWrapper').animate({'top': '0px'}, 100);
                    button.attr('title', $.i18n('Show time controller'));
                    me.__displayTimeController = false;
                } else {
                    $('#mapWrapper').animate({'top': '60px'}, 100, function(){$('#mapHeader').show()});
                    button.attr('title', $.i18n('Hide time controller'));
                    me.__displayTimeController = true;
                }
                
            })
            .attr('title', $.i18n('Show time controller'));
            
            this.__config.container.append(button);
        }
        
    }

    /**
     * Sets the stations content
     * @param stations
     *      The station list
     */
    ctor.prototype._setStations = function(stations) {
        for(var station in stations) {
            this.__addComboStation(stations[station]);
        }
    };
    
    ctor.prototype._resetSelectedNamedView = function() {
        if (!this.__config) {
            return;
        }
        
        var namedViewButtons = this.__config.container.find('.ui-buttonset label');
        for (var i = 0, l = namedViewButtons.length; i < l; i++) {
            $(namedViewButtons[i]).removeClass('ui-state-active');
        }
    };

    // TODO this should be completely controlled by a configuration
    ctor.prototype.addSettings = function(frame, conf, createSettingsPanel) {
        var me = this;
        
        var map = frame.__map.__map;
        
        var userConfig = !!localStorage.mapFilters ? JSON.parse(localStorage.mapFilters) : {};
        me.__filterList = conf.filters;
        
        var settings = [];
        
        // add the markers that are type "route"
        var providers = map._markerProviderRegister.getAllMarkerProvider();
        $.each(providers, function(index, provider) {
            if (provider._type == "route") {
                var providerCheckBoxVisibility = me.__getUserConfigValue(userConfig, provider._type, provider._enabledByDefault);
                settings.push(map.__getProviderSetting(providerCheckBoxVisibility, provider, frame));
            }
        });
        
        // add the markers that are type "station"
        $.each(providers, function(index, provider) {
            if (provider._type == "station") {
                var providerCheckBoxVisibility = me.__getUserConfigValue(userConfig, provider._type, provider._enabledByDefault);
                settings.push(map.__getProviderSetting(providerCheckBoxVisibility, provider, frame));
            }
        });
        
     // add the markers that are type "bridge"
        $.each(providers, function(index, provider) {
            if (provider._type == "bridge") {
                var providerCheckBoxVisibility = me.__getUserConfigValue(userConfig, provider._type, provider._enabledByDefault);
                settings.push(map.__getProviderSetting(providerCheckBoxVisibility, provider, frame));
            }
        });
        
    // add the markers that are type "detector"
       $.each(providers, function(index, provider) {
           if (provider._type == "detector") {
               var providerCheckBoxVisibility = me.__getUserConfigValue(userConfig, provider._type, provider._enabledByDefault);
               settings.push(map.__getProviderSetting(providerCheckBoxVisibility, provider, frame));
           }
       });
        
        $.each(conf.filters, function(index, filter) {
        	settings.push({
                id: filter.id,
                label: $.i18n(filter.label),
                value: me.__checkVisibility(filter),
                type: filter.type,
            	operator: filter.operator,
            	filterChecked: filter.filterChecked,
                visibleOnScreen: (frame.__config.singleUnitMode ? filter.showOnUnitLocation : true),
                action: function(on) {
                	me.__saveUserConfigChange(filter.id, $('#' + filter.id).is(":checked"));
                	me.__executeAction(filter.id, $('#' + filter.id).is(":checked"));
                }
            });
        });
        
        // Add the Fleets to filter options
        var fleetInfoList = conf.fleetInfoList;
        if(!!fleetInfoList && fleetInfoList.length > 1) {
            $.each(fleetInfoList, function(index, fleetInfo) {
                var fleetLabel = fleetInfo.code;
                if (conf.fleetFullNames) {
                    fleetLabel = fleetInfo.description;
                }
                
                var value = me.__getUserConfigValue(userConfig, fleetInfo.code, true);
                
                settings.push(
                    {
                        id: fleetInfo.code, 
                        label: $.i18n("Show")+" " + fleetLabel, 
                        type: 'checkbox', 
                        value: value,
                        action: function(on) {
                        	me.__saveUserConfigChange(fleetInfo.code, on);
                            me.__frame.refresh();	
                        }
                    });
            });
        }
        
        // add the rest of the markers
        $.each(providers, function(index, provider) {
            if (provider._type != "route" && provider._type != "station" && provider._type != "bridge" && provider._type != "detector") {
                var providerCheckBoxVisibility = me.__getUserConfigValue(userConfig, provider._type, provider._enabledByDefault);
                settings.push(map.__getProviderSetting(providerCheckBoxVisibility, provider, frame));
            }
        });
        
        nx.main.addSettings(settings, conf, createSettingsPanel);
        me.__fleetInfoList = fleetInfoList;
        me.__settingsList = settings;
        me.__updateIcon();
        
        if(createSettingsPanel && conf.clearApplyButtons) {
	        $('#cancelSettingButton').on("click", function(e) {	            
	            for (var i in me.__fleetInfoList) {
	                var filter = me.__fleetInfoList[i];
	                if (filter.type == 'textBox'){
	                    $('#' + filter.id).val("");
	                }else{// checkbox
	                    $('#' + filter.id).removeAttr("checked");
	                }
	            }
	            
	            for (var i in me.__settingsList) {
	                var filter = me.__settingsList[i];
	                if (filter.type == 'textBox'){
	                    $('#' + filter.id).val("");
	                }else{// checkbox
	                    $('#' + filter.id).removeAttr("checked");
	                }
	            }
	            
	            me.__filtersOn = false;
	            $.uniform.update();
	        }).addClass('ui-button-secondary');
	
	        $('#applySettingButton').on("click", function(e) {
                // change cursor to loading right after clicking apply button
                $("body").addClass("cursorWaiting");

	            me.__filtersOn = true;
	            me.__applyFilters (true);
	            me.__updateIcon();
	            nx.main.hideFilters();
	            nx.main.__hideSettings();
	        }).addClass('ui-button-primary'); 
        } 
    };
    
    ctor.prototype.__checkVisibility = function (filter) {
    	var me = this;
        var userConfig = !!localStorage.mapFilters ? JSON.parse(localStorage.mapFilters) : {};
    	var visibility = me.__getUserConfigValue(userConfig, filter.id, filter.enableByDefault);
    	if(filter.id == "Trains" || filter.id == "TrainLabels" || filter.id == "Aerial") {
    		me.__executeAction(filter.id, visibility);
    	}
    	return visibility;
    }
    
    ctor.prototype.__executeAction = function (filterId, action) {
    	var me = this;
    	if (filterId == "Trains") {
            me.__frame._setLayer('trainLayer', action);
    	
    	} else if (filterId == "TrainLabels") {
            if (action) {
                me.__frame._showTrainLabels();
            } else {
                me.__frame._hideTrainLabels();
            }
    	
    	} else if (filterId == "Aerial") {
            if (action){
                me.__frame.getMap().aerial();
            } else {
                me.__frame.getMap().road();
            }
    	} else {
    		me.__frame.refresh();
    	}
    }
    
    ctor.prototype.__applyFilters = function(saveFilters) {
        var me = this;
        me.__frame.applyFilters = true;
        
        // Add Filters
        for (var i in me.__settingsList) {
            var setting = me.__settingsList[i];
            me.__saveUserConfigChange(setting.id, ($('#' + setting.id).is(":checked")));
            setting.action(($('#' + setting.id).is(":checked")), true);
        }
        var provider = this.__frame.__map.__map.__config.mapMarkersProvider;
        
        me.__frame.refresh();
    };
    
    ctor.prototype.__updateIcon = function() {
    	var me = this;
    	if (me.__checkFiltersActive() == true) {
    		$('#settingsButton span.ui-icon').addClass('ui-icon-red');
    	} else {
    		$('#settingsButton span.ui-icon').removeClass('ui-icon-red');
    	}
    };
    
    ctor.prototype.__checkFiltersActive = function() {
    	var me = this;
    	var filtersActive = false;
    	
    	var screenName = $('#links').find('.link-active').find('a').attr('href');
    	
    	for (var i in me.__settingsList) {
    		var setting = me.__settingsList[i];
    		if(screenName != 'unit' || (screenName == 'unit' && setting.visibleOnScreen == true)) {
	        	if (setting.type.toLowerCase() == "checkbox") {
	        		if($('#' + setting.id).is(":checked")) {
	        			filtersActive = true;
	        		}
	        	} else if (setting.type.toLowerCase() == "textbox") {
	    			if($('#' + setting.id).val() != '') {
	    				filtersActive = true;
	    			}
	        	} 
    		}
    	}
    	
    	return filtersActive;
    };
    
    /** Change the value of the config passed in parameter to newValue, and save in the user configuration*/
    ctor.prototype.__saveUserConfigChange = function(config, newValue) {
    	var userConfJs = !!localStorage.mapFilters ? JSON.parse(localStorage.mapFilters) : {};
    	
    	userConfJs[config] = newValue;
    	localStorage.setItem('mapFilters', JSON.stringify(userConfJs));
		
		var userConf = {
			variable: "mapFilters",
			data: localStorage.mapFilters
		};
		
		// Save the user config
		nx.rest.system.setUserConfiguration(userConf, function() {
        	
        });
    };
    
    /** get the user configuration for the fieldName in parameter, otherwise return the default value */
    ctor.prototype.__getUserConfigValue = function(userConfig, fieldName, defaultValue) {
    	var value = defaultValue;
        if (typeof userConfig[fieldName] != 'undefined')  {
        	value = userConfig[fieldName];
    	}
    	return value;
    }
    
    /** Adds the component table content to the page */
    ctor.prototype.__addTable = function() {
        var container = this.__config.container;
        var me = this;
        
        me.__searchUnitButton = $('<button>')
        .attr('id', 'mapUnitSearch')
        .button({icons: {primary: 'ui-icon-search'}})
        .css({
                display: (this.__config.searchUnit ? 'inline' : 'none')
            });
        
        
        nx.rest.unit.unitSearchConfiguration(function(searchConfig) {
        	me.__searchUnitButton.click(function(e) {
                e.stopPropagation();
                
                if (me.__unitSearchVisible) {
                    me.hideUnitSearch(me);
                } else {
                    me.showUnitSearch();
                }
            });
        	
            me.__unitSelector = $('#mapUnitSelector').unitSelector(searchConfig, function(select){
            	me.__frame.__map._searchSet(select.unitnumber, false, true);
            }, function(){me.hideUnitSearch(me)});
            
        });
        
        var stationSelector = $('<select>')
            .attr('id', 'selComboStations')
            .attr('name', 'selComboStations')
            .addClass('mapStationSelect')
            .addClass('js-location-selector');
        
        $('<option>')
            .attr('value', '0')
            .text($.i18n('Select a station'))
            .appendTo(stationSelector);
        
        container.append(stationSelector);

        container.append("<div id='extraDropdown'></div>");

        this._addTimeControllerButton();
        container.append(me.__searchUnitButton);
        stationSelector.uniform();
        
        
    };
    
    ctor.prototype.showUnitSearch = function() {
        var me = this;
        
        var onComplete = function() {
            var input = me.__unitSearch.find('input');
            input.select();
            input.focus();
        };
        
        this.__unitSearch.show('slide', {direction: 'up'}, 100, onComplete);
        
        this.__unitSearchVisible = true;
    };
    
    ctor.prototype.hideUnitSearch = function(me) {
        me.__unitSearch.hide('slide', {direction: 'up'}, 100);
        me.__unitSearchVisible = false;
    };
    
    /**
     * Adds a new entry in the combo station
     * @param station
     *      The station to be added.
     */
    ctor.prototype.__addComboStation = function(station) {
        this.__comboStationsField.append('<option value="' + station.code + '" >' + station.name + '</option>');
    };

    ctor.prototype.__addNamedViews = function(otherViews) {
        var buttons = $('<div>');
        
        var elements = this.__addNamedView(this.__config.defaultView, false);
        buttons.append(elements[0]);
        buttons.append(elements[1]);
        
        for (var i = 0; i < otherViews.length; i++) {
            var elements = this.__addNamedView(otherViews[i], false);
            buttons.append(elements[0]);
            buttons.append(elements[1]);
        }
        
        buttons.buttonset();
        
        this.__config.container.append(buttons);
    };
    
    ctor.prototype.__addNamedView = function(namedView, isDefault) {
        var me = this;
        
        return [
            $('<input>')
                .attr('type', 'radio')
                .attr('name', 'radio')
                .attr('id', namedView.id)
                .click(function() {
                    // Show map and hide route map
                    me.__frame._hideRouteMap();
                    
                    me.__frame._setNamedView(namedView);
                    
                    if (isDefault) {
	                    if (namedView.type == Microsoft.Maps.MapTypeId.aerial) {
	                        me._setAerialTypeMode();
	                    } else {
	                        me._setRoadTypeMode();
	                    }
                    }
                }),
            $('<label>')
                .attr('for', namedView.id)
                .text($.i18n(namedView.desc))
        ];
    };
    
    ctor.prototype._setRoadTypeMode = function() {
        this.__aerialTypeModeField.prop('checked', false);
        this.__frame.getMap().road();
        $.uniform.update(this.__aerialTypeModeField);
    };

    ctor.prototype._setAerialTypeMode = function() {
        this.__aerialTypeModeField.prop('checked', true);
        this.__frame.getMap().aerial();
        $.uniform.update(this.__aerialTypeModeField);
    };

    /** Attaches events to the components */
    ctor.prototype.__addEvents = function() {
        var me = this;
        
        this.__comboStationsField.change(function(e){
            // JQuery Framework Uniform trigger the onChange event
        	// when clicking on the <select> element which doesn't change anything yet
        	// so do nothing for that sort of event
        	if (e.hasOwnProperty('isTrigger')
        		&& e.isTrigger == true) {
        		return;
        	}
        	
            //hide route map
            me.__frame._hideRouteMap();
            var stationCode = me.__comboStationsField.find('option:selected').val();
            me.__frame._selectStation(stationCode);
            
            nx.maps.utils.updateLocationDropdownLists(this);
        });
        
        $('.mapCheck').click(function(event){
            var name = event.srcElement.name;
            var status = event.srcElement.checked;
            
            if (name == 'trainLabel') {
                if (!!status) {
                    me.__frame._showTrainLabels();
                } else {
                    me.__frame._hideTrainLabels();
                }
            } else {
                me.__frame._setLayer(name, status);
            
                if (name == 'trainLayer' && !status) {
                    me.__trainLabelField.prop('checked', false);
                    me.__frame._hideTrainLabels();
                }
            }
        });
        
    };

    /** Returns the status of the train label visibility */
    ctor.prototype._isTrainLabelChecked = function() {
        if (!!this.__config.trainLabel.visible === true) {
            return this.__trainLabelField.is(':checked');
        }
        
        return false;
    };
    
    return ctor;
})();


nx.fl.maps.Map = (function() {
    /**
     * Wrap the nx.maps.Map to add some functionality to it.
     * Map constructor
     * @param frame
     *      The parent frame object.
     * @param conf
     *      The map configuration object {}.
     *      Options:
     *          container: DOM element to hold the map content
     *          defaultView: View to be used as default/overview
     *          followMode: indicates if the main map should follow a unit
     *          stations: Array of station objects
     *          routes: Array of route objects
     *          
     *      View object:
     *      {
     *          desc: 'view description',
     *          type: 'view type',
     *          id: 'view id',
     *          northWest: {
     *              latitude: 'north west latitude coordinate', 
     *              longitude: 'north west longitude coordinate'
     *          },
     *          southEast: {
     *              latitude: 'south east latitude coordinate', 
     *              longitude: 'south east longitude coordinate'
     *          }
     *      }
     *      
     *      A Station object:
     *      {
     *          name: 'station name',
     *          code: 'station code',
     *          coordinate : {
     *              latitude: 'station coordinate latitude',
     *              longitude': 'station coordinate longitude'
     *          }
     *      }
     *      
     *      A Route object:
     *      {
     *          // Array of all coordinates in the segment route
     *          coordinate: [
     *              {
     *                  latitude: 'coordinate latitude',
     *                  longitude: 'coordinate longitude'
     *              }
     *          ]
     *      }
     */
    function ctor(frame, conf) {
        this.__frame = frame;
        this.__config = conf;
        this.__selectedSet = null;
        this.__sets = {};
        this.__setInfoBoxes = {};
        this.__unitNumberToSet = {};
        this.__trainLabel = conf.trainLabel;
        this.__imgPath = 'img/';
        
        var defaultView = this.__config.defaultView;
        
        

        var mapOptions = {
            /* map options */
            'credentials': 'AgJGCohfkXMsUBWvLBNVmL4OmIIOQYU-L8xeYNBjTNaMmD8ooayUqKzYwcsS8i9c',
            'disableBirdsEye': true,
            'enableSearchLogo': false,
            'enableClickableLogo': false,
            'showScalebar': false,
            'showDashboard': false,
            'showMapTypeSelector': true
        };

        // startsWidth is not supported by IE thus use a polyfill
        if (!String.prototype.startsWith) {
            String.prototype.startsWith = function(searchString, position) {
                position = position || 0;
                return this.indexOf(searchString, position) === position;
            };
        }
        
        this.__map = new nx.maps.Map(conf, mapOptions);
        
        var initialBounds = Microsoft.Maps.LocationRect.fromCorners(
                new Microsoft.Maps.Location(defaultView.northWest.latitude, defaultView.northWest.longitude),
                new Microsoft.Maps.Location(defaultView.southEast.latitude, defaultView.southEast.longitude)
            );
            
        this.__overviewBounds = initialBounds;

        var layers = this.__getLayers();
        for (var i = 0, len = layers.icons.length; i < len; i++) {
            var trainLayer = new Microsoft.Maps.Layer(layers.icons[i]);
            trainLayer.setZIndex(layers.zIndexs[i]);
            this._getBingMap().layers.insert(trainLayer);
        }
        
        this.__addEvents();
    };
    
    ctor.prototype._getBingMap = function() {
        return this.__map.__map;
    };

    ctor.prototype._isFollowMode = function() {
        if (!!this.__config && this.__config.followMode == true) {
            return true;
        }
        return false;
    };
    
    ctor.prototype._setFollowMode = function(followMode) {
        if (!this.__config) {
            return;
        }
        
        if (followMode == true) {
            this.__config.followMode = true;
        } else {
            this.__config.followMode = false;
        }
    };
    
    /**
     * Sets the visibility status (visible/invisible) of a layer
     * @param name
     *      Layer name
     * @param status
     *      New visibility status (true/false)
     */
    ctor.prototype._setLayer = function(name, status) {
        for (var i = 0, len = this._getBingMap().layers.length; i < len; i++) {
            if (this._getBingMap().layers[i].getId().startsWith(this.__imgPath)) {
                this._getBingMap().layers[i].setVisible(status);
            }
        }
    };

    /**
     * Selects a station in the map.
     *      Sets the zoom and centralizes the station coordinates if is a valid
     *      station, otherwise uses the default overview bounds.
     * @param code
     *      The station code
     */
    ctor.prototype._selectStation = function(code) {
        this._setFollowMode(false);
        this.__frame._resetSelectedNamedView();
        var view = { 'bounds': this.__overviewBounds };
        
        var stationProvider = this.__map._markerProviderRegister.getMarkerProvider("station");
        var bridgeProvider = this.__map._markerProviderRegister.getMarkerProvider("bridge");
        
        for (var i in stationProvider.__stations) {
            var station = stationProvider.__stations[i];
            
            if (station.code == code) {
                view = {
                    'center': new Microsoft.Maps.Location(station.coordinate.latitude, station.coordinate.longitude),
                    'zoom': 17
                };
                break;
            }
        }
        if (station.code != code) {
            i = 0;
            for (var i in bridgeProvider.__stations) {
                var bridge = bridgeProvider.__stations[i];
                
                if (bridge.code == code) {
                    view = {
                        'center': new Microsoft.Maps.Location(bridge.coordinate.latitude, bridge.coordinate.longitude),
                        'zoom': 17
                    };
                    break;
                }
            }
        }
        this._getBingMap().setView(view);
    };
    
    /**
     * Sets the zoom, centralizes the event coordinates and displays a visual alert.
     * @param eventCoordinates
     *      The event coordinates
     * @param zoom
     *      The zoom value
     */
    ctor.prototype._viewEvent = function(fleetId, eventCoordinates, zoom, eventId, eventIdList) {
        var me = this;
        if (!!eventCoordinates) {
            if ($("#mapEventAlert").size() > 0) {
                return;
            }
            
            var lat = eventCoordinates.latitude;
            var long = eventCoordinates.longitude;
            if (lat != null && lat != '' && long != null && long != '') {
                this._getBingMap().setView({
                    'center': new Microsoft.Maps.Location(eventCoordinates.latitude, eventCoordinates.longitude),
                    'zoom': zoom
                });
                
                var mapContainer = $(this.__config.container);
                
                //this element must have a width and a height defined here or in the CSS
                var divAlert = ["<div id='mapEventAlert'></div>"].join("");
                mapContainer.append(divAlert);
                    
                var divContainer = $("#mapEventAlert");
                    
                var top = (mapContainer.height() - divContainer.height()) / 2;
                var left = (mapContainer.width() - divContainer.width()) / 2;

                divContainer.css({ 'top': top, 'left': left });

                setTimeout(function() {
                    nx.raphael.eventAlert(divContainer);
                    
                    setTimeout(function() {
                        divContainer.html('');
                        nx.raphael.eventAlert(divContainer);
                        setTimeout(function() {
                            divContainer.remove();
                            nx.event.open(eventId, fleetId, eventIdList);
                        }, 1100);
                    }, 1100);
                }, 200);
            } else {
                nx.event.open(eventId, fleetId, eventIdList);
            }
        }
    };

    /**
     * Sets the zoom and centralizes the set coordinates.
     * @param set
     *      The set to viewed
     * @param zoom
     *      The zoom value
     */
    ctor.prototype._viewSet = function(set, zoom) {
        if (!set) {
            return;
        }
        
        this._getBingMap().setView({
            'center': new Microsoft.Maps.Location(set.getLocation().latitude, set.getLocation().longitude),
            'zoom': zoom
        });
    };
    
    /**
     * Selects a named view.
     * @param view
     *      The named view
     */
    ctor.prototype._setNamedView = function(view) {
        if (!view) {
            return;
        }

        var boundsView = Microsoft.Maps.LocationRect.fromCorners(
            new Microsoft.Maps.Location(view.northWest.latitude, view.northWest.longitude),
            new Microsoft.Maps.Location(view.southEast.latitude, view.southEast.longitude)
        );
        
        this._setFollowMode(false);
        this._getBingMap().setView({'bounds': boundsView});
    };

    /** 
     * Searches for a unit.
     *  If the unit is found, sets the unit as selected, sets the zoom
     *  and centralizes the unit coordinates.
     *  @param filter
     *      The filter to be used to search the unit
     * */
    ctor.prototype._searchSet = function(filter, isIdSearch, hideRouteMap) {
        var setId = null;
        
        if (!!isIdSearch) {
            setId = filter;
        } else {
            setId = this.__unitNumberToSet[filter];
        }
        
        if (!setId) {
            return;
        }

        // Hide the route map
        if(hideRouteMap) this.__frame._hideRouteMap();

        var set = this.__sets[setId];
        var zoom = this._getBingMap().getZoom();
        this._viewSet(set, (zoom < 9 ? 9 : zoom));
        if (!!set) {
            this.__selectSet(set);
        }
    };

    /** Sets visible all train labels */
    ctor.prototype._showTrainLabels = function() {
        for (var id in this.__setInfoBoxes) {
            this.__setInfoBoxes[id].setOptions({'visible': true});
        }
    };

    /** Sets invisible all train labels */
    ctor.prototype._hideTrainLabels = function() {
        
        var trainsAmount = Object.keys(this.__setInfoBoxes).length;
        
        for (var id in this.__setInfoBoxes) {
            if (trainsAmount > 1) {
                if (this.__selectedSet == null || this.__selectedSet._id != id) {
                    this.__setInfoBoxes[id].setOptions({'visible': false});
                }
            } else {
                this.__setInfoBoxes[id].setOptions({'visible': false});
            }
        }
    };

    /**
     * Displays the train label for a specific unit
     * @param id
     *      The unit id to have the label displayed
     */
    ctor.prototype.__showTrainLabel = function(id) {
        if (!!this.__setInfoBoxes[id]) {
            this.__setInfoBoxes[id].setOptions({'visible': true});
        }
    };

    /**
     * Hides the train label for a specific unit
     * @param id
     *      The unit id to have the label hidden
     */
    ctor.prototype.__hideTrainLabel = function(id) {
        if (!!this.__frame._isTrainLabelChecked()) {
            return;
        }
        
        var trainsAmount = Object.keys(this.__setInfoBoxes).length;
        
        if (this.__selectedSet != null && this.__selectedSet._id == id && trainsAmount > 1) {
            return;
        }
        
        if (!!this.__setInfoBoxes[id]) {
            this.__setInfoBoxes[id].setOptions({'visible': false});
        }
    };

    /** Resets the selected set and hides the train label */
    ctor.prototype._clearSelectedSet = function() {
        if (!!this.__selectedSet) {
            var oldSet = this.__selectedSet;
            this.__selectedSet = null;
            oldSet.setOptions({ 'zIndex': null });
            this.__hideTrainLabel(oldSet._id);
        }
    };

    /**
     * Selects a specific set
     * @param set
     *      The set to be set as selected
     */
    ctor.prototype.__selectSet = function(set) {
        if (this.__selectedSet != null && set != null && this.__selectedSet._id == set._id) {
            return;
        }
        
        this._clearSelectedSet();
        
        this.__selectedSet = set;
        this.__frame._showDetail(set);
    };

    /** Removes invalid units/formation from the map.
     *      These "invalid units" are the result from formation changes */
    ctor.prototype._updateFormation = function(currentIdMap) {
        var idsToRemove = [];
        
        for (var id in this.__sets) {
            if (currentIdMap[id] == undefined || currentIdMap[id] == null) {
                idsToRemove.push(id);
            }
        }

        for (var i = 0, l = idsToRemove.length; i < l; i++) {
            var id = idsToRemove[i];
            
            this.__removeSet(this.__sets[id]);
            delete this.__sets[id];
            
            this.__setInfoBoxes[id].setMap(null);
            delete this.__setInfoBoxes[id];
            
            for (var unitNumber in this.__unitNumberToSet) {
                if (this.__unitNumberToSet[unitNumber] == id) {
                    delete this.__unitNumberToSet[unitNumber];
                    break;
                }
            }
        }
    };
    
    /**
     * Updates the map content
     * @param set
     *      The new set.
     *      
     *      The Set object:
     *      {
     *          id: 'set id',
     *          formation: 'list of unit numbers in the formation (comma separated values)',
     *          lat: 'gps latitude',
     *          lng: 'gps longitude
     *      }
     */
    ctor.prototype._update = function(set) {
    	var invalidGps = false;
        if (set.lat == null || set.lng == null || (set.lat == 0 && set.lng == 0)) {
            invalidGps = true;
        }
        
        var icon = this.__getTrainIcon(set);
        
        if (this.__sets[set.id] !== undefined) {
            if (!!invalidGps) {
            	this.__removeSet(this.__sets[set.id]);
            	this.__sets[set.id] = undefined;
            } else {
            	var location = new Microsoft.Maps.Location(set.lat, set.lng);
            	var setLocation = this.__sets[set.id].getLocation();
                
            	// Update if location or icon has changed
            	if (JSON.stringify(location) !== JSON.stringify(setLocation) || this.__sets[set.id].getIcon() !== icon) {
                    for (var i = 0, len = this._getBingMap().layers.length; i < len; i++) {
                		if (this._getBingMap().layers[i].getId().startsWith(this.__imgPath)) {
                			if (this._getBingMap().layers[i].getId() === icon) {
                				var exits = false
                				for(var j in this._getBingMap().layers[i].getPrimitives()) {
                        			if (this.__sets[set.id].entity.id === this._getBingMap().layers[i].getPrimitives()[j].entity.id) {
                        				exits = true;
                        				break;
                        			}
                        		}
                				if (!exits) {
                					this._getBingMap().layers[i].add(this.__sets[set.id]);
                				}
                        	} else {
                        		for(var j in this._getBingMap().layers[i].getPrimitives()) {
                        			if (this.__sets[set.id].entity.id === this._getBingMap().layers[i].getPrimitives()[j].entity.id) {
                        				this._getBingMap().layers[i].removeAt(j);
                        				break;
                        			}
                        		}
                        	}
                        }
                	}
                    
                    this.__sets[set.id].setLocation(location);
                    this.__setInfoBoxes[set.id].setLocation(location);
                    this.__sets[set.id].setOptions({icon: icon});
            	}

                this.__updateTrainInfoBox(this.__setInfoBoxes[set.id], set);
            }
        } else {
            if (!invalidGps) {
                this.__addSet(set);
            }
        }
    };
    
    ctor.prototype.__removeSet = function(set) {
        if(set != undefined) {
            for (var i = 0, len = this._getBingMap().layers.length; i < len; i++) {
                if (this._getBingMap().layers[i].getId().startsWith(this.__imgPath)) {
                    for(var j in this._getBingMap().layers[i].getPrimitives()) {
                        if (set.entity.id === this._getBingMap().layers[i].getPrimitives()[j].entity.id) {
                            this._getBingMap().layers[i].removeAt(j);
                        }
                    }
                }
            }
        }
    };

    
    ctor.prototype._clearSets = function() {
        this.__selectedSet = null;
        
        this.__sets = {};
        this.__setInfoBoxes = {};
        this.__unitNumberToSet = {};
        
        for (var i = 0, len = this._getBingMap().layers.length; i < len; i++) {
            if (this._getBingMap().layers[i].getId().startsWith(this.__imgPath)) {
                this._getBingMap().layers[i].clear();
            }
        }
    };

    /**
     * Adds the specified set to the map, if it already exists, it will
     * be moved to the new location
     * @param set
     *      The set to be added (see ctor.prototype._udpate doc)
     */
    ctor.prototype.__addSet = function(set) {
        var location = new Microsoft.Maps.Location(set.lat, set.lng);
        var icon = this.__getTrainIcon(set);
        // Add a pin to the center of the map, using a custom icon
        var pin = new Microsoft.Maps.Pushpin(location, {
            'icon': icon,
            'width': '32px',
            'height': '37px',
            'anchor': new Microsoft.Maps.Point(16,37)
        });

        
        pin._id = set.id;
        pin._fleetId = set.fleetId;
        
        for (var i = 0, len = this._getBingMap().layers.length; i < len; i++) {
            if (this._getBingMap().layers[i].getId() === icon) {
                this._getBingMap().layers[i].add(pin);
            }
        }

        var unitNumbers = set.formation.split(",");
        for (var i = 0, l = unitNumbers.length; i < l; i++) {
            this.__unitNumberToSet[$.trim(unitNumbers[i])] = set.id;
        }
        
        this.__sets[set.id] = pin;

        var infobox = this.__createTrainInfoBox(pin.getLocation(), set);
        
        infobox.setMap(this.__map.__map);
        this.__setInfoBoxes[set.id] = infobox;

        if (!!this.__frame._isTrainLabelChecked()) {
            this.__showTrainLabel(set.id);
        }

        var me = this;
        
        Microsoft.Maps.Events.addHandler(pin, 'click', function (mouseEvent) {
            me.__selectSet(pin);
        });

        Microsoft.Maps.Events.addThrottledHandler(pin, 'mouseover',function (mouseEvent) {
            me._getBingMap().getRootElement().style.cursor = 'pointer';
            me.__showTrainLabel(pin._id);
        }, 250);

        Microsoft.Maps.Events.addThrottledHandler(pin, 'mouseout',function (mouseEvent) {
            me.__hideTrainLabel(pin._id);
            me._getBingMap().getRootElement().style.cursor = 'auto';
        }, 250);
    }; 
    
    ctor.prototype.__getTrainIcon = function(set) {
        var img = "img/train.png";
        
        if (this.__config.trainIcons.length > 0) {
            $.each(this.__config.trainIcons, function(index, icon) {
                if (typeof set.trainIcon != "undefined" && set.trainIcon.toUpperCase() == icon.name.toUpperCase()) {
                    img = icon.image;
                    return false;
                }
            });
        }
        
        return img
    };   
    
    ctor.prototype.__getLayers = function() {
        var icons = [];
        var zIndexs = [];
        
        if (this.__config.trainIcons.length > 0) {
            $.each(this.__config.trainIcons, function(index, icon) {
            	icons.push(icon.image);
            	zIndexs.push(icon.zIndex);
            });
        }
        
        return {
            'icons': icons,
            'zIndexs': zIndexs
        };;
    };
        
    ctor.prototype.__createTrainInfoBox = function(location, set) {
        var infoboxOptions = {
                'id': set.id,
                'visible': false,
                'showCloseButton': false,
                'showPointer': this.__trainLabel.pointer,
                'offset': new Microsoft.Maps.Point(parseInt(this.__trainLabel.xOffset), parseInt(this.__trainLabel.yOffset)),
                'maxWidth': parseInt(this.__trainLabel.width),
                'maxHeight': parseInt(this.__trainLabel.height)
        };
            
        var infobox = new Microsoft.Maps.Infobox(location, infoboxOptions);
            
        this.__updateTrainInfoBox(infobox, set);
            
        return infobox;
    };
    
    ctor.prototype.__updateTrainInfoBox = function(infobox, set) {
        var title = this.__trainLabel.title;
        if (!!title) {
            infobox.setOptions({'title': set[this.__trainLabel.title]});
        }
        
        var description = this.__trainLabel.description;
        if (!!description) {
            var desc = "";
            
            $.each(description, function(index, row) {
                desc += "<b>" + row.label + ": </b>";
                
                var field = row.value;
                if (!!field && !!set[field]) {
                    if (!!row.constant) {
                        desc += row.constant;
                    } else if (!!row.format) {
                        desc += $.format(row.format, set[field]);
                    } else {
                        desc += set[field];
                    }
                }
                
                desc += "<br>";
            });
            
            infobox.setOptions({'description': desc});
        }
        
        if (!title && !description) {
            infobox.setHtmlContent(['<div class="trainInfo">', set.formation, '</div>'].join(""));
        }
    };

    
    ctor.prototype.road = function() {
        this.__map.road();
    };
    
    ctor.prototype.aerial = function() {
        this.__map.aerial();
    };

    /** Attaches events to the components */
    ctor.prototype.__addEvents = function() {
    };
    
    return ctor;
})();

nx.maps.Detail = (function() {
    /**
     * Detail constructor
     * @param frame
     *      The parent frame object.
     * @param conf
     *      The detail configuration object {}.
     *      Options:
     *          container: JQuery object to hold the detail content
     *          defaultView: View to be used as default/overview
     *          stations: Array of station objects, if this parameter is not
     *              provided, it will be loaded asynchronous
     *          maxHeight: The max available height for the detail panel
     *      
     *      View object:
     *      {
     *          desc: 'view description',
     *          type: 'view type',
     *          id: 'view id',
     *          northWest: {
     *              latitude: 'north west latitude coordinate', 
     *              longitude: 'north west longitude coordinate'
     *          },
     *          southEast: {
     *              latitude: 'south east latitude coordinate', 
     *              longitude: 'south east longitude coordinate'
     *          }
     *      }
     *      
     *      A Station object:
     *      {
     *          name: 'station name',
     *          code: 'station code',
     *          coordinate : {
     *              latitude: 'station coordinate latitude',
     *              longitude': 'station coordinate longitude'
     *          }
     *      }
     */
    function ctor(frame, conf, mapConf) {
        this.__frame = frame;
        this.__config = conf;
        
        this.__stations = {};
        this.__stationInfoBoxes = {};
        this.__inProgress = true;
        this.__set = null;
        this.__dataSet = null;
        this.__windowIntervalIdEventsRefresh = null;
        this.__refreshRate = frame.getRefreshRate();
        
        this.__fleetData = {};

        this.__initializeDetailPanel();
        
        if ($("#mapInfoOverlayMap").length > 0) {
            var mapContainer = this.__config.container.find("#mapInfoOverlayMapContent")[0];
            var mapOptions = {
                'credentials': 'AgJGCohfkXMsUBWvLBNVmL4OmIIOQYU-L8xeYNBjTNaMmD8ooayUqKzYwcsS8i9c',
                'disableBirdsEye': false,
                'enableSearchLogo': false,
                'enableClickableLogo': false,
                'backgroundColor': 'red',
                'showScalebar': false,
                'showDashboard': false,
                'width': 298, 
                'height': 190,
                'showMapTypeSelector': false,
                'disablePanning': true,
                'mapTypeId': Microsoft.Maps.MapTypeId.road
            };
        
            var detailMapConf = new Object();
            detailMapConf.container = mapContainer;
            detailMapConf.defaultView = mapConf.defaultView;
        
            // Add only station marker provider
            $.each(mapConf.mapMarkersProvider, function(index, provider) {
                if (provider.markerType == "station") {
                    detailMapConf.mapMarkersProvider = [provider];
                }
            });
        
            this.__map = new nx.maps.Map(detailMapConf, mapOptions);

            if (!!this.__config.stations) {
                this._setStations(this.__config.stations);
            }
        
            this.__trainLayer = new Microsoft.Maps.Layer('trainLayerDetails');
            this.__trainLayer.setZIndex(15000);
            this._getBingMap().layers.insert(this.__trainLayer);
        }    
            
        this.__addEvents();

        //TODO: remove (to hide the invalid licence message)
        //$($('.MicrosoftMap').children('div')[3]).hide();
    };
    
    ctor.prototype._getBingMap = function() {
        if (!this.__map) {
            return;
        }
        
        return this.__map.__map;
    };


    /** Creates the detail panel and the inside elements/objects */
    ctor.prototype.__initializeDetailPanel = function() {
        var me = this;
        var container = this.__config.container;
        
        container.css({ 'height': this.__config.maxHeight });
        
        var toolbarDef = ["<div id='mapInfoOverlayToolbar'>"];
        toolbarDef.push("<div id='mapInfoOverlayToolbar_Content' class = 'ui-widget-header'>&nbsp;</div>");
        
        toolbarDef.push("<div id='mapInfoOverlayToolbar_Close' style='display: ", (this.__frame._isSingleUnitMode() ? 'none' : 'block'), "'></div>");
        toolbarDef.push("</div>");
        
        container.append(toolbarDef.join(""));
        
        var mapDivDef = ["<div id='mapInfoOverlayMap'>"];
        mapDivDef.push("<div id='mapInfoOverlayMapContent'></div>");
        mapDivDef.push("</div>");
        
        container.append(mapDivDef.join(""));
        
        var buttonList = this.__config.buttons;
        var headerDef = "";
        var addButtons = false;
        if (!!buttonList && buttonList.length > 0) {
            var buttonsDef = ["<div id='unitDetailHeader'></div>"];
            container.append(buttonsDef.join(""));
            addButtons = true;
        } else {
            headerDef = "<tr><th id='unitDetailHeader' colspan='2' class='header ui-widget-header'><span id='unitDetailHeaderLbl'>"+$.i18n("Details")+"</span></th></tr>";
        }
        
        var detailsDef = ["<div id='mapInfoOverlayDetail'>"];
        detailsDef.push("<table id='mapInfoOverlayDetailTable'>");
        detailsDef.push(headerDef);
        
        $.each(this.__config.detailList, function(index, detail) {
            detailsDef.push("<tr id='" + detail.name + "Field'><td width='110'><div class='dataHeader'>" + $.i18n(detail.label) + "</div></td><td><div class='data'>&nbsp;</div></td></tr>");
        });
        
        detailsDef.push("</table>");
        detailsDef.push("</div>");

        container.append(detailsDef.join(""));

        var eventsDef = ["<div id='mapInfoOverlayEvents'>"];
        eventsDef.push("<div id='mapInfoOverlayEventsContent'>");
        eventsDef.push("<div id='mapInfoOverlayEventsTabs'>");
        eventsDef.push("<ul>");
        eventsDef.push("<li><a href='#eventsTabLive'>"+$.i18n('Live')+"</a></li>");
        eventsDef.push("<li><a href='#eventsTabRecent'>"+$.i18n('Recent')+"</a></li>");
        eventsDef.push("</ul>");
        eventsDef.push("<div id='eventsTabLive'>");
        eventsDef.push("<table id='mapInfoOverlayEventsLiveTable'></table>");
        eventsDef.push("</div>");
        eventsDef.push("<div id='eventsTabRecent'>");
        eventsDef.push("<table id='mapInfoOverlayEventsRecentTable'></table>");
        eventsDef.push("</div>");
        eventsDef.push("</div>");
        eventsDef.push("</div>");
        eventsDef.push("</div>");
        
        container.append(eventsDef.join(""));
        
        var tabs = $('#mapInfoOverlayEventsTabs');
        tabs.tabs();
        
        if (addButtons) {
            this.__addLinkButtons();
        } else {
            $("#unitDetailHeader").click(function(e) {
                var unitId = $("#unitDetailHeader").data('unitId');
                var fleetId = $("#unitDetailHeader").data('fleetId');
                var unitNumber = $("#unitDetailHeader").data('unitNumber');

                me.__openUnitSummary(unitId, fleetId, null, unitNumber);
            });
        }
    };
    
    ctor.prototype.__addLinkButtons = function() {
        var me = this;

        var linksContainer = $('#unitDetailHeader');
        
        var panelWidth = $("#mapInfoOverlay").width();
        var buttonsNumber = this.__config.buttons.length;
        var width = panelWidth / buttonsNumber - 1;
        
        $.each(this.__config.buttons, function(index, button) {
            var tab = button.tab;
            if (!tab) {
                tab = null;
            }
            
            var btn = $('<button>' + button.label + '</button>').button({
            })
            .addClass('mapButton')
            .css('width', width + 'px')
            .data('tab', tab)
            .click(function(e) {
                e.stopPropagation(); // Prevent header handler to catch the event, as it will open default tab
    
                var unitId = $("#unitDetailHeader").data('unitId');
                var fleetId = $("#unitDetailHeader").data('fleetId');
                var unitNumber = $("#unitDetailHeader").data('unitNumber');
    
                me.__openUnitSummary(unitId, fleetId, btn.data('tab'), unitNumber);
            });
                
            btn.appendTo(linksContainer);
        });
    };

    /** Resets the detail map to the original state (default view) */
    ctor.prototype.__resetMap = function() {
        if (this.__set != null) {
            this.__trainLayer.remove(this.__set);
        }
        
        this._getBingMap().setView({
            'center': new Microsoft.Maps.Location(this.__config.defaultView.latitude, this.__config.defaultView.longitude),
            'zoom': this.__config.defaultView.zoom
        });
    };

    /** Clear the detail table */
    ctor.prototype.__resetTable = function() {
        $.each($('.data'), function(index, field) {
            $(field).text('');
        });
    };

    /** Clear detail events tables */
    ctor.prototype.__resetEvents = function(full) {
        if (!!full) {
            $('#mapInfoOverlayEventsTabs').tabs('select', 0);
        }
        
        $('#mapInfoOverlayEventsLiveTable').html('');
        $('#mapInfoOverlayEventsRecentTable').html('');
    };

    /** Resets the detail panel */
    ctor.prototype.__resetContent = function() {
        this.__set = null;
        this.__dataSet = null;
        
        this.__resetMap();
        this.__resetTable();
        this.__resetEvents(true);
    };

    /** Removes the events scheduled update process */
    ctor.prototype.__unScheduleEventsUpdate = function() {
        window.clearInterval(this.__windowIntervalIdEventsRefresh);
    };

    /** Adds the events schedule update process */
    ctor.prototype.__scheduleEventsUpdate = function() {
        this.__unScheduleEventsUpdate();
        
        if (!this.__dataSet || !this.__dataSet.fleetFormationID) {
            return;
        }

        var me = this;
        
        nx.rest.event.formation(this.__dataSet.fleetFormationID, this.__dataSet.fleetId, function(response) {
            if (!me.__dataSet) {
                return;
            }
            me.__updateEvents(response, me.__dataSet.fleetId);
            me.__inProgress = false;
        });

        this.__windowIntervalIdEventsRefresh = window.setInterval(function() {
            if (!me.__dataSet || !me.__dataSet.fleetFormationID || !!me.__inProgress) {
                return;
            }
            
            me.__inProgress = true;

            nx.rest.event.formation(me.__dataSet.fleetFormationID, me.__dataSet.fleetId, function(response) {
                if (!me.__dataSet) {
                    return;
                }
                me.__updateEvents(response, me.__dataSet.fleetId);
                me.__inProgress = false;
            });
        }, me.__refreshRate);
    };

    /**
     * Displays the detail panel for a specific set
     * @param set
     *      The set to display the details
     */
    ctor.prototype._showDetail = function(set) {
        var me = this;
                
        $.event.trigger({
            type: "showDetail",
            message: me.__config.container
        });
        
        if (!this.__config.container.is(':visible')) {
            this.__config.container.show('slide', {'direction': 'down'});
        }

        // update table content
        this.__resetContent();
        this.__updateTableContent(this.__fleetData[set._id]); // We use data previously saved
        
        this.__addSet({
            'id': set._id,
            'lat': set.getLocation().latitude,
            'lng': set.getLocation().longitude
        });
    };

    /** Hides the detail panel */
    ctor.prototype.__hideDetail = function() {
        this.__set = null;
        this.__dataSet = null;
        var me = this;
        
        $.event.trigger({
            type: "hideDetail",
            message: me.__config.container
        });
        
        this.__unScheduleEventsUpdate();
        this.__frame._clearSelectedSet();
        this.__config.container.hide('slide', {'direction': 'down'});
    };

    /**
     * Updates the detail content
     * @param set
     *      The new set.
     *      
     *      The Set object:
     *      {
     *          id: 'set id',
     *          timestamp: 'latest timestamp',
     *          headcode: 'headcode',
     *          formation: 'list of unit numbers in the formation (comma separated values)',
     *          activeVehicle: 'active vehicle number',
     *          lat: 'gps latitude',
     *          lng: 'gps longitude,
     *          location: 'tiploc location',
     *          speed: 'speed'
     *      }
     */
    ctor.prototype._update = function(set) {
        this.__fleetData[set.id] = set; // We cache the data
        if (!this.__config.container.is(':visible')
                || this.__set == null || set == null || this.__set._id != set.id) {
            return;
        }

        if (!this.__dataSet) {
            this.__dataSet = set;
            this.__scheduleEventsUpdate();
        }

        this.__dataSet = set;
        
        var icon = this.__frame.__map.__getTrainIcon(set);
        this.__set._icon = icon.img;
        
        this.__updateMap({
            'lat': set.lat,
            'lng': set.lng
        });
        
        this.__updateTableContent(set);
    };
    
    ctor.prototype.__updateTableContent = function(set) {
        if (!set) {
            return;
        }

        //update detail header
        $('#mapInfoOverlayToolbar_Content').text(set.formation);

        var unitId = set.fleetFormationID.split(" ")[0]; // Get the first unit
        
        $('#unitDetailHeader').data('unitId', unitId).data('fleetId', set.fleetId);
        
        $.each(this.__config.detailList, function(index, detail) {
            var element = $('#' + detail.name + 'Field');
            var fleets = detail.fleets;
            if (!!fleets) {
                if (fleets.indexOf(set.fleetId) > -1) {
                    element.show();
                } else {
                    element.hide();
                    return;
                }
            }
            var cell = element.find('.data');
            var value = set[detail.ref];
            if (!!detail.format) {
                cell.text($.format(detail.format, value));
            } else {
                cell.text($.format(value));
            }
        });
        
        this.__resizeEventContainer();
    };
    
    ctor.prototype.__resizeEventContainer = function() {        
        var newHeight = $('#map').height() - $('#mapInfoOverlayToolbar').height() - $('#mapInfoOverlayMap').height() 
            - $('#mapInfoOverlayDetail').height() - 15;
        
        var btnHeight = $('.mapButton').height();
        if (!!btnHeight) {
            newHeight -= btnHeight + 3;
        }
        
        $('#mapInfoOverlayEvents').css('max-height', newHeight + 'px');
    }

    /**
     * Updates the detail map content
     * @param set
     *      The data set
     *      {
     *          lat: 'gps latitude',
     *          lng: 'gps longitude'
     *      }
     */
    ctor.prototype.__updateMap = function(set) {
        if (set == null || set.lat == null || set.lat == undefined || set.lng == null || set.lng == undefined) {
            this.__resetMap();
        } else {
            this.__trainLayer.remove(this.__set);
            this.__trainLayer.add(this.__set);
            var zoom = this._getBingMap().getZoom();
            
            this._getBingMap().setView({
                'center': new Microsoft.Maps.Location(set.lat, set.lng),
                'zoom': (zoom < 13 ? 13 : zoom)
            });
            
            this.__set.setLocation(new Microsoft.Maps.Location(set.lat, set.lng));
        }
    };

    /**
     * Update the events table
     * @param events
     *      The list of events to be updated
     */
    ctor.prototype.__updateEvents = function(events, fleetId) {
        this.__resetEvents();
        
        for (var i in events) {
            var event = events[i].fields;

            var descriptionTitle = ['Description: ', event.description, '&#013;', 'Location: ', event.locationCode].join("");
            
            var newRow = ["<tr style='height: 20px;' eventId='", event.id,"' fleetId='", fleetId, "' latitude='", event.latitude, "' longitude='", event.longitude, "'>"];
            newRow.push("<td title='", event.type, "' style='vertical-align: middle; line-height: 18px; overflow: hidden; width: 20px; text-align:center;'>");
            newRow.push($.format("<div class = 'eventIcon' style = 'background: {string}'></div>", event.typeColor));
            newRow.push("</td>");
            newRow.push("<td title='", descriptionTitle, "' style='line-height: 18px; overflow: hidden; width: 150px;'>");
            newRow.push(event.eventCode);
            newRow.push("</td>");
            newRow.push("<td title='", $.format("{date}", event.createTime), "' style='line-height: 18px; overflow: hidden;'>");
            newRow.push(nx.util.timeAgoFormat(event.createTime)); // XXX add the $.format?
            newRow.push("</td>");
            newRow.push("</tr>");
            
            if (event.current == true) {
                $("#mapInfoOverlayEventsLiveTable").append(newRow.join(""));
            } else {
                $("#mapInfoOverlayEventsRecentTable").append(newRow.join(""));
            }
        }
    };

    /**
     * Adds the set to the map
     * @param set
     *      The set to be added
     *      {
     *          id: 'set id',
     *          lat: 'gps latitude',
     *          lng: 'gps longitude'
     *      }
     */
    ctor.prototype.__addSet = function(set) {
        // Add a pin to the center of the map, using a custom icon
        var pin = new Microsoft.Maps.Pushpin(
            new Microsoft.Maps.Location(set.lat, set.lng),
            {
                'icon': 'img/train.png',
                'width': '32px',
                'height': '37px',
                'anchor': new Microsoft.Maps.Point(16,37)
            }
        );

        pin._id = set.id;
        
        this.__trainLayer.clear();
        this.__trainLayer.add(pin);
        this.__set = pin;

        this.__updateMap(set);
    };

    /** Attaches events to the components */
    ctor.prototype.__addEvents = function() {
        var me = this;
        
        $("#mapInfoOverlayToolbar_Close").click(function(e) {
            me.__hideDetail();
        });
        
        Microsoft.Maps.Events.addHandler(this._getBingMap(), 'click', function (mouseEvent) {
            me.__frame._setFollowMode(true);
            me.__frame._viewSet(me.__set, 14);
            me.__frame._resetSelectedNamedView();
        });
        
        
        $('#mapInfoOverlayEventsLiveTable').on( {
            click: (
                function(me) {
                    return function(eventObj) {
                        return me.__clickEvent(eventObj);
                    };
                }
            )(me),
            touchend: (
                function(me) {
                    return function(eventObj) {
                        return me.__clickEvent(eventObj);
                    };
                }
            )(me)
        }, "tr");

        $('#mapInfoOverlayEventsRecentTable').on( {
            click: (
                function(me) {
                    return function(eventObj) {
                        return me.__clickEvent(eventObj);
                    };
                }
            )(me),
            touchend: (
                function(me) {
                    return function(eventObj) {
                        return me.__clickEvent(eventObj);
                    };
                }
            )(me)
        }, "tr");
    };

    /**
     * Processes the click event
     * @param eventObj
     *      The source event object
     */
    ctor.prototype.__clickEvent = function(eventObj) {
        var latitude = $(eventObj.srcElement.parentElement).attr('latitude');
        var longitude = $(eventObj.srcElement.parentElement).attr('longitude');
        var eventId = $(eventObj.srcElement.parentElement).attr('eventId');
        var fleetId = $(eventObj.srcElement.parentElement).attr('fleetId');
        
        var eventIdList = []
        $.each($(eventObj.srcElement.parentElement.parentElement.children), function(index, element) {
            eventIdList.push($(element).attr('fleetId') + "-" + $(element).attr('eventId'));
        });
        
        this.__frame._setFollowMode(false);
        this.__frame._viewEvent(fleetId, {'latitude': latitude, 'longitude': longitude }, 14, eventId, eventIdList);
    };
    
    /**
     * Show the unit summary
     * @param unitId
     * @param fleetId
     * @param tab
     * @param unitNumber
     */
    ctor.prototype.__openUnitSummary  = nx.util.openUnitSummary;
    
    return ctor;
})();
