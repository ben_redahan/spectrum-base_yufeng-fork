/**
 * 
 */
var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.ClearButton = (function () {
    var COLOR1 = "#eabe3f";
    var COLOR2 = "#e8cf8a";
    var BORDER = "#666";
    var BLACK = "#000";
    var SECTIONS = 20;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        
        this.settings = $.extend({
            cx: component.x + component.width / 2,
            cy: component.y + component.height / 2,
            radius: Math.min(component.width, component.height) / 2,
            buttonRadius: Math.min(component.width, component.height) / 4,
            glowColor: 'red',
            glowWidth: Math.min(component.width, component.height) / 2,
            glowOpacity: 0.6,
            rimFill: 'silver',
            rimStroke: '#AAA',
            rimWidth: 0.06,
            rimHlFill: '0-#FFF-#AAA'
        }, settings);
        
        this.__value = false;
        this._paper = el;
        this._draw();
        this.__off();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (channelValue == null) {
            return;
        }
        
        if (channelValue == true) {
            if (!this.__value) {
                this.__on();
            }
        } else {
            if (this.__value) {
                this.__off();
            }
        }
        
        this.__value = channelValue;
    };
    
    ctor.prototype._reset = function() {
        this.__value = false;
        this.__off();
    };

    ctor.prototype._draw = function() {
        var me = this;
        
        this.__button = this._paper.set(
            this._paper.circle(
                this.settings.cx,
                this.settings.cy,
                this.settings.radius - 1
            ).attr({
                fill: this.settings.rimHlFill,
                stroke: '#AAA'
            }),

            this._paper.circle(
                this.settings.cx,
                this.settings.cy,
                this.settings.radius * 0.9
            ).attr({
                fill: this.settings.rimFill,
                stroke: '#999'
            }),

            this._paper.circle(
                this.settings.cx,
                this.settings.cy,
                this.settings.radius * 0.7
            ).attr({
                fill: 'black',
                stroke: '#666'
            }),

            this._paper.circle(
                this.settings.cx,
                this.settings.cy,
                this.settings.radius * 0.7
            ).attr({
                fill: this.settings.fill,
                opacity: 1,
                stroke: 'none'
            }),

            this._paper.circle(
                this.settings.cx,
                this.settings.cy,
                this.settings.radius * 0.6
            ).attr({
                fill: this.settings.fill,
                stroke: 'none'
            })
            
        );
    };
    
    ctor.prototype.__on = function() {
        this.__glow = this.__button[3].glow({
            color: this.settings.glowColor,
            width: this.settings.glowWidth,
            opacity: this.settings.glowOpacity
        });
    };
    
    ctor.prototype.__off = function() {
        if (!!this.__glow) {
            this.__glow.remove();
            this.__glow = null;
        }
        
        this.__button[3].attr({
            opacity: 1
        });

    };
    
    return ctor;
}());