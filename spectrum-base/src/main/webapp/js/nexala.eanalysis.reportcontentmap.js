var nx = nx || {};
nx.ea = nx.ea || {};

nx.ea.MapRenderer = (function() {
    function ctor(eventAnalysis) {
        this.__eventAnalysis = eventAnalysis;

        this.__content = $('#eaReportMapContent');

        this.__map = null;
        this.__defaultOptions = null;

        this.__data = null;
        this.__grouping = null;

        this.__cluster = null;
        
        var me = this;

        $.when(mapReady).done(function() {
        	me.__initialize();
        });
    };

    ctor.prototype.__initialize = function() {
        var me = this;

        this.__defaultOptions = {
            'credentials': 'AgJGCohfkXMsUBWvLBNVmL4OmIIOQYU-L8xeYNBjTNaMmD8ooayUqKzYwcsS8i9c',
            'disableBirdsEye': true,
            'enableSearchLogo': false,
            'enableClickableLogo': false,
            'backgroundColor': 'red',
            'showScalebar': false,
            'showDashboard': false,
            'showMapTypeSelector': true,
            'mapTypeId': Microsoft.Maps.MapTypeId.road
        };

        this.__content.append(
            $("<div>")
                .addClass('map-zoomer')
                .append(
                    $("<div>")
                        .addClass('map-zoom-out ui-corner-left ui-widget-header')
                        .text("-")
                )
                .append(
                    $("<div>")
                        .addClass('map-zoom-in ui-corner-right ui-widget-header')
                        .text("+")
                )
                .append(
                    $("<div>")
                        .addClass('map-zoom-overview ui-corner-right ui-widget-header')
                        .text($.i18n("Overview"))
                )
        );

        this.__content.find('.map-zoom-out')
            .on('mousedown', function() {
                $(this).addClass('ui-state-active');
    
                var range = me.__map.getZoomRange();
                var zoom = me.__map.getZoom() - 1;
    
                if (zoom > range.min) {
                    me.__map.setView({
                        'zoom' : zoom
                    });
                }
            })
            .on('mouseup', function() {
                $(this).removeClass('ui-state-active');
            });

        this.__content.find('.map-zoom-in')
            .on('mousedown', function() {
                $(this).addClass('ui-state-active');
    
                var range = me.__map.getZoomRange();
                var zoom = me.__map.getZoom() + 1;
    
                if (zoom < range.max) {
                    me.__map.setView({
                        'zoom' : zoom
                    });
                }
            })
            .on('mouseup', function() {
                $(this).removeClass('ui-state-active');
            });

        this.__content.find('.map-zoom-overview')
            .on('mousedown', function() {
                $(this).addClass('ui-state-active');
    
                me.__zoomOverview();
            })
            .on('mouseup', function() {
                $(this).removeClass('ui-state-active');
            });
    };

    ctor.prototype.__createMap = function() {
        if (!!this.__grouping.params.latitude && !!this.__grouping.params.longitude) {
            this.__defaultOptions.center =
                new Microsoft.Maps.Location(this.__grouping.params.latitude, this.__grouping.params.longitude);
        }

        if (!!this.__grouping.params.zoom) {
            this.__defaultOptions.zoom = parseInt(this.__grouping.params.zoom);
        }

        this.__map = new Microsoft.Maps.Map(this.__content[0], this.__defaultOptions);

        this.__cluster = new nx.ea.MapEventCluster(this.__eventAnalysis, this.__map, this.__grouping);
    };

    ctor.prototype.__zoomOverview = function() {
        this.__map.setView({
            'center': this.__defaultOptions.center
        });

        this.__map.setView({
            'zoom': this.__defaultOptions.zoom
        });
    };

    ctor.prototype.show = function() {
        if (this.__content.css('display') == 'none') {
            this.__content.show();
        }

        if (!this.__map) {
            this.__createMap();
        }

        this.__zoomOverview();
    }

    ctor.prototype.hide = function() {
        if (this.__content.css('display') != 'none') {
            this.__content.hide();
        }
    }

    ctor.prototype.update = function(data, grouping, filters) {
        var me = this;

        this.__grouping = grouping;
        
        if (!!this.__map) {
            this.__map.__grouping = grouping;
        }

        this.show(grouping);

        if (!filters) {
            return;
        }

        var detailFilter = {};
        for (var iFilter in filters) {
            detailFilter[iFilter] = filters[iFilter];
        }

        detailFilter.grouping = grouping.code;
        detailFilter.referenceKey = null;

        nx.rest.eventanalysis.searchDetailData(detailFilter, function(data) {
            me.__data = data;
            me.__updateContent();
        });
    };

    ctor.prototype.__updateContent = function() {
        this.__cluster.update(this.__data);
    };

    return ctor;
})();

nx.ea.MapEventCluster = (function() {
    function ctor(eventAnalysis, map, grouping) {
        this.__eventAnalysis = eventAnalysis;
        this.__map = map;
        this.__grouping = grouping;
        
        this.__routes = grouping.extraFields[0];
        this.__stations = grouping.extraFields[1];
        
        this.__routeProviderDesc = grouping.extraFields[2];
        this.__stationProviderDesc = grouping.extraFields[3];

        this.__options = {
            'gridSize': 50,
            'minRadio': 5,
            'maxRadio': 25
        };
        
        this.__routeLayer = new Microsoft.Maps.Layer('route');
        this.__stationLayer = new Microsoft.Maps.Layer('station');

        this.__clusterLayer = new Microsoft.Maps.Layer('cluster');
        this.__infoboxLayer = new Microsoft.Maps.Layer('infobox');
        


        this.__zoomLevel = null;

        this.__data = null;

        this.__initialize();
    }

    ctor.prototype.__initialize = function() {
        var me = this;
        
        this.__map.layers.insert(this.__routeLayer);
        this.__map.layers.insert(this.__stationLayer);

        this.__map.layers.insert(this.__clusterLayer);
        this.__map.layers.insert(this.__infoboxLayer);
        
        Microsoft.Maps.Events.addHandler(this.__map, 'viewchangeend', function() {
			
			for (var i = 0; i < me.__clusterLayer.getPrimitives().length; i++) {
                var pin = me.__clusterLayer.getPrimitives()[i];
                var options = pin._infobox.getOptions();
                if (options.visible) {
                    options.visible = false;

                    //'__hasClick' is not part of Bing specification, it's an attribute added manually to the object 
                    options['__hasClick'] = false;

                    pin._infobox.setOptions(options);
                }
            }
            //useful to get map center coordinates
            //var ctr = me.__map.getCenter();
            //var lt = ctr.latitude;
            //var lng= ctr.longitude;
            //console.log('latitude:' + lt + ', longitude:' + lng);

        	me.__doCluster();

        });

        Microsoft.Maps.Events.addThrottledHandler(this.__map, 'click', function() {
            for (var i = 0; i < me.__clusterLayer.getPrimitives().length; i++) {
                var pin = me.__clusterLayer.getPrimitives()[i];
                var options = pin._infobox.getOptions();
                if (options.visible) {
                    options.visible = false;

                    //'__hasClick' is not part of Bing specification, it's an attribute added manually to the object 
                    options['__hasClick'] = false;

                    pin._infobox.setOptions(options);
                }
            }
        });
        
        var routeProvider = new nx.map.marker.RouteMarkerProvider(this.__routeProviderDesc);
        routeProvider._enable = true;
        routeProvider.setRoute(this.__getRoutes(this.__routes));
        
        var stationProvider = new nx.map.marker.StationMarkerProvider(this.__stationProviderDesc);
        stationProvider._enable = true;
        stationProvider.setStations(this.__getStations(this.__stations));
        
        this._redrawProviders(routeProvider, this.__routeLayer);
        this._redrawProviders(stationProvider, this.__stationLayer);
        
    };
    
    ctor.prototype.__getRoutes = function(routeDefs) {
        var routes = [];
        
        for (var index in routeDefs) {
            var route = routeDefs[index];

            var coordinates = [];
            for (var i in route.points) {
                var coordinate = route.points[i];
                coordinates.push({
                    'latitude': coordinate.latitude,
                    'longitude': coordinate.longitude
                });
            }
            
            routes.push({
                'coordinate' : coordinates
            });
        }
        
        return routes;
    };
    
    ctor.prototype.__getStations = function(stationDefs) {
        var stations = [];
        
        for (var index in stationDefs) {
            var station = stationDefs[index];
            stations.push({
                'name': station.name,
                'code': station.code,
                'coordinate' : {
                    'latitude': station.coordinate.latitude,
                    'longitude': station.coordinate.longitude
                }
            });
        }
        
        return stations;
    };
    
    ctor.prototype._redrawProviders = function(provider, layer) {
        var me = this;
        var bounds = this.__map.getBounds();
        var zoom = this.__map.getZoom();
        var time = new Date();

            
        if (provider._enable) {
            // Get markers for this provider
            provider.getMarkers(bounds, zoom, time, function(markers) {
                  
                layer.clear();
                jQuery.each(markers, function(mk) {
                    var marker = markers[mk];
                    var pin = marker.getPushPin();
                       
                    // add the pin to the layer
                    layer.add(pin);
                        
                    var popup = marker.getPopup();

                    if (popup != null) {
                        // Add the popup to the layer if exist
                    	popup.setMap(me.__map);
                    }
                        
                    // initialise the marker
                    marker.initMarker(pin, popup);
                        
                });
            });
        } else {
            layer.clear();
        }
        
    };
    

    ctor.prototype.update = function(data) {
        this.__data = data;
        this.__doCluster();
    };

    ctor.prototype.__reset = function() {
		for (var i = 0; i < this.__clusterLayer.getPrimitives().length; i++) {
                var pin = this.__clusterLayer.getPrimitives()[i];
                var options = pin._infobox.getOptions();
                if (options.visible) {
                    options.visible = false;

                    //'__hasClick' is not part of Bing specification, it's an attribute added manually to the object 
                    options['__hasClick'] = false;

                    pin._infobox.setOptions(options);
                }
            }
        this.__clusterLayer.clear();
        this.__infoboxLayer.clear();
    };

    ctor.prototype.__doCluster = function() {
        this.__reset();

        if (!this.__data || this.__data.length == 0) {
            return;
        }

        if (this.__data.length == 0) {
            return;
        }

        var gridCells = this.__groupInGrid(this.__data);

        for (iCell in gridCells) {
            this.__addPin(gridCells[iCell], this.__data);
        }
    };

    ctor.prototype.__addPin = function(cellData, visibleData) {
        var me = this;

        var location = this.__getPinLocation(cellData);
        var radio = this.__getPinRadio(cellData, visibleData);
        var colour = this.__getPinColour(cellData);

        var options = {
            'icon': this.__getPinIcon(radio, colour),
            'anchor': new Microsoft.Maps.Point(radio, radio)
        };

        var pin = new Microsoft.Maps.Pushpin(location, options);
        var infobox = this.__getPinInfobox(location, radio, cellData)

        pin._infobox = infobox;

        Microsoft.Maps.Events.addThrottledHandler(pin, 'click',function() {
            var options = pin._infobox.getOptions();
            options.visible = true;

            //'__hasClick' is not part of Bing specification, it's an attribute added manually to the object
            options['__hasClick'] = true;

            pin._infobox.setOptions(options);

            $('.infobox-info table')
                .on('click', 'tr', function() {
                    $(this).css('background', 'white');
                    me.__infoboxClickHandler(this, pin);
                })
                .on('mouseover', 'tr', function() {
                    $(this).css('background', '#EEE');
                })
                .on('mouseout', 'tr', function() {
                    $(this).css('background', 'white');
                });
        });

        Microsoft.Maps.Events.addThrottledHandler(pin, 'mouseover',function (mouseEvent) {
            var options = pin._infobox.getOptions();

            if (!options.visible) {
                options.visible = true;
                pin._infobox.setOptions(options);
            }
        }, 250);

        Microsoft.Maps.Events.addThrottledHandler(pin, 'mouseout',function (mouseEvent) {
            var options = pin._infobox.getOptions();

            //'__hasClick' is not part of Bing specification, it's an attribute added manually to the object
            if (!options['__hasClick']) {
                options.visible = false;
                pin._infobox.setOptions(options);
            }
        }, 250);

        this.__clusterLayer.add(pin);
        infobox.setMap(me.__map);
    };

    ctor.prototype.__infoboxClickHandler = function(el, pin) {
        //'__eventTypes' is not part of Bing specification, it's an attribute added manually to the object
        var eventTypes = pin._infobox['__eventTypes'];

        var faultTypeId = null;

        var inputs = $(el).find('input');
        if (inputs.length > 0) {
            faultTypeId = $(inputs[0]).val();
        }

        var data = [];

        for (iEventType in eventTypes) {
            var eventType = eventTypes[iEventType];

            if (!faultTypeId || eventType.id == faultTypeId) {
                data = data.concat(eventType.data);

                if (eventType.id == faultTypeId) {
                    break;
                }
            }
        }

        this.__eventAnalysis.openReportDetailData(data);
    }

    ctor.prototype.__getPinInfobox = function(location, radio, cellData) {
        var eventTypes = this.__getCellDataEventTypes(cellData);

        var options = {
            'visible': false,
            'showPointer': true,
            'showCloseButton': false,
            'description': this.__getInfoboxDescription(cellData, eventTypes, radio),
            'offset': new Microsoft.Maps.Point(0, radio),
            'width': 120,
            'height': (eventTypes.length + 1) * 24,
            'zIndex': 0
        };

        var infobox = new Microsoft.Maps.Infobox(location, options);

        //'__eventTypes' is not part of Bing specification, it's an attribute added manually to the object
        infobox['__eventTypes'] = eventTypes;

        return infobox;
    };

    ctor.prototype.__getCellDataEventTypes = function(cellData) {
        var eventTypeMap = {};

        for (var iData in cellData) {
            var data = cellData[iData];

            if (!eventTypeMap[data.faultTypeId]) {
                eventTypeMap[data.faultTypeId] = { 'id': data.faultTypeId, 'name': data.faultType, 'data': [] };
            }

            eventTypeMap[data.faultTypeId].data.push(data);
        }

        var eventTypes = [];

        for (var iEventType in eventTypeMap) {
            var eventType = eventTypeMap[iEventType];
            eventTypes.push(eventType);
        }

        return eventTypes;
    };

    ctor.prototype.__getInfoboxDescription = function(cellData, eventTypes, radio) {
        var description = ['<table>'];
        description.push('<tr>');
        description.push('<td class="eaInfoboxLabel">Total Events:</td>');
        description.push('<td class="eaInfoboxValue">' + cellData.length + '</td>');
        description.push('</tr>');

        for (var iEventType in eventTypes) {
            var eventType = eventTypes[iEventType];

            description.push('<tr>');
            description.push('<input type="hidden" value="' + eventType.id + '" />');
            description.push('<td class="eaInfoboxLabel">' + eventType.name + '</td>');
            description.push('<td class="eaInfoboxValue">' + eventType.data.length + '</td>');
            description.push('</tr>');
        }

        description.push('</table>');

        return description.join("");
    };

    ctor.prototype.__getPinRadio = function(cellData, visibleData) {
        var area = this.__options.maxRadio * this.__options.maxRadio * Math.PI * cellData.length / visibleData.length;
        var radio = Math.sqrt(area / Math.PI)

        return Math.max(radio, this.__options.minRadio);
    };

    ctor.prototype.__getPinLocation = function(cellData) {
        var cellLocation = null;

        for(var iCell in cellData) {
            var cell = cellData[iCell];

            if (cellLocation == null || cellLocation.length < cell.length) {
                cellLocation = cell;
            }
        }

        return new Microsoft.Maps.Location(cellLocation.latitude, cellLocation.longitude);
    };

    ctor.prototype.__getPinColour = function(cellData) {
        var maxPriority = null;
        var colour = null;

        for(var iData in cellData) {
            var data = cellData[iData];

            if (maxPriority == null || maxPriority > data.faultTypePriority) {
                maxPriority = data.faultTypePriority;
                colour = data.faultTypeColor;
            }
        }

        return colour;
    };

    ctor.prototype.__getPinIcon = function(radio, colour) {
        if (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1")) {
            return [ "<svg version = '1.1' xmlns = 'http://www.w3.org/2000/svg' width = '", radio * 2, "px' height = '",
                     radio * 2, "px'>", "<circle cx = '", radio, "' cy = '", radio, "' r = '", radio, "' fill = '",
                     colour, "' pointer-events = 'visiblePainted' />", "</svg>" ].join("");
        }

        return [ "<v:oval fillcolor = '", colour, "' strokecolor = '", colour, "' style = 'height:", radio * 2,
                    "px; width:", radio * 2, "px; position: absolute; behavior: url(#default#VML);'></v:oval>" ].join("");
    };

    ctor.prototype.__groupInGrid = function(visibleData) {
        var boundaries = this.__getBoundaries(visibleData);

        // Happens if none of the data is visible on screen at the moment
        if (boundaries.length != 2) {
            return [];
        }

        var gridWidth = boundaries[1].x - boundaries[0].x;
        var gridHeight = boundaries[1].y - boundaries[0].y;

        var gridCols = parseInt(Math.ceil(gridWidth / this.__options.gridSize));
        var gridRows = parseInt(Math.ceil(gridHeight / this.__options.gridSize));

        var gridCells = new Array(gridCols * gridRows);

        for (var iData in visibleData) {
            var data = visibleData[iData];

            var location;
            try {
            	location = new Microsoft.Maps.Location(data.latitude, data.longitude);
            } catch (err) {
            	continue;
            }
            
            var pixel = this.__map.tryLocationToPixel(location, Microsoft.Maps.PixelReference.control);

            if (pixel.x > boundaries[0].x && pixel.x < boundaries[1].x && pixel.y > boundaries[0].y && pixel.y < boundaries[1].y) {
            
                var relativeX = pixel.x - boundaries[0].x;
                var relativeY = pixel.y - boundaries[0].y;
    
                var column = Math.round(relativeX / this.__options.gridSize);
                var row = Math.round(relativeY / this.__options.gridSize);
    
                var position = column + (row * gridCols);
    
                if (gridCells[position] == null) {
                    gridCells[position] = [];
                }
    
                gridCells[position].push(data);
            }
        };

        return gridCells;
    };

    ctor.prototype.__getBoundaries = function(visibleData) {
        var topLatitude = null;
        var bottomLatitude = null;
        var leftLongitude = null;
        var rightLongitude = null;
        
        // We only want to display the points that we can see 
        // (and a bit more in case the user scrolls around, we add 1 to each latitude/longitude later...)
        var bounds = this.__map.getBounds();
        var minLatitude = bounds.getSouth();
        var maxLatitude = bounds.getNorth();
        var minLongitude = bounds.getWest()
        var maxLongitude = bounds.getEast();

        for (var iData in visibleData) {
            var data = visibleData[iData];

            if ((topLatitude == null || data.latitude > topLatitude) 
                    && data.latitude < maxLatitude && data.latitude > minLatitude) {
                topLatitude = data.latitude;
            }

            if ((bottomLatitude == null || data.latitude < bottomLatitude)
                    && data.latitude < maxLatitude && data.latitude > minLatitude) {
                bottomLatitude = data.latitude;
            }

            if ((leftLongitude == null || data.longitude < leftLongitude) 
                    && data.longitude > minLongitude && data.longitude < maxLongitude) {
                leftLongitude = data.longitude;
            }

            if ((rightLongitude == null || data.longitude > rightLongitude)
                    && data.longitude > minLongitude && data.longitude < maxLongitude) {
                rightLongitude = data.longitude;
            }
        }

        if (topLatitude == null || bottomLatitude == null || leftLongitude == null || rightLongitude == null) {
            return [];
        }
        
        var topLeftlocation = new Microsoft.Maps.Location(topLatitude + 1, leftLongitude - 1);
        var topLeftPixel = this.__map.tryLocationToPixel(topLeftlocation, Microsoft.Maps.PixelReference.control);

        var bottomRightlocation = new Microsoft.Maps.Location(bottomLatitude - 1, rightLongitude + 1);
        var bottomRightPixel = this.__map.tryLocationToPixel(bottomRightlocation, Microsoft.Maps.PixelReference.control);

        return [topLeftPixel, bottomRightPixel];
    };

    return ctor;
})();
