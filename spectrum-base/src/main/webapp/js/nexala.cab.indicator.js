var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Indicator = (function () {

    // Parameters
    var __radiusRoundedCorner = 4;
    var __borderSize = 2;
    var __borderColor = '#555555';
    var __disableColor = '#ddd';

    var ctor = function (el, component,  settings) {
        nx.cab.Widget.call(this);
        
        this._component = component; 
        this.settings = $.extend({
            width: component.width,
            height: component.height
        }, settings);
        
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (channelValue == null) {
            this.__disable();
        } else if (channelValue == this.settings.onValue) {
            this.__on();
        } else {
            this.__off();
        }
    };
    
    ctor.prototype.__on = function() {
        var me = this;
        this.__centralRect.attr({
            fill: me.settings.onColorCenter
        });
        
        this.__indicator.attr({
            fill: me.settings.onColor
        });
    };
    
    ctor.prototype.__off = function() {
        var me = this;
        this.__centralRect.attr({
            fill: me.settings.offColorCenter
        });
        
        this.__indicator.attr({
            fill: me.settings.offColor
        });
    };
    
    
    ctor.prototype.__disable = function() {
        this.__centralRect.attr({
            fill: __disableColor
        });
        
        this.__indicator.attr({
            fill: __disableColor
        });
    };
    
    
    ctor.prototype._reset = function() {
        this.__disable();
    };
    
    ctor.prototype._draw = function() {
        var xOffset = this._component.x;
        var yOffset = this._component.y;
        
        // Draw border
        this._paper.rect(xOffset,yOffset,
                this.settings.width,
                this.settings.height - 0,
                __radiusRoundedCorner
            ).attr({
                fill: __borderColor,
                stroke: 'none'});
        
        // Draw indicator
        this.__indicator = this._paper.rect(xOffset+__borderSize, yOffset+__borderSize,
                this.settings.width - (__borderSize*2) ,
                this.settings.height - (__borderSize*2),
                __radiusRoundedCorner).attr({
                    fill: __disableColor,
                    stroke: 'none' });
        
        // Draw verticals lines
        
        // The width without borders
        var width = this.settings.width - (__borderSize*2);
        
        this._paper.rect(xOffset+width*0.27, yOffset+__borderSize, 
                __borderSize , this.settings.height - (__borderSize*2),
                0).attr({
                    fill: __borderColor,
                    stroke: 'none' });
        
        this._paper.rect(xOffset+width*0.73, yOffset+__borderSize, 
                __borderSize , this.settings.height - (__borderSize*2),
                0).attr({
                    fill: __borderColor,
                    stroke: 'none' });
        
        // Draw central rect
        this.__centralRect = this._paper.rect(
                xOffset+width*0.27 + __borderSize ,
                yOffset+ __borderSize, 
                width*0.46- __borderSize  , 
                this.settings.height - __borderSize*2,
                0).attr({ 
                    fill: __disableColor,
                    stroke: 'none' });

    };
     
    return ctor;
}());