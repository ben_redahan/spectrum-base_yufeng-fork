var nx = nx || {};

// XXX move to nx.core.js.
nx.core = (function() {
	return {
		keys : function(map) {
			if (!!Object.keys) {
				return Object.keys(map);
			} else {
				var items = [];

				for ( var key in map) {
					if (map.hasOwnProperty(key)) {
						items.push(key);
					}
				}

				return items;
			}
		}
	};
}());

/**
 * nx.rest A collection of functions for retrieving spectrum data
 */
nx.rest = (function() {
	var UNIT_SERVICE = "rest/unit";
	var UNIT_FIND = UNIT_SERVICE + "/find/?";
	var UNIT_SEARCH_CONFIGURATION = UNIT_SERVICE + "/unitSearchConfiguration";
	var UNIT_STOCK = UNIT_SERVICE + "/?/?";
	var UNIT_ALL = UNIT_SERVICE + "/allUnits";
	var UNIT_FORMATION = UNIT_SERVICE + "/formationUnits/?/?";
	var UNIT_LAST_TIMESTAMP = UNIT_SERVICE + "/timestamp/?/?";
	var UNIT_LAST_MAINTENANCE = UNIT_SERVICE + "/lastMaintenance/?/?";
	var UNIT_FLEET_UNITS = UNIT_SERVICE + "/fleetUnits/?";
	
	var UNIT_CHANNELDATA_SERVICE = "rest/channeldata/unit/?/?/?/?/?/?";

	var COMPONENT_CHANNEL_DATA_CONFIGURATION = "rest/componentchanneldata/configuration/?/?/?/?/?";
	var COMPONENT_CHANNEL_DATA_VEHICLES = "rest/componentchanneldata/vehicles/?";
	var COMPONENT_CHANNEL_DATA_AXLES = "rest/componentchanneldata/axles/?";
	var COMPONENT_CHANNEL_DATA_WHEELS = "rest/componentchanneldata/wheels/?";
	var COMPONENT_CHANNEL_DATA = "rest/componentchanneldata/?/?/?/?/?/?";

	var COMPONENT_HISTORY_COLUMNS = "rest/componenthistory/columns/?";
	var COMPONENT_HISTORY_SEARCH = "rest/componenthistory/search/";

	var COMPONENT_WHEEL_PROFILE = "rest/wheelprofile/?";
	var COMPONENT_WHEEL_PROFILES = "rest/wheelprofile/wheelProfiles/?";

	var CHART_SERVICE = "rest/chart";
	var CHART_SERVICE_EXPORT = "rest/chart/export";
	var CHART_SERVICE_EXPORT_AS_PDF = "rest/chart/exportaspdf";
	var CHART_SERVICE_VALUES = "rest/chart/values/?/?/?/?/?/?";
	var CHART_SAVE = "rest/chart/save/?/?/?/?";
	var CHART_SAVE_NEW = "rest/chart/save/?/?/?";
	var CHART_RENAME = "rest/chart/rename/?/?/?";
	var CHART_DELETE = "rest/chart/delete/?/?";
	var CHART_SERVICE_SYSTEM = "rest/chart/system/?";
	var CHART_SERVICE_USER = "rest/chart/user/?";
	var CHART_SERVICE_CHANNELS = "rest/chart/channels/?/?";
	var CHART_CONFIGURATION = "rest/chart/configuration";

	var GROUP_SERVICE = "rest/groups";
	var HELP_SERVICE_CONF = "rest/help/config";

	var EVENT_SERVICE_SEARCH = "rest/event/search/";
	var EVENT_SERVICE_SEARCH_ADVANCED = "rest/event/advancedSearch/";
	var EVENT_SERVICE_SEARCH_COUNT = "rest/event/searchCount/";
	var EVENT_SERVICE_SEARCH_ADVANCED_COUNT = "rest/event/searchAdvancedCount/";
	var EVENT_SERVICE_DOWNLOAD = "rest/event/download/";
	var EVENT_SERVICE_DOWNLOAD_ADVANCED = "rest/event/advancedDownload/";
	var EVENT_SERVICE_FORMATION = "rest/event/formation/?/?";

	var DOWNLOAD_SERVICE_SEARCH = "rest/download/search/";
	var DOWNLOAD_SERVICE_CHECK = "rest/download/checkDownload";
	var DOWNLOAD_SERVICE_INSERT = "rest/download/insertDownload";
	var DOWNLOAD_SERVICE_RETRIEVE = "rest/download/retrieve";

	return {
		/**
		 * @param callback -
		 *            A function to be called when the ajax request completes,
		 *            the first function parameter will be the ajax response.
		 * @param timestamp -
		 *            Optional parameter, if omitted returns the entire fleet,
		 *            otherwise returns the fleet changes since the specified
		 *            timestamp. If the data for this timestamp does not exist
		 *            in the server cache, the entire fleet will be returned.
		 */
		fleet : {
			FLEET_SERVICE_COLUMNS : 'rest/fleet/columns/?',
			FLEET_SERVICE_DEFAULTSORT : 'rest/fleet/defaultsort/?',
			FLEET_SERVICE_ALL : 'rest/fleet/?',
			FLEET_SERVICE_INC : 'rest/fleet/?/?',
			FLEET_SERVICE_DRILLDOWN : 'rest/fleet/drilldown/?/?/?/?',
			FLEET_SERVICE_STYLE : 'rest/fleet/style',
			FLEET_SERVICE_GET_FLEET_LIST : 'rest/fleet/fleetList',
			FLEET_SERVICE_GET_FLEET_TABS : 'rest/fleet/fleetTabs',
			FLEET_SERVICE_GET_COMMON_CONFIGURATION : 'rest/fleet/commonConfiguration',
			FLEET_SERVICE_GET_FILTER_LIST : 'rest/fleet/filterList',
			FLEET_SERVICE_GET_COLUMNS_UNIT_IDENTIFIER : 'rest/fleet/unitColumsIdentifier/?',
			FLEET_SERVICE_CONFIGURATION : 'rest/fleet/fleetConfiguration/?',

			configuration : function(fleetId, callback) {
				var service = this.FLEET_SERVICE_CONFIGURATION.replace("?",
						fleetId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},
			all : function(fleetId, callback) {
				var service = this.FLEET_SERVICE_ALL.replace("?", fleetId);

				var params = {
					cache : false,
					success : callback
				};

				return $.ajax(service, params);
			},

			drilldown : function(callback, unitIds, group, config, fleetId) {
				var service = this.FLEET_SERVICE_DRILLDOWN
						.replace("?", fleetId).replace("?", unitIds).replace(
								"?", group).replace("?", config);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			getFleetTabs : function(callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.FLEET_SERVICE_GET_FLEET_TABS, params);
			},

			getFleetList : function(callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.FLEET_SERVICE_GET_FLEET_LIST, params);
			},

			getCommonConfiguration : function(callback) {
				var params = {
					cache : true,
					success : callback
				};

				$.ajax(this.FLEET_SERVICE_GET_COMMON_CONFIGURATION, params);
			}
		},

		unitDetail : {
			UNIT_DETAIL_SERVICE_DATA : "rest/unitdetail/data/?/?/?",
            UNIT_DETAIL_ALL_EVENTS : "rest/unitdetail/events/all/?/?",
			UNIT_DETAIL_LIVE_EVENTS : "rest/unitdetail/events/live/?/?",
			UNIT_DETAIL_RECENT_EVENTS : "rest/unitdetail/events/recent/?/?",
			UNIT_DETAIL_ACKNOWLEDGED_EVENTS : "rest/unitdetail/events/acknowledged/?/?",
			UNIT_DETAIL_NOT_ACKNOWLEDGED_EVENTS : "rest/unitdetail/events/notacknowledged/?/?",
            UNIT_DETAIL_ALL_RESTRICTIONS : "rest/unitdetail/restrictions/all/?/?",
			UNIT_DETAIL_LIVE_RESTRICTIONS : "rest/unitdetail/restrictions/live/?/?",
			UNIT_DETAIL_RECENT_RESTRICTIONS : "rest/unitdetail/restrictions/recent/?/?",
			UNIT_DETAIL_WORK_ORDERS : "rest/unitdetail/workOrders/?/?",
            UNIT_DETAIL_WORK_ORDER_COMMENT : "rest/unitdetail/workOrderComment/?/?",
			UNIT_DETAIL_SERVICE_CONF : "rest/unitdetail/conf/?",
			UNIT_DETAIL_LICENCE : "rest/unitdetail/haslicence/?",
			UNIT_DETAIL_LIVE_FORMATION_EVENTS : "rest/unitdetail/events/liveFormation/?/?",
            UNIT_DETAIL_RECENT_FORMATION_EVENTS : "rest/unitdetail/events/recentFormation/?/?",
			UNIT_DETAIL_SERVICE_DATA_INFO : "rest/unitdetail/data/info/?/?/?",
			UNIT_DETAIL_SERVICE_VEHICLES_TIMESTAMP : "rest/unitdetail/vehiclesTimestamp/?/?/?",

			conf : function(fleetId, callback) {
				var service = this.UNIT_DETAIL_SERVICE_CONF;
				service = service.replace("?", fleetId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			hasLicence : function(licenceKey, callback) {
				var service = this.UNIT_DETAIL_LICENCE.replace("?", licenceKey);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			data : function(unitId, fleetId, timestamp, callback) {
				var service = this.UNIT_DETAIL_SERVICE_DATA.replace("?",
						fleetId).replace("?", unitId).replace("?", timestamp);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			dataInfo : function(unitId, fleetId, timestamp, callback) {
				var service = this.UNIT_DETAIL_SERVICE_DATA_INFO.replace("?",
						fleetId).replace("?", unitId).replace("?", timestamp);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			vehiclesTimestamp : function(unitId, fleetId, timestamp, callback) {
				var service = this.UNIT_DETAIL_SERVICE_VEHICLES_TIMESTAMP
						.replace("?", fleetId).replace("?", unitId).replace(
								"?", timestamp);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

            allEvents : function(fleetId, unitId, callback) {
                var service = this.UNIT_DETAIL_ALL_EVENTS
                        .replace("?", fleetId).replace("?", unitId);

                var params = {
                    cache : false,
                    success : callback
                };

                $.ajax(service, params);
            },

			liveEvents : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_LIVE_EVENTS
						.replace("?", fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			recentEvents : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_RECENT_EVENTS.replace("?",
						fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			acknowledgedEvents : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_ACKNOWLEDGED_EVENTS.replace("?",
						fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			notAcknowledgedEvents : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_NOT_ACKNOWLEDGED_EVENTS.replace(
						"?", fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

            allRestrictions : function(fleetId, unitId, callback) {
                var service = this.UNIT_DETAIL_ALL_RESTRICTIONS.replace(
                        "?", fleetId).replace("?", unitId);

                var params = {
                    cache : false,
                    success : callback
                };

                $.ajax(service, params);
            },

			liveRestrictions : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_LIVE_RESTRICTIONS.replace(
                        "?", fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			recentRestrictions : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_RECENT_RESTRICTIONS.replace(
                        "?", fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			workOrders : function(fleetId, unitId, callback) {
				var service = this.UNIT_DETAIL_WORK_ORDERS
						.replace("?", fleetId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

            workOrderComment : function(fleetId, woNumber, callback) {
                var service = this.UNIT_DETAIL_WORK_ORDER_COMMENT
                        .replace("?", fleetId).replace("?", woNumber);

                var params = {
                    dataType  : "html",
                    cache : false,
                    success : callback
                };

                $.ajax(service, params);
            },
            
            liveFormationEvents : function(fleetId, unitId, callback) {
                var service = this.UNIT_DETAIL_LIVE_FORMATION_EVENTS
                        .replace("?", fleetId).replace("?", unitId);

                var params = {
                    cache : false,
                    success : callback
                };

                $.ajax(service, params);
            },
            
            recentFormationEvents : function(fleetId, unitId, callback) {
                var service = this.UNIT_DETAIL_RECENT_FORMATION_EVENTS
                        .replace("?", fleetId).replace("?", unitId);

                var params = {
                    cache : false,
                    success : callback
                };

                $.ajax(service, params);
            }
		},
		
		unitSummary : {
			UNIT_SUMMARY_SERVICE_CONF : "rest/unitsummary/conf/?",
			
			conf : function(fleetId, callback) {
				var service = this.UNIT_SUMMARY_SERVICE_CONF;
				service = service.replace("?", fleetId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			}, 
		},

		componentUnitDetail : {
			UNIT_DETAIL_SERVICE_DATA : "rest/componentunitdetail/data/?/?/?",
			UNIT_DETAIL_SERVICE_CONF : "rest/componentunitdetail/conf/?",
			UNIT_DETAIL_LICENCE : "rest/componentunitdetail/haslicence/?",

			conf : function(fleetId, callback) {
				var service = this.UNIT_DETAIL_SERVICE_CONF;
				service = service.replace("?", fleetId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			hasLicence : function(licenceKey, callback) {
				var service = this.UNIT_DETAIL_LICENCE.replace("?", licenceKey);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			data : function(unitId, fleetId, timestamp, callback) {
				var service = this.UNIT_DETAIL_SERVICE_DATA.replace("?",
						fleetId).replace("?", unitId).replace("?", timestamp);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			}
		},

		channelData : {
			GROUP_DATA : 'rest/channeldata/groupdata/?/?/?',

			unit : function(unitId, fleetId, timestamp, groupId, all,
					direction, callback) {

				var service = UNIT_CHANNELDATA_SERVICE.replace("?", fleetId)
						.replace("?", unitId).replace("?", timestamp).replace(
								"?", direction).replace("?", groupId).replace(
								"?", all);

				var params = {
					cache : false,
					success : callback
				};

				return $.ajax(service, params);
			},
			groupData : function(fleetId, groupId, unitId, callback) {
				var service = this.GROUP_DATA.replace("?", fleetId).replace(
						"?", groupId).replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);

			}
		},

		/**
		 * A collection of recovery services
		 */
		recovery : {
			RECOVERY_COLUMNS : 'rest/recovery/columns',
			EXECUTE_RECOVERY : 'rest/recovery/execute/?/?/?',
			RECOVERY_NEXT_STEP : 'rest/recovery/nextStep/?/?',
			RECOVERY_WORST_CASE_SCENARIO : 'rest/recovery/worstCaseScenario/?/?',
			RECOVERY_STEP_OUTCOME : 'rest/recovery/setStepOutcome/?/?/?/?/?/?/?',
			COMPLETE_RECOVERY : 'rest/recovery/completeRecovery/?/?/?',
			RESUME_RECOVERY : 'rest/recovery/resumeRecovery/?/?',
			ACTIVE_RECOVERY : 'rest/recovery/isActive/?/?',
			RECOVERY_AUDIT_ITEMS : 'rest/recovery/auditItems/?/?',

			columns : function(callback) {
				$.ajax(this.RECOVERY_COLUMNS, {
					cache : true,
					success : callback
				});
			},

			execute : function(fleetId, faultCode, faultId, callback) {
				var service = this.EXECUTE_RECOVERY.replace('?', fleetId)
						.replace('?', faultCode).replace('?', faultId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			nextStep : function(fleetId, contextId, callback) {
				var service = this.RECOVERY_NEXT_STEP.replace('?', fleetId)
						.replace('?', contextId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			worstCaseScenario : function(fleetId, contextId, callback) {
				var service = this.RECOVERY_WORST_CASE_SCENARIO.replace('?',
						fleetId).replace('?', contextId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			setStepOutcome : function(fleetId, contextId, faultId, answerId,
					answer, action, worked, notes, completed, callback) {
				var service = this.RECOVERY_STEP_OUTCOME.replace('?', fleetId)
						.replace('?', contextId).replace('?', faultId).replace(
								'?', answerId).replace('?', answer).replace(
								'?', worked).replace('?', completed);

				if (action != null) {
					service = service + "?action=" + action;
				}

				if (notes != null && notes != "") {
					service = service + "?notes=" + notes;
				}

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			completeRecovery : function(fleetId, contextId, faultId, callback) {
				var service = this.COMPLETE_RECOVERY.replace('?', fleetId)
						.replace('?', contextId).replace('?', faultId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			resumeRecovery : function(fleetId, contextId, callback) {
				var service = this.RESUME_RECOVERY.replace('?', fleetId)
						.replace('?', contextId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			isActive : function(fleetId, faultId, callback) {
				var service = this.ACTIVE_RECOVERY.replace('?', fleetId)
						.replace('?', faultId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			getAuditItems : function(fleetId, faultId, callback) {
				var service = this.RECOVERY_AUDIT_ITEMS.replace('?', fleetId)
						.replace('?', faultId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			}
		},

		componentChannelData : {
			data : function(id, componentId, groupId, vehicleId, axleId, all,
					callback) {
				var service = COMPONENT_CHANNEL_DATA.replace("?", id).replace(
						"?", componentId).replace("?", groupId).replace("?",
						vehicleId).replace("?", axleId).replace("?", all);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			configuration : function(id, componentId, groupId, vehicleId,
					axleId, callback) {
				var service = COMPONENT_CHANNEL_DATA_CONFIGURATION.replace("?",
						id).replace("?", componentId).replace("?", groupId)
						.replace("?", vehicleId).replace("?", axleId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			vehicles : function(id, callback) {
				var service = COMPONENT_CHANNEL_DATA_VEHICLES.replace("?", id);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			axles : function(id, callback) {
				var service = COMPONENT_CHANNEL_DATA_AXLES.replace("?", id);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			wheels : function(id, callback) {
				var service = COMPONENT_CHANNEL_DATA_WHEELS.replace("?", id);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			}

		},

		componentHistory : {
			columns : function(columns, callback) {
				var service = COMPONENT_HISTORY_COLUMNS.replace("?", columns);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			search : function(parameters, callback) {
				$.ajax(COMPONENT_HISTORY_SEARCH, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback
				});
			}
		},

		wheelProfile : {
			wheelProfile : function(measurementId) {
				var service = COMPONENT_WHEEL_PROFILE;

				if (!!measurementId) {
					service = service.replace("?", measurementId);
				}
				return service;
			},

			wheelProfiles : function(wheelId, callback) {
				var service = COMPONENT_WHEEL_PROFILES.replace("?", wheelId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			}
		},
		
		/**
		 * Schematic service
		 */
		
		schematic : {
			SCHEMATIC_SERVICE : 'rest/schematic',
			SCHEMATIC_ITEMS : 'rest/schematic/items',
			SCHEMATIC_CHANNELS : 'rest/schematic/unit/?/?/?',
			SCHEMATIC_SERVICE_STOCK : 'rest/schematic/stock/?/?',
			
			channelData : function(fleetId, unitId, timestamp, callback) {
				var service = this.SCHEMATIC_CHANNELS.replace("?", fleetId).replace(
						"?", unitId).replace("?", timestamp);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},
			
			getItemsList : function(callback) {
				var params = {
					cache : true,
					success : callback
				};

				$.ajax(this.SCHEMATIC_ITEMS, params);
			},
			
			stock : function(fleetId, unitId, callback) {
				var service = this.SCHEMATIC_SERVICE_STOCK.replace("?", fleetId)
						.replace("?", unitId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			}
		},

		/**
		 * Cab service
		 */
		cab : {
			CAB_SERVICE : 'rest/cab',
			CAB_SERVICE_LAYOUT : 'rest/cab/layout/?/?',
			CAB_SERVICE_DATA : 'rest/cab/data/?/?/?/?/?',
			CAB_SERVICE_DATA_LIVE : 'rest/cab/data/?/?/?/?',
			CAB_SERVICE_STOCK : 'rest/cab/stock/?/?',

			layout : function(fleetId, stockId, callback) {
				var service = this.CAB_SERVICE_LAYOUT.replace("?", fleetId)
						.replace("?", stockId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			data : function(fleetId, unitId, stockId, prevTimestamp, timestamp,
					callback) {

				var service = timestamp == null ? this.CAB_SERVICE_DATA_LIVE
						: this.CAB_SERVICE_DATA;

				service = service.replace("?", fleetId).replace("?", unitId)
						.replace("?", stockId).replace("?", prevTimestamp);

				if (timestamp != null) {
					service = service.replace("?", timestamp);
				}

				$.ajax(service, {
					cache : false,
					success : callback
				});
			},

			stock : function(fleetId, unitId, callback) {
				var service = this.CAB_SERVICE_STOCK.replace("?", fleetId)
						.replace("?", unitId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			}
		},

		/**
		 * Charting
		 */
		chart : {
			system : function(fleetId, callback) {
				var service = CHART_SERVICE_SYSTEM.replace("?", fleetId);

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			user : function(fleetId, callback) {
				var service = CHART_SERVICE_USER.replace("?", fleetId);
				$.ajax(service, {
					cache : false,
					success : callback
				});
			},

			channels : function(fleetId, configId, callback) {
				var service = CHART_SERVICE_CHANNELS.replace("?", fleetId)
						.replace("?", configId);

				$.ajax(service, {
					cache : false,
					success : callback
				});
			},

			/**
			 * Returns the path to the rest service which will create the chart.
			 * 
			 * @param vehicle
			 *            The id of the vehicle to plot data for
			 * @param interval
			 *            Number of seconds before the end time
			 * @param etime
			 *            The end time for the chart data
			 * @param the
			 *            browser timezone. Should be the time-zone offset from
			 *            UTC, in minutes, for the current locale. provide by
			 *            new Date().getTimezoneOffset()
			 * @param height
			 *            The height of the chart - if null, height will be
			 *            chosen by the server.
			 * @param width
			 *            The width of the chart
			 * @param channels
			 *            an array of channel ids.
			 */
			chart : function(fleetId, vehicle, interval, etime, timezone,
					height, width, channels) {
				var url = [ CHART_SERVICE, '/', fleetId, '/', vehicle, '/',
						interval, '/', etime, '/', timezone, '/',
						channels.join(',') ];

				if (height != null) {
					url.push('?');
					url.push('height=');
					url.push(height);
				}

				if (width != null) {
					if (!!height) {
						url.push('&');
					} else {
						url.push('?');
					}
					url.push('width=');
					url.push(width);
				}

				// force ie to don't use the cached image
				if (!!height || !!width) {
					url.push('&');
				} else {
					url.push('?');
				}
				url.push('requestDate=');
				url.push(new Date().getTime());

				return url.join('');
			},
			/**
			 * Returns the path to the rest service which will create the export
			 * file.
			 * 
			 * @param vehicle
			 *            The id of the vehicle to plot data for
			 * @param interval
			 *            Number of seconds before the end time
			 * @param etime
			 *            The end time for the chart data
			 * @param channels
			 *            an array of channel ids.
			 * @param the
			 *            browser timezone
			 */
			chartExport : function(fleetId, vehicle, interval, etime, channels,
					timezone) {
				var url = [ CHART_SERVICE_EXPORT, '/', fleetId, '/', vehicle,
						'/', interval, '/', etime, '/', channels.join(','),
						'/', timezone ];

				return url.join('');
			},

			exportaspdf : function(fleetId, unit, vehicle, interval, etime,
					timezone, height, width, channels, type) {
				var url = [ CHART_SERVICE_EXPORT_AS_PDF, '/', fleetId, '/',
						unit, '/', vehicle, '/', interval, '/', etime, '/',
						timezone, '/', channels.join(','), '/', type ];

				if (!!height) {
					url.push('?');
					url.push('height=');
					url.push(height);
				}

				if (!!width) {
					if (!!height) {
						url.push('&');
					} else {
						url.push('?');
					}
					url.push('width=');
					url.push(width);
				}

				// force ie to don't use the cached image
				if (!!height || !!width) {
					url.push('&');
				} else {
					url.push('?');
				}
				url.push('requestDate=');
				url.push(new Date().getTime());

				return url.join('');
			},

			/**
			 * Returns a list of values by timestamp
			 * 
			 * @param vehicle
			 *            The id of the vehicle to plot data for
			 * @param interval
			 *            Number of seconds before the end time
			 * @param endTime
			 *            The end time for the chart data
			 * @param channels
			 *            an array of channel ids.
			 * @param callBack
			 *            function
			 */
			values : function(fleetId, vehicle, interval, endTime, endRow,
					channels, callback) {
				var service = CHART_SERVICE_VALUES.replace(/\?/, fleetId)
						.replace(/\?/, vehicle).replace(/\?/, interval)
						.replace(/\?/, endTime).replace(/\?/, endRow).replace(
								/\?/, channels);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			remove : function(fleetId, chartId, callback) {
				var service = CHART_DELETE.replace(/\?/, fleetId).replace(/\?/,
						chartId);

				var params = {
					cache : false,
					success : callback,
					type : "PUT"
				};

				$.ajax(service, params);
			},

			rename : function(fleetId, chartId, newName, callback) {
				var service = CHART_RENAME.replace(/\?/, fleetId).replace(/\?/,
						chartId).replace(/\?/, newName);

				var params = {
					cache : false,
					success : callback,
					type : "PUT"
				};

				$.ajax(service, params);
			},

			save : function(fleetId, chartId, newName, channels, callback) {
				var service;

				if (chartId != null) {
					service = CHART_SAVE.replace(/\?/, fleetId).replace(/\?/,
							chartId).replace(/\?/, newName).replace(/\?/,
							channels.join(','));
				} else {
					service = CHART_SAVE_NEW.replace(/\?/, fleetId).replace(
							/\?/, newName).replace(/\?/, channels.join(','));
				}

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			configuration : function(callback) {
				var service = CHART_CONFIGURATION;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},
		},

		/**
		 * A collection of services for returning Channel Group information.
		 */
		group : {
			/** Calls /spectrum/rest/groups/[fleetId] */
			all : function(fleetId, callback) {
				$.ajax(GROUP_SERVICE + "/" + fleetId, {
					cache : true,
					success : callback
				});
			},

			/** Calls /spectrum/rest/groups/[fleetId]/[groupId] */
			channels : function(fleetId, groupId, callback) {
				$.ajax(GROUP_SERVICE + "/" + fleetId + "/" + groupId, {
					cache : true,
					success : callback
				});
			}
		},

		help : {
			config : function(callback) {
				$.ajax(HELP_SERVICE_CONF, {
					cache : true,
					success : callback
				});
			}
		},
		
        FLEET_SERVICE_CONFIGURATION : 'rest/fleet/fleetConfiguration/?',
        
        configuration: function(fleetId, callback) {
            var service = this.FLEET_SERVICE_CONFIGURATION.replace("?", fleetId);
            
            var params = {
                    cache : false,
                    success : callback
            };

            $.ajax(service, params);
        },  

		stock : {
			STOCK : "/rest/stock",
			SIBLINGS : "/rest/stock/siblings",
			STOCKS_BY_EVENT: "/rest/stock/stocksByEvent",
			CHILDREN : "/rest/stock/children",

			find : function(stockId, callback) {
				var service = this.STOCK + '/' + stockId;

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			siblings : function(fleetId, stockId, callback) {
				var service = this.SIBLINGS + '/' + fleetId + '/' + stockId;

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},
			
			stocksByEvent : function(fleetId, eventId, callback) {
                var service = this.STOCKS_BY_EVENT + '/' + fleetId + '/' + eventId;

                $.ajax(service, {
                    cache : true,
                    success : callback
                });
            },

			children : function(stockCollectionId, callback) {
				var service = this.CHILDREN + '/' + stockCollectionId;

				$.ajax(service, {
					cache : true,
					success : callback
				});
			}
		},

		/**
		 * A collection of unit services..
		 */
		unit : {
			/**
			 * Finds a unit with a descriptor, i.e. headcode, unit number, etc.
			 * matching ^.*<substr>.*$
			 * 
			 * @param substr
			 *            The string to match
			 * 
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes, the first function parameter will be the
			 *            ajax response.
			 */
			find : function(substr, callback) {
				var service = UNIT_FIND.replace(/\?$/, substr);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},
			
			unitSearchConfiguration : function(callback) {
			    var service = UNIT_SEARCH_CONFIGURATION;
                var params = {
                    cache : true,
                    success : callback
                };
                $.ajax(service, params);
            },

			stock : function(unitId, fleetId, callback) {
				var service = UNIT_STOCK.replace("?", unitId).replace("?",
						fleetId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			allUnits : function(callback) {
				var service = UNIT_ALL;

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			formationUnits : function(unitId, fleetId, callback) {
				var service = UNIT_FORMATION.replace("?", unitId).replace("?",
						fleetId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			lastTimestamp : function(fleetId, unitId, callback) {
				var service = UNIT_LAST_TIMESTAMP.replace("?", fleetId)
						.replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},
			
			lastMaintenance : function(fleetId, unitId, callback) {
				var service = UNIT_LAST_MAINTENANCE.replace("?", fleetId)
						.replace("?", unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			fleetUnits : function(fleetId, callback) {
				var service = UNIT_FLEET_UNITS.replace("?", fleetId);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},
		},

		/**
		 * A collection of event services
		 */
		event : {
			CURRENT : "rest/event/current",
			LIVE_COUNT : "rest/event/livecount",
			CHANNEL_GROUP : "rest/event/channelGroup",
			EVENT_UPDATE : "rest/event/update/",
			EVENT_COMMENTS : "rest/event/comments/?/?",
			EVENT_SAVE_COMMENT : "rest/event/saveComment",
			EVENT_DETAIL : "rest/event/eventDetail",
			EVENT_DETAIL_CONFIGURATION : "rest/event/eventDetailConfiguration",
			EVENT_DETAIL_EXTENSION : "rest/event/extendedEventDetail",
			// EVENT_FORMATION: "rest/event/eventFormation",
			EVENT_DETAIL_DATA : "rest/event/eventData",
			EVENT_DETAIL_SEND_FAULT : "rest/event/sendFault",
			EVENT_DETAIL_ACKNOWLEDGE_FAULT : "rest/event/acknowledge",
			EVENT_DETAIL_DEACTIVATE_FAULT : "rest/event/deactivate",
			EVENT_GROUP_CONFIGURATION : "rest/event/eventGroupConfiguration",
			EVENT_GET_LICENCES : "rest/event/getLicences",
			EVENT_GET_ENDTIME : "rest/event/getEndTime/?/?",
			EVENT_CATEGORIES : "rest/event/eventCategories",
			EVENT_TYPES : "rest/event/eventTypes",
			EVENT_CODES : "rest/event/eventCodes",
			EVENT_COLUMNS : "rest/event/eventColumns",
			EVENT_PANEL_CONFIGURATION : "rest/event/panelConfiguration/?",
			ADDITIONAL_FIELDS : "rest/event/additionalSearchFields",
			EVENT_SEVERITIES : "rest/event/eventSeverities",
			NEW_EVENTS : "rest/event/newEvents",
			EVENT_HISTORY_CONF : "rest/event/eventHistoryConfiguration",
			EVENT_PANEL_LICENCE : "rest/event/panelLicence",
			EVENT_CREATE_NEW_FAULT : "rest/event/createNewFault/?/?/?/?",
			EVENT_CREATE_NEW_FAULT_DROPDOWNS : "rest/event/newFaultDropDowns",
			EVENTS_BY_GROUP : "rest/event/eventsByGroup/?/?",
			EVENT_CREATE_GROUP : "rest/event/createFaultGroup/?/?/?",
			EVENT_UPDATE_GROUP : "rest/event/updateFaultGroup/?/?/?",
			EVENT_FIND_UNIT : "rest/event/find/?",
			EVENT_DETAIL_TIME_PERIOD : "rest/event/saveFaultPeriod",

			liveCount : function(filters, timestamp, callback, onCompleteCallback) {
				var service = this.LIVE_COUNT;

				if (timestamp != null) {
					service += '/' + timestamp;
				}

				$.ajax(service, {
					type : 'POST',
					contentType : 'application/json; charset=utf-8',
					data : JSON.stringify(filters),
					cache : false,
					success : callback,
					complete : onCompleteCallback
				});
			},

			panelConfiguration : function(isCurrentScreenFleetSummary, callback) {
				var service = this.EVENT_PANEL_CONFIGURATION.replace(/\?/,
						isCurrentScreenFleetSummary);

				$.ajax(service, {
					cache : false,
					success : callback
				});
			},

			additionalSearchFields : function(callback) {
				$.ajax(this.ADDITIONAL_FIELDS, {
					cache : true,
					success : callback
				});
			},

			newEvents : function(timestamp, callback) {
				var service = this.NEW_EVENTS;

				if (timestamp != null) {
					service += '/' + timestamp;
				}

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			allCurrent : function(filters, callback) {
				var service = this.CURRENT;

				$.ajax(service, {
					type : 'POST',
					contentType : 'application/json; charset=utf-8',
					data : JSON.stringify(filters),
					cache : false,
					success : callback
				});
			},

			channelGroup : function(fleetId, callback) {
				var service = this.CHANNEL_GROUP;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			eventCategories : function(callback) {
				var service = this.EVENT_CATEGORIES;

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			eventCategoriesByFleet : function(fleetId, callback) {
				var service = this.EVENT_CATEGORIES + '/' + fleetId;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			eventCodes : function(callback) {
				var service = this.EVENT_CODES;

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			eventTypes : function(callback) {
				$.ajax(this.EVENT_TYPES, {
					cache : true,
					success : callback
				});
			},

			eventColumns : function(callback) {
				var service = this.EVENT_COLUMNS;

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			eventSeverities : function(callback) {
				var service = this.EVENT_SEVERITIES;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			eventSeveritiesByFleet : function(fleetId, callback) {
				var service = this.EVENT_SEVERITIES + '/' + fleetId;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns the events for a selected formation.
			 * 
			 * @param formation
			 *            A formation can be a single unit or a list of units
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			formation : function(formation, fleetId, callback) {
				var service = EVENT_SERVICE_FORMATION.replace("?", fleetId)
						.replace("?", formation);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			searchCount : function(parameters, callback) {
				$.ajax(EVENT_SERVICE_SEARCH_COUNT, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback
				});
			},

			searchAdvancedCount : function(parameters, callback) {
				$.ajax(EVENT_SERVICE_SEARCH_ADVANCED_COUNT, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback
				});
			},

			search : function(parameters, callback) {
				$.ajax(EVENT_SERVICE_SEARCH, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback
				});
			},

			advancedSearch : function(parameters, callback) {
				$.ajax(EVENT_SERVICE_SEARCH_ADVANCED, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback
				});
			},

			download : function() {
				return EVENT_SERVICE_DOWNLOAD;
			},

			advancedDownload : function() {
				return EVENT_SERVICE_DOWNLOAD_ADVANCED;
			},

			eventDetailConfiguration : function(callback) {
				$.ajax(this.EVENT_DETAIL_CONFIGURATION, {
					cache : true,
					success : callback
				});
			},

			eventDetailExtension : function(fleetId, eventId, callback) {
				var service = this.EVENT_DETAIL_EXTENSION;
				service += '/' + fleetId + "/" + eventId;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			eventDetail : function(fleetId, eventId, callback) {
				var service = this.EVENT_DETAIL;
				service += '/' + fleetId + "/" + eventId;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);

			},

			eventGroupConfiguration : function(callback) {
				var service = this.EVENT_GROUP_CONFIGURATION;
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			getLicences : function(success) {
				var service = this.EVENT_GET_LICENCES;

				var params = {
					cache : false,
					success : success
				};

				$.ajax(service, params);
			},

			update : function(parameters, success, error) {
				$.ajax(this.EVENT_UPDATE, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(parameters),
					cache : false,
					success : success,
					error : error
				});
			},

			comments : function(fleetId, eventId, success) {
				var service = this.EVENT_COMMENTS.replace(/\?/, fleetId)
						.replace(/\?/, eventId);

				$.ajax(service, {
					cache : false,
					success : success
				});
			},

			saveComment : function(fleetId, eventId, timestamp, comment,
					action, callback) {
				var service = this.EVENT_SAVE_COMMENT;

				var parameters = {
					"fleetId" : fleetId,
					"eventId" : eventId,
					"timestamp" : timestamp,
					"comment" : comment,
					"action" : action
				};

				$.ajax(service, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(parameters),
					cache : false,
					success : callback
				});
			},

			getEndTime : function(fleetId, eventId, callback) {
				var service = this.EVENT_GET_ENDTIME.replace(/\?/, fleetId)
						.replace(/\?/, eventId);

				$.ajax(service, {
					cache : false,
					success : callback
				});
			},

			eventFormation : function(eventId, fleetId, callback) {
				var service = this.EVENT_FORMATION;
				service += '/' + fleetId;
				service += '/' + eventId;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			eventDetailData : function(eventId, groupId, vehicleId, fleetId,
					period,timestamp,callback) {
				var service = this.EVENT_DETAIL_DATA;
				service += '/' + eventId + '/' + groupId + '/' + vehicleId
						+ '/' + fleetId 
						+ '/' + period
						+ '/' + timestamp;
				

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			sendFault : function(fleetId, eventId, callback) {
				var service = this.EVENT_DETAIL_SEND_FAULT;
				service += '/' + fleetId + '/' + eventId;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			acknowledge : function(fleetId, eventId, rowVersion, eventGroupId, callback,
					errorCallback) {
				var service = this.EVENT_DETAIL_ACKNOWLEDGE_FAULT;

				var parameters = {
					"fleetId" : fleetId,
					"eventId" : eventId,
					"rowVersion" : rowVersion,
					"eventGroupId" : eventGroupId
				};

				$.ajax(service, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback,
					error : errorCallback
				});
			},
			
			saveFaultPeriod : function(fleetId,periodSelected, eventID, timeStamp, vehicleId, callback,
					errorCallback) {
				var service = this.EVENT_DETAIL_TIME_PERIOD;
				
				service += '/' + fleetId + '/' + periodSelected + '/' + eventID
				+ '/' + timeStamp 
				+ '/' + vehicleId;
		

				$.ajax(service, {
					type : "POST",
					cache : true,
					success : callback,
					error : errorCallback
				});
				
				
			},

			deactivate : function(fleetId, eventId, userName, isCurrent,
					rowVersion, callback, errorCallback) {
				var service = this.EVENT_DETAIL_DEACTIVATE_FAULT;

				var parameters = {
					"fleetId" : fleetId,
					"eventId" : eventId,
					"userName" : userName,
					"isCurrent" : isCurrent,
					"rowVersion" : rowVersion
				};

				$.ajax(service, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					data : JSON.stringify(parameters),
					success : callback,
					error : errorCallback
				});
			},

			eventHistoryConf : function(callback) {
				var service = this.EVENT_HISTORY_CONF;

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			panelLicence : function(callback) {
				var service = this.EVENT_PANEL_LICENCE;

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},

			getCreateFaultDropDowns : function(callback) {
				var service = this.EVENT_CREATE_NEW_FAULT_DROPDOWNS;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			createNewFault : function(fleetId, unitId, faultCode, timestamp,
					callback) {
				var service = this.EVENT_CREATE_NEW_FAULT.replace('?', fleetId)
						.replace('?', unitId).replace('?', faultCode).replace(
								'?', timestamp);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);

			},

			getEventsByGroup : function(fleetId, groupId, callback) {
				var service = this.EVENTS_BY_GROUP.replace('?', fleetId)
						.replace('?', groupId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			createEventGroup : function(fleetId, events, leadEventId, username,
					callback, errorCallback) {
				var service = this.EVENT_CREATE_GROUP.replace('?', fleetId)
						.replace('?', username).replace('?', leadEventId);

				var params = {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					traditional : true,
					data : JSON.stringify(events),
					success : callback,
					error : errorCallback
				};

				$.ajax(service, params);
			},

			updateEventGroup : function(fleetId, events, leadEventId, username,
					callback, errorCallback) {
				var service = this.EVENT_UPDATE_GROUP.replace('?', fleetId)
						.replace('?', username).replace('?', leadEventId);

				var params = {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					traditional : true,
					data : JSON.stringify(events),
					success : callback,
					error : errorCallback
				};

				$.ajax(service, params);
			},
			
			/**
			 * Finds a unit with a descriptor, i.e. headcode, unit number, etc.
			 * matching ^.*<substr>.*$
			 * 
			 * @param substr
			 *            The string to match
			 * 
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes, the first function parameter will be the
			 *            ajax response.
			 */
			find : function(substr, callback) {
				var service = this.EVENT_FIND_UNIT.replace(/\?$/, substr);

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			}

		},

		/**
		 * A collection of map services
		 */
		map : {
			MAP_SERVICE_CONF : 'rest/map/conf',
			MAP_SERVICE_LICENCED_CONF : 'rest/map/licencedConf',
			MAP_SERVICE_ALL : 'rest/map/data',
			MAP_SERVICE : 'rest/map/data/?/?',
			MAP_MARKERS : 'rest/map/markers/?/?/?/?/?/?/?',

			/**
			 * Returns the map configuration.
			 * 
			 */
			conf : function(callback) {
				var service = this.MAP_SERVICE_CONF;

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns the map configuration for licenced objects (stations,
			 * routes and named views).
			 * 
			 */
			licencedConf : function(callback) {
				var service = this.MAP_SERVICE_LICENCED_CONF;

				var params = {
					cache : true,
					success : callback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns all formations data.
			 * 
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			// XXX: Is it used in any project?
			all : function(callback, onCompleteCallback) {
				var service = this.MAP_SERVICE_ALL;

				var params = {
					cache : false,
					success : callback,
				    complete : onCompleteCallback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns all formations data at a timestamp.
			 * 
			 * @param timestamp
			 *            Timestamp for which data has been requested
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			allByTime : function(timestamp, callback, onCompleteCallback) {
				var service = this.MAP_SERVICE_ALL;

				if (!!timestamp) {
					service += '/' + timestamp;
				}

				var params = {
					cache : false,
					success : callback,
				    complete : onCompleteCallback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns a single unit data.
			 * 
			 * @param fleetId
			 *            The fleet id to get data
			 * @param unitId
			 *            The unit id to get data
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			unitLive : function(fleetId, unitId, callback) {
				var service = this.MAP_SERVICE.replace('?', fleetId).replace(
						'?', unitId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns a single unit data at a timestamp.
			 * 
			 * @param fleetId
			 *            The fleet id to get data
			 * @param unitId
			 *            The unit id to get data
			 * @param timestamp
			 *            Timestamp for which data has been requested
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			unit : function(fleetId, unitId, timestamp, callback) {
				var service = this.MAP_SERVICE.replace('?', fleetId).replace(
						'?', unitId);

				if (timestamp != null) {
					service += '/' + timestamp;
				}

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			/**
			 * Returns markers for a certain location, time and marker provider.
			 * 
			 * @param markerType
			 *            The type of marker, used to find the marker provider
			 * @param latNW
			 *            The latitude of the north west corner
			 * @param longNW
			 *            The longitude of the north west corner
			 * @param latSE
			 *            The latitude of the south east corner
			 * @param longSE
			 *            The longitude of the south east corner
			 * @param zoom
			 *            Level of zoom
			 * @param data
			 *            additionnal data retrieved for the screen, could be
			 *            null
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			markers : function(markerType, latNW, longNW, latSE, longSE, zoom,
					data, callback) {
				var service = this.MAP_MARKERS.replace('?', markerType)
						.replace('?', latNW).replace('?', longNW).replace('?',
								latSE).replace('?', longSE).replace('?', zoom)
						.replace('?', data);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			}
		},

		/**
		 * Services for location
		 */
		location : {

			LOCATION_BY_CODE : 'rest/location/byCode/?/?',
			LOCATION_BY_VEHICLE_ID : 'rest/location/vehicle/?/?',

			byCode : function(fleetId, code, callback) {
				var service = this.LOCATION_BY_CODE.replace("?", fleetId)
						.replace("?", code);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},
			byVehicle : function(fleetId, vehicleId, callback) {
				var service = this.LOCATION_BY_VEHICLE_ID.replace("?", fleetId)
						.replace("?", vehicleId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			}
		},

		/**
		 * A collection of download services
		 */
		download : {
			DOWNLOAD_SERVICE_CONFIG : 'rest/download/downloadConfig',
			
			downloadConfig : function(callback) {
				var service = this.DOWNLOAD_SERVICE_CONFIG;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			search : function(callback, startDate, endDate, fleetId, unit,
					vehicle, fileType) {

				var dateSplit = startDate.split("/");
				var newStartDate = dateSplit[1] + "/" + dateSplit[0] + "/"
						+ dateSplit[2];

				var dateSplit2 = endDate.split("/");
				var newEndDate = dateSplit2[1] + "/" + dateSplit2[0] + "/"
						+ dateSplit2[2];

				// FIXME
				var fromDate = new Date(newStartDate + "/00:00:00").getTime();
				var toDate = new Date(newEndDate + "/23:59:59").getTime();

				var service = DOWNLOAD_SERVICE_SEARCH + fromDate + "/" + toDate;
				service = service + "?fleetId=" + fleetId + "&unit=" + unit
						+ "&fileType=" + fileType;

				if (!!vehicle) {
					service = service + "&vehicle=" + vehicle;
				}

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			checkCurrent : function(callback, fleetId, vehicle, fileType) {

				var service = DOWNLOAD_SERVICE_CHECK;
				service = service + "?fleetId=" + fleetId + "&vehicle="
						+ vehicle + "&fileType=" + fileType;

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			retrieve : function(fleetId, path, callback) {
				var service = DOWNLOAD_SERVICE_RETRIEVE;
				service = service + "?fleetId=" + fleetId + "&path=" + path;

				var params = {
						cache : false,
						success : callback
				};
				
				$.ajax(service, params);
			},

			insert : function(callback, fleetId, vehicle, fileType, startDate, startTime, endTime, requestTypeId, devices) {

				var service = DOWNLOAD_SERVICE_INSERT;
				service = service + "?fleetId=" + fleetId + "&vehicle="
						+ vehicle + "&fileType=" + fileType + "&startDate=" 
						+ startDate + "&startTime=" + startTime+ "&endTime=" 
						+ endTime
						+ "&requestTypeId=" + requestTypeId + "&devices=" + devices

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			}
		},

		system : {
			SYSTEM_USER : 'rest/system/user',
			GLOBAL_REFRESH_RATE : 'rest/system/globalRefreshRate',
			SET_USER_CONFIG : 'rest/system/setUserConfiguration',
			GET_USER_CONFIG : 'rest/system/getUserConfiguration/?',
			SPECTRUM_CONFIG : 'rest/system/spectrumConfiguration',

			user : function(callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.SYSTEM_USER, params);
			},

			globalRefreshRate : function(callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.GLOBAL_REFRESH_RATE, params);
			},

			setUserConfiguration : function(parameters, callback) {
				$.ajax(this.SET_USER_CONFIG, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(parameters),
					cache : false,
					success : callback
				});
			},

			getUserConfiguration : function(variable, callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.GET_USER_CONFIG.replace('?', variable), params);
			},
			
			getSpectrumConfiguration : function(callback) {
				var params = {
						cache : false,
						success : callback
					};

				$.ajax(this.SPECTRUM_CONFIG, params);
			}
		},

		/**
		 * A collection of route map services
		 */
		route : {
			ROUTE_VIEW : 'rest/routemap/routeview/?',
			ALL_ROUTES : 'rest/routemap/allroutes',

			/**
			 * Returns all stations and depots for this route view.
			 * 
			 * @param callback
			 *            A function to be called when the ajax request
			 *            completes
			 */
			view : function(routeId, callback) {
				var service = this.ROUTE_VIEW.replace('?', routeId);

				var params = {
					cache : false,
					async : false,
					success : callback
				};

				$.ajax(service, params);
			},

			all : function(callback) {
				var service = this.ALL_ROUTES;

				var params = {
					cache : false,
					async : false,
					success : callback
				};

				$.ajax(service, params);
			}

		},

		/**
		 * A collection of channelDefinition services
		 */

		channelDefinition : {
			CHANNELDEFINITION_CONF : "rest/channeldefinition/conf",
			CHANNELDEFINITION_SEARCH : "rest/channeldefinition/search",
			CHANNELDEFINITION_GROUPS : "rest/channeldefinition/groups/?",
			CHANNELDEFINITION_STATUS : "rest/channeldefinition/status/?",
			CHANNELDEFINITION_CHANNEL : "rest/channeldefinition/channel/?/?",
			CHANNELDEFINITION_CHANNELS : "rest/channeldefinition/channels/?",
			CHANNELDEFINITION_SAVE : "rest/channeldefinition/save",
			CHANNELDEFINITION_EDIT_LIC : "rest/channeldefinition/getEditLicence",


			conf : function(callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.CHANNELDEFINITION_CONF, params);
			},

			columns : function(callback) {
				var params = {
					cache : false,
					success : callback
				};

				$.ajax(this.CHANNELDEFINITION_COLUMNS, params);
			},

			search : function(parameters, callback) {
				$.ajax(this.CHANNELDEFINITION_SEARCH, {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(parameters),
					cache : false,
					success : callback
				});
			},

			groups : function(fleetId, callback) {
				var service = this.CHANNELDEFINITION_GROUPS.replace("?",
						fleetId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			status : function(fleetId, callback) {
				var service = this.CHANNELDEFINITION_STATUS.replace("?",
						fleetId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			channel : function(fleetId, channelId, callback) {
				var service = this.CHANNELDEFINITION_CHANNEL.replace("?",
						fleetId).replace("?", channelId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			channels : function(fleetId, callback) {
				var service = this.CHANNELDEFINITION_CHANNELS.replace("?",
						fleetId);

				var params = {
					cache : false,
					success : callback
				};

				$.ajax(service, params);
			},

			save : function(par, rules, callback) {

				var params = {
					type : "POST",
					contentType : "application/json; charset=utf-8",
					cache : false,
					traditional : true,
					data : JSON.stringify({
						"parameters" : par,
						"rules" : rules
					}),
					success : callback
				};

				$.ajax(this.CHANNELDEFINITION_SAVE, params);
			},
			
			getEditLicence : function(callback) {
				var service = this.CHANNELDEFINITION_EDIT_LIC;

				$.ajax(service, {
					cache : true,
					success : callback
				});
			},
		},

		logging : {
			LOGGING_FLEET : 'rest/logging/fleet',
			LOGGING_UNIT : 'rest/logging/unit',
			LOGGING_UNIT_DETAIL : 'rest/logging/unit/detail',
			LOGGING_UNIT_CHANNELDATA : 'rest/logging/unit/channeldata',
			LOGGING_UNIT_DATAPLOTS : 'rest/logging/unit/dataplots',
			LOGGING_UNIT_CABVIEW : 'rest/logging/unit/cabview',
			LOGGING_UNIT_EVENT : 'rest/logging/unit/event',
			LOGGING_UNIT_MAP : 'rest/logging/unit/map',
			LOGGING_MAP : 'rest/logging/map',
			LOGGING_EVENT : 'rest/logging/event',
			LOGGING_DOWNLOAD : 'rest/logging/download',
			LOGGING_CHANNEL_DEFINITION : 'rest/logging/channeldefinition',
			LOGGING_EVENT_ANALYSIS : 'rest/logging/eventanalysis',

			fleet : function() {
				$.ajax(this.LOGGING_FLEET, {
					cache : false
				});
			},

			unit : function() {
				$.ajax(this.LOGGING_UNIT, {
					cache : false
				});
			},

			unitDetail : function() {
				$.ajax(this.LOGGING_UNIT_DETAIL, {
					cache : false
				});
			},

			unitChannelData : function() {
				$.ajax(this.LOGGING_UNIT_CHANNELDATA, {
					cache : false
				});
			},

			unitCabView : function() {
				$.ajax(this.LOGGING_UNIT_CABVIEW, {
					cache : false
				});
			},

			unitDataPlots : function() {
				$.ajax(this.LOGGING_UNIT_DATAPLOTS, {
					cache : false
				});
			},

			unitEvent : function() {
				$.ajax(this.LOGGING_UNIT_EVENT, {
					cache : false
				});
			},

			unitMap : function() {
				$.ajax(this.LOGGING_UNIT_MAP, {
					cache : false
				});
			},

			map : function() {
				$.ajax(this.LOGGING_MAP, {
					cache : false
				});
			},

			event : function() {
				$.ajax(this.LOGGING_EVENT, {
					cache : false
				});
			},

			download : function() {
				$.ajax(this.LOGGING_DOWNLOAD, {
					cache : false
				});
			},

			channelDefinition : function() {
				$.ajax(this.LOGGING_CHANNEL_DEFINITION, {
					cache : false
				});
			},

			eventAnalysis : function() {
				$.ajax(this.LOGGING_EVENT_ANALYSIS, {
					cache : false
				});
			}
		}
	};
}());