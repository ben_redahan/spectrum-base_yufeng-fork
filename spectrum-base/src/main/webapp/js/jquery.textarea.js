/**
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 * @author Paul O'Riordan
 */
(function( $ ) {
    $.fn.textarea = function(settings) {
        var input = $(this);
        
        var heightPx = $(this).height() + 'px';
        
        var settings = $.extend({
            height: 100,
            onchange: function(value) {}
        }, settings);
        
        input.wrap($('<div>').css({
            position: 'relative',
            display: 'inline-table',
            padding: '0px',
            margin: '0px',
            'z-index': 1000
        }));
        
        input.on('click', function() {
            // hide the input box - and replace with a text area.
            var textarea = $('<textarea>').css({
                position: 'absolute',
                padding: input.css('padding'),
                top: '0px',
                left: '0px',
                right: '0px',
                height: heightPx,
                margin: input.css('margin'),
                'line-height': heightPx,
                'z-index': 1001
            });
            
            if (!!input.attr('readonly')) {
                textarea.attr('readonly', 'readonly');
                textarea.addClass('ui-state-disabled');
            }

            textarea.val(input.val());
            
            textarea.on('input propertychange', function() {
                var newVal = $(textarea).val();
                
                if (newVal != $(input).val()) {
                    $(input).val(newVal);
                    $(input).attr('title', newVal);
                    settings.onchange(newVal);
                }
            });
            
            textarea.blur(function() {
                textarea.animate({
                    height: heightPx
                }, 300, function() {
                    input.css('visibility', 'visible')
                    textarea.remove();
                })
            });
            
            input.parent().append(textarea);
            input.css('visibility', 'hidden');
            
            textarea.animate({
                height: settings.height + 'px'
            }, 300);
            
            textarea.focus();
        });
    };
})(jQuery);