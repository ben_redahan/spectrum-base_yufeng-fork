var nx = nx || {};
nx.ea = nx.ea || {};

nx.ea.FilterDialog = (function() {
    function ctor(eventAnalysis, callback) {
        var me = this;

        this.ALL = "all";
        this.EMPTY_VAL = "";
        this.ALL_DISPLAY_TXT_SELECT = $.i18n("Any");
        
        /*
         * If new SELECT filter is added, no js changes needed.
         * If new TEXT filter is added, autocomplete needs to be implemented,
         * otherwise if no autocomplete required, no js changes needed.
         * Fleet and Date selectors are mandatory filters and shouldn't be removed.
         */
        
        /*Filter names needed for Unit and Vehicle autocomplete*/
        this.FLEET = "fleetId";
        this.UNIT = "unitId";
        this.VEHICLE_TYPE = "vehicleType";
        this.VEHICLE = "vehicleId";
        
        /*Filter names needed for Fault Meta autocomplete*/
        this.FAULT_GROUP = "faultGroupId";
        this.EVENT_TYPE = "eventTypeId"; // User defined or Train events. This is not equal to Fault Type
        this.FAULT_TYPE = "faultTypeId";
        this.FAULT_CATEGORY = "faultCategoryId";
        this.FAULT_META = "faultMetaId";
        this.FAULT_CODE = "faultCode";
        this.FAULT_META_KEYWORD = "faultMetaKeyword";
        
        /*Filter names needed for Location autocomplete*/
        this.LOCATION = "locationId";

        this.DATE_FROM = "dateFrom";
        this.DATE_TO = "dateTo";

        this.__eventAnalysis = eventAnalysis;

        this.__dateRange = null;
        this.__filtersPerRow = null;
        
        this.__unitMap = {};
        this.__vehicleMap = {};
        this.__textFiltersValueMap = {};
        this.__filterLabelsMap = {};
        
        this.__textFilters = {}; //input text that requires a lookup/autocomplete
        this.__simpleTextFilters = {}; //input text that doesn't requite a lookup/autocomplete
        this.__selectFilters = {};
        this.__multiSelectFilters = {};
        this.__visibleFilters = [];
        this.__nonVisibleFilters = {};
        this.__filterOptions = {};
        
        this.__eaFilterDialog = $("#eaFilterDialog");

        var endDate = new Date();
        var startDate = new Date(endDate);
        startDate.setDate(endDate.getDate() - 1);
        this.__dateFrom = this.__newDateFilter(this.DATE_FROM, $.i18n("From"), startDate, "00:00:00");
        this.__visibleFilters.push(this.__dateFrom);
        this.__dateTo = this.__newDateFilter(this.DATE_TO, $.i18n("To"), endDate, "23:59:59");
        this.__visibleFilters.push(this.__dateTo);
        
        var searchLabel = $.i18n("Period")+": " + $.format('{date : DD/MM/YYYY}', startDate) + " - "
            + $.format('{date : DD/MM/YYYY}', endDate) + " / "+$.i18n("Fleet")+": "+$.i18n("Any");      
        this.__eventAnalysis.__eaFilterLabel.text(searchLabel);
        this.__eventAnalysis.__eaFilterLabel.attr("title", searchLabel);
        
        nx.rest.eventanalysis.getFiltersPerRow(function(filtersPerRow){
                me.__filtersPerRow = filtersPerRow
        });
        
        nx.rest.eventanalysis.getFilters(function(filters){
            $.each(filters, function (index, filter) {
                var filterField = null;
                if (filter.visible && filter.type == 'input_text') {
                    filterField = me.__newTextFilter(filter);
                    me.__textFilters[filter.name] = filterField.find("input");
                    me.__textFiltersValueMap[filter.name] = {};
                    me.__filterLabelsMap[filter.name] = $.i18n(filter.label);
                    me.__visibleFilters.push(filterField);
                } else if (filter.visible && filter.type == 'simple_text') {
                    filterField = me.__newTextFilter(filter);
                    me.__simpleTextFilters[filter.name] = filterField.find("input");
                    me.__filterLabelsMap[filter.name] = $.i18n(filter.label);
                    me.__visibleFilters.push(filterField);
                } else if (filter.visible && filter.type == 'select') {
                    filterField = me.__newSelectFilter(filter);
                    me.__selectFilters[filter.name] = filterField.find("select");
                    me.__filterLabelsMap[filter.name] = $.i18n(filter.label);
                    me.__visibleFilters.push(filterField);
                } else if (filter.visible && filter.type == 'multiple_select') {
                	filterField = me.__newMultipleSelectFilter(filter);
                	me.__multiSelectFilters[filter.name] = filterField.find("select");
                	me.__filterLabelsMap[filter.name] = $.i18n(filter.label);
                	me.__visibleFilters.push(filterField);
                } else if (!filter.visible) {
                	me.__nonVisibleFilters[filter.name] = true;
                }
            });
            
            
            
	            var filterLine = $('<div class="eaFilterLine"/>'); 
	            i = 0;
	            $.each(me.__visibleFilters, function (index, filter) {
	            	filterLine.append(filter);
	            	i ++;
	            	if (i  >= me.__filtersPerRow) {
	            		var newLine = filterLine;
	            		me.__eaFilterDialog.append(newLine);
	            		filterLine = $('<div class="eaFilterLine"/>');
	            		i = 0;
	            	}
	            });
            
            
	            if (i != 0) {
	            	me.__eaFilterDialog.append(filterLine);
	            }
            

            me.__eaFilterFleetCombo = $('#eaFilter' + me.FLEET + 'Combo');

            var filterOptionsDfd = $.Deferred();
            var filterOptionsLoaded = filterOptionsDfd.promise();
            nx.rest.eventanalysis.getFilterOptions(function(filterOptions) {
                me.__filterOptions = filterOptions;
                filterOptionsDfd.resolve();
            });

            $.when(filterOptionsLoaded).done(function() {
                nx.rest.fleet.getFleetList(function(fleets) {
                    me.__initialize();
                    me.__loadFleetCombo(fleets);
                    if (!!callback) {
                    	callback();
                    }
                });
            });
        });
    };    
      
    ctor.prototype.__newDateFilter = function(name, title, defaultDate, defaultTime) {
        var elem = $('<div class="eaFilterField"/>');
        var label = $('<div class="eaFilterLabel">' + title + '</div>').appendTo(elem);
        var input = $('<div id="eaFilter' + name + '" class="timePicker" style="display:inline-block">');
        $('<div class="inputDiv">').appendTo(input);
        input.timepicker();
        var date = $.format('{date : YYYY/MM/DD}', defaultDate);
        input.data('obj').setTime(new Date(date + " " + defaultTime));
        input.appendTo(elem);
        return elem;
    };
    
    ctor.prototype.__newTextFilter = function(filter) {
        var me = this;
        var elem = me.__newFilterElement(filter);
        var input = $('<input id="eaFilter' + filter.name + 'Text" type="text" class="textInput">').appendTo(elem);
        return elem;
    };
        
    ctor.prototype.__newSelectFilter = function(filter) {
        var me = this;
        var elem = me.__newFilterElement(filter);
        var select = $('<select id="eaFilter' + filter.name + 'Combo"/>').appendTo(elem);
        select.uniform();
        return elem;
    };
    
    ctor.prototype.__newMultipleSelectFilter = function(filter) {
    	var me = this;
    	var elem = me.__newFilterElement(filter);
    	var multiSelect = $('<select multiple id="eaFilter' + filter.name + 'Multi" class="multiSelector"/>').appendTo(elem);
    	multiSelect.uniform();
    	return elem;
    }
    
    ctor.prototype.__newFilterElement = function(filter) {
        var me = this;
        var elem = $('<div class="eaFilterField"/>');
        var label = $('<div class="eaFilterLabel">' + $.i18n(filter.label) + '</div>').appendTo(elem);
        elem.uniform();
        return elem;
    };

    ctor.prototype.__initialize = function() {
        var me = this;

        //fleet combo
        this.__eaFilterFleetCombo.change(function() {
            var fleetId = $(this).val();
            me.__resetFilters(fleetId);
        });

        //unit input field
        if (!!this.__textFilters[this.UNIT]) {
            this.__textFilters[this.UNIT].autocomplete({
                'minLength': 2,
                'source': function(request, response) {
                    var fleetId = me.__eaFilterFleetCombo.val();

                    var unitNumber = me.__textFilters[me.UNIT].val();

                    nx.rest.eventanalysis.searchUnit(fleetId, unitNumber, function(unitList) {
                    	me.__unitMap = {};
                        response($.map(unitList, function(unit) {
                        	if (!me.__unitMap[unit.number] && (fleetId === unit.fleetCode || fleetId === me.ALL)) {
                        		me.__unitMap[unit.number] = unit.fleetCode + "-" + unit.id;

                        		return {
	                                'label': unit.number,
	                                'value': unit.fleetCode + "-" + unit.id
	                            };
                        	}
                            
                        }));
                    });
                },
                'focus': function(event, ui) {
                    me.__textFilters[me.UNIT].val(ui.item.label);
                    return false;
                },
                'select': function(event, ui) {
                    me.__textFilters[me.UNIT].val(ui.item.label);
                    return false;
                }
            });
        }

        //vehicle input field
        if (!!this.__textFilters[this.VEHICLE]) {
            this.__textFilters[this.VEHICLE].autocomplete({
                'minLength': 2,
                'source': function(request, response) {
                    var fleetId = me.__eaFilterFleetCombo.val();
                    
                    var vehicleType = me.EMPTY_VAL;
                    if (!!me.__selectFilters[me.VEHICLE_TYPE]) {
                        vehicleType = me.__selectFilters[me.VEHICLE_TYPE].val();
                    } else if (!!me.__multiSelectFilters[me.VEHICLE_TYPE]) {
                    	var selectedTypes = me.__multiSelectFilters[me.VEHICLE_TYPE].val();
                    	if (!!selecetdTypes) {
                            $.each(selectedTypes, function (index, type) {
                            	if (index > 0) {
                            		vehicleType += ",";
                            	}
                            	vehicleType += type;
                            });
                    	}
                    }
                    
                    var unitNumber = '';
                    var unitId = me.EMPTY_VAL;
                    if (!!me.__textFilters[me.UNIT]) {
                        unitNumber = me.__textFilters[me.UNIT].val();
                        unitId = me.__unitMap[unitNumber];
                    } else if (!!me.__multiSelectFilters[me.UNIT]) {
                    	var selectedUnits = me.__multiSelectFilters[me.UNIT].val();
                    	if (!!selectedUnits) {
                            $.each(selectedUnits, function (index, unit) {
                            	if (index > 0) {
                            		unitId += ",";
                            	}
                            	unitId += unit;
                            });
                    	}
                    }
                    
                    var vehicleNumber = me.__textFilters[me.VEHICLE].val();

                    // gives all fleets with this unit number!
                    nx.rest.eventanalysis.searchVehicle(fleetId, vehicleType, unitId, vehicleNumber, function(vehicleList) {
                        me.__vehicleMap = {};
                        response($.map(vehicleList, function(vehicle) {
                        	if (!me.__vehicleMap[vehicle.number] && (fleetId === vehicle.fleetCode || fleetId === me.ALL)) {
                        		me.__vehicleMap[vehicle.number] = vehicle.fleetCode + "-" + vehicle.id;

	                            return {
	                                'label': vehicle.number,
	                                'value': vehicle.fleetCode + "-" + vehicle.id
	                            };
                        	}
                        }));
                    });
                },
                'focus': function(event, ui) {
                    me.__textFilters[me.VEHICLE].val(ui.item.label);
                    return false;
                },
                'select': function(event, ui) {
                    me.__textFilters[me.VEHICLE].val(ui.item.label);
                    return false;
                }
            });
        }
        //fault meta input field
        if (!!this.__textFilters[this.FAULT_META] || !!this.__textFilters[this.FAULT_CODE]) {
            var filterId = !!this.__textFilters[this.FAULT_META] ? this.FAULT_META : this.FAULT_CODE;
            this.__textFilters[filterId].autocomplete({
                'minLength': 2,
                'source': function(request, response) {
                	var faultMetaFilters = {};
                		
                	var faultGroup = me.EMPTY_VAL;
                    if (!!me.__selectFilters[me.FAULT_GROUP]) {
                    	faultGroup = me.__selectFilters[me.FAULT_GROUP].val();
                    	faultMetaFilters[me.FAULT_GROUP] = faultGroup;
                    } else if (!!me.__multiSelectFilters[me.FAULT_GROUP]) {
                    	var selectedGroups = me.__multiSelectFilters[me.FAULT_GROUP].val();
                    	if (!!selectedGroups) {
                            $.each(selectedGroups, function (index, group) {
                            	if (index > 0) {
                            		faultGroup += ",";
                            	}
                            	faultGroup += group;
                            });
                    	}
                    	faultMetaFilters[me.FAULT_GROUP] = faultGroup;
                    }
                    
                    var eventType = me.EMPTY_VAL;
                    if (!!me.__selectFilters[me.EVENT_TYPE]) {
                    	eventType = me.__selectFilters[me.EVENT_TYPE].val();
                    	faultMetaFilters[me.EVENT_TYPE] = eventType;
                    } else if (!!me.__multiSelectFilters[me.EVENT_TYPE]) {
                    	var selectedTypes = me.__multiSelectFilters[me.EVENT_TYPE].val();
                    	if (!!selectedTypes) {
                            $.each(selectedTypes, function (index, type) {
                            	if (index > 0) {
                            		eventType += ",";
                            	}
                            	eventType += type;
                            });
                    	}
                    	faultMetaFilters[me.EVENT_TYPE] = eventType;
                    }
                    
                    var faultType = me.EMPTY_VAL;
                    if (!!me.__selectFilters[me.FAULT_TYPE]) {
                    	faultType = me.__selectFilters[me.FAULT_TYPE].val();
                    	faultMetaFilters[me.FAULT_TYPE] = faultType;
                    } else if (!!me.__multiSelectFilters[me.FAULT_TYPE]) {
                    	var selectedTypes = me.__multiSelectFilters[me.FAULT_TYPE].val();
                    	if (!!selectedTypes) {
                            $.each(selectedTypes, function (index, type) {
                            	if (index > 0) {
                            		faultType += ",";
                            	}
                            	faultType += $.i18n(type);
                            });
                    	}
                    	faultMetaFilters[me.FAULT_TYPE] = faultType;
                    }
                    
                    var faultCategory = me.EMPTY_VAL;
                    if (!!me.__selectFilters[me.FAULT_CATEGORY]) {
                    	faultCategory = me.__selectFilters[me.FAULT_CATEGORY].val();
                    	faultMetaFilters[me.FAULT_CATEGORY] = faultCategory;
                    } else if (!!me.__multiSelectFilters[me.FAULT_CATEGORY]) {
                    	var selectedCategories = me.__multiSelectFilters[me.FAULT_CATEGORY].val();
                    	if (!!selectedCategories) {
                            $.each(selectedCategories, function (index, category) {
                            	if (index > 0) {
                            		faultCategory += ",";
                            	}
                            	faultCategory += category;
                            });
                    	}
                    	faultMetaFilters[me.FAULT_CATEGORY] = faultCategory;
                    }                    
                    if (!!me.__selectFilters[me.FLEET]) {
                    	var fleetId = me.__selectFilters[me.FLEET].val();
                    	faultMetaFilters[me.FLEET] = !!fleetId ? fleetId : null;
                    }

                    var faultMetaKeyword = me.__textFilters[filterId].val();
                    faultMetaFilters[me.FAULT_META_KEYWORD] = faultMetaKeyword;

                    nx.rest.eventanalysis.searchFaultMeta(faultMetaFilters, function(faultMetaList) {
                        me.__textFiltersValueMap[filterId] = {};
                        response($.map(faultMetaList, function(faultMeta) {
                            var label = faultMeta.code + " - " + faultMeta.description;

                            var value = faultMeta.id;
                            if (filterId == me.FAULT_CODE) {
                                value = faultMeta.code;
                            }
                            
                            if (!me.__textFiltersValueMap[filterId][label]) {
                            	me.__textFiltersValueMap[filterId][label] = value;

	                            return {
	                                'label': label,
	                                'value': value
	                            };
                            }
                        }));
                    });
                },
                'focus': function(event, ui) {
                    me.__textFilters[filterId].val(ui.item.label);
                    return false;
                },
                'select': function(event, ui) {
                    me.__textFilters[filterId].val(ui.item.label);
                    return false;
                }
            });
        }

        //location input field
        if (!!this.__textFilters[this.LOCATION]) {
            this.__textFilters[this.LOCATION].autocomplete({
                'minLength': 2,
                'source': function(request, response) {
                    var fleetId = me.__eaFilterFleetCombo.val();
                    var location = me.__textFilters[me.LOCATION].val();

                    nx.rest.eventanalysis.searchLocation(fleetId, location, function(locationList) {
                        response($.map(locationList, function(location) {
                            var label = location.code + " - " + location.name;                        
                            
                            me.__textFiltersValueMap[me.LOCATION][label] = location.id;

	                        return {
	                            'label': label,
	                            'value': location.id
	                        };
                        }));
                    });
                },
                'focus': function(event, ui) {
                    me.__textFilters[me.LOCATION].val(ui.item.label);
                    return false;
                },
                'select': function(event, ui) {
                    me.__textFilters[me.LOCATION].val(ui.item.label);
                    return false;
                }
            });
        }
    };

    ctor.prototype.__loadFleetCombo = function(fleets) {
        var me = this;

        $('<option />', {
            'val': me.ALL,
            'text': me.ALL_DISPLAY_TXT_SELECT
        }).appendTo(me.__eaFilterFleetCombo);

        $.each(fleets, function (id, fleet) {
            $('<option />', {
                'val': fleet.code,
                'text': fleet.name
            }).appendTo(me.__eaFilterFleetCombo);
        });

        this.__eaFilterFleetCombo.val(this.EMPTY_VAL).change();
    };

    ctor.prototype.__resetFilters = function(fleetId) {
        for (var f in this.__textFilters) {
            this.__textFilters[f].val(this.EMPTY_VAL);
        }

        for (var f in this.__selectFilters) {
            if (f != this.FLEET) {
                var combo = this.__selectFilters[f];
                combo.empty();
                this.__loadFilterCombo(combo, f, fleetId, false);
                $.uniform.update(combo);
            }
        }
        
        for (var f in this.__multiSelectFilters) {
        	var combo = this.__multiSelectFilters[f];
        	combo.empty();
        	this.__loadFilterCombo(combo, f, fleetId, true);
        	$.uniform.update(combo);
        }
    };

    ctor.prototype.__loadFilterCombo = function(combo, filterId, fleetId, multi) {
        var me = this;
        var options = me.__filterOptions[filterId];

        if (!multi) {
            $('<option />', {
                'val': me.EMPTY_VAL,
                'text': me.ALL_DISPLAY_TXT_SELECT
            }).appendTo(combo);	
        }

        var displayOptions = {};

        if (me.ALL == fleetId) {
            $.each(options, function(key, value){
                $(value).each(function () {
                    if (!displayOptions[this.id]) {
                        displayOptions[this.id] = this.name;
                    }
                });
                
            });
    
        } else {
            $(options[fleetId]).each(function () {
                    displayOptions[this.id] = this.name;
            });
        }
        
        // Copy options into array to sort alphabetically by name
        var sortedOptions = [];
        $.each(displayOptions, function(key, value){
        	sortedOptions.push({'id':key,'name':value});
        });
        
        sortedOptions.sort(
        		function(a,b){
        			if(a.name > b.name){return 1;}
        			if(a.name < b.name){return -1;}
        			return 0;
        		}
        );

        $.each(sortedOptions, function(i, l){
            var option = $('<option />', {
                'val': l.id,
                'text': $.i18n(l.name)
            })
            if (multi) {
            	option.attr('title', $.i18n(l.name));
            }
            option.appendTo(combo);
        });
    };

    ctor.prototype.getFilters = function() {
        var me = this;
        var filters = {};
        var filterString = "";
        
        var unitId = this.EMPTY_VAL;
        var unitNumber = this.EMPTY_VAL;
        var unitLabel = me.__filterLabelsMap[this.UNIT];
        if (!!this.__textFilters[this.UNIT]) {
            unitNumber = this.__textFilters[this.UNIT].val();
            unitId = this.getTrainNumberValue(this.__unitMap, unitNumber, (this.__eaFilterFleetCombo.val() + "-0"));
            filters[this.UNIT] = !!unitId ? unitId : this.EMPTY_VAL;
            filterString += unitNumber != "" ? " / " + unitLabel + ": " + unitNumber : "";
        } else if (!!this.__multiSelectFilters[this.UNIT]) {
        	var selectedUnits = this.__multiSelectFilters[this.UNIT].val();
        	var selectedOptions = this.__multiSelectFilters[this.UNIT].find('option:selected');
        	if (!!selectedUnits) {
                $.each(selectedUnits, function (index, unit) {
                	if (index > 0) {
                		unitId += ",";
                		unitNumber += ", ";
                	}
                	unitId += unit;
                	unitNumber += selectedOptions[index].title;
                });
        	}
        	filters[this.UNIT] = unitId;
        	filterString += unitNumber != "" ? " / " + unitLabel + ": " + unitNumber : "";
        }

        var vehicleId = this.EMPTY_VAL;
        var vehicleNumber = this.EMPTY_VAL;
        var vehicleLabel = me.__filterLabelsMap[this.VEHICLE];
        if (!!this.__textFilters[this.VEHICLE]) {
            var vehicleNumber = this.__textFilters[this.VEHICLE].val();
            vehicleId = this.getLookupValue(this.__vehicleMap, vehicleNumber, (this.__eaFilterFleetCombo.val() + "-0"));
            filters[this.VEHICLE] = !!vehicleId ? vehicleId : this.EMPTY_VAL;
            filterString += vehicleNumber != "" ? " / " + vehicleLabel + ": " + vehicleNumber : "";
        } else if (!!this.__multiSelectFilters[this.VEHICLE]) {
        	var selectedVehicles = this.__multiSelectFilters[this.VEHICLE].val();
        	var selectedOptions = this.__multiSelectFilters[this.VEHICLE].find('option:selected');
        	if (!!selectedVehicles) {
                $.each(selectedVehicles, function (index, vehicle) {
                	if (index > 0) {
                		vehicleId += ",";
                		vehicleNumber += ", ";
                	}
                	vehicleId += vehicle;
                	vehicleNumber += selectedOptions[index].title;
                });
        	}
        	filters[this.VEHICLE] = vehicleId;
        	filterString += vehicleNumber != "" ? " / " + vehicleLabel + ": " + vehicleNumber : "";
        }

        for (var key in me.__selectFilters) {
        	if (key != this.FLEET) {
        		var selectedId = me.__selectFilters[key].val();
        		var label = me.__filterLabelsMap[key];
        		filters[key] = !!selectedId ? selectedId : this.EMPTY_VAL;
        		filterString += !!selectedId ? " / " + label + ": " + me.__selectFilters[key].find('option:selected').text() : "";
        	}
        }

        for (var key in me.__textFilters) {
        	if (key != this.UNIT && key != this.VEHICLE) {
        		var inputVal = me.__textFilters[key].val();
        		
        		var defaultValue = -2;
        		if (key == this.FAULT_CODE) {
        			defaultValue = inputVal;
        		}
        		var inputId = this.getLookupValue(this.__textFiltersValueMap[key], inputVal, defaultValue);
        		var label = me.__filterLabelsMap[key];
        		filters[key] = !!inputId ? inputId : this.EMPTY_VAL;
        		filterString += inputVal != "" ? " / " + label + ": " + inputVal : "";
        	}
        }
        
        for (var key in me.__simpleTextFilters) {
            if (key != this.UNIT && key != this.VEHICLE) {
                var inputVal = me.__simpleTextFilters[key].val();
                
                var label = me.__filterLabelsMap[key];
                filters[key] = !!inputVal ? inputVal : this.EMPTY_VAL;
                filterString += inputVal != "" ? " / " + label + ": " + inputVal : "";
            }
        }
        
        for (var key in me.__multiSelectFilters) {
        	if (key != this.UNIT && key != this.VEHICLE) {
        		var idList = this.EMPTY_VAL;
        		var itemList = this.EMPTY_VAL;
        		var label = me.__filterLabelsMap[key];
        		var selectedIds = me.__multiSelectFilters[key].val();
        		var selectedItems = me.__multiSelectFilters[key].find('option:selected');
        		if (!!selectedIds) {
                    $.each(selectedIds, function (index, id) {
                    	if (index > 0) {
                    		idList += ",";
                    		itemList += ", ";
                    	}
                    	idList += id;
                    	itemList += $.i18n(selectedItems[index].title);
                    });
        		}
        		filters[key] = idList;
        		filterString += itemList != "" ? " / " + label + ": " + itemList : "";
        	}
        }

        for (var key in me.__nonVisibleFilters) {
        	filters[key] = null;
        }

        var fleetId = this.__eaFilterFleetCombo.val();
        var dateFrom = $("#eaFilter" + this.DATE_FROM).data("obj");
        var dateTo = $("#eaFilter" + this.DATE_TO).data("obj");

        filters[this.FLEET] = fleetId;
        filters[this.DATE_FROM] = dateFrom.getTime();
        filters[this.DATE_TO] = dateTo.getTime();

        var toString = function() {
            return $.i18n('Period')+': ' + $.datepicker.formatDate('dd/mm/yy', dateFrom.__date) + ' - ' +
                    $.datepicker.formatDate('dd/mm/yy', dateTo.__date) + ' / '+$.i18n('Fleet')+': ' +
                    me.__eaFilterFleetCombo.find('option:selected').text() + filterString;
        }
		
		filters['toString'] = toString;

		return filters;
    };

    ctor.prototype._open = function() {
        var eaExpandButtonTopMargin = 40;
    
        var eaReportContainerTopMargin = 60;
        var eaFilterDialogRowMargin = 95;
        
        eaReportContainerTopMargin = eaReportContainerTopMargin + (eaFilterDialogRowMargin * $(eaFilterDialog).children().length);
        eaExpandButtonTopMargin = eaExpandButtonTopMargin + (eaFilterDialogRowMargin * $(eaFilterDialog).children().length);
    
    	$("#eaReportContainer").attr("style", "top: " + eaReportContainerTopMargin + "px");
    	this.__eventAnalysis.__eaExpandButton.attr("style", "top: " + eaExpandButtonTopMargin + "px; right: 25px");
    	this.__eventAnalysis.__selectGrouping(this.__eventAnalysis.__selectedGrouping);
    	this.__eaFilterDialog.show();
    };
    
    ctor.prototype._close = function() {
    	this.__eaFilterDialog.hide();
    	this.__eventAnalysis.__eaExpandButton.attr("style", "top: 35px; right: 0px");
    	$("#eaReportContainer").attr("style", "top: 55px");
    	this.__eventAnalysis.__selectGrouping(this.__eventAnalysis.__selectedGrouping);
    };
    
    ctor.prototype.getLookupValue = function(map, text, defaultValue) {
    	var value = map[text];
    	if (!value && !!text) {
    		return defaultValue;
    	}
    	return value;
    }

    ctor.prototype.getTrainNumberValue = function(map, text, defaultValue) {
    	if (text) {
            var unitNumbers = "";
            for (var key in map) {
                if (matchWildcard(key, text)) {
                    unitNumbers += map[key] + ",";
                }
            }
            unitNumbers = unitNumbers.substring(0, unitNumbers.length - 1);
            if (unitNumbers) {
                return unitNumbers;
            } else {
                return defaultValue;
            }
        } else {
            return null;
        }    	
    }
    
    function matchWildcard(str, rule) {
        return new RegExp("^" + rule.split("*").join(".*") + "$").test(str);
    }

    return ctor;
})();
