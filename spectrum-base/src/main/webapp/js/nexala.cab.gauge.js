var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Gauge = (function () {
    var ctor = function (el, component,  settings) {
        nx.cab.Widget.call(this);
        
        
        this._component = component;
        this._xOffset = component.x;
        this._yOffset = component.y;
        /** The gauge value, i.e. the number that it's pointing to */
        this._values = {};
        this._pointers = {};

        this.defaultDial = {
            angleStart: -210,
            angleEnd: 30,
            arcStrokeWidth: 3,
            arcStrokeFill: '#000',
            /* The animation type for the pointer, other options include
              * "bounce", "elastic", etc.
             */
            easing: "backOut",

            /** Allows a dial (arc + ticks) to be hidden, this is useful when
             *  we want the appearance of two pointers on a single dial
             */
            dial: true,
            labelRadius: 0.72,
            labelFill: '#CCC',
            labelIncrement: 10,
            displayLabel: true,
            majorStrokeLength: 20,
            majorStrokeWidth: 3,
            majorStrokeFill: '#000',
            majorStrokeLabels: true,
            majorIncrement: 10,
            minorStrokeLength: 10,
            minorStrokeWidth: 1,
            minorStrokeFill: '#000',
            minorStrokeLabels: false,
            minorIncrement: 2,
            pivotFill: '#444',
            pointer: true,
            pointerFill: 'orange',
            pointerRadius: 0.8,
            pointerStroke: 'darkorange',
            radius: 0.88,
            animationTimeMs: 1000,
            displayValue: true,
            displayValuePosition: 90,
            displayValueRadius: 0.55,
            displayValueFontSize: 15,
            displayValueSuffix: '',
            displayValueFontColor: '#eee',
            displayValueFontWeight: 'bold'
        };


        var halfGaugeBottomSpacer = (!!settings.halfGaugeBottomSpacer)? settings.halfGaugeBottomSpacer : 0;
        
        this.settings = $.extend({
            borderColor: '#888888',
            borderWidth: 0.004,
            cx: component.width /2,
            cy: settings.halfGauge ? component.height -8-halfGaugeBottomSpacer : component.height /2 ,
            dials: [],
            faceFill: '90-#333-#222',
            faceRadius: 0.91,
            faceRenderer: null,
            faceShadowFill: '90-#222-#333-#444',
            faceShadowStroke: '#333',
            fontSize: 12,
            halfGauge : 'false',
            labels: [],
            pivotRadius: 0.076,
            radius: settings.halfGauge ? Math.min(component.width/2, component.height-5-halfGaugeBottomSpacer)  - 3 : Math.min(component.width, component.height) / 2 - 3,
            rimFill: '#555555',
            rimWidth: 0.06,
            rimHlFill: '90-#999-#333-#999-#333-#333-#888',
            slices: []
        }, settings);

        // Add the offset for cx and cy
        this.settings.cx += component.x;
        this.settings.cy += component.y;
        
        /**
         * specific values for half gauge
         
        if (settings.halfGauge == true) {
            this.settings = $.extend({
                cy:  /*,
                radius: 
            }, settings);
        }*/
        

        this._paper = el;
        this._draw();
    };

    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, value, channelCategory) {
        var pointer = this._pointers[channelName];
        
        if (!!pointer) {
            if (arguments.length == 2) {
                return this._values[pointer];
            } else if (arguments.length == 3) {
                this._setPointerValue(pointer, arguments[1]);
                return arguments[1];
            }
        }
        
        return null;
    };
    
    ctor.prototype._reset = function() {
        for (var key in this._pointers) {
            var pointer = this._pointers[key];
            this._setPointerValue(pointer, 0);
        }
    };


    ctor.prototype.paper = function() {
        return this._paper;
    };

    ctor.prototype._draw = function() {
        var cx = this.settings.cx;
        var cy = this.settings.cy;
        var r = this.settings.radius;
        var faceRadius = r * this.settings.faceRadius;
        var rimWidth = r * this.settings.rimWidth;
        var borderColor = this.settings.borderColor;
        var borderWidth = r * this.settings.borderWidth;
        var outerGaugeBorder = this._paper.path(this.getCircletoPath(cx, cy, r));
        var i = 0;
        var l = 0;

        outerGaugeBorder.attr({
            fill: borderColor,
            stroke: null,
            height: this.settings.radius
        });

        var rim = this._paper.path(this.getCircletoPath(cx, cy, r - borderWidth));

        rim.attr({
            fill: this.settings.rimFill,
            height: this.settings.radius
        });

        var rimHl = this._paper.path(this.getCircletoPath(
            cx,
            cy - 3,
            r - borderWidth - rimWidth * 0.8
        ));

        rimHl.attr({
            fill: this.settings.rimHlFill,
            stroke: null,
            height: this.settings.radius
        });
    
        var faceShadow = this._paper.path(this.getCircletoPath(
            cx,
            cy,
            r - rimWidth - borderWidth));

        faceShadow.attr({
            fill: this.settings.faceShadowFill,
            stroke: this.settings.faceShadowStroke,
            height: this.settings.radius
        });  
    
        var face = this._paper.path(this.getCircletoPath(
            cx,
            cy,
            faceRadius));

        face.attr({
            fill: this.settings.faceFill,
            stroke: null,
            height: this.settings.radius
        });

        if (this.settings.faceRenderer !== null) {
            this.settings.faceRenderer(this._paper, cx, cy, r);
        }
        
        for (i = 0, l = this.settings.slices.length; i < l; i++) {
            var slice = this.settings.slices[i];
            
            this._slice(slice.startAngle, slice.endAngle, slice.radius, {
                stroke: null,
                fill: slice.fill
            });
        }
        
        var me = this;
        
        for (i = 0, l = this.settings.labels.length; i < l; i++) {
            this._drawLabel(this.settings.labels[i]);
        }

        for (i = 0, l = this.settings.dials.length; i < l; i++) {
            var fun = this._createFunction(i, this.settings.dials[i]);
            fun();
        }

        for (i in this._pointers) {
            var pointerObj = this._pointers[i];
            var pointer = pointerObj.pointer;
            pointer.toFront();
        }
        
        // Pointer shadow
        var pivotShadow = this._paper.circle(cx, cy, r * this.settings.pivotRadius);

        if (this.settings.pivotRadius > 0.018) {
            var pivot = this._paper.circle(
                    cx,
                    cy,
                    r * (this.settings.pivotRadius-0.018)    
                );
            
            pivot.attr({
                fill: '#444',
                stroke: null
            });
        }
        
        
        pivotShadow.attr({
            fill: 'r#222-#222-#222-#222-#333', // XXX conf
            stroke: null
        });

        
    };
    
    /**
     * Return an equivalent of a circle using path.
     * Render an half gauge if the parameter halfGauge is true
     */
    ctor.prototype.getCircletoPath = function(x , y, r)  {

    var s = "M";
      
    if (this.settings.halfGauge == true) {
       s = s + "" + (x-r) + "," + (y) +"A"+r+","+r+",0,1,1,"+
        (x+r)+","+(y)+"z";
    } else {
        s = s + "" + (x) + "," + (y-r) + "A"+r+","+r+",0,1,1,"+
        (x-0.1)+","+(y-r)+"z";
    }
      
    return s;

   };
    

    ctor.prototype._createFunction = function(i, dial) {
        var me = this;
    
        return function() {
            var extDial = {};
            $.extend(extDial, me.defaultDial, dial);
            me.settings.dials[i] = extDial;
            me._drawDial(extDial);
        };
    };

    ctor.prototype._getCircleCoordinate = function(radius, position) {
        var cx = this.settings.cx;
        var cy = this.settings.cy;
        var cr = this.settings.radius;

        return {
            x: cx + (cr * radius) * Math.cos(Raphael.rad(position)),
            y: cy + (cr * radius) * Math.sin(Raphael.rad(position))
        };
    };

    ctor.prototype._drawLabel = function(label) {
        var conf = $.extend({
            radius: 0.70,
            position: 90,
            fontSize: 15,
            fontWeight: 'bold',
            color: '#ddd',
            anchor: 'middle'
        }, label);

        var pos = this._getCircleCoordinate(conf.radius, conf.position);


        this._paper.text(pos.x, pos.y, conf.text).attr({
            'font-size': conf.fontSize,
            'font-weight': conf.fontWeight,
            'fill': conf.color,
            'text-anchor': conf.anchor
        });
    };

    ctor.prototype._drawDial = function(dialSettings) {
        var cx = this.settings.cx,
            cy = this.settings.cy,
            r = this.settings.radius,
            angleStart = dialSettings.angleStart,
            angleEnd = dialSettings.angleEnd;
        
       if (angleEnd < angleStart) {
           angleStart = dialSettings.angleEnd;
           angleEnd = dialSettings.angleStart;
      }
        var dialRadius = dialSettings.radius * r,
            x1 = cx + dialRadius * Math.cos(Raphael.rad(angleStart)),
            x2 = cx + dialRadius * Math.cos(Raphael.rad(angleEnd)),
            y1 = cy + dialRadius * Math.sin(Raphael.rad(angleStart)),
            y2 = cy + dialRadius * Math.sin(Raphael.rad(angleEnd)),
            largeArc = angleEnd - angleStart > 180 ? 1 : 0;

        if (dialSettings.dial) {
            // Draw the arc..
            this._paper.path(["M", x1, y1, "A", dialRadius, dialRadius, 0,
                    largeArc, 1, x2, y2])
                .attr({
                'stroke-width': dialSettings.arcStrokeWidth,    
                'stroke': dialSettings.arcStrokeFill
            });

            this._drawDialMajorStrokes(dialSettings);
            this._drawDialMinorStrokes(dialSettings);
        }
    
        if (dialSettings.pointer === true) {
            if (dialSettings.pointerType == "DEFAULT") {
                var pointer = this._paper.path([
                    "M", cx - r * 0.03, cy - (r * 0.125),
                    "L", cx -0.7, cy + (r * dialSettings.pointerRadius),
                    "L", cx +0.7, cy + (r *dialSettings.pointerRadius),
                    "L", cx + r * 0.03, cy - (r * 0.125),
                    "A", 10, 10, cx + 10, cy - 60,
                    "Z"
                ].join(","));
            } else if (dialSettings.pointerType == "INTERNAL") {
                var pointer = this._paper.path([
                    "M", cx, cy + (r * 0.56),
                    "L", cx - (r * 0.07), cy + (r * 0.51),
                    "L", cx - (r * 0.07), cy + (r * 0.30),
                    "L", cx + (r * 0.07), cy + (r * 0.30),
                    "L", cx + (r * 0.07), cy + (r * 0.51),
                    "L", cx, cy + (r * 0.56),
                    "A", 10, 10, cx + 10, cy - 60,
                    "Z"
                ].join(",")); 
            } else {
                var pointer = this._paper.path([
                    "M", cx, cy + (r * 0.72),
                    "L", cx - (r * 0.07), cy + (r * 0.80),
                    "L", cx - (r * 0.07), cy + (r * 0.98),
                    "L", cx + (r * 0.07), cy + (r * 0.98),
                    "L", cx + (r * 0.07), cy + (r * 0.80),
                    "L", cx, cy + (r * 0.72),
                    "A", 10, 10, cx + 10, cy - 60,
                    "Z"
                ].join(","));    
            }
        	
            pointer.attr({
                stroke: dialSettings.pointerStroke,
                fill: dialSettings.pointerFill
            });

            var pointerObj = {};
            pointerObj.pointer = pointer;
            pointerObj.dialSettings = dialSettings;
    
            if (dialSettings.displayValue) {
                var pos = this._getCircleCoordinate(
                dialSettings.displayValueRadius,
                dialSettings.displayValuePosition);

                pointerObj.displayValue = this._paper.text(pos.x, pos.y, '0'+dialSettings.displayValueSuffix).attr({
                    'font-size': dialSettings.displayValueFontSize,
                    'font-weight': dialSettings.displayValueFontWeight,
                    'fill': dialSettings.displayValueFontColor
                });
            }

            this._pointers[dialSettings.id] = pointerObj;
            this._setPointerValue(pointerObj, 0);
        }
    };

    ctor.prototype._drawDialMajorStrokes = function(dialSettings) {
        this._drawDialStrokes(dialSettings, {
            increment: dialSettings.majorIncrement,
            labels: dialSettings.labelIncrement,
            length: dialSettings.majorStrokeLength,
            width: dialSettings.majorStrokeWidth,
            stroke: dialSettings.majorStrokeFill
        }, false);
    };

    ctor.prototype._drawDialMinorStrokes = function(dialSettings) {
        this._drawDialStrokes(dialSettings, {
            increment: dialSettings.minorIncrement,
            labels: dialSettings.labelIncrement,
            length: dialSettings.minorStrokeLength,
            width: dialSettings.minorStrokeWidth,
            stroke: dialSettings.minorStrokeFill
        }, true);
    };

    ctor.prototype._drawDialStrokes = function(dialSettings, strokeSettings, drawLabel) {
        var cx = this.settings.cx,
            cy = this.settings.cy,
            r = this.settings.radius,
            dr = r * dialSettings.radius,
            lr = r * dialSettings.labelRadius,
            sv = dialSettings.startValue,
            ev = dialSettings.lastTick || dialSettings.endValue,
            degrees = dialSettings.angleEnd - dialSettings.angleStart,
            min = Math.min(dialSettings.startValue, dialSettings.endValue),
            max = Math.max(dialSettings.startValue, dialSettings.endValue),
            range = max - min,
            degreesPerUnit = degrees / range,
            nextValue = sv;

        var path = "";
        while ((sv < ev && nextValue <= ev) || nextValue >= ev) {
            var a = nextValue * degreesPerUnit + dialSettings.angleStart;
            var radians = a * (Math.PI / 180);
            var strokeEnd = strokeSettings.length;
            var px1 = cx + dr * Math.cos(radians);
            var py1 = cy + dr * Math.sin(radians);
            var px2 = cx + (dr - strokeEnd) * Math.cos(radians);
            var py2 = cy + (dr - strokeEnd) * Math.sin(radians);
    
    
            path += "M"+ px1 +","+ py1+ "L"+ px2+","+ py2;
            
        
            if (drawLabel && dialSettings.displayLabel && (nextValue % dialSettings.labelIncrement === 0)) {
                var px3 = cx + (lr * Math.cos(radians)); // XXX conf
                var py3 = cy + (lr * Math.sin(radians));

        
                var label = this._paper.text(px3, py3, nextValue).attr({
                    'fill': dialSettings.labelFill,
                    'font-size': this.settings.fontSize
                });
                

                if (dialSettings.labelsPerpendicular) {
                    label.rotate(a + 90);
                }
            }

            // draw the value.
            if (sv < ev) {
                nextValue = nextValue + strokeSettings.increment;

                if (nextValue > ev) {
                    break;
                }
            } else {
                nextValue = nextValue - strokeSettings.increment;

                if (nextValue < ev) {
                    nextValue = ev;
                }
            }
            
            nextValue = Math.round(nextValue * 100) / 100;
        }
        
        this._paper.path(path).attr({
                'stroke-width': strokeSettings.width,
                'stroke': strokeSettings.stroke
            });
    };
    
    ctor.prototype._slice = function(startAngle, endAngle, r, params) {
        var cx = this.settings.cx,
            cy = this.settings.cy,
            r = this.settings.radius * r;
        
        var x1 = cx + r * Math.cos(Raphael.rad(-startAngle));
        var x2 = cx + r * Math.cos(Raphael.rad(-endAngle));
        var y1 = cy + r * Math.sin(Raphael.rad(-startAngle));
        var y2 = cy + r * Math.sin(Raphael.rad(-endAngle));
        
        return this._paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0,
                +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
    };

    ctor.prototype._setPointerValue = function(pointerObj, value) {
        var pointer = pointerObj.pointer;
        var dialSettings = pointerObj.dialSettings;
        var display = pointerObj.displayValue;
        
        var cx = this.settings.cx,
            cy = this.settings.cy,
            as = dialSettings.angleStart,
            ae = dialSettings.angleEnd,
            sv = dialSettings.startValue,
            ev = dialSettings.endValue,
            animMs = dialSettings.animationTimeMs,
            degrees = Math.abs(ae - as),
            position = 0,
            transformation = null;

        if (value < dialSettings.startValue) {
            value = dialSettings.startValue;
        } else if (value > dialSettings.endValue) {
            value = dialSettings.endValue;
        }

        degrees = degrees / Math.abs(sv - ev) * value;

        if (as < ae) {
            position = as + degrees - 90;
        } else {
            position = as - degrees - 90;
        } 

        transformation = 'R' + position + ',' + cx + ',' + cy;

        if (!!display) {
            display.attr({text: value != null ? value+dialSettings.displayValueSuffix : '0'+dialSettings.displayValueSuffix});
        }

        if (this._values[pointer.id] != null && animMs != null) {
            pointer.animate({transform: transformation}, animMs, dialSettings.easing);
        } else {
            pointer.transform(transformation);
        }

        this._values[pointer.id] = value;
    };

    return ctor;
}());
