var nx = nx || {};
nx.unit = nx.unit || {};
nx.unit.diagram = nx.unit.diagram || {};

nx.unit.diagram.Diagram = (function() {
    function ctor(unit, data, clickHandler) {
        this.LEFT = -1;
        this.RIGHT= 1;
        
        this._unit = unit;
        this._data = data;
        this._clickHandler = clickHandler;
    };
    
    ctor.prototype._setData = function(data) {
        this._data = data;
    };
    
    ctor.prototype.draw = function(container) {};
    
    ctor.prototype.udpate = function(data) {};
    
    return ctor;
})();

nx.unit.diagram.DiagramProvider = new (function() {
    var ctor = function() {
        this.__clazz = null;
    };
    
    ctor.prototype.register = function(clazz) {
        this.__clazz = clazz;
    };
    
    ctor.prototype.get = function(unit, data, clickHandler) {
        if (this.__clazz != null) {
            return new this.__clazz(unit, data, clickHandler);
        } else {
            return null;
        }
    };
    
    return ctor;
}());
