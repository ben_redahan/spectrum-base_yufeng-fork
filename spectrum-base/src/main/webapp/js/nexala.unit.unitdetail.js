var nx = nx || {};
nx.unit = nx.unit || {};
nx.unit.detail = nx.unit.detail || {};

nx.unit.UnitDetail = (function() {
    function ctor(us) {
        nx.unit.Tab.call(this, us);
        
        this.__unitId = null;
        this.__fleetId = null;
        this.__timestamp = null;
        this.__stockId = null;
        this.__stockType = null;
        
        this.__udPanelRows = null;
        this.__udPanelCellTables = [];
        
        this.__conf = null;
        
        this._initUnitDetail();
        
    };
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();
    
    /**
     * Could be override to do specific initialisation.
     */
    ctor.prototype._initUnitDetail = function() {
        
        this.__info = new nx.unit.detail.Info($("#udInfo"));
        this.__diagram = new nx.unit.detail.Diagram(this, $("#udDiagram"));
                       
    };
    
    var thisUnitId = null;
    
    ctor.prototype.__updateUD = function(unitId, fleetId, timestamp, unitChanged) {
        if (!unitId || (!timestamp || timestamp == "")) {
            return;
        }

        var me = this;

        nx.rest.unitDetail.data(unitId, fleetId, timestamp, function(response) {
        	localStorage.setItem("fleetId", fleetId);
        	me.__updateInfo(fleetId, unitId, response.infoData);
            me.__updateDiagram(response.unit, response.diagramData, unitChanged);
            
            if (unitChanged) {
                thisUnitId = unitId;
                me.__stockId = null;
                me.__stockType = null;
                me.__setStock(response.tabs);
            }
            else if (thisUnitId != unitId || thisUnitId == null){
                thisUnitId = unitId;
                me.__setStock(response.tabs);
                me.__updateDiagram(response.unit, response.diagramData, true);
            }

            var stockId = me.__stockId != null ? me.__stockId : unitId;
            if (me.__conf) {
            	var stockType = me.__stockType != null ? me.__stockType : me.__conf.stockTabsType;
                
                for(var cell in me.__udPanelCellTables) {
                    me.__udPanelCellTables[cell].getCellEvents(fleetId, stockType, stockId);
                }
            }
        });
    };

    ctor.prototype.__updateInfo = function(fleetId, unitId, data) {
        this.__info._update(fleetId, unitId, data.channels);
    };
    
    ctor.prototype.__updateDiagram = function(unit, data, unitChanged) {
        this.__diagram._update(unit, data, unitChanged);
    };

    ctor.prototype._logging = function() {
        //logs the unit summary - unit detail access
        nx.rest.logging.unitDetail();
    };

    ctor.prototype._hasTimeController = function() {
        return false;
    };

    ctor.prototype._update = function(data) {
        var me = this;
        
        if (this.__fleetId != data.fleetId) {
            this.__fleetId = data.fleetId;
            
            nx.rest.unitDetail.conf(this.__fleetId, function(response) {
                me.__conf = response;
                
                me.__info.initialize(response.numberOfHeaderColumns, response.headerFields, response.showEventHistoryButton);
                
                // init tabs (if not already done)
                me.__initTabs();
                
                for (var row in response.udPanelRows) {
                    me.__createPanelRow(response.udPanelRows[row]);
                }
                
                var stockType = response.defaultStockTabType != null ? response.defaultStockTabType : response.stockTabsType;
                
                for (displayTable in me.__udPanelCellTables) {
                    me.__udPanelCellTables[displayTable].initializeTables();
                    me.__udPanelCellTables[displayTable].getCellEvents(data.fleetId, stockType, data.unitId);
                };
               
            });
        }
        
        this.__unitId = data.unitId;
        this.__timestamp = data.timestamp;
        
        this.__updateUD(data.unitId, data.fleetId, data.timestamp, data.unitChanged());
    };  
    
    ctor.prototype.__createPanelRow = function(rowConf) {
        var me = this;
        
        var row = $('<div>').attr('id', 'row' + rowConf.position).addClass('udRow').appendTo($('#udTables'));
        
        if(me.__conf.udPanelRows.length > 1) {
            row.addClass('doubleRow');
        } else {
            row.addClass('singleRow');
        }
                
        var container = $('<div>').attr('id', 'udTableEventContainer').addClass('udTableContainer').appendTo(row);
        
        for (var cellConf in rowConf.cells) {
            var cell = this.__createRowCell(rowConf.cells[cellConf]);
            
            // dynamic sizing: needs new css classes if config goes beyond 2x2
            // flexbox html property works but not with IE8
            if (rowConf.cells.length > 1){
                cell.addClass('doubleCell');
            } else {
                cell.addClass('singleCell');
            }
            
            container.append(cell);
        }       
    };
    
    ctor.prototype.__createRowCell = function(cellConf) {
        
        var cell = $('<div>').attr('id', 'udTable_' + cellConf.name).addClass('udCell');
        
        // Header
        var header = $('<div>').attr('id', 'udTableHeader_' + cellConf.name).addClass('header ui-widget-header').appendTo(cell);
        var title = $('<div>').addClass('tableTitle').html($.i18n(cellConf.title)).appendTo(header);
        
        // Table
        var table = $('<div>').attr('id', 'udTableDiv_' + cellConf.name).addClass('tableDiv').appendTo(cell);
        
        var cellTable = $('#udTable_' + cellConf.name);
        var tableDiv = $('#udTableDiv_' + cellConf.name);
        
        var displayTable = new nx.unit.detail.GenericCell(cellTable, tableDiv, cellConf, this.__fleetId);
        
        if (!!cellConf.filters) {
            this._createCellFilters(header, displayTable, cellConf);
        }

        // add cell object to list
        this.__udPanelCellTables.push(displayTable);
        
        return cell;
    };
    
    ctor.prototype._createCellFilters = function(header, table, conf) {
        var me = this;
        var filterDiv = $('<div>').addClass('tableFilters').attr('id', 'udTableFilter_' + conf.name);
        
        $.each(conf.filters, function(index, filter) {
            $('<span>').html($.i18n(filter.label) + " :").appendTo(filterDiv);
            
            var filterId = 'udFilter_' + conf.name + '_' + filter.name;
            
            switch(filter.fieldType.toLowerCase()) {
            // TODO support more filter types
            case 'multiple_select': 
                var select = $('<select>').attr('id', filterId).appendTo(filterDiv);
                me.__initMultiSelectFilter(select, filter.defaultOptions, filter.options, table);
                break;
            case 'checkbox':
                var checkbox = $('<input type="checkbox">').attr('id', filterId).appendTo(filterDiv);
                me.__initCheckboxFilter(checkbox, filter.checkedAsDefault, table);
                break;
            case 'input_text':
                var textFilter = $('<input type="text">').attr('id', filterId).appendTo(filterDiv);
                me.__initInputTextFilter(textFilter, table);
                break;
            }

        });
        
        filterDiv.appendTo(header);
    };
    
    ctor.prototype.__initMultiSelectFilter = function(select, defaultOptions, options, table) {
        
        if (!!defaultOptions) {
            $.each(defaultOptions, function(index, option) {
                select.append('<option value="' + option.id + '">' + $.i18n(option.displayName) + '</option>');
            });
        }
        
        if (!!options) {
            $.each(options, function(index, option) {
                select.append('<option value="' + option.id + '">' + $.i18n(option.displayName) + '</option>');
            });
        }
        
        select.change(function(){
            table.__applyFilters();
        });
        
        select.uniform();
    };
    
    ctor.prototype.__initCheckboxFilter = function(checkbox, defaultChecked, table) {
        
        if(!!defaultChecked) {
            checkbox.prop('checked', true);
        }
        
        checkbox.change(function() {
            table.__applyFilters();
        });
        
        checkbox.uniform();
        $.uniform.update();
    };
    
    ctor.prototype.__initInputTextFilter = function(textFilter, table) {

        textFilter.addClass('tableTextInputFilter');
        
        textFilter.keyup(function() {
            if(textFilter.val().length != 1) {
                table.__applyFilters();
            }
        });
    };
    
    ctor.prototype.__initTabs = function() {
        var me = this;
        
        $('#udTables').append('<ul>');
        $('#udTables').tabs({
            'show': function(event, ui) {
                // apply filters to all GenericCells
                var tabId = ui.panel.id.split("_");
                var stockType = tabId[0];
                var stockId = tabId[1];
                me.__changeTab(stockType, stockId);
            }
        });
    };
    
    ctor.prototype.__setStock = function(vehicles) {
        var me = this;
        var tableContainer = $('#udTables');
        
        vehicles.sort(function(a, b) {
            return a.position - b.position;
        });

        var oldTabs = tableContainer.find('li').length;
        
        for (var i = 0; i < oldTabs; i++) {
            tableContainer.tabs('remove', 0);
        }
        
        if(vehicles.length > 1){
            if(!!this.__conf.defaultStockTabName) {
                tableContainer.tabs('add', '#' + this.__conf.defaultStockTabType + '_' + this.__unitId , $.i18n(this.__conf.defaultStockTabName)); // add method is  deprecated in jQuery UI 1.10
            }
            
            for (var index = 1; index < vehicles.length; index++) {
                var vehicle = vehicles[index];            
                tableContainer.tabs('add', '#' + this.__conf.stockTabsType + '_'  + vehicle.id, $.i18n(vehicle.description));
            }
        }
    };
    
    ctor.prototype.selectActiveStock = function (id) {
        $('#udTables').tabs('select', '#' + this.__conf.stockTabsType + '_' + id);       
    }
    
    ctor.prototype.__changeTab = function(stockType, stockId) {
        this.__stockType = stockType;
        this.__stockId = stockId;
        for(var displayTable in this.__udPanelCellTables) {
            this.__udPanelCellTables[displayTable].updateStock(this.__fleetId, stockType, stockId);
        }
    };
       
    
    /**
     * If the tab need update even if the unit and timestamp didn't changed
     */
    ctor.prototype.needUpdate = function() {
        // If we have a table of event, we need a refresh even if the vehicle didn't received new data
        if (this.__udPanelCellTables.length > 0) {
            return true;
        } else {
            return false;
        }
        
    };

    return ctor;
})();

nx.unit.detail.GenericCell = (function() {
    function ctor(panel, cellTable, conf, fleetId) {
        this.__panel = $('#udTable_' + conf.name);
        this.__cellTable = $(cellTable);
        this.__conf = conf;
        this.__mode = conf.mode;
        
        this.__cellGrid = null;
        
        this.__cellEvents = [];

        this.__fleetId = fleetId;
        
        this.__columns = [];
        
    }
    
    ctor.prototype.updateStock = function(fleetId, stockType, stockId) {
        this.getCellEvents(fleetId, stockType, stockId);
        this.__applyFilters();
    };
    
    // currently not used: one set of tabs is initialized for all rows/cells
    ctor.prototype.__initializeTabs = function() {
        var me = this;
        
        $('#udTable_' + me.__conf.name).append('<ul>');
        $('#udTable_' + me.__conf.name).tabs({
            'show': function(event, ui) {
                me.__applyFilters();
            }
        });
        
        $('#maintenanceTabs').tabs();
    };
    
    ctor.prototype.__getRow = function(obj) {
        var row = [];
        
        for (var i in this.__columns) {
            var column = this.__columns[i];
            row.push(obj[column.name]);
        }
        
        return row;
    };

    ctor.prototype.__applyFilters = function() {
        var me = this;
        
        if (!!this.__cellGrid) {
            
            data = [];
            var header = $('#udTableHeader_' + me.__conf.name);
            $.each(this.__cellEvents, function(index, record) {
                var includeRecord = true;
                $.each(me.__conf.filters, function(index, filter) {
                    if (!!includeRecord) {
                        switch(filter.fieldType) {
                        case 'MULTIPLE_SELECT':
                            var filterVal = header.find('#udFilter_' + me.__conf.name + '_' + filter.name +
                            ' option:selected').val();
                            includeRecord = (me.__applyFilterExtraConditions(record, filter.name, filterVal) || record[filter.name] == filterVal);
                            break;
                        case 'CHECKBOX':
                            var checked = header.find('#udFilter_' + me.__conf.name + '_' + filter.name).is(":checked");
                            if(!!checked) {
                                filterVal = filter.defaultOptions;
                                includeRecord = me.__applyCheckboxFilterConditions(record, filter.name, filterVal, filter.includeValues);
                            }
                            break;
                        case 'INPUT_TEXT':
                            var filterVal = header.find('#udFilter_' + me.__conf.name + '_' + filter.name).val();
                            includeRecord = me.__applyInputTextFilterConditions(record, filter.name, filterVal);
                            break;
                        }
                        
                    }
                });
                
                if (!!includeRecord) {
                    data.push({
                        id: index, 
                        data: me.__getRow(record)
                    });
                }
            });
            
            this.__cellGrid.setData(data);
        }
    };
    
    ctor.prototype.__applyFilterExtraConditions = function(record, field, val) {
               
        if(val == 'all' ){
            return true;
        } else if(field == 'faultTypeId' && (val == record.typeId)){
            return true;
        } else if(field == 'isAcknowledged' && ($.parseJSON(val) == record.isAcknowledged)){
            return true;
        } else if(field == 'categoryId' && ((val == 'allExceptTest' && record.category.toLowerCase() != 'test')
                    || (!!record.optionalCategoryIdList && record.optionalCategoryIdList.indexOf(parseInt(val)) > -1))){
            return true;
        }
        
        return false;
    };
    
    ctor.prototype.__applyCheckboxFilterConditions = function(record, field, val, includeValues) {
        if(includeValues == true) { // include records that match the values in the config
            var result = null;
            for(var i in val) {
                var targetValue = val[i].id;
                if(record[field] != targetValue) {
                    result = false;
                } else {
                    return true;
                }
            }
            return result;
        } else { // exclude records that match the values in the config
            var result = true;
            for(var i in val) {
                var targetValue = val[i].id;
                if (record[field] == targetValue) {
                    result = false;
                    return result;
                }
            }
            return result;
        }
    };
    
    ctor.prototype.__applyInputTextFilterConditions = function(record, field, searchString) {
        searchString = searchString.replace(/[.][*]/g, "[a-zA-Z0-9]*");
        searchString = searchString.replace(/[.]/g, "[a-z0-9]");
        
        var pattern = new RegExp(searchString, 'i');
        if(pattern.test(record[field]) == true ) {
            return true
        } else {
            return false;
        }
        
    };

    ctor.prototype.initializeTables = function() {
        var me = this;
        var conf = me.__conf;
        
        this.__columns = [];
        for (var i = 0; i < conf.columns.length; i++) {
            var column = conf.columns[i];
            
            this.__columns.push({
                'name': column.name,
                'text': $.i18n(column.header),
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'sortByColumn': column.sortByColumn,
                'format': column.format,
                'handler': column.handler,
                'cssClass': column.cssClass,
                'formatter': function(el, value) {
                    var value = me.__columnFormatter(el, value);
                    return value;
                }
            });
        }

        // cell table
        this.__cellTable.html('<table class = "ui-corner-all"></table>');
        $('#udTableDiv_' + conf.name).html('<table class = "ui-corner-all"></table>');
        
        this.__cellGrid = $('#udTableDiv_' + conf.name).tabular({
            columns: [this.__columns],
            cellClickHandler: (function(me) {
                return function(cell, data, columnDef) {
                    me.__clickEventHandler(cell, me.__cellEvents[data.id], columnDef);
                };
            })(me),
            cellMouseOverHandler: (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseOverEventHandler(cell, me.__cellEvents[data.id], columnDef);
                };
            })(me),
            scrollbar: true,
            fixedHeader: true
        });
        
    };
    
    ctor.prototype.__columnFormatter = function(column, value) {
        var content = '';
        if (column.name == 'hasComment') {
            if (!!value) {
                content = '<div class="hasComment">&nbsp;</div>';
            }
        } else if (!!column.format) {
            //content = column.format;
            var val = !!column.format ? $.format(column.format, value) : value;
            var displayValue = (typeof val === 'object') && (val != null)? val[0] : val;
            
            // null or undefined
            if (displayValue == null) {
                content = '<div>&nbsp;</div>';
            } else {
                content = '<div>' + displayValue + '</div>';
            }
        } else {
            var cellText = !!value ? value || '&nbsp;' : '&nbsp;';
            content = cellText;
        }
        
        return content;
    };

    // TODO: support different click handlers
    ctor.prototype.__clickEventHandler = function(cell, data, columnDef) {
        if (columnDef.handler == 'openEventDetail') {
            var eventIdList = []
            $.each(this.__cellGrid.__model, function(index, filteredCellEvent) {
                eventIdList.push(filteredCellEvent.data[0]);
            });
            
            nx.event.open(data.id, data.fleetId, eventIdList);
        } else if (columnDef.handler == 'openWorkOrderComment') {
            var woNumber = data.wonum;
            
            if (!!data.hasComment) {
                nx.rest.unitDetail.workOrderComment(this.__fleetId, woNumber, function(response){
                    var comment = $("<div>"+ response +"</div>");
                    comment.dialog({
                        resizable : false,
                        modal : true,
                        buttons : {
                            'Ok' : function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                });
            }
        }
    };

    ctor.prototype.__mouseOverEventHandler = function(cell, data, columnDef) {
        var title = "";
        $.each(this.__conf.rowMouseOver, function(index, mouseOverLine) {
            if (index > 0) {
                title += '\n'
            }
            title += $.i18n(mouseOverLine.label) + ": " + data[mouseOverLine.value];
        });
        
        for(var i in this.__conf.columns){
            var col = this.__conf.columns[i];
            if(col.name == columnDef.name && col.mouseOverContent.length > 0){
                title = ""; 
                mouseOver = col.mouseOverContent[0];
                
                if (!!mouseOver.label) {
                    title += $.i18n(mouseOver.label) + ': ';
                }
                
                if (!!mouseOver.value) {
                    value = data[mouseOver.value];
                    if (!!mouseOver.format) {
                        value = $.format(mouseOver.format, value);
                    }
                    title += (value || '');
                }
            }
        }
        
        cell.attr('title', title);
    };
    
    ctor.prototype.__columnMouseOver = function(column, content) {
        
    }

    ctor.prototype.setStock = function(tabs) {
        this.__setStock(tabs);
    };
    
    // call the correct service for this mode and update accordingly
    ctor.prototype.getCellEvents = function(fleetId, stockType, stockId) {
        var me = this;
        switch(this.__mode) {
            case 'allEvents':
                me.getAllEvents(fleetId, stockType, stockId);
                break;
            case 'liveEvents':
                me.getLiveEvents(fleetId, stockType, stockId);
                break;
            case 'recentEvents':
                me.getRecentEvents(fleetId, stockType, stockId);
                break;
            case 'acknowledgedEvents':
                nx.rest.unitDetail.acknowledgedEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'notAcknowledgedEvents':
                nx.rest.unitDetail.notAcknowledgedEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'allRestrictions':
                nx.rest.unitDetail.allRestrictions(fleetId, stockId,function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'liveRestrictions':
                nx.rest.unitDetail.liveRestrictions(fleetId, stockId,function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'recentRestrictions':
                nx.rest.unitDetail.recentRestrictions(fleetId, stockId,function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'workOrders':
                nx.rest.unitDetail.workOrders(fleetId, stockId, function(response) { 
                    me.setCellEvents(response);
                });
                break;
        }
    };
    
    ctor.prototype.getAllEvents = function(fleetId, stockType, stockId) {
        var me = this;
        switch(stockType) {
            case 'formation':
                // TODO     
            case 'unit':
                nx.rest.unitDetail.allEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'vehicle':
                // TODO
                break;
        }
    };
    
    ctor.prototype.getLiveEvents = function(fleetId, stockType, stockId) {
        var me = this;
        switch(stockType) {
            case 'formation':
                nx.rest.unitDetail.liveFormationEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                    });
                break;
            case 'unit':
                nx.rest.unitDetail.liveEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'vehicle':
                // TODO
                break;
        }
    };
    
    ctor.prototype.getRecentEvents = function(fleetId, stockType, stockId) {
        var me = this;
        switch(stockType) {
            case 'formation':
                nx.rest.unitDetail.recentFormationEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                    });
                break;
            case 'unit':
                nx.rest.unitDetail.recentEvents(fleetId, stockId, function(response){
                    me.setCellEvents(response);
                });
                break;
            case 'vehicle':
                // TODO
                break;
        }
    };
    
    ctor.prototype.setCellEvents = function(events) {
        var me = this;
        this.__cellEvents = [];
        $.each(events, function(index, event) {
            me.__cellEvents.push(event.fields);
        });
        this.__applyFilters();
    };
    
    return ctor;
})();

nx.unit.detail.Info = (function() {
    function ctor(panel, container) {
        this.__panel = panel;
        this.__container = container;
        
        this.__timestamp = null;
        this.__diagram = null;
        this.__location = null;
        this.__headcode = null;
        this.__unit = null;
        this.__speed = null;
        this.__faults = null;
        this.__warnings = null;
        this.__leadingVehicle = null;
        this.__headers = null;
        this.__headerToTdMap = {};
        this.__headerNameToHeader = {};

        this.__fleetId = null;
        this.__unitId = null;
    };
    
    ctor.prototype.initialize = function(numberOfColumns, headers, showEventHistoryButton) {
        var me = this;
        
        if (this.__panel.length == 0) {
            return;
        }

        if (this.__panel.length == 0) {
            return;
        }
        
        this.__headers = headers;
        
        var tableDiv = $('<div>').attr('id', 'tableDiv').addClass('udInfoDiv');
        
        var table = $('<table>').addClass('udInfoTable');
        
        for (var i = 0, l = headers.length; i < l; i += numberOfColumns) {
            var row = $('<tr>');
            
            for (var j = 0; i + j < headers.length && j < numberOfColumns; j++) {
                var header = headers[i + j];
                
                if (header.visible) {
                    
                    var tdLabel = $('<td>');
                    var tdValue = $('<td>');
                    
                    var label = $('<span>')
                        .text($.i18n(header.label) + ':')
                        .addClass('udInfoLabel');
                        
                    var value = $('<span>')
                        .addClass('udInfoData');
                    
                    $("#faultTable tbody").on({
                        click : (function(me) {
                            return function(eventObj) {
                                me.__openDetails(eventObj);
                            };
                        })(me),
                        
                        touchend : (function(me) {
                            return function(eventObj) {
                                me.__openDetails(eventObj);
                            };
                        })(me)
                    }, "tr");

                    label.appendTo(tdLabel);
                    value.appendTo(tdValue);
                    
                    var editIcon = null;
                    
                    if (header.editable) {
                        (function(element, header, value) {
                            nx.rest.unitDetail.hasLicence(header.editLicence, function(response) {
                                if (response) {
                                    editIcon = $('<span>')
                                        .addClass('udInfoEditIcon ui-icon ui-icon-pencil')
                                        .click(
                                            (function(me, headerField, valueElement) {
                                                return function(eventObj) {
                                                    me.__callEditHandler(headerField, valueElement);
                                                };
                                            })(me, header, value)
                                        );
                                    editIcon.appendTo(element);
                                }
                            });
                        })(tdValue, header, value);
                    }

                    tdLabel.appendTo(row);
                    tdValue.appendTo(row);
                    
                    this.__headerToTdMap[header.name] = value;
                    this.__headerNameToHeader[header.name] = header;
                    
                }
                
            }
            
            row.appendTo(table);
        }
        
        table.appendTo(tableDiv);
        tableDiv.appendTo(udInfo);
        
        var buttonsNumber = 0;
        
        nx.rest.unitDetail.hasLicence('spectrum.eventhistory', function(response) {
            if (response && showEventHistoryButton) {
                var buttonsDiv = $('<div>').attr('id', 'buttonsDiv').addClass('udInfoButtonsDiv');
                var eventHistoryButton = nx.main.addShowEventHistoryButton();
                eventHistoryButton.appendTo(buttonsDiv);
                buttonsDiv.appendTo(udInfo);
            }
        });
    };

    ctor.prototype.__callEditHandler = function(field, element) {
        var handler = field.editHandler.split('.');

        var handlerObject = window || this;
        for (var i in handler) {
            handlerObject = handlerObject[handler[i]];
        }

        if (typeof handlerObject !== 'function') {
            throw new Error("Object " + field.editHandler + " not found");
        } else {
            new handlerObject(this.__fleetId, this.__unitId, field, element);
        }
    };

    /** Clears the headers */
    ctor.prototype.__reset = function() {
        if (this.__headers != null) {
            for (var i = 1, l = this.__headers.length; i < l; i += 2) {
                var header = this.__headers[i];
                var td = this.__headerToTdMap[header.name];
                
                if (!!td) {
                    $(td).text('');
                }
            }
        }
        
    };

    /** Updates the values */
    ctor.prototype._update = function(fleetId, unitId, data) {
        this.__reset();

        this.__fleetId = fleetId;
        this.__unitId = unitId;

        for (var k in data) {
            var td = this.__headerToTdMap[k];
            var header = this.__headerNameToHeader[k];
            
            if (!!td && !!header) {
                var value = data[k][0];
                var formatString = header.formatter;
                
                if (value != null) {
                    if (formatString) {
                        td.text($.format(formatString, value));
                    } else {
                        td.text(value);
                    }
                } else {
                    td.text('');
                }
            }
        }
    };
    
    return ctor;
})();

nx.unit.detail.Diagram = (function() {
    function ctor(unitDetail, panel) {
        this.__unitDetail = unitDetail;
        this.__panel = panel;
        
        this.__provider = null;
    };
    
    ctor.prototype._update = function(unit, data, unitChanged) {
        if (!this.__provider || !!unitChanged) {
            this.__createDiagram(unit, data);
        }
        
        if (!!this.__provider) {
            this.__provider._update(data, unit);
        }
    };
    
    // TODO: replace with Generic implementation?
//    ctor.prototype.setEvents = function(events) {
//        this.__provider.updateEvents(events);
//    };
//    
//    ctor.prototype.setRestrictions = function(restrictions) {
//        this.__provider.updateEvents(restrictions);
//    };
    
    ctor.prototype.__clickHandler = function(vehicle) {
        this.__unitDetail.selectActiveStock(vehicle);
    };
    
    ctor.prototype.__createDiagram = function(unit, data) {
        var me = this;
        unit.__fleetId = this.__unitDetail.__fleetId;
        this.__provider = nx.unit.diagram.DiagramProvider.get(
            unit, 
            data, 
            (function(me) {
                return function(data) {
                    me.__clickHandler(data);
                };
            })(me)
        );
        
        if (!!this.__provider) {
            this.__provider.draw(this.__panel);
        }
    };
    
    
    
    return ctor;
})();
