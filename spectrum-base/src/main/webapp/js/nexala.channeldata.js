var nx = nx || {};
nx.unit = nx.unit || {};

nx.ChannelData = (function() {
    function ctor(us, parameters) {
        var me = this;
        
        this.__unitSummary = us;
        this.__id = null;
        this.__fleetId = null;
        this.__timestamp = null;
        this.__groupId = null;
        this.__columns = null;
        // Contains a map of channels definitions indexed by id 
        this.__groupChannels = null;

        this.__hasIndependentTimeController = false;
        this.__timer = null;
        this.__timePicker = null;
        this.__toggleButton = null;
        
        // Contain the ajax call to channeldata service
        this.__unitCall = null;
        this.__updatingUnit = false;

        if (!!parameters) {

            if (parameters.hasIndependentTimeController) {
                this.__hasIndependentTimeController = true;
                
                this.__subscribeToEvents();
                this.setLive();

                this.doUpdate(this.__fleetId, this.__id, me.getTimestamp());
            }

            if (!!parameters.timePicker) {
                this.__timePicker = parameters.timePicker;
            }

            if (!!parameters.toggleButton) {
                this.__toggleButton = parameters.toggleButton;
            }
        }

        this.__table = $('#cdTable');
        this.__grid = null;

        this.__showAll = false;

        this.__buildChannelGroupTabs();
        
    };
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();
    
    ctor.prototype.__displayShowHideButton = function(fleetId, groupId) {
        var me = this;
        this.__isShowHideButton = false;
        
        
        $.each(this.__groupChannels, function(index, chan) {
            if (!chan.alwaysDisplayed) {
                me.__isShowHideButton = true;
                return false;
            }
            return true;
          });

        if(me.__isShowHideButton) {
            this.__showAll = false;
            if (me.__showHideButton == null) {
                me.__showHideButton = $('<button title = ""></button>').button()
                .click(function(e) {
                    if (me.__showAll) {
                        me.__hideChannels();
                    } else {
                        me.__showAllChannels();
                    }
                });
                $('#cdShowHideButton').append(me.__showHideButton);
            } else {
                me.__showHideButton.show();
            }
            
            me.__showHideButton.text($.i18n('Show Additional'));
            
        } else {
            if (me.__showHideButton != null) {
                me.__showHideButton.hide();
            }
            this.__showAll = true;
        }
    };
    
    ctor.prototype.getUnit = function() {
        return this.__id;
    };
    
    ctor.prototype.setUnit = function(newId) {
        this.__id = newId;
    };
    
    ctor.prototype._onShow = function() {
        
    };
    
    ctor.prototype._onHide = function() {
        
    };
    
    ctor.prototype.__subscribeToEvents = function() {
        var me = this;
        
        // Remove all change subscriptions, i.e.
        // from nexala.spectrum (main toolbar), unit summary
        // has its own subscription below.
        nx.hub.unitChanged.unsubscribe();
        
        nx.hub.play.subscribe(function(e) {
            me.play();
        });
        
        nx.hub.pause.subscribe(function(e) {
            me.pause();
        });
        
        nx.hub.timeChanged.subscribe(function(e, timestamp) {
            me.timeChanged(timestamp);
        });
        
        nx.hub.startLive.subscribe(function(e) {
            me.start();
        });
        
        nx.hub.stopLive.subscribe(function(e) {
            me.stop();
        });
    };
    
    ctor.prototype.play = function() {
        this.isLive() ? this.setLive() :
                this.__replay();
    };
    
    ctor.prototype.setLive = function() {
        
        if (!!this.__timePicker) {
            this.getTimePicker().setLive();
        } else {
        	this.setTimestamp(new Date().getTime());
        }
        
        this.__replay();
    };
    
    ctor.prototype.setNotLive = function() {
        
        if (!!this.__timePicker) {
            this.getTimePicker().setNotLive();
        }
    };
    
    ctor.prototype.__replay = function() {
        var me = this;
        var refreshRate = this.__unitSummary.getRefreshRate();

        if (this.__timer != null) {
            window.clearInterval(this.__timer);
        }
        this.__timer = window.setInterval(function() {
            var timestamp = me.getTimestamp();
            me.setTimestamp(timestamp + refreshRate);
            me.doUpdate(me.__fleetId, me.__id, timestamp + refreshRate);
        }, refreshRate);
    };
    
    ctor.prototype.pause = function() {
        if (this.__timer != null) {
            window.clearInterval(this.__timer);
            this.__timer = null;
        }
    };
    
    /**
     * When the user changes the time, we flip the toggle button to
     * paused
     */
    ctor.prototype.timeChanged = function(timestamp) {
        this.pause();
        this.getTimePicker().setNotLive();
        this.getToggleButton().pause();
        this.doUpdate(this.__fleetId, this.__id, timestamp);
    };
    
    ctor.prototype.start = function() {
        this.getToggleButton().play();
        this.play();
    };
    
    ctor.prototype.stop = function() {
        this.getToggleButton().pause();
        this.pause();
    };
    
    ctor.prototype.doUpdate = function(fleetId, id, timestamp) {
        var idChanged = false, timeChanged = false;

        if (fleetId != null) {
            if (this.__fleetId != fleetId) {
                this.__fleetId = fleetId;
                idChanged = true;
            }
        }

        if (id != null) {
            if (this.__id != id) {
                this.__id = id;
                idChanged = true;
            }
        }
        
        if (timestamp != null) {
            if (this.__timestamp != timestamp) {
                this.__timestamp = timestamp;
                timeChanged = true;
            }
        }

        this._update({
            "fleetId": fleetId,
            "unitId" : id,
            "timestamp" : timestamp,
            "unitChanged" : function() {
                return idChanged;
            },
            "timeChanged": function() {
                return timeChanged;
            }
        });
    };
    
    ctor.prototype.__buildChannelGroupTabs = function() {
        var me = this;

        var groupTabs = $('#cdChannelGroups');

        groupTabs.tabs({
            'show' : function(event, ui) {
                me.__groupId = ui.tab.hash.substr(7);
                $.event.trigger({
                    type: "showGroup",
                    group: me.__groupId
                });

                var timestamp = me.getTimestamp() == "" ? 0 : me.getTimestamp();
                
                me.__updateTable(me.__fleetId, me.__id, timestamp, me.__groupId, true, nx.util.directions.TIME, true);
            }
        }).addClass('ui-tabs-vertical ui-helper-clearfix');

        groupTabs.find("li").removeClass('ui-corner-top').addClass('ui-corner-left');
    };

    ctor.prototype.__updateTable = function(fleetId, id, timestamp, groupId, unitChanged, direction, resetScroll) {
        if (!fleetId || !id || !groupId) {
            return;
        }

        var me = this;
        
        var populateTable = function() {
        	if (me.__unitCall != null) {
        	    if (unitChanged) {
        	        me.__unitCall.abort();
        	        me.__unitCall = null;
                    me.__updatingUnit = false;
        	    } else {
        	        return;
        	    }
        	}
        	
        	me.__unitCall = nx.rest.channelData.unit(id, fleetId, timestamp, groupId, me.__showAll, direction, function(response) {
            	
                $.event.trigger({
                    type: "setNewData",
                    newData: response
                });
                // If time controller is in record mode and direction != 0, we don't update the table if there is no data
                if ((direction == 0) || (!me.__isEmptyRecord(response.data)) || (me.__isEmptyTable())) {
                    if (direction != 0) {
                        var timestamp = me.__getDataTimestamp(response.data);
                        me.setTimestamp(timestamp);
                    }
                    me._refreshTable(response, resetScroll);
                }
                
                me.__unitCall = null;
            });
        };
        
        if (unitChanged) {
            
            nx.rest.channelData.groupData(fleetId, groupId, id, function(response) {
                me.__applyFilter(groupId, fleetId, response)
                me.__setData(response.columns, []);
                
                me.__groupChannels = new Object();
                $.each(response.channels, function(index, chan) {
                    me.__groupChannels[chan.id] = chan;
                  });
                
                me.__displayShowHideButton(fleetId, groupId);
                
                populateTable();
                
            });
            
        } else {
            populateTable();
        }
        
    };

    ctor.prototype.__applyFilter = function(groupId, fleetId, response) {
    };
    
    ctor.prototype.__isEmptyTable = function() {
        return this.__grid.getTable().find("tr").length <= 1;
    };

    ctor.prototype.__isEmptyRecord = function(data) {
        var empty = true;
        for(var i = 0, l = data.length; i < l; i++) {
            for(var channel in data[i].channels) {
                if (channel != "ID") {
                    if (data[i].channels[channel][0] != null) {
                        empty = false;
                    }
                }
            }
        }
        
        return empty;
    };

    ctor.prototype.__getDataTimestamp = function(data) {
        var timestampChannel = this.__getTimestampChannel(data);
        
        for(var key in timestampChannel) {
            if (key != "ID") {
                if (timestampChannel[key][0] != null) {
                    return parseInt(timestampChannel[key][0]);
                }
                else {
                    return null;
                }
            }
        }
    };

    ctor.prototype.__getTimestampChannel = function(data) {
        var timestampChannel = null;
        
        for(var i = 0, l = data.length; i < l; i++) {
            if (data[i].channels.ID[0] == "TimeStamp") {
                timestampChannel = data[i].channels;
                break;
            }
        }
        
        return timestampChannel;
    };

    ctor.prototype._refreshTable = function(parameters, resetScroll) {
        this.__updateGroups(parameters.groups);

        var columns = parameters.columns;
        var data = parameters.data;
        this.__setData(columns, data);

        if (resetScroll) {
            this.__table.find('.scroller').scrollTop(0);
        }

    };
    
    ctor.prototype.__setData = function(columns, data) {
        if (!!columns && columns.length > 0) {
            this.__columns = columns;
            this.__buildTable(columns);
        } else {
            columns = this.__columns;
        }

        if (columns == null) {
            return;
        }

        var gridData = [];
        for ( var i = 0, l = data.length; i < l; i++) {
            gridData.push({
                id : i,
                data : this.__getRow(columns, data[i].channels)
            });
        }
        this.__grid.setData(gridData);
    };

    ctor.prototype.__hideChannels = function() {
        this.__showAll = false;

        if(!!this.__showHideButton) {
            this.__showHideButton.text($.i18n('Show Additional')).attr('title', $.i18n('Show Additional'));
        }
        
        if (!this.__grid) {
            return;
        }

        var trs = this.__table.find('tbody tr');

        for ( var i = 0, l = trs.length; i < l; i++) {
            // Get the alwaysDisplayed column
            var alwaysDisplayed = this.__grid.getRowData(i)[1][0];
            if (!alwaysDisplayed) {
                this.__grid.hideRow(i);
            }
        }

        this.__grid.updateStriping();

    };

    ctor.prototype.__showAllChannels = function() {
        this.__showAll = true;
        this.__showHideButton.text($.i18n('Hide Additional')).attr('title', $.i18n('Hide Additional'));
        
        this.__updateTable(this.__fleetId, this.__id, this.getTimestamp(), this.__groupId, false, nx.util.directions.TIME, false);

    };

    ctor.prototype.__updateGroups = function(groups) {
        
        var grpById = {};
        $.map(groups, function(value, key) {
            grpById[key] = value;
        });
        
        $('#cdChannelGroups > ul > li').each(function() {
            var tab = $(this);
            var groupId = tab.find("a").attr("href").substr(7);
            
            var group = grpById[groupId];
            
            if (!group) {
                tab.hide();
            } else {
                var category = nx.util.CATEGORIES[group[1]];

                for ( var index in nx.util.CATEGORIES) {
                    var cat = nx.util.CATEGORIES[index];
                    tab.removeClass('ui-tabs-selected-' + cat);
                    tab.removeClass('ui-state-default-' + cat);
                }

                if (tab.hasClass('ui-tabs-selected')) {
                    tab.addClass('ui-tabs-selected-' + category);
                } else {
                    tab.addClass('ui-state-default-' + category);
                }
            }
            
        });
    };

    ctor.prototype.__buildTable = function(columns) {
        var me = this;

        this.__table.html('<table class = "ui-corner-all"></table>');
        this.__grid = null;

        if (!columns) {
            return;
        }

        var cols = [];

        for ( var i = 0, l = columns.length; i < l; i++) {
            var col = columns[i];

            cols.push({
                name : col.name,
                text : (!!col.header ? col.header : ''),
                width : col.width + 'px',
                sortable : col.sortable,
                resizable : col.resizable,
                visible : col.visible,
                styles : col.styles,
                formatter : function(el, value) {
                    var value = me.__columnFormatter(el, value);
                    return value;
                }
            });
        }

        this.__grid = this.__table.tabular({
            columns : [ cols ],
            fixedHeader : true,
        });
    };

    ctor.prototype.__columnFormatter = function(column, value) {
        var cellText = '&nbsp;';
        var tooltip = '&nbsp;';
        if (!!value && !!value[0] && value[0] != 'null') {
            cellText = value[0];
        }
        if (!!value && !!value[2] && value[2] != 'null') {
        	tooltip = value[2];
			tooltip = tooltip.replace(/>/g, "&gt;");//Need to escape > as it is the html close tag, see R2M-8900
			tooltip = tooltip.replace(/'/g, "&apos;");//also need to escape ', see R2M-8900
        }

        var cellCat = !!value ? value[1] || null : null;
        var className = !!cellCat ? column.styles[nx.util.CATEGORIES[cellCat]] : 'channeldata_default';

        return "<div class = '" + className + "' title = '" + tooltip + "'>" + cellText + "</div>";
    };

    ctor.prototype.__getRow = function(columns, channels) {
        var row = [];
        
        if (!!columns) {
            
            var chanId = null;
            
            for ( var i = 0, l = columns.length; i < l; i++) {
                var colName = columns[i].name;
                
                if(colName == "channelName") {
                    chanId = channels["ID"][0];
                    if (!!columns[i].mouseOverContent[0] && !!this.__groupChannels[chanId]) {
                        // Add the channel name cell content
                        row.push([$.i18n(this.__groupChannels[chanId][columns[i].displayField]), 1, $.i18n(this.__groupChannels[chanId][columns[i].mouseOverContent[0].value])]);
                    } else if (!!this.__groupChannels[chanId]) {
                    	row.push([$.i18n(this.__groupChannels[chanId].description), 1]);
                    }                    	
                    else {
                        row.push([ null, 0 ]);
                    }

                } else if (colName == "alwaysDisplayed") {
                    chanId = channels["ID"][0];
                    if (!!this.__groupChannels[chanId]) {
                        // Add the hidden alwaysDisplayed cell content
                        row.push([this.__groupChannels[chanId].alwaysDisplayed, 1]);
                    } else {
                        row.push([ null, 0 ]);
                    }
                    
                } else {
                    // A channel value
                    var channel = channels[colName];
                    
                    if (!!channel) {
                        
                        // Check if that's a timestamp to format
                        if (!!chanId && !!this.__groupChannels[chanId]) {
                            var type = this.__groupChannels[chanId].type;
                            if (type == 5 && !!channel[0]) {
                                // It's a timestamp
                                
                                var formatStr = "{date}";
                                
                                if (!!this.__groupChannels[chanId].formatPattern) {
                                    formatStr = "{date:"+this.__groupChannels[chanId].formatPattern+"}";
                                }

                                if (!isNaN(channel[0])) {
                                    channel[0] = $.format(formatStr, Number(channel[0]));
                                }
                            }
                        }
                        
                        row.push(channel);
                    } else {
                        row.push([ null, 0 ]);
                    }
                }
                
            }
        }

        return row;
    };

    ctor.prototype.getTimestamp = function() {
        if (!!this.__timePicker) {
            return this.getTimePicker().getTime();
        } else {
            return this.__timestamp;
        }
    };
    
    ctor.prototype.setTimestamp = function(timestamp) {
        if (!!this.__timePicker) {
            this.getTimePicker().setTime(timestamp);
        }
        else {
            this.__unitSummary.setTimestamp(timestamp);
        }
        this.__timestamp = timestamp;
    };
    
    ctor.prototype.isPaused = function() {
        return this.getToggleButton().isPaused();
    };
    
    ctor.prototype.isLive = function() {
        return this.getTimePicker().isLive();
    };

    ctor.prototype.getToggleButton = function() {
        if (!!this.__toggleButton) {
            return this.__toggleButton.data('obj');
        } else {
            return null;
        }
    };

    ctor.prototype.getTimePicker = function() {
        return this.__timePicker.data('obj');
    };
    
    ctor.prototype._logging = function() {
        // logs the unit summary - channel data access
        nx.rest.logging.unitChannelData();
    };

    ctor.prototype._hasTimeController = function() {
        return true;
    };

    ctor.prototype._update = function(data) {
    	var me = this;
    	
    	if(!!me.__updatingUnit && !data.unitChanged()) {
        	return;
        }
    	
    	if (data.unitChanged()) {
    		me.__updatingUnit = true;
          	var selectedTab = 0;
            if (!!data.sameFormation) {
            	selectedTab = $('#cdChannelGroups').tabs('option', 'selected');
            }
            
            me.__fleetId = data.fleetId;
            me.__unitId = data.unitId;
            me.__timestamp = data.timestamp;
            
            $('#cdChannelGroups').tabs( "destroy" );
            $('#cdChannelGroups').empty();
            
            nx.rest.group.all(me.__fleetId,function(groups) {
                var toAdd = '<ul>';
                
                for (var i = 0; i < groups.length; i++) {
                    var grp = groups[i];
                    toAdd += '<li><a href="#cdTab_'+grp.id;
                    toAdd += '">'+$.i18n(grp.description)+'</a></li>';
                    toAdd += '<div id="cdTab_'+ grp.id+'"></div>';
                
                }
                
                toAdd += '</ul>';
                $('#cdChannelGroups').html(toAdd);
                me.__buildChannelGroupTabs();
                
                $('#cdChannelGroups').tabs('select', selectedTab);
                
                me.__groupId = groups[selectedTab].id;

                /*
                 * The FORWARD and BACKWARD direction are used only if the
                 * time/record controller type is set to RECORD and only when the 
                 * previous/next record buttons are pressed.
                 * The normal navigation is by TIME.
                */
                
                var timestamp = data.timestamp == "" ? 0 : data.timestamp;
                me.__updatingUnit = false;
                
            });

    	} else {
            this.__id = data.unitId;
            this.__timestamp = data.timestamp;
            this.__fleetId = data.fleetId;
            
            var direction = !!data.direction? data.direction: 0;

            this.__updateTable(data.fleetId, data.unitId, this.getTimestamp(), this.__groupId, data.unitChanged(), direction, false);
    	}
    };

    return ctor;
})();
