var nx = nx || {};
nx.ea = nx.ea || {};

nx.ea.EventAnalysis = (function() {
    function ctor() {
        var me = this;

        this.__data = null;
        this.__groupings = null;
        this.__selectedGrouping = null;
        this.__filters = null;

        this.__tabs = null;
        this.__filterDialog = null;
        this.__reportContents = null;
        this.__reportTable = null;
        this.__reportDetailTable = null;

        this.__eaTabs = $("#eventAnalysisTabs");
        this.__eaFilterLabel = $("#eaFilterLabel");
        this.__eaFilterButton = null;
        this.__eaSearchButton = null;
        this.__eaExpandButton = $("#eaExpandButton");
        
        this.__filtersExpanded = false;
        this.__filtersSet = false;
        this.__expanded = false;
        
        //tabs
        this.__tabs = this.__eaTabs.tabs();
        
        nx.rest.eventanalysis.getGroupings(function(groupings){
            me.__setGroupings(groupings);
            me.__initialize();
        });
    };

    ctor.prototype.__initialize = function() {
        var me = this;
        
        //search button
	    this.__eaSearchButton = $('<button title = "'+$.i18n('Search')+'">'+$.i18n('Search')+'</button>').button({
            icons: {
                secondary: 'ui-icon-search'
            },
            disabled: true
        }).addClass('ui-button-primary');

        //filter dialog
        this.__filterDialog = new nx.ea.FilterDialog(this, function() {
        	// Enable the search button only once the filters are initialized 
        	me.__eaSearchButton.button('enable');
        });

        var filterIds = '';
        for (var key in this.__filterDialog) {
            if ((this.__filterDialog.hasOwnProperty(key)) && (typeof this.__filterDialog[key] === 'string')) {
                filterIds += ('#eaFilter' + this.__filterDialog[key] + 'Text, ');
            }
        }
        filterIds = filterIds.substring(0, filterIds.length - 2);
        //report table
        this.__reportTable = new nx.ea.ReportTable(this, $("#eaReportTablePanel"));

        //report detail table
        this.__reportDetailTable = new nx.ea.ReportDetailTable(this, $("#eaReportDetailTablePanel"));

        //filter button
        this.__eaFilterButton = $('<button title = "'+$.i18n('Show Filters')+'"></button>').button({
            icons: {
                primary: 'ui-icon-triangle-1-s'
            },
            text: false
        })

	    this.__eaFilterButton.on({
            'click': (function(me) {
                return function(eventObj) {
                	if (!me.__filtersExpanded) {
                        $(this).attr("title", $.i18n("Hide Filters"));
                        $(this).button("option", {
                            icons: { primary: "ui-icon-triangle-1-n" }
                        });
                		me.__filterDialog._open();
                		me.__filtersExpanded = true;
                		me.__filtersSet = true;
                	} else {
                        $(this).attr("title", $.i18n("Show Filters"));
                        $(this).button("option", {
                            icons: { primary: "ui-icon-triangle-1-s" }
                        });
                        me.__filterDialog._close();
                        me.__filtersExpanded = false;
                	}
                };
            })(me)
        });
        
	    $("#eaFilterButton").append(this.__eaFilterButton);
	    
	    // Search button click handler
        this.__eaSearchButton.click(function(e) {
        	me.__eaFilterButton.attr("title", $.i18n("Show Filters"));
        	me.__eaFilterButton.button("option", {
                icons: { primary: "ui-icon-triangle-1-s" }
            });
            me.__filterDialog._close();
            me._update(me.__filterDialog.getFilters());
	    	me.__filtersExpanded = false;
        });
        
	    
	    
	    $(document).keypress(function(e) { //If enter is pressed inside input box
	        if(e.which == 13) {
	            if ((filterIds.includes(e.target.id)) && (!e.target.id == '')) {
	                me.__eaFilterButton.attr("title", $.i18n("Show Filters"));
	                me.__eaFilterButton.button("option", {
	                    icons: { primary: "ui-icon-triangle-1-s" }
	                });
	                me.__filterDialog._close();
	                me._update(me.__filterDialog.getFilters());
	                me.__filtersExpanded = false;
	            }
	        }
	    });

	    $("#eaSearchButton").append(this.__eaSearchButton);
        
	    //expand button
        this.__eaExpandButton.on({
            'click': (function(me) {
                return function(eventObj) {
                    if (me.__expanded == false) {
                        me.__expanded = true;
                        
                        $("#eaReportTablePanel").hide();
                        $("#eaReportContentPanel").height('100%');
                        $("#eaExpandButton").css("background", "url('../img/icon_less.png') no-repeat").attr('title', $.i18n('Collapse'));
                        me.__selectGrouping(me.__selectedGrouping);
                    }
                    else {
                        me.__expanded = false;

                        $("#eaReportTablePanel").show();
                        $("#eaExpandButton").css("background", "url('../img/icon_plus.png') no-repeat").attr('title', $.i18n('Expand'));

                        me.__selectGrouping(me.__selectedGrouping);
                    }
                };
            })(me)
        });

        //tabs
        this.__tabs.tabs('disable');
        this.__tabs.tabs('option', 'show', function(event, ui) {
                me.__selectGrouping(ui.tab.hash.substr(7));
            }
        );
        
        var group = me.__tabs.find("ul>li>a:first").attr("href").substr(7);
        me.__selectGrouping(group);
    };

    ctor.prototype.__setGroupings = function(groupings) {
        var me = this;

        this.__groupings = {};
        this.__reportContents = {};

        for (var index in groupings) {
            var grouping = groupings[index];

            grouping.getColumn = function(columnName) {
                for (var i = 0; i < this.columns.length; i++) {
                    if (this.columns[i].name.toLowerCase() == columnName.toLowerCase()) {
                        return this.columns[i];
                    }
                }

                return null;
            };

            grouping.getColumnPosition = function(columnName) {
                for (var i = 0; i < this.columns.length; i++) {
                    if (this.columns[i].name.toLowerCase() == columnName.toLowerCase()) {
                        return i;
                    }
                }

                return null;
            };

            if (!this.__reportContents[grouping.rendererObject]) {
                var renderer = grouping.rendererObject.split('.');

                var rendererObject = window || this;
                for (var iRenderer in renderer) {
                    rendererObject = rendererObject[renderer[iRenderer]];
                }

                if (typeof rendererObject !== 'function') {
                    throw new Error("Ojbect " + grouping.rendererObject + " not found");
                } else {
                    this.__reportContents[grouping.rendererObject] = new rendererObject(me);
                }
            }

            this.__groupings[grouping.code] = grouping;
        }
        
        
    };

    ctor.prototype.__selectGrouping = function(groupingCode) {
        this.__selectedGrouping = groupingCode;

        this.__reportDetailTable.hide();

        var grouping = this.__groupings[groupingCode];
        var data = !!this.__data ? this.__data[groupingCode] : null; 

        for (var iRenderer in this.__reportContents) {
            if (iRenderer != grouping.rendererObject) {
                this.__reportContents[iRenderer].hide();
            }
        }
        
        if (!this.__expanded) {
            if (!!grouping.contentSize && grouping.contentSize >= 0 && grouping.contentSize <= 100) {
                var tableSize = 100 - grouping.contentSize;
                
                $("#eaReportContentPanel").height(grouping.contentSize + '%');
                $("#eaReportTablePanel").height(tableSize + '%');
            }
            else {
                $("#eaReportContentPanel").height('50%');
                $("#eaReportTablePanel").height('50%');
            }
        }

        this.__reportContents[grouping.rendererObject].update(data, grouping, this.__filters);
        this.__reportTable.update(grouping, data);
    };

    ctor.prototype.openReportDetailData = function(data) {
        this.__reportDetailTable.show(data);
    }

    ctor.prototype.openReportDetail = function(referenceKey) {
        var me = this;
        
        var filters = this.__filterDialog.getFilters();

        filters.grouping = this.__selectedGrouping;
        filters.referenceKey = !!referenceKey ? referenceKey : null;

        nx.rest.eventanalysis.searchDetailData(filters, function(data){
            me.__reportDetailTable.show(data);
            me.__eaExpandButton.hide();
        });
    };

    ctor.prototype._update = function(filters) {
        var me = this;

        this.__filters = filters;

        var label = filters.toString();
        var shortLabel = label;
        if (label.length > 600) {
        	shortLabel = label.substring(0, 600) + "...";
        }
        
        this.__eaFilterLabel.text(shortLabel);
        this.__eaFilterLabel.attr("title", $.i18n(label));
        
        this.__reportDetailTable.hide();

        $("body").addClass("wait");
        
        filters.timezone = jstz.determine().name().replace('/', '-');
        nx.rest.eventanalysis.searchData(filters, function(data){
        	$("body").removeClass("wait");
            me.__setData(data);
            
            if (me.__data[me.__selectedGrouping].records.length == 0) {
            	$('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('Notification'),
                    modal: true
                  }).text($.i18n('The search returned no results.'));
            }
            
        });
    };

    ctor.prototype.__setData = function(groupingData) {
        for (var groupingCode in groupingData) {
            var grouping = this.__groupings[groupingCode];
            var referenceColumn = grouping.getColumn("referenceValue");

            groupingData[groupingCode] = { 'records': groupingData[groupingCode], 'reference': {} };
            var data = groupingData[groupingCode].records;
            var reference = groupingData[groupingCode].reference;

            for (var iRecord in data) {
                var record = data[iRecord];

                if (record.referenceValue != null) {
                    var referenceValue = record.referenceValue;
                    if (referenceColumn.type == 'timestamp') {
                        referenceValue = $.format("{date:DD/MM/YYYY}", parseFloat(referenceValue));
                        record.referenceValue = referenceValue;
                    }
                    reference[referenceValue] = record.referenceKey;
                }
            }
        }

        this.__data = groupingData;

        var grouping = this.__groupings[this.__selectedGrouping];
        var data = this.__data[this.__selectedGrouping]; 

        this.__reportContents[grouping.rendererObject].update(data, grouping, this.__filters);
        this.__reportTable.update(grouping, data);
    };

    return ctor;
}());

//event analysis access logging
nx.rest.logging.eventAnalysis();

$(document).ready(function() {
    new nx.ea.EventAnalysis();
});