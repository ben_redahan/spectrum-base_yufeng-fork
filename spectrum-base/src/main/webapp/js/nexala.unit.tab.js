var nx = nx || {};
nx.unit = nx.unit || {};

/** A UnitSummary tab */
nx.unit.Tab = (function() {
    function ctor(unitsummary) {
        this.__unitsummary = unitsummary;
        this.__unit = null;
        this.__timestamp = null;
    };
    
    ctor.prototype.getUnitSummary = function() {
        return this.__unitsummary;
    };
    
    /**
     * @param data an object containing
     */
    ctor.prototype._update = function(data) {};
    
    ctor.prototype.getTimestamp = function() {
        return this.__timestamp;
    };
    
    ctor.prototype.getUnit = function() {
        return this.__unit;
    };
    
    ctor.prototype.setTimestamp = function(timestamp) {
        this.__timestamp = timestamp;
    };
    
    ctor.prototype.setUnit = function(unit) {
        this.__unit = unit;
    };
    
    ctor.prototype._onShow = function() {
    };
    
    ctor.prototype._onHide = function() {
    };
    
    /**
     * If the tab need update even if the unit and timestamp didn't changed
     */
    ctor.prototype.needUpdate = function() {
    	return false;
    };

    return ctor;
})();