var nx = nx || {};
nx.map = nx.map || {};
nx.map.marker = nx.map.marker || {};

nx.map.marker.StationMarker = (function() {
    function ctor(data, icon) {
        nx.map.marker.Marker.call(this, data);
        this.icon = icon;
    };

    /** Extend nx.map.marker.Marker */
    ctor.prototype = new nx.map.marker.Marker();

    /*
     *  Return a Microsoft.Maps.Pushpin for this marker.
     *  Used to add the marker to the map
     */
    ctor.prototype.getPushPin = function() {
    	
    	var location = new Microsoft.Maps.Location(this._data.coordinate.latitude, this._data.coordinate.longitude);
        
        // Add a pin to the map, using a custom icon
        var pin = new Microsoft.Maps.Pushpin(location, {
            'icon': this.icon,
            'anchor': new Microsoft.Maps.Point(8,8)
        });

        pin._id = this._data.code;

        return pin;
    };
    
    
    /*
     * return a Microsoft.Maps.Infobox for this marker.
     * Used to create the marker popup
     */
    ctor.prototype.getPopup = function() {
    	var location = new Microsoft.Maps.Location(this._data.coordinate.latitude, this._data.coordinate.longitude);
    	
        var lat = parseFloat(this._data.coordinate.latitude);
        var lng = parseFloat(this._data.coordinate.longitude);
        lat = lat.toFixed(4);
        lng = lng.toFixed(4);
        var desc = "<b>"+$.i18n("Coordinates")+":</b> " + lat + ", " + lng;
        
        var infobox = new Microsoft.Maps.Infobox(location, {
            'title': this._data.name, 
            'visible': false,
            'showPointer': true,
            'showCloseButton': false,
            'offset': new Microsoft.Maps.Point(0,5),
            'description': desc,
            'height': 80,
            'width': 250,
            'zIndex': 999,
            'id': 'id' + this._data.code
        });
        return infobox;
    };

    return ctor;
})();


nx.map.marker.StationMarkerProvider = (function() {
    function ctor(providerDesc) {
        nx.map.marker.MarkerProvider.call(this, providerDesc);
        
        if (!!providerDesc.config) {
        	this._config = JSON.parse(providerDesc.config);
        }
    };

    /** Extend nx.unit.diagram.Provider */
    ctor.prototype = new nx.map.marker.MarkerProvider();
    
    /** Return the div for the legend of this kind of marker */
    ctor.prototype.getLegend = function() {
        return "";
    };
    
    ctor.prototype.setStations = function(stations) {
        this.__stations = [];
        
        var type;
    	if (!!this._config) {
			// Get the icon if defined
			if (!!this._config.type) {
				type = this._config.type;
			}
		} 
    	
    	for(var station in stations) {
    		// if no type is define, return all the stations otherwise only the stations with this particular type
    		if (typeof type == 'undefined' || type == stations[station].type)  {
    			this.__stations.push(stations[station]);
    		}	
        }
        
    };
    
    /** Should return an array of markers*/ 
    ctor.prototype.getMarkers = function(targetBounds, zoom, time, callback) {
        var me = this;
        var markers = [];
        var icon = 'img/station.gif';
        var type;
        if (!!this._config) {
            // Get the icon if defined
            if (!!this._config.icon) {
                icon = this._config.icon;
            }
        }

        // only push stationpins if custom station (and bridge) icon is loaded
        var iconTester = new Image;
        iconTester.onload = function() {
            for(var station in me.__stations) {
                markers.push(new nx.map.marker.StationMarker(me.__stations[station], icon));          
            }
            
            callback(markers);
        }

        iconTester.src = icon;
    };

    return ctor;
})();

