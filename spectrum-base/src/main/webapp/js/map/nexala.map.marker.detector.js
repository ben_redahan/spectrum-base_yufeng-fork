var nx = nx || {};
nx.map = nx.map || {};
nx.map.marker = nx.map.marker || {};

nx.map.marker.DetectorMarker = (function() {
    function ctor(data, icon) {
        nx.map.marker.Marker.call(this, data);
        this.icon = icon;
    };

    /** Extend nx.map.marker.Marker */
    ctor.prototype = new nx.map.marker.Marker();

    /*
     *  Return a Microsoft.Maps.Pushpin for this marker.
     *  Used to add the marker to the map
     */
    ctor.prototype.getPushPin = function() {
    	
    	var location = new Microsoft.Maps.Location(this._data.latitude, this._data.longitude);
        
        // Add a pin to the map, using a custom icon
        var pin = new Microsoft.Maps.Pushpin(location, {
            'icon': this.icon,
            'anchor': new Microsoft.Maps.Point(8,8)
        });

        pin._id = this._data.code;

        return pin;
    };
    
    
    /*
     * return a Microsoft.Maps.Infobox for this marker.
     * Used to create the marker popup
     */
    ctor.prototype.getPopup = function() {
    	var location = new Microsoft.Maps.Location(this._data.latitude, this._data.longitude);
    	var id = this._data.id;
    	var siteName = this._data.siteName;
    	var tracks = this._data.tracksBetween;
    	var detA_id = this._data.detectors[0].id;
        var detA_from = this._data.detectors[0].from;
        var detA_to = this._data.detectors[0].to;
    	var detB_id = null;
    	var detB_from = null;
    	var detB_to = null;
    	var descB = "";
    	
    	if(this._data.detectors.length > 1){
            detB_id = this._data.detectors[1].id;
            detB_from = this._data.detectors[1].from;
            detB_to = this._data.detectors[1].to;
            descB = "<div >" + $.i18n("ID: ") + detB_id + ", " + detB_from + " - " + detB_to + " </div>";
    	}
    	
        var description =  [
            "<div ><b>", $.i18n("ID: "), "</b>", id, "</div>",
            "<div ><b>", $.i18n("Name: "), "</b>", siteName, "</div>",
            "<div ><b>", $.i18n("Tracks: "), "</b>", tracks, "</div>",
            "<div ><b>", $.i18n("Detectors:"), "</b>", "</div>",
            "<div >", $.i18n("ID: "), "", detA_id + ", " + detA_from + " - " + detA_to, " </div>",
            descB
        ].join('');        
        
        
        var infobox = new Microsoft.Maps.Infobox(location, {
            'visible': false,
            'showPointer': true,
            'showCloseButton': false,
            'offset': new Microsoft.Maps.Point(0,5),
            'description': description,
            'height': 80,
            'width': 250,
            'zIndex': 999,
            'id': 'id' + this._data.code
        });
        return infobox;
    };

    return ctor;
})();


nx.map.marker.DetectorMarkerProvider = (function() {
    function ctor(providerDesc) {
        nx.map.marker.MarkerProvider.call(this, providerDesc);
        
        if (!!providerDesc.config) {
        	this._config = JSON.parse(providerDesc.config);
        }
    };

    /** Extend nx.unit.diagram.Provider */
    ctor.prototype = new nx.map.marker.MarkerProvider();
    
    /** Return the div for the legend of this kind of marker */
    ctor.prototype.getLegend = function() {
        return "";
    };
    
    ctor.prototype.setDetectors = function(detectors) {
        this.__detectors = [];
        
        var type;
    	if (!!this._config) {
			// Get the icon if defined
			if (!!this._config.type) {
				type = this._config.type;
			}
		} 
    	
    	for(var detector in detectors) {
    		// if no type is define, return all the stations otherwise only the stations with this particular type
    		if (typeof type == 'undefined' || type == detectors[detector].type)  {
    			this.__detectors.push(detectors[detector]);
    		}	
        }
        
    };
    
    /** Should return an array of markers*/ 
    ctor.prototype.getMarkers = function(targetBounds, zoom, time, callback) {
        var me = this;
        var markers = [];
        var icon = 'img/station.gif';
        var type;
        if (!!this._config) {
            // Get the icon if defined
            if (!!this._config.icon) {
                icon = this._config.icon;
            }
        }
        
        nx.rest.map.markers(this._type, 
            targetBounds.getNorthwest().latitude,
            targetBounds.getNorthwest().longitude,
            targetBounds.getSoutheast().latitude,
            targetBounds.getSoutheast().longitude,
            zoom,
            null,
            function(response) {
                me.setDetectors(response);
                
                var iconTester = new Image;
                iconTester.onload = function() {
                    for(var detector in me.__detectors) {
                        markers.push(new nx.map.marker.DetectorMarker(me.__detectors[detector], icon));          
                    }
                    
                    callback(markers);
                }

                iconTester.src = icon;
        });
    };

    return ctor;
})();

