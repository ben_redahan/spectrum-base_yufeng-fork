var nx = nx || {};
nx.map = nx.map || {};
nx.map.marker = nx.map.marker || {};

nx.map.marker.AdhesionMarker = (function() {
    function ctor(data, legend) {
        nx.map.marker.Marker.call(this, data, legend);
    };

    /** Extend nx.map.marker.Marker */
    ctor.prototype = new nx.map.marker.Marker();

    /*
     *  Return a Microsoft.Maps.Pushpin for this marker.
     *  Used to add the marker to the map
     */
    ctor.prototype.getPushPin = function() {
        var location = new Microsoft.Maps.Location(this._data.coordinate.latitude, this._data.coordinate.longitude);

        // Add a pin to the map, using a custom icon
        var pushPin = new Microsoft.Maps.Pushpin(location, {
            'icon': this.getAdhesionIcon(this._data.count, this._data.bearing, this._data.category),
            'anchor': new Microsoft.Maps.Point(8,8)
        });
        
        
        return pushPin;
    };
    
    
    /*
     * return a Microsoft.Maps.Infobox for this marker.
     * Used to create the marker popup
     */
    ctor.prototype.getPopup = function() {
        var location = new Microsoft.Maps.Location(this._data.coordinate.latitude, this._data.coordinate.longitude);
        
        
        var track1Desc = 'Count: ';
        var mostRecentActivityLabel = 'Last Update: ';
        var lastUpdate = new Date().getTime();

        var description =  [
        	'<div class = \'mapPopupHeader\' > ' + this._legend.label + ' Activity</div>',
            "<div class = 'mapPopupBody'>",
            "<div class = 'mapPopupBodyRow'><b>", track1Desc, "</b>", this._data.count, " counts </div>",
            "<div class = 'mapPopupBodyRow'><b>", mostRecentActivityLabel, "</b>", $.format("{date}", lastUpdate), "</div>",
            "</div>"
        ].join('');
        
        
        var infobox = new Microsoft.Maps.Infobox(location, {
            'visible': false,
            'showPointer': true,
            'showCloseButton': false,
            'offset': new Microsoft.Maps.Point(0,5),
            'description': description
        });
        
        return infobox;
    };
    
    ctor.prototype.getAdhesionIcon = function(count, bearing, category) {
        
        var color = 'black';
        var lowerThreshold = 0;
        var upperThreshold = 0;

        $.each(this._legend.options, function(index, option) {
            if (option.category == category) {
                color = option.color;
                lowerThreshold = option.lowerThreshold;
                upperThreshold = option.upperThreshold;
                return false;
            }
        });
        
        var height = 8;
        var width = 8;
        
        if (count > upperThreshold) {
            width = 20;
        } else if (count > lowerThreshold) {
            width = 12;
        }

        if (this.supportsSvg()) {
            return this.getSvgAdhesionIcon(height, width, bearing, color);
        } else {
            return this.getVmlAdhesionIcon(height, width, bearing, color);
        }
    };
    
    ctor.prototype.supportsSvg = function() {
        if (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1")) {
            return true;
        }

        return false;
    };
    
    /* Get vml icon */
    ctor.prototype.getVmlAdhesionIcon = function(height, width, bearing, colour) {

        if (bearing != null) {
            bearing += 180;
        }


        return [
                "<v:oval fillcolor = '", colour, "' style = 'height:", height, "px; width:",
                width, "px; rotation:", bearing,"; position: absolute; behavior: url(#default#VML);'></v:oval>"
            ].join("");
    };

    /* Get svg icon */
    ctor.prototype.getSvgAdhesionIcon = function(height, width, bearing, colour) {

        /* pointer-events: all is needed because it's not added to the  pushpin by bing */
        var svg = [
            "<svg style='pointer-events: all' width = '", width, "' height = '", width, "' version = '1.1' xmlns = 'http://www.w3.org/2000/svg'>",
                "<ellipse cx = '", width/2, "' cy = '", width/2, "' rx = '", width/2,
                    "' ry = '", height/2, "' stroke = 'black' stroke-width = '1' fill = '", colour,
                    "' transform = 'rotate(", bearing, " ", width/2, ", ", width/2, ")", "'/>",
            "</svg>"
        ].join("");

        return svg;
    };

    return ctor;
})();


nx.map.marker.AdhesionMarkerProvider = (function() {
    function ctor(providerDesc) {
        nx.map.marker.MarkerProvider.call(this, providerDesc);
    };
    


    /** Extend nx.unit.diagram.Provider */
    ctor.prototype = new nx.map.marker.MarkerProvider();
    
    
    /** Return the div for the legend of this kind of marker */
    ctor.prototype.getLegend = function() {
        
        if ($('#' + this._legend.id))
        var result = $('<div>').attr('id', this._legend.id);
        
        var table = $('<table>');
                        
        var header = $('<tr>');
        header.append($('<th>'));
        
        $.each(this._legend.options, function(index, opt) {
            header.append('<th>' + opt.label + '</th>');
        });
        
        table.append(header);
        
        var firstRow = $('<tr>');
        firstRow.append('<td>Show</td>');
        
        $.each(this._legend.options, function(index, opt) {
            var checkbox = $('<input>')
                .attr('id', opt.id)
                .attr('type', 'checkbox')
                .attr('tabindex', index)
                .addClass('legendOption');
            
            if (opt.enabled) {
                checkbox.attr('checked', 'checked');
            }
            
            var col = $('<td>');
            col.append(checkbox);
            firstRow.append(col);
        });
        
        table.append(firstRow);
        table.append(this.getMarkerRow(this._legend, true));
        
        var optNumber = this._legend.options.length;
        
        if (!!this._legend.exportable) {
            var exportRow = $('<tr>');
            exportRow.append('<td>Export</td>');
            var content = $('<td>').attr('colspan', optNumber);
            var img = $('<img>')
                .attr('id', 'exportMarkerCsv')
                .addClass('exportIcon')
                .attr('src', 'img/export_excel.png')
                .attr('alt', '');
            var text = $('<span>')
                .addClass('caption')
                .text('csv');
            
            content.append(img);
            content.append(text);
            exportRow.append(content);
            table.append(exportRow);
        }
        
        var lastRow = $('<tr>');
        lastRow.append($('<td>')
                .attr('colspan', optNumber + 1)
                .text('Hover over the boxes for details.')
                .css({
                    'font-size': '9.5px'
                }));
        table.append(lastRow);
        
        result.append(table);
        
        return result;
    };
    
    ctor.prototype.getMarkerRow = function(conf, firstRow) {
        var markerRow = $('<tr>');
        
        if(!!firstRow) {
            markerRow.attr('id', 'firstMarkerRow');
        }
        
        markerRow.append('<td>' + conf.label + '</td>');
        
        $.each(conf.options, function(index, opt) {
            var col = $('<td>');
            var content = $('<span>')
                .addClass('legendColorBox')
                .css({
                    'background-color': opt.color
                });
            
            var textMouseover = "Thresholds:\n" 
                + "S: 0-" + opt.lowerThreshold + "\n"
                + "M: " + (parseInt(opt.lowerThreshold) + 1) + "-" + (opt.upperThreshold) + "\n"
                + "L: " + (parseInt(opt.upperThreshold) + 1) + "+";
            
            content.attr("title", textMouseover);
            col.append(content);
            markerRow.append(col);
        });
        
        return markerRow;
    }

    ctor.prototype.initLegend = function(map) {
        
        var me = this;
        
        $('#' + this._legend.id).on("hideDetail", function(event){
            $('#' + me._legend.id).css("right", "10px");
        });
        
        $('#' + this._legend.id).on("showDetail", function(event){
            var right = event.message.width() + 15;
            $('#' + me._legend.id).css("right", right + "px");
        });
        
        $('.legendOption').click(function() {
            Microsoft.Maps.Events.invoke(map, "viewchangeend");
        });

        $('#exportMarkerCsv').click(function() {
            
            var categories = [];
            
            $.each(me._legend.options, function(index, opt) {
                if ($('#' + opt.id).is(':checked')) {
                    categories.push(opt.category);
                }
            });
            
            me.__downloadReport(map.getBounds(), categories);
        });
    };
    
    ctor.prototype.updateLegend = function() {
        var newRow = this.getMarkerRow(this._legend, false);
        newRow.insertAfter($('#firstMarkerRow'));
    };

    /** Should return an array of markers*/ 
    ctor.prototype.getMarkers = function(targetBounds, zoom, time, callback) {
        var me = this;
        var categories = '';
        
        var firstCategory = true;
        $.each(this._legend.options, function(index, opt) {
            if ($('#' + opt.id).is(':checked')) {
                if (!firstCategory) {
                    categories += ',';
                } else {
                    firstCategory = false;
                }
                categories += opt.category;
            }
        });

        if (categories != '') {
            nx.rest.map.markers(this._type, 
                targetBounds.getNorthwest().latitude,
                targetBounds.getNorthwest().longitude,
                targetBounds.getSoutheast().latitude,
                targetBounds.getSoutheast().longitude,
                zoom,
                categories,
                
                function(response) {
                    var markers = new Array();
                    
                    for( var i in response) {
                    	try {
                    		markers.push(new nx.map.marker.AdhesionMarker(response[i], me._legend));
                    	} catch (err) {
                    		// If this marker can't be pin, just skip it 
                    	}
                        
                    }
                    
                    callback(markers);
            });
        } else {
            callback([]);
        }        
    };
    
    
    ctor.prototype.__downloadReport = function(targetBounds, categories) {

        var url = '/rest/map/exportMarkers'+
        '?markerType=' + this._type +
        '&leftTopLatitude=' + targetBounds.getNorthwest().latitude +
        '&leftTopLongitude=' + targetBounds.getNorthwest().longitude +
        '&rightBottomLatitude=' + targetBounds.getSoutheast().latitude +
        '&rightBottomLongitude=' + targetBounds.getSoutheast().longitude;
        
        $.each(categories, function(index, value) {
                url += '&category='+value;
        });
        
        nx.util.download(url);
    };

    return ctor;
})();

