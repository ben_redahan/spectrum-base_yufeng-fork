var translationNl = {
	"Unit search (Ctrl+S)" : "Treinstel Zoeken",
	"Profile": "Profiel",
	"Manage users": "Beheren Gebruikers",
	"Logout": "Uitloggen",
	"Fleet Summary": "Vlootoverzicht",
	"Unit Summary": "Treinsteloverzicht",
	"Fleet Location": "Vlootlocatie",
	"Event History": "Event Historie",
	"Event Analysis": "Event Analyse",
	"Rules Editor": "Rules Editor",
	"Channel Definition": "Channel Definitie",
	"Administration": "Beheer",
	"Data View": "Data Overzicht",
	"Unit No": "Treinstelnummer",
	"$1 {{PLURAL:$1|year|years}} ago": "$1 {{PLURAL:$1|jaar|jaren}} geleden",
	"$1 {{PLURAL:$1|month|months}} ago": "$1 {{PLURAL:$1|maand|maanden}} geleden",
	"$1 {{PLURAL:$1|week|weeks}} ago": "$1 {{PLURAL:$1|week|weken}} geleden",
	"$1 {{PLURAL:$1|day|days}} ago": "$1 {{PLURAL:$1|dag|dagen}} geleden",
	"$1 {{PLURAL:$1|hour|hours}} ago": "$1 {{PLURAL:$1|uur|uren}} geleden",
	"$1 {{PLURAL:$1|minute|minutes}} ago": "$1 {{PLURAL:$1|minuut|minuten}} geleden",
	"$1 {{PLURAL:$1|second|seconds}} ago": "$1 {{PLURAL:$1|seconde|seconden}} geleden",
	"Unit": "Treinstel",
	"Vehicle": "Bak",
	"Time": "Tijd",
	"Location": "Locatie",
	"Description": "Beschrijving",
	"Type": "Ernst",
	"Channel Type": "Type signaal",
	"Category": "Categorie",
	"LIVE EVENTS": "Live Events",
	"RECENT EVENTS": "Recente Events",
	"ACKNOWLEDGED": "Behandeld",
	"NOT ACKNOWLEDGED": "Niet Behandeld",
	"Fleet": "Vloot",
	"Priority": "Prioriteit",
	"Clear": "Wis",
	"Apply": "Toepassen",
	"Filters": "Filters",
	"Service Info": "Inzet Info",
	"Unit 1": "Treinstel 1",
	"Unit 2": "Treinstel 2",
	"Unit 3": "Treinstel 3",
	"Last Update Time": "Laatste Contact",
	"GPS Coordinates": "GPS Coördinaten",
	"Speed": "Snelheid",
	"Event": "Event",
	"Send to E2M": "Stuur Naar Maximo",
	"&lt; Prev": "&lt; Vorige",
	"Next &gt;": "Volgende &gt;",
	"Error - Unable to send fault": "Fout - niet mogelijk de Serviceaanvraag te verzenden.",
	"Event Detail": "Event Detail",
	"Event Location": "Event Locatie",
	"Date / Time": "Datum / Tijd",
	"End Time": "Eind Tijd",
	"End Date": "Eind Datum",
	"Latitude": "Breedtegraad",
	"Longitude": "Lengtegraad",
	"Event Code": "Event Code",
	"Event Description": "Event Beschrijving",
	"Event Category": "Event Categorie",
	"Position Code": "Positiecode",
	"Severity": "Ernst",
	"URL": "URL",
	"Acknowledged": "Behandeld",
	"Not Acknowledged": "Niet Behandeld",
	"Has Recovery": "Afhandeling Aanwezig",
	"Event Summary": "Event Overzicht",
	"Additional Info": "Extra Info",
	"Comment History": "Opmerkingen Historie",
	"Comment Entry": "Opmerking Toevoegen",
	"Navigation": "Navigatie",
	"Action": "Actie",
	"Info": "Info",
	"Error": "Fout",
	"Event status saved": "Event Status Opgeslagen",
	"Error updating event": "Fout - met bijwerken van event",
	"Submit Comment": "Opmerking Opslaan",
	"Acknowledge": "Accepteren",
	"Please enter a comment first.": "Plaats eerst een opmerking.",
	"Can't save the change, another user did changes to this event.": "Kan de wijziging niet opslaan, een andere gerbuiker heeft dit al gedaan",
    "Can't send the request, another user did changes to this event.": "Can't send the request, another user did changes to this event.",
	"Deactivate": "Deactiveren",
	"Train Number": "Treinnummer",
	"Operations": "Operatie",
	"Traction": "Tractie",
	"Brakes": "Remmen",
	"Doors": "Deuren",
	"Safety": "Veiligheid",
	"HVAC": "Klimaat",
    "Communication": "Communicatie",
    "Toilets": "Sanitair",
    "High Voltage": "Hoogspanning",
    "Low Voltage": "Laagspanning",
    "Air Supply": "Luchtvoorziening",
	"Other": "Overige",
    "Last Updated Time": "Laatste Contact",
    "Diagram": "Omloop",
    "Formation": "Formatie",
    "Leading Vehicle": "Vooroplopende Cabine",
    "Unit Detail": "Treinstel Details",
	"Channel Data": "Channel Data",
	"Data view": "Data Overzicht",
	"Cab": "Cabine",
	"Schematic": "Schematisch",
	"Events": "Events",
	"Counters": "Tellers",
	"TimeStamp": "Tijd",
	"timestamp": "Tijd",
	"Diagram": "Diagram",
	"Unit Number": "Treinstelnummer",
	"LeadingVehicle": "Vooroplopende Cabine",
	"Faults": "Storingen",
	"Warnings": "Warnings",
	"Code": "Code",
	"Date": "Datum",
	"Not Acknowledged Events": "Niet Behandelde Storingen",
	"Acknowledged Events": "Behandelde Storingen",
	"All Except Test": "Alles Behalve Testdata",
	"All": "Alles",
	"Hide Additional": "Verberg Extra's",
	"Show Additional": "Toon Extra's",
	"Chart Name": "Grafiek Naam",
	"This configuration will be permanently deleted. Are you sure?": "Deze configuratie wordt definitief verwijderd. Weet u het zeker?",
	"Please enter chart name": "Voer Grafiek Naam in",
	"System charts may not be modified.": "Systeem grafieken mogen niet bewerkt worden.",
	"Save/Rename": "Opslaan / Naam Wijzigen",
	"Delete": "Verwijderen",
	"Error": "Error",
	"Delete": "Verwijderen",
	"Rename": "Naam Wijzigen",
	"Save As": "Opslaan Als",
	"Yes": "Ja",
	"No": "Nee",
	"groupLead": "groupsleider",
	"grouped": "gegroepeerd",
	"Save": "Opslaan",
	"Cancel": "Annuleren",
	"Show Table": "Toon Tabel",
	"Show Charts": "Toon Grafiek",
	"Refresh Charts": "Ververs Grafiek",
	"Export Chart Data to PDF": "Exporteer data naar PDF",
	"Export Chart Data to CSV": "Exporteer data naar CSV",
	"{{PLURAL:$1|year|years}}": "{{PLURAL:$1|jaar|jaren}}",
	"{{PLURAL:$1|month|months}}": "{{PLURAL:$1|maand|maanden}}",
	"{{PLURAL:$1|week|weeks}}": "{{PLURAL:$1|week|weken}}",
	"{{PLURAL:$1|day|days}}": "{{PLURAL:$1|dag|dagen}}",
	"{{PLURAL:$1|hour|hours}}": "{{PLURAL:$1|uur|uren}}",
	"{{PLURAL:$1|minute|minutes}}": "{{PLURAL:$1|minuut|minuten}}",
	"{{PLURAL:$1|second|seconds}}": "{{PLURAL:$1|seconde|seconden}}",
	"PAUSE": "Pauze",
	"PLAY": "Afspelen",
	"LIVE": "Live",
	"Overview": "Overzicht",
	"Coordinates": "Coordinaten",
	"Live": "Live",
	"Recent": "Recentelijk",
	"Last Updated Time": "Last Updated Time",
	"Formation": "Formation",
	"Leading Vehicle": "Leading Vehicle",
	"GPS": "GPS",
	"Details": "Details",
	"Select a station": "Selecteer station",
	"Settings": "Instellingen",
	"Show Trains": "Toon Treinen",
	"Show Train Labels": "Toon Treinstelnummers",
	"Show Detectors": "Toon Gotcha meetpunten",
	"Aerial": "Satelliet",
	"Show": "Toon",
	"Show Tracks": "Toon Sporen",
	"Show Stations": "Toon Stations",
	"Fault Code": "Storingscode",
	"Summary": "Overzicht",
	"Current": "Huidig",
	"Reporting Only": "Alleen Rapporteren",
	"Advanced Export": "Geavanceerde Export",
	"Advanced Export Events to CSV": "Geavanceerde export van events naar CSV",
	"Export Events to CSV": "Export van events naar CSV",
	"Show Advanced Options": "Toon uitgebreide opties",
	"Expand": "Uitklappen",
	"Hide Advanced Options": "Verberg uitgebreide opties",
	"Show predefined filters": "Toon voorgedefinieerde filters",
	"Predefined Filters": "Voorgedefinieerde filters",
	"Search Events": "Zoek Events",
	"Search": "Zoek",
	"Live w/ Recovery": "Live met afhandelscenario",
	"SR Last 8 Hours": "SA's Aangemaakt, Laatste 8 Uur",
	"SR Created By Rule Last 8 Hours": "Automatisch Aangemaakt SA's, Laatste 8 Uur",
	"Live Ack Without SR Last 8 Hours": "Behandelde Live Events Zonder SA, Laatste 8 Uur",
	"Live w/ Maximo": "Live met SA",
	"Has Maximo SR": "Heeft Maximo SA",
	"Export": "Exporteren",
	"endDate": "Eind datum",
	"Event Group": "Event groep",
	"Vehicle Number": "Baknummer",
	"From": "Van",
	"To": "Tot",
	"Not live": "Niet Live",
	"Keyword": "Trefwoord",
	"Hide Filters": "Verberg filters",
	"Show Filters": "Toon Filters",
	"Collapse": "Dichtklappen",
	"Notification": "Notificatie",
	"The search returned no results.": "Helaas, het zoeken heeft niets opgeleverd.",
	"The export returned no results.": "Er zijn geen resultaten gevonden voor deze exportopdracht.",
	"Period": "Periode",
	"Any": "Iedere",
	"Count": "Aantal",
	"Total Count": "Totaal Aantal",
	"Total Duration": "Totale Duur",
	"Average Duration": "Gemiddelde Duur",
	"Fault Category": "Storingscategorie",
	"Fault Code/Desc": "Storingscode/ Beschrijving",
	"Fault Type": "Storingtype",
	"Create Time": "Stel tijd in",
	"Headcode": "Treinnummer (rit)",
	"Event Data": "Event Gegevens",
	"ID": "ID",
	"Name": "Naam",
	"Group": "Groep",
	"Reference": "Referentie",
	"Unit of Measure": "Eenheid",
	"Display Order": "Weergave Volgorde",
	"Always Displayed": "Altijd Weergeven",
	"Rules Status": "Validatie",
	"Add rule": "Toevoegen Rule",
	"Remove rule": "verwijderen Rule",
	"Channel Definition Details": "Channel Definition Details",
	"Rules": "Regel",
	"Status": "Status",
	"Active": "Actief",
	"Comment": "Opmerkingen",
	"Validations": "Validaties",
	"Add validation": "Validatie Toevoegen",
	"Remove validation": "Validatie Verwijderen",
	"type": "Type",
	"date": "Datum",
	"unitNumber": "Treinstelnummer",
	"headcode": "Treinnummer (rit)",
	"vehicle": "Bak",
	"location": "Locatie",
	"latitude": "Breedtegraad",
	"longitude": "Lengtegraad",
	"faultCode": "Fout Code",
	"category": "Categorie",
	"hasRecovery": "Afhandeling Aanwezig",
	"description": "Beschrijving",
	"summary": "Overzicht",
	"additionalInfo": "Extra Info",
	"current": "Huidig",
	"reportingOnly": "Alleen Rapporteren",
	"acknowledged the event": "heeft event behandeld",
	"deactivated the event": "Deactivated the event",
	"issued a service request": "heeft een service aanvraag gecreëerd",
	"commented": "heeft commentaar toegevoegd",
	"Show Bridges": "Toon Bruggen",
	"setCode" : "Set Code",
	"Show Event History": "Show Event History",
	"RTM-O only": "RTM-O only",
	"Exclude closed WO/SR": "Exclude closed WO/SR",
	"isAcknowledged": "Is Acknowledged",
	"Active Cab": "Active Cab",
	"Show WSP": "Show WSP",
	"Show Sanding": "Show Sanding",
	"Live Events": "Live Events",
	"Recent Events": "Recente Events",
	"You do not have permission to access this.": "U hebt geen toestemming om hier toegang toe te krijgen.",
	"Rule Name": "Rule Name",
	"Date Occurred": "Date Occurred",
	"Show time controller": "Show time controller",
	"endTime": "Eind Tijd",
	"Two Events needed to create Event Group.": "Er moeten minimal twee events geselecteerd worden om een groep te maken.",
	"No lead event selected. Please select an event to flag as the lead of this group.": "Er is geen hoofdevent geselecteerd. Selecteer een event dat leidend is voor deze groep.",
	"Create": "Creëer",
	"Cancel": "Annuleren",
	"Event Group was created.": "Event groep is gecreëerd."



};