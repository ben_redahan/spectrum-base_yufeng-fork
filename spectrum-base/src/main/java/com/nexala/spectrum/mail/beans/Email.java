/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.mail.beans;

import java.util.List;

/**
 * Represents an e-mail template as defined by the editor module.
 * @author Severinas Monkevicius
 */
public class Email {
    /**
     * The e-mail's <code>from</code> address.
     */
    private String from;

    /**
     * The list of recipients the e-mail is intended for.
     */
    private List<Recipient> recipients;

    /**
     * The subject of the e-mail.
     */
    private String subject;

    /**
     * The body of the e-mail.
     */
    private String body;

    /**
     * Returns the e-mail's <code>from</code> address.
     * @return the e-mail's <code>from</code> address.
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the e-mail's <code>from</code> address.
     * @param from/ the e-mail's <code>from</code> address.
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * Returns the list of recipients the e-mail is intended for.
     * @return the list of recipients the e-mail is intended for.
     */
    public List<Recipient> getRecipients() {
        return recipients;
    }

    /**
     * Set the list of recipients the e-mail is intended for.
     * @param recipients the list of recipients the e-mail is intended for.
     */
    public void setRecipients(List<Recipient> recipients) {
        this.recipients = recipients;
    }

    /**
     * Returns the subject of the e-mail.
     * @return the subject of the e-mail.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Set the subject of the e-mail.
     * @param subject the subject of the e-mail.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Returns the body of the e-mail.
     * @return the body of the e-mail.
     */
    public String getBody() {
        return body;
    }

    /**
     * Set the body of the e-mail.
     * @param body the body of the e-mail.
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * Returns the e-mail as a string.
     * @return the e-mail as a string.
     */
    public String toString() {
        return String.format("%s, %s, %s, %s", from, recipients, subject, body);
    }
}
