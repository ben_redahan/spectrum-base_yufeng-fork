/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.mail.beans;

/**
 * A recipient of an e-mail message.
 * @author Severinas Monkevicius
 */
public class Recipient {
    /**
     * The type of the recipient.
     */
    private Type type;
    /**
     * The name of the recipient.
     */
    private String name;

    /**
     * Returns the type of the recipient.
     * @return the type of the recipient.
     */
    public Type getType() {
        return type;
    }

    /**
     * Set the type of the recipient.
     * @param type the type of the recipient.
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Returns the name of the recipient.
     * @return the name of the recipient.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the recipient.
     * @param name the name of the recipient.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the recipient converted into a string.
     * @return the recipient converted into a string.
     */
    public String toString() {
        return String.format("%s %s", type, name);
    }

    /**
     * The valid recipient types.
     * @author Severinas Monkevicius
     */
    public static enum Type {
        /**
         * A user recipient is associated with a single user and thus a single
         * e-mail address.
         */
        USER,

        /**
         * A role recipient is associated with all users that have the role and
         * thus potentially represents multiple actual e-mail addresses.
         */
        ROLE;
    }
}
