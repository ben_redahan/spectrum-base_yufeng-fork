package com.nexala.spectrum.scheduling;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class TimedScheduler implements TimedSchedulerMBean {
    
    private final class Time {
        private final int h;
        private final int m;
        
        public Time(int h, int m) {
            this.h = h;
            this.m = m;
        }
        
        public int getHour() {
            return h;
        }
        
        public int getMinute() {
            return m;
        }
        
        public String toString() {
            return String.format("%02d:%02d", h, m);
        }
    }
    
    private final Logger logger = Logger.getLogger(TimedScheduler.class);

    private String name;
    private String taskModuleClass;
    private String runTimes;
    private boolean startAtStartup;
    private boolean started;
    private Timer scheduler = null;
    private ScheduledTask task = null;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTaskModuleClass() {
        return taskModuleClass;
    }

    @Override
    public void setTaskModuleClass(String taskModuleClass) {
        this.taskModuleClass = taskModuleClass;
    }

    @Override
    public boolean getStartAtStartup() {
        return startAtStartup;
    }

    @Override
    public void setStartAtStartup(boolean startAtStartup) {
        this.startAtStartup = startAtStartup;
    }

    @Override
    public boolean getStarted() {
        return started;
    }

    @Override
    public void start() throws Exception {
        if (getStartAtStartup()) {
            startSchedule();
        }
    }

    @Override
    public void stop() {
        stopSchedule();
    }

    @Override
    public synchronized void startSchedule() {
        if (started || scheduler != null || task != null) {
            return;
        }
        
        List<Time> times = parseRunTimes();

        logger.info(String.format("Starting simple scheduler %s", name));
        
        task = newScheduledTask();
        scheduler = new Timer();
        
        if (task != null) {
            for (final Time time : times) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, time.getHour());
                c.set(Calendar.MINUTE, time.getMinute());
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                
                if (c.getTimeInMillis() < System.currentTimeMillis()) {
                    c.add(Calendar.DATE, 1);
                }
                
                logger.info("Task will execute @ " + c.getTime());
                
                scheduler.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            logger.info("Executing simple scheduler @ " + time);
                            task.run(System.currentTimeMillis());
                        } catch (Exception ex) {
                            logger.error("Error running simple scheduler", ex);
                        }
                    }
                }, c.getTime(), 86400000);
            }
            
            started = true;
        }
    }

    @Override
    public synchronized void stopSchedule() {
        if (!started || scheduler == null) {
            return;
        }

        logger.info(String.format("Stopping simple scheduler %s", name));

        scheduler.cancel();
        scheduler = null;
        task = null;
        started = false;
    }

    @Override
    public void restartSchedule() throws SchedulerException {
        stopSchedule();
        startSchedule();
    }

    protected ScheduledTask newScheduledTask() {
        try {
            Module module = resolveModule(getTaskModuleClass());
            final Injector injector = Guice.createInjector(module);
            TaskFactory factory = injector.getInstance(TaskFactory.class);
            ScheduledTask task = factory.getTask(name);

            return task;
        } catch (Exception ex) {
            logger.error("Error creating simple scheduler task module", ex);
        }

        return null;
    }

    private Module resolveModule(String className) {
        Module module = null;

        if (className != null) {
            try {
                Class<?> clazz = Class.forName(className);
                Object object = clazz.newInstance();

                if (object instanceof Module) {
                    module = (Module) object;
                } else {
                    throw new IllegalArgumentException(className + " is " + "not an instance of "
                            + Module.class.getName());
                }
            } catch (ClassNotFoundException e) {
                logger.fatal("Class " + className + " not found");
            } catch (InstantiationException e) {
                logger.fatal(className + " is not default instantiable");
            } catch (IllegalAccessException e) {
                logger.fatal(className + " is not default instantiable");
            }
        } else {
            logger.warn("Module class name not specified");
        }

        return module;
    }

    @Override
    public String getRunTimes() {
        return runTimes;
    }

    @Override
    public void setRunTimes(String runTimes) {
        this.runTimes = runTimes;
    }
    
    private List<Time> parseRunTimes() {
        String [] times = this.runTimes.split(",");
        List<Time> schedule = new ArrayList<Time>();
        
        for (String time : times) {
            if (time.length() == 4) {
                Integer hours = Integer.parseInt(time.substring(0, 2));
                Integer minutes = Integer.parseInt(time.substring(2, 4));
                
                if (hours < 0 || hours > 23) {
                    logger.warn("Invalid hour specified: " + hours);
                    continue;
                }
                
                if (minutes < 0 || minutes > 59) {
                    logger.warn("Invalid minute specified: " + minutes);
                    continue;
                }

                schedule.add(new Time(hours, minutes));
            } else {
                logger.warn("Invalid time specified");
            }
        }
        
        return schedule;
    }
}
