/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.scheduling;

/**
 * The managed bean interface for {@link com.nexala.spectrum.scheduling.TimedScheduler}
 * 
 * @author Paul O'Riordan
 * @see com.nexala.spectrum.scheduling.TimedScheduler
 */
public interface TimedSchedulerMBean {
    /**
     * Called when the scheduler is initialized.
     * 
     * @throws Exception
     *             if an error occurs during initialization.
     */
    void start() throws Exception;

    /**
     * Called when the scheduler is destroyed.
     */
    void stop();

    /**
     * Starts the scheduler.
     * 
     * @throws SchedulerException
     *             if the scheduler failed to start.
     */
    void startSchedule() throws SchedulerException;

    /**
     * Stops the scheduler.
     * 
     * @throws SchedulerException
     *             if the scheduler failed to stop.
     */
    void stopSchedule() throws SchedulerException;

    /**
     * Restarts the scheduler.
     * 
     * @throws SchedulerException
     *             if the scheduler failed to restart.
     */
    void restartSchedule() throws SchedulerException;

    /**
     * Returns the name of the scheduler.
     * 
     * @return the name of the scheduler.
     */
    String getName();

    /**
     * Sets the name of the scheduler.
     * 
     * @param name
     *            the name of the scheduler. It should correspond to the ID of the
     *            {@link com.nexala.spectrum.monitoring.MonitoringConfiguration} that will be used to configure the
     *            scheduler.
     */
    void setName(String name);

    /**
     * Returns the fully qualified class name of the task module that will be used to instantiate the class that will be
     * performed by the scheduler.
     * 
     * @return the fully qualified class name.
     */
    String getTaskModuleClass();

    /**
     * Sets the fully qualified class name of the task module that will be used to instantiate the class that will be
     * performed by the scheduler.
     * 
     * @param taskModuleClass
     *            the fully qualified class name.
     */
    void setTaskModuleClass(String taskModuleClass);

    /**
     * Returns whether the scheduler will be started as soon as it is created.
     * 
     * @return <code>true</code> if the scheduler will be started as soon as it is created, <code>false</code>
     *         otherwise.
     */
    boolean getStartAtStartup();

    /**
     * Sets whether the scheduler will be started as soon as it is created.
     * 
     * @param startAtStartup
     *            <code>true</code> if the scheduler will be started as soon as it is created. <code>false</code>
     *            otherwise.
     */
    void setStartAtStartup(boolean startAtStartup);

    /**
     * Returns whether the scheduler has been started.
     * 
     * @return <code>true</code> if the scheduler has been started. <code>false</code> otherwise.
     */
    boolean getStarted();

    /**
     * Gets the times that the task should run at.
     * 
     * @return A command separated list of times (HHMM) in 24 hour clock format. E.g. 1000,1300,1800
     */
    public String getRunTimes();

    /**
     * Sets the times that the task should be run at
     * 
     * @param s
     *            A command separated list of times (HHMM) in 24 hour clock format. E.g. 1000,1300,1800
     */
    public void setRunTimes(String s);
}
