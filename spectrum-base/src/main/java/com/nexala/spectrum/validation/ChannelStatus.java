package com.nexala.spectrum.validation;

public final class ChannelStatus {
    private final Integer id;
    private final String name;
    private final Integer priority;

    public ChannelStatus(Integer id, String name, Integer priority) {
        this.id = id;
        this.name = name;
        this.priority = priority;
    }
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPriority() {
        return priority;
    }
}
