/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.validation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;

public class ChannelRuleDao extends GenericDao<ChannelRule, Long> {

    @Inject
    private ChannelRuleValidationDao validationDao;

    @Inject
    private Provider<ChannelRuleMapper> provider;

    private static class ChannelRuleMapper extends JdbcRowMapper<ChannelRule> {

        @Inject
        private ChannelConfiguration channelConfiguration;

        @Inject
        private ChannelStatusUtil channelStatusUtil;

        private List<ChannelRuleValidation> validations = null;

        public void setValidations(List<ChannelRuleValidation> validations) {
            this.validations = validations;
        }

        @Override
        public final ChannelRule createRow(ResultSet rs) throws SQLException {
            ChannelRule record = new ChannelRule();
            record.setId(rs.getInt("ID"));
            record.setName(rs.getString("Name"));
            record.setActive(rs.getBoolean("Active"));
            record.setChannel(channelConfiguration.getConfig(rs.getInt("ChannelID")));
            record.setStatus(channelStatusUtil.getChannelStatusById(rs.getInt("ChannelStatusID")));

            if (validations != null) {
                for (ChannelRuleValidation validation : validations) {
                    if (validation.getRule() == null && validation.getRuleId() == record.getId()) {
                        record.addValidation(validation);
                        validation.setRule(record);
                    }
                }
            }

            return record;
        }
    }

    public List<ChannelRule> findAll() throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_RULE_QUERY);

        ChannelRuleMapper mapper = provider.get();
        mapper.setValidations(validationDao.findAll());

        return findByQuery(query, mapper);
    }

    public List<ChannelRule> findByChannelId(int channelId) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_RULE_CHANNELID_QUERY);

        ChannelRuleMapper mapper = provider.get();
        mapper.setValidations(validationDao.findByChannelId(channelId));

        return findByQuery(query, mapper, channelId);
    }

}