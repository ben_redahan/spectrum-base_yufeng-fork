package com.nexala.spectrum.rest.service.bean;

/**
 * Bean for the branding.
 * @author BBaudry
 *
 */
public class BrandingBean {
    
    protected String logo;
    
    protected String logoStyle;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogoStyle() {
        return logoStyle;
    }

    public void setLogoStyle(String logoStyle) {
        this.logoStyle = logoStyle;
    }

}
