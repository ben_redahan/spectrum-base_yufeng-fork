package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.rest.data.beans.StockDesc;

@ImplementedBy(DefaultStockProvider.class)
public interface StockProvider {
    List<StockDesc> getSiblings(int stockId);
    
    List<StockDesc> getStocksByEvent(int eventId);
}
