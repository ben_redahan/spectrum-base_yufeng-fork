/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.ParameterDto;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.DownloadProvider;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.Response;
import com.nexala.spectrum.rest.data.beans.DownloadInfo;
import com.nexala.spectrum.rest.service.bean.DownloadConfigurationUIBean;
import com.nexala.spectrum.view.conf.DownloadConfiguration;

@Path("/download")
@Singleton
public class DownloadService {
	private final static Logger LOGGER = Logger.getLogger(RailStreamDataService.class);
	
    @Inject
    private Map<String, DownloadProvider> downloadProviders;

    @Inject
    private DownloadConfiguration downloadConfiguration;

    @Inject
    private DataLicensor dataLicensor;

    @Inject
    private Map<String, FleetProvider> fleetProviders;

    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;

    @GET
    @LoggingData
    @Path("/search/{startDate: [0-9]+}/{endDate: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DownloadInfo> search(@PathParam("startDate") long startDate,
            @PathParam("endDate") long endDate, @QueryParam("fleetId") String fleetId, 
            @QueryParam("unit") String unit, @QueryParam("vehicle") String vehicle,
            @QueryParam("fileType") String fileType) {

        Date fromDate = new Date(startDate);
        Date toDate = new Date(endDate);

        List<DownloadInfo> downloadList = new ArrayList<DownloadInfo>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
        if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
            downloadList.addAll(downloadProviders.get(fleetId)
                    .searchDownload(fromDate, toDate, unit, vehicle, fileType));
        }
        
        List<DownloadInfo> returnList = dataLicensor.getLicenced(downloadList);

        return returnList;
    }

    @GET
    @LoggingData
    @Path("/checkDownload")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> checkDownload(@QueryParam("fleetId") String fleetId,
            @QueryParam("vehicle") String vehicle, @QueryParam("fileType") String fileType) {

        Boolean isCurrent = downloadProviders.get(fleetId).checkCurrentDownload(vehicle, fileType);
        return Response.of(isCurrent);
    }

    @GET
    @Cache
    @LoggingData
    @Path("/downloadConfig")
    @Produces(MediaType.APPLICATION_JSON)
    public DownloadConfigurationUIBean downloadConfiguration() throws IOException {
        DownloadConfigurationUIBean result = new DownloadConfigurationUIBean();
        result.setVehicleSelector(downloadConfiguration.isVehicleSelector());
        result.setColumns(downloadConfiguration.getColumns());
        result.setFileTypes(downloadConfiguration.getFileTypes());
        result.setRetrievable(downloadConfiguration.isRetrievable());
        result.setDownloadFields(downloadConfiguration.getFields());
        result.setPopupFilters(downloadConfiguration.getPopupFilters());
        result.setThresholdTime(downloadConfiguration.getThresholdTime());
        result.setGapTime(downloadConfiguration.getGapTime());
        
        return result;
    }

    @GET
    @LoggingData
    @Path("/insertDownload")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> insertDownload(@QueryParam("fleetId") String fleetId, 
            @QueryParam("vehicle") String vehicle, @QueryParam("fileType") String fileType,
            @QueryParam("startDate") String startDate, @QueryParam("startTime") String startTime,
            @QueryParam("endTime") String endTime
            , @QueryParam("requestTypeId") String requestTypeId,
            @QueryParam("devices") String devices) {

        // insert a new download
        // TODO make insert generic - requestTypeId and devices are railhead specific
        downloadProviders.get(fleetId).insertDownload(vehicle, fileType, startDate, startTime, endTime, requestTypeId, devices);
        // check it has been inserted
        Boolean isCurrent = downloadProviders.get(fleetId).checkCurrentDownload(vehicle, fileType);
        return Response.of(isCurrent);
    }
    
    
    @POST
    @Path("/createDownloadRequest")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    //public Boolean createDownloadRequest(InputStream is) {
    public Boolean createDownloadRequest(String requestJson) {
    	Boolean result = true;
    	try {
    		
        	ObjectMapper oMapper = new ObjectMapper();
        	Map<String, List<ParameterDto>> bodyMap = oMapper.readValue(requestJson, new TypeReference<Map<String, List<ParameterDto>>>() {});
        	List<ParameterDto> parameters = bodyMap.get("body");
        	
        	String fleetId = null; 
        	for (ParameterDto param : parameters) {
        		if (param.getName().equalsIgnoreCase("fleetid")) {
        			fleetId = param.getValue();
        		}
        	}
        	
        	downloadProviders.get(fleetId).insertDownload(parameters);
        	
        } catch (JsonMappingException e) {
        	e.printStackTrace();
        	LOGGER.info("Failed to map JSON string : " + requestJson);
        	result = false;
        } catch (Exception e) {
        	e.printStackTrace();
        	LOGGER.info("Failed to create download request");
        	result = false;
        }
        return result;
    }

    @GET
    @LoggingData
    @Path("/retrieve")
    @Produces(MediaType.TEXT_HTML)
    public String download(@QueryParam("fleetId") String fleetId, @QueryParam("path") String path)
            throws IOException {        
    	return downloadProviders.get(fleetId).downloadFile(path);
    }
}
