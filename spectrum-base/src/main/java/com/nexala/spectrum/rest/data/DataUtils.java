/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.EventChannelValue;
import com.nexala.spectrum.db.beans.LookupValue;
import com.nexala.spectrum.db.beans.RelatedChannelGroup;
import com.nexala.spectrum.db.beans.Vehicle;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.rest.data.beans.SourceEventChannelData;

public class DataUtils {
    private static final Logger logger = Logger.getLogger(DataUtils.class);

    private static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator = new Comparator<K>() {
            public int compare(K k1, K k2) {
                return map.get(k1).compareTo(map.get(k2));
            }
        };

        Map<K, V> sortedMap = new TreeMap<K, V>(valueComparator);
        sortedMap.putAll(map);
        return sortedMap;
    }
    
    public static String analogueFormat(String pattern, Number value) {
        if (pattern != null && value != null) {
            DecimalFormat formatter = new DecimalFormat(pattern);
            String formattedValue = formatter.format(value);
            
            // Remove the minus sign if the value is -0
            formattedValue = formattedValue.replaceAll( "^-(?=0(.0*)?$)", "");
            
            return formattedValue;
        }

        return null;
    }

    public static Map<Integer, DataSet> toDataSetMap(List<DataSet> vehicles) {
        Map<Integer, DataSet> map = new HashMap<Integer, DataSet>();

        if (vehicles != null) {
            for (DataSet vehicle : vehicles) {
                map.put(vehicle.getSourceId(), vehicle);
            }
        }

        return map;
    }

    public static List<DataSet> changeset(List<DataSet> oldDataSets,
            List<DataSet> newDataSets) {

        List<DataSet> changeset = new ArrayList<DataSet>();

        Map<Integer, DataSet> mapOld = DataUtils.toDataSetMap(oldDataSets);
        Map<Integer, DataSet> mapNew = DataUtils.toDataSetMap(newDataSets);

        Set<Integer> left = new HashSet<Integer>(mapOld.keySet());
        Set<Integer> right = new HashSet<Integer>(mapNew.keySet());

        Set<Integer> intersection = new HashSet<Integer>(mapOld.keySet());
        intersection.retainAll(right);

        left.removeAll(intersection);
        right.removeAll(intersection);

        for (final Integer dataSetId : left) {
            changeset.add(new DataSetImpl(dataSetId, null));
        }

        for (Integer dataSetId : right) {
            changeset.add(mapNew.get(dataSetId));
        }

        for (Integer dataSetId : intersection) {
            final DataSet oldDataSet = mapOld.get(dataSetId);
            final DataSet newDataSet = mapNew.get(dataSetId);

            if (oldDataSet == null || oldDataSet.getChannels() == null) {
                changeset.add(newDataSet);
            } else if (newDataSet == null || newDataSet.getChannels() == null) {
                changeset.add(oldDataSet);
            } else {
                final ChannelCollection changes = DataUtils.getChangeSet(
                        oldDataSet.getChannels(), newDataSet.getChannels());

                changeset.add(new DataSetImpl(newDataSet.getSourceId(), changes));
            }
        }

        return changeset;
    }

    public static Map<Integer, List<DataSet>> getFormation(List<DataSet> data) {
        Map<Integer, List<DataSet>> formationDataSet = new HashMap<Integer, List<DataSet>>();

        for (DataSet ds : data) {
            ChannelCollection channels = ds.getChannels();

            ChannelInt fleetFormationId = channels.getByName(
                    Spectrum.FLEET_FORMATION_ID, ChannelInt.class);

            ChannelInt unitId = channels.getByName(Spectrum.UNIT_ID,
                    ChannelInt.class);

            Integer id = null;
            if (fleetFormationId != null && fleetFormationId.getValue() != null) {
                // TODO : Converting to negative id isn't a good approach
                // For the moment this id is used only to grouping, so negative
                // id means formation grouping.
                // The problem regards the fact that unitId and fleetFormationId
                // could coincide the same values

                id = -fleetFormationId.getValue();
            } else if (unitId != null && unitId.getValue() != null) {
                id = unitId.getValue();
            }

            if (id != null) {
                if (formationDataSet.get(id) == null) {
                    formationDataSet.put(id, new ArrayList<DataSet>());
                }

                formationDataSet.get(id).add(new DataSetImpl(id, channels));
            } else {
                logger.warn("Unable to identify the FleetFormationID or the UnitID");
            }
        }

        return formationDataSet;
    }

    public static boolean hasNewFormation(List<DataSet> newData,
            List<DataSet> oldData) {
        if (newData.size() != oldData.size()) {
            return true;
        }

        List<Integer> newIds = new ArrayList<Integer>();
        for (DataSet newDs : newData) {
            newIds.add(newDs.getSourceId());
        }

        List<Integer> oldIds = new ArrayList<Integer>();
        for (DataSet oldDs : oldData) {
            oldIds.add(oldDs.getSourceId());
        }

        if (!newIds.containsAll(oldIds) || !oldIds.containsAll(newIds)) {
            return true;
        }

        return false;
    }

    public static ChannelCollection getChangeSet(ChannelCollection oldList,
            ChannelCollection newList) {
        ChannelCollection changeSet = new ChannelCollection();

        for (Channel<?> newChannel : newList) {
            final Channel<?> oldChannel;

            if (newChannel.getId() > -1) {
                oldChannel = oldList.getById(newChannel.getId());
            } else {
                oldChannel = oldList.getByName(newChannel.getName());
            }

            if (oldChannel == null || !oldChannel.equals(newChannel)) {
                changeSet.put(newChannel);
            }
        }

        return changeSet;
    }

    public static List<DataSet> getChannelEventList(List<DataSet> data,
            ChannelConfiguration configuration, List<Integer> channelIds) {
        List<DataSet> result = new ArrayList<DataSet>();

        for (DataSet ds : data) {
            ChannelCollection channels = new ChannelCollection();
            Channel<?> timeStampChannel = ds.getChannels().getByName(
                    Spectrum.TIMESTAMP);

            if (timeStampChannel != null && timeStampChannel.getValue() != null) {
                channels.put(new ChannelLong(Spectrum.TIMESTAMP, new Long(
                        timeStampChannel.getStringValue())));
            }

            for (Integer channelId : channelIds) {
                ChannelConfig config = configuration.getConfig(channelId);

                if (config == null) {
                    continue;
                }
                Channel<?> channel = ds.getChannels().getById(config.getId());
                String unitMeasure = "";
                if (config.getUnitOfMeasure() != null) {
                    unitMeasure = " " + config.getUnitOfMeasure();
                }
                if (channel != null && channel.getValue() != null) {
                    channels.put(new ChannelString(config.getName(), channel
                            .getStringValue() + unitMeasure, (channel
                            .getValue() != null ? channel.getCategory()
                            : ChannelCategory.NODATA)));
                }
            }

            result.add(new DataSetImpl(ds.getSourceId(), channels));
        }

        return result;
    }
    
    public static List<DataSet> getVehiclesInColumn(List<DataSet> data,
            ChannelConfiguration channelConfig, List<Integer> channelIds) {
        return getVehiclesInColumn(data, channelConfig, channelIds, true, true);
    }

    public static List<DataSet> getVehiclesInColumn(List<DataSet> data,
            ChannelConfiguration channelConfig, List<Integer> channelIds, boolean displayAll,  boolean withChannelName) {

        List<DataSet> result = new ArrayList<DataSet>();

        for (Integer channelId : channelIds) {
            ChannelConfig config = channelConfig.getConfig(channelId);

            // Add the data if everything is displayed or this channel is never hide.
            if (config != null && channelId != null &&
                    (displayAll || config.isAlwaysDisplayed())) {
                getVehicleRow(data, result, config, withChannelName);
            }
            
        }

        return result;
    }

    private static void getVehicleRow(List<DataSet> data,
            List<DataSet> result, ChannelConfig config, boolean channelName) {
        ChannelCollection channels = new ChannelCollection();

        if(channelName) {
            channels.put(new ChannelString(Spectrum.COLUMN_CHANNEL_NAME, config
                    .getDescription(), ChannelCategory.NORMAL));
            channels.put(new ChannelString(Spectrum.COLUMN_CHANNEL_DESCRIPTION, config
                    .getName(), ChannelCategory.NORMAL));
        } else {
            channels.put(new ChannelInt(Spectrum.ID, config.getId(), ChannelCategory.NORMAL));
        }
        

        for (DataSet ds : data) {
            ChannelCollection cc = ds.getChannels();

            Channel<?> channel = cc.getById(config.getId());

            if (channel != null) {
                String unitMeasure = "";

                if (config.getUnitOfMeasure() != null) {
                    unitMeasure = " " + config.getUnitOfMeasure();
                }

                String val = channel.getStringValue();
                if (val != null) {
                    val += (channel.getValue() != null ? unitMeasure : "");
                }

                ChannelCategory cat = channel.getValue() != null ? channel
                        .getCategory() : ChannelCategory.NODATA;

                        channels.put(new ChannelString(ds.getSourceId().toString(), val, cat));
            }
        }

        result.add(new DataSetImpl(config.getId(), channels));
    }

    public static void loadLookupValues(ChannelLookupProvider lookupProvider,
            ChannelConfiguration channelConfig, List<DataSet> data) {
        List<Integer> lookupChannelIds = lookupProvider.getLookupChannelIds();

        for (DataSet ds : data) {
            ChannelCollection cc = ds.getChannels();

            for (Integer channelId : lookupChannelIds) {
                ChannelConfig config = channelConfig.getConfig(channelId);
                if (config == null) {
                    continue;
                }

                Channel<?> channel = cc.getById(config.getId());
                if (channel == null) {
                    continue;
                }

                Object objValue = channel.getValue();
                String value = "";

                if (objValue == Boolean.TRUE) {
                    value = "1";
                } else if (objValue == Boolean.FALSE) {
                    value = "0";
                } else {
                    if (channel.getValue() != null) {
                        Integer intValue = channel.getValueAsDouble()
                                .intValue();
                        value = intValue.toString();
                    }
                }

                LookupValue lookupValue = lookupProvider.get(config, value);
                
                if (lookupValue != null) {
                    ChannelString newChannel = new ChannelString(
                            channel.getName(), lookupValue.getDisplayValue(),
                            channel.getCategory());

                    newChannel.setId(channel.getId());
                    newChannel.setColumnName(channel.getColumnName());

                    cc.remove(channel.getId());
                    cc.put(newChannel);
                }
            }
        }
    }

    public static DataSet getVehicleData(List<DataSet> data,
            ChannelConfiguration channelConfiguration,
            boolean hasTransientStates) {

        if (data == null || data.size() < 1) {
            return null;
        }

        ChannelCollection cc = new ChannelCollection();

        Integer vehicleId = null;
        Long timestamp = null;

        for (DataSet ds : data) {
            if (vehicleId == null) {
                vehicleId = ds.getSourceId();
            }

            Long dsTimestamp = ds.getTimestamp();

            ChannelCollection dsCc = ds.getChannels();
            Collection<Channel<?>> dsChannels = dsCc.getChannels().values();

            for (Channel<?> dsChannel : dsChannels) {
                final Channel<?> ccChannel = cc.getByChannel(dsChannel);

                ChannelConfig config = channelConfiguration.getConfig(dsChannel);

                boolean isTransient = config != null
                        && config.getDefaultValue() != null;

                if (ccChannel == null) {
                    cc.put(dsChannel);
                } else {
                    if (isTransient && hasTransientStates) {
                        if ((dsChannel.getValue() == Boolean.TRUE && !config
                                .getDefaultValue())
                                || (dsChannel.getValue() == Boolean.FALSE && config
                                        .getDefaultValue())) {

                            cc.remove(dsChannel.getId());
                            cc.put(dsChannel);
                        }
                    } else {
                        if (timestamp == null || dsTimestamp > timestamp) {
                            cc.remove(dsChannel.getId());
                            cc.put(dsChannel);
                        }
                    }
                }
            }

            if (timestamp == null || dsTimestamp > timestamp) {
                timestamp = dsTimestamp;
            }
        }

        return new DataSetImpl(vehicleId, timestamp, cc);
    }
    
    public static String formatValueAndUnit(ChannelConfig channelConf, Number value) {
        
        String formatted = analogueFormat("0.0", value);
        if(formatted != null && channelConf != null && channelConf.getUnitOfMeasure() != null) {
            formatted += " " + channelConf.getUnitOfMeasure();
        }
        
        return formatted;
    }

    public static Map<String, DataSet> getFormationByStockMap(List<DataSet> formations) {
        Map<String, DataSet> formationMap = new HashMap<String, DataSet>();
        
        for (DataSet formation : formations) {
            Channel<?> temp = formation.getChannels().getByName(Spectrum.FLEET_FORMATION_ID);
            Channel<?> fleetId = formation.getChannels().getByName(Spectrum.FLEET_ID);

            if ((temp != null) && (temp.getStringValue() != null)) {
                String[] stockIds;
                if ((fleetId != null) && (fleetId.getStringValue() != null)) {
                    String fleetCode = fleetId.getStringValue() + "_" + temp.getStringValue();
                    stockIds = fleetCode.split(" ");
                } else {
                    stockIds = temp.getStringValue().split(" ");
                }

                for (String id : stockIds) {
                    formationMap.put(id, formation);
                }
            }
        }
        return formationMap;
    }
    
    public static List<DataSet> convertUnitDataToVehicleData(List<DataSet> data, List<Vehicle> vehicles, ChannelConfiguration channelConfiguration) {
        return convertUnitDataToVehicleData(data, vehicles, channelConfiguration, false);
    }

    public static List<DataSet> convertUnitDataToVehicleData(List<DataSet> data, List<Vehicle> vehicles, 
            ChannelConfiguration channelConfiguration, boolean useRelatedChannelId) {

        // if we have no channel to vehicle type config, then just return everything at unit level
        if (channelConfiguration.getChannelVehicleTypeConfigList() == null || channelConfiguration.getChannelVehicleTypeConfigList().size() == 0) {
            return data;
        }
        
        List<DataSet> result = new ArrayList<DataSet>();
        Long timestamp = null;
        
        // Initialize a map to keep track of Channels that belong to each vehicle type
        Map<Integer, ChannelCollection> channelCollectionByVehicleType = new HashMap<Integer, ChannelCollection>();
        for (Vehicle v : vehicles) {
            if (!channelCollectionByVehicleType.containsKey(v.getVehicleTypeId())) {
                channelCollectionByVehicleType.put(v.getVehicleTypeId(), new ChannelCollection());
                channelCollectionByVehicleType.get(v.getVehicleTypeId()).put(new ChannelInt(Spectrum.VEHICLE_ID, v.getId()));
                channelCollectionByVehicleType.get(v.getVehicleTypeId()).put(new ChannelString(Spectrum.VEHICLE_TYPE, v.getVehicleType()));

            }
        }
        
        Set<Integer> vehicleTypeIds = channelCollectionByVehicleType.keySet();
        
        for (DataSet dataset: data) {
            timestamp = dataset.getTimestamp();
            
            for (Channel<?> c : dataset.getChannels()) {
                if (c != null) {
                    int channelId = c.getId();
                    if (useRelatedChannelId) {
                        ChannelConfig channelConf = channelConfiguration.getConfig(channelId);
                        if (channelConf != null 
                                && channelConf.getRelatedChannelGroup() != null 
                                && channelConf.getRelatedChannelGroup().getId() != null) {
                            c.setId(channelConf.getRelatedChannelGroup().getId());
                        }
                    }

                    boolean dataAdded = false;
                    List<Integer> vehicleTypesForChannel = channelConfiguration.getVehicleTypeListByChannelId(channelId);
                    if (vehicleTypesForChannel != null) {
                        for (Integer vehicleType : vehicleTypesForChannel) {
                            if (vehicleTypeIds.contains(vehicleType)) {
                                channelCollectionByVehicleType.get(vehicleType).put(c);
                                dataAdded = true;
                            }
                        }
                    }

                    
                    // If the data was not added for a particular vehicle type, assume it belongs to all of them
                    if (!dataAdded) {
                        for (ChannelCollection channelCollection : channelCollectionByVehicleType.values()) {
                            channelCollection.put(c);
                        }
                    }
                }
            }
        }
        
        for (Vehicle v : vehicles) {
            DataSet dataset = new DataSetImpl(v.getId(), timestamp, channelCollectionByVehicleType.get(v.getVehicleTypeId()));
            result.add(dataset);
        }
        
        return result;
    }
    
    public static List<DataSet> getRelatedChannelVehiclesInColumn(List<DataSet> data, List<Vehicle> vehicles,
            ChannelConfiguration channelConfig, List<Integer> channelIds, boolean displayAll,  boolean withChannelName) {

        List<DataSet> result = new ArrayList<DataSet>();

        Map<Integer, SortableChannelConfigList> relatedChannelConfigMap = new TreeMap<Integer, SortableChannelConfigList>();

        for (Integer channelId : channelIds) {
            ChannelConfig config = channelConfig.getConfig(channelId);
            
            Integer relatedChannelId = -1;
            if (config.getRelatedChannelGroup() != null && config.getRelatedChannelGroup().getId() != null) {
                relatedChannelId = config.getRelatedChannelGroup().getId();
            }
            
            if (!relatedChannelConfigMap.containsKey(relatedChannelId)) {
                relatedChannelConfigMap.put(relatedChannelId, new SortableChannelConfigList(relatedChannelId,
                        config.getChannelGroupOrder(), new ArrayList<ChannelConfig>()));
            }
            
            relatedChannelConfigMap.get(relatedChannelId).getChannelConfigList().add(config);
        }
        
        relatedChannelConfigMap = sortByValues(relatedChannelConfigMap);

        for (Map.Entry<Integer, SortableChannelConfigList> relatedChannelConfig : relatedChannelConfigMap.entrySet()) {
            if (relatedChannelConfig.getKey() >= 0) {
                boolean display = displayAll;
                for (ChannelConfig config : relatedChannelConfig.getValue().getChannelConfigList()) {
                    if (config.isAlwaysDisplayed()) {
                        display = true;
                    }
                }
                if (display) {
                    getRelatedChannelVehicleRow(data, result, channelConfig, vehicles,
                            relatedChannelConfig.getValue().getChannelConfigList(), relatedChannelConfig.getKey(), withChannelName);
                }
            }
            else {
                // These channels are not related to any other channel, so treat them individually
                for (ChannelConfig config : relatedChannelConfig.getValue().getChannelConfigList()) {
                    if (displayAll || config.isAlwaysDisplayed()) {
                        getVehicleRow(data, result, config, false);
                    }
                }
            }
        }

        return result;
    }

    private static void getRelatedChannelVehicleRow(List<DataSet> data, List<DataSet> result, 
            ChannelConfiguration channelConfig, List<Vehicle> vehicles,
            List<ChannelConfig> relatedConfigs, Integer relatedChannelId, boolean channelName) {
        
        String relatedChannelName = null;
        
        Map<Integer, ChannelConfig> configsByVehicleType = new HashMap<Integer, ChannelConfig>();
        for (ChannelConfig config : relatedConfigs) {
            relatedChannelName = config.getRelatedChannelGroup().getDisplayName();
            
            boolean configAdded = false;
            List<Integer> vehicleTypesForChannel = channelConfig.getVehicleTypeListByChannelId(config.getId());
            if (vehicleTypesForChannel != null) {
                for (Integer vehicleType : vehicleTypesForChannel) {
                    configsByVehicleType.put(vehicleType, config);
                    configAdded = true;
                }
            }
            
            if (!configAdded) {
                configsByVehicleType.put(-1, config);
            }
        }
       
        ChannelCollection channels = new ChannelCollection();
        if(channelName) {
            channels.put(new ChannelString(Spectrum.COLUMN_CHANNEL_NAME, relatedChannelName, ChannelCategory.NORMAL));    
        } else {
            channels.put(new ChannelInt(Spectrum.ID, relatedChannelId, ChannelCategory.NORMAL));
        }
        
        for (Vehicle vehicle : vehicles) {
            Integer vehicleTypeId = vehicle.getVehicleTypeId();
            
            ChannelConfig config = configsByVehicleType.get(vehicleTypeId);
            
            if (config == null) {
                config = configsByVehicleType.get(-1);
            }
            
            if (config != null) {
                for (DataSet ds : data) {
                    if (ds.getSourceId() == vehicle.getId()) {
                        ChannelCollection cc = ds.getChannels();
        
                        Channel<?> channel = cc.getById(config.getId());
        
                        if (channel != null) {
                            String unitMeasure = "";
        
                            if (config.getUnitOfMeasure() != null) {
                                unitMeasure = " " + config.getUnitOfMeasure();
                            }
        
                            String val = channel.getStringValue();
                            if (val != null) {
                                val += (channel.getValue() != null ? unitMeasure : "");
                            }
        
                            ChannelCategory cat = channel.getValue() != null ? channel
                                    .getCategory() : ChannelCategory.NODATA;
        
                            channels.put(new ChannelString(ds.getSourceId().toString(), val, cat));
                        }
                    }
                }
            }
        }


        result.add(new DataSetImpl(relatedChannelId, channels));
    }
    
    /**
     * Returns a map with the name of the related channel and the ChannelConfig associated to it for specific
     * group and vehicle type.
     * If one channel does not have any related channel, then the map will contain the name of the channel itself.
     */
    public static Map<String, SortableChannelConfigList> getGroupVehicleChannelConfig(
            ChannelConfiguration channelConfig,
            Integer vehicleTypeId, Integer groupId) {
        
        Map<String, SortableChannelConfigList> channelsMap = new TreeMap<String, SortableChannelConfigList>();
        List<Integer> channelIds = channelConfig.getGroup(groupId).getChannelIdList();
        
        for (Integer channelId : channelIds) {
            List<ChannelConfig> configList = new ArrayList<ChannelConfig>();
            ChannelConfig config = channelConfig.getConfig(channelId);
            configList.add(config);
            RelatedChannelGroup relatedChannel = config.getRelatedChannelGroup();
            if (relatedChannel != null) {
                String relatedChannelName = relatedChannel.getDisplayName();

                if (!channelsMap.keySet().contains(relatedChannelName)) {
                    channelsMap.put(relatedChannelName, new SortableChannelConfigList(relatedChannel.getId(),
                            config.getChannelGroupOrder(), new ArrayList<ChannelConfig>()));
                }

                boolean isCommonChannel = true;
                List<Integer> vehicleTypesForChannel = channelConfig.getVehicleTypeListByChannelId(channelId);
                if (vehicleTypesForChannel != null) {
                    isCommonChannel = false;
                    for (Integer vehicleType : vehicleTypesForChannel) {
                        if (vehicleType == vehicleTypeId) {
                            channelsMap.get(relatedChannelName).getChannelConfigList().add(config);

                        }
                    }
                }
                
                if (isCommonChannel) {
                    channelsMap.get(relatedChannelName).getChannelConfigList().add(config);
                }
            } else {
                // If the channel has no related channel associated, put the name of the channel itself
                channelsMap.put(config.getDescription(), new SortableChannelConfigList(config.getId(),
                        config.getChannelGroupOrder(), configList));
            }
        }
        
        return sortByValues(channelsMap);
    }
    
    public static List<ChannelCollection> getVehicleChannels(List<DataSet> data) {
        List<ChannelCollection> vehicleChannels = new ArrayList<ChannelCollection>();

        List<Integer> processedIds = new ArrayList<Integer>();
        for (DataSet dataSet : data) {
            ChannelCollection cc = dataSet.getChannels();
            if (!processedIds.contains(cc.getByName(Spectrum.VEHICLE_ID)
                    .getValue())) {
                vehicleChannels.add(cc);
                processedIds.add((Integer) cc.getByName(Spectrum.VEHICLE_ID)
                        .getValue());
            }
        }

        return vehicleChannels;
    }
    
    public static void addEventChannelsIntoDatasetsWithMatchingSourceId(List<EventChannelValue> eventData, List<DataSet> data, ChannelConfiguration channelConf) {
        if (eventData == null || eventData.size() == 0) {
            return;
        }
        
        Map<Integer, List<ChannelBoolean>> eventDataPerSourceId = new HashMap<>();
        
        for (EventChannelValue ecv : eventData) {
            if (!eventDataPerSourceId.containsKey(ecv.getSourceId())) {
                eventDataPerSourceId.put(ecv.getSourceId(), new ArrayList<>());
            }
            
            eventDataPerSourceId.get(ecv.getSourceId()).add(new ChannelBoolean(channelConf.getConfig(ecv.getChannelId()).getName(), ecv.getValue()));
        }
        
        for (DataSet ds : data) {
            if (eventDataPerSourceId.containsKey(ds.getSourceId())) {
                for (ChannelBoolean channel : eventDataPerSourceId.get(ds.getSourceId())) {
                    ds.getChannels().put(channel);
                }
            }
        }
    }
    
    public static void addEventChannelsIntoDatasets(List<SourceEventChannelData> eventData, List<DataSet> data, ChannelConfiguration channelConf) {
        if (eventData == null || eventData.size() == 0) {
            return;
        }
        
        Map<Integer, List<ChannelBoolean>> eventDataPerSourceId = new HashMap<>();
        
        for (SourceEventChannelData ecv : eventData) {
            if (!eventDataPerSourceId.containsKey(ecv.getSourceId())) {
                eventDataPerSourceId.put(ecv.getSourceId(), new ArrayList<>());
            }
            
            List<ChannelBoolean> lst = eventDataPerSourceId.get(ecv.getSourceId());
            
            for (Entry<Integer, Boolean> entrySet : ecv.getValueByChannelId().entrySet()) {
                ChannelBoolean channel = new ChannelBoolean(channelConf.getConfig(entrySet.getKey()).getName(), entrySet.getValue());
                channel.setId(entrySet.getKey());
                lst.add(channel);
            }
            
        }
        
        for (DataSet ds : data) {
            if (eventDataPerSourceId.containsKey(ds.getSourceId())) {
                for (ChannelBoolean channel : eventDataPerSourceId.get(ds.getSourceId())) {
                    ds.getChannels().put(channel);
                }
            }
        }
    }
    
    public static void mergeDatasets(List<DataSet> dataSetMaster, List<DataSet> dataSetSlave) {
        if (dataSetMaster == null || dataSetSlave == null) {
            return;
        }
        
        for (DataSet mData : dataSetMaster) {
            for (DataSet sData : dataSetSlave) {
                if (mData.getSourceId().equals(sData.getSourceId())) {
                    ChannelCollection mChannels = mData.getChannels();
                    for (Channel sChannel : sData.getChannels()) {
                            long mTime = mData.getTimestamp();
                            if (sChannel.getName().equalsIgnoreCase("TimeStamp")) {
                                sChannel.setName("TimeStamp");//The GPS channel Timestamp will not overwrite the TimeStamp channel
                                long sTime = sData.getTimestamp();
                                if (((mTime != 0) && (sTime != 0)) && (sTime > mTime)) {
                                    mChannels.put(sChannel);
                                }
                            } else {
                                mChannels.put(sChannel);
                            }
                    }
                }
            }
        }
        
        for (DataSet sData : dataSetSlave) {
            boolean containsSourceId = false;
            for (DataSet mData : dataSetMaster) {
                if (mData.getSourceId().equals(sData.getSourceId())) {
                    containsSourceId = true;
                    break;
                }
            }
            if (!containsSourceId) {
                dataSetMaster.add(sData);
            }
        }
    }
    
    public static boolean isSecondDataSetLatestTimestamp(DataSet dataSet1,
            DataSet dataSet2) {
    	boolean isLatestTimestampSet2 = false;
        if(dataSet1.getTimestamp() == null && dataSet2.getTimestamp() != null){
        	isLatestTimestampSet2 = true;
        } else if (dataSet1.getTimestamp() != null && dataSet2.getTimestamp() == null) {
        	isLatestTimestampSet2 = false;
        } else if (dataSet1.getTimestamp() != null && dataSet2.getTimestamp() != null) {
        	isLatestTimestampSet2 = dataSet2.getTimestamp() > dataSet1.getTimestamp() ? true : false;
        }
        return isLatestTimestampSet2; 
    }
}