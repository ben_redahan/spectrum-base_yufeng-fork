package com.nexala.spectrum.rest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.rest.data.beans.Schematic;
import com.nexala.spectrum.rest.data.beans.SchematicChannel;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.view.conf.SchematicConfiguration;

// REST service created for displaying data in Schematic tab

@Path("/schematic")
@Singleton
public class SchematicService {

	@Inject
	private SchematicConfiguration schemConf;

	@Inject
	private Map<String, ChannelDataProvider> channelDataProviders;

	@Inject
	private Map<String, UnitProvider> unitProviders;

	@GET
	@Path("/items")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Schematic> getConfigurationItems() {
		return schemConf.getItems();
	}

	@GET
	@Path("/unit/{fleetId}/{stockId}/{timestamp}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DataSet> getChannelData(@PathParam("fleetId") String fleetId, @PathParam("stockId") int stockId,
			@PathParam("timestamp") long timestamp) {

		List<DataSet> data = channelDataProviders.get(fleetId).getUnitData(stockId, timestamp, 0);
		
		List<SchematicChannel> schemConfChannelList = schemConf.getSchematicData();
		List<DataSet> newData = new ArrayList<DataSet>();
		
		for (DataSet dataSet : data) {
			ChannelCollection cc = dataSet.getChannels();
			ChannelCollection schematicChannels = new ChannelCollection();

			// loop through channels and add value to channels from data
			for (SchematicChannel confChannel : schemConfChannelList) {

				Channel<?> dataChannel = cc.getById(confChannel.getChannelId());
				if (!confChannel.getType().equals("Boolean")) {
					ChannelString csTemp = new ChannelString(confChannel.getName());
					String value = "";

					if (dataChannel.getStringValue() != null) {
						value = dataChannel.getStringValue();
						if (confChannel.getUnitOfMeasurement() != null) {
							value = value + confChannel.getUnitOfMeasurement();
						}
					} else {
						value = confChannel.getDefaultValue();
					}
					csTemp.setValue(value);
					schematicChannels.put(csTemp);

				} else {
					ChannelBoolean cbTemp = new ChannelBoolean(confChannel.getName());
					dataChannel.getValue();
					cbTemp.setValue((Boolean) dataChannel.getValue());
					
					schematicChannels.put(cbTemp);
				}
			}

			DataSetImpl newDataSet = new DataSetImpl(dataSet.getSourceId(), dataSet.getTimestamp(), schematicChannels);
			newData.add(newDataSet);
		}
		return newData;
	}

	@GET
	@LoggingData
	@Path("/stock/{fleetId}/{unitId: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StockDesc> stock(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
		return unitProviders.get(fleetId).getStockForUnit(unitId);
	}

}
