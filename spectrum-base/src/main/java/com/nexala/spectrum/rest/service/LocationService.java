package com.nexala.spectrum.rest.service;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.Location;
import com.nexala.spectrum.rest.data.LocationProvider;

@Path("/location")
@Singleton
public class LocationService {
    
    @Inject
    private Map<String, LocationProvider> locationProviders;
    
    @GET
    @Path("/byCode/{fleetId}/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    public Location getLocationByCode(@PathParam("fleetId") String fleetId,
            @PathParam("code") String code ) {
        return locationProviders.get(fleetId).getLocationByCode(code);
    }
    
    
    @GET
    @Path("/vehicle/{fleetId}/{vehicleId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Location getVehicleCurrentLocation(@PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") Integer vehicleId) {
        return locationProviders.get(fleetId).getVehicleCurrentLocation(vehicleId);
    }

}
