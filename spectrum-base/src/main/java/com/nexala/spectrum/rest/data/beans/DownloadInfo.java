package com.nexala.spectrum.rest.data.beans;

/**
 * This class is used to hold download request information
 */
public class DownloadInfo extends GenericBean {

	/* Probably no needed */

    /*// STATUS CODES
    public static final int SUCCESS = 0;
    public static final int REQUESTED = 1;
    public static final int IN_PROGRESS = 2;
    public static final int FAILED = 3;
    
    private static final int LESS_THAN_24_HOURS = 0;
    private static final int MORE_THAN_24_HOURS = 1;
    private static final int MORE_THAN_48_HOURS = 2;
    // private static final int NEVER_DOWNLOADED = 3;

    public void calcStatus(int file) {
        
        int status = DownloadInfo.FAILED;

        if (file == DownloadInfo.SUCCESS) {
            status = DownloadInfo.SUCCESS;

        } else if (file == DownloadInfo.IN_PROGRESS) {
            status = DownloadInfo.IN_PROGRESS;

        } else if (file == DownloadInfo.REQUESTED) {
            status = DownloadInfo.REQUESTED;
        }

        if (file != DownloadInfo.SUCCESS) {
            try {
                Calendar cal = Calendar.getInstance();
                Long currentTime = cal.getTimeInMillis();

                Long time = 60000 * Spectrum.DOWNLOAD_MINUTES_TIME_FAILED;

                if ((requestTime + time) < currentTime) {
                    status = DownloadInfo.FAILED;
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }

        this.setStatus(status);
    }

    public void calcVehicleStatus() {
        int vehicleStatus = -1;

        int status = this.getStatus();
        Long requestTime = this.getRequestTimeI();
        Long compTime = this.getCompTimeI();
        Calendar cal = Calendar.getInstance();
        Long currentTime = cal.getTimeInMillis();

        if (compTime == 0L) {

            if (currentTime - requestTime > 172800000) {
                // red
                vehicleStatus = DownloadInfo.MORE_THAN_48_HOURS;
            } else if (currentTime - requestTime > 86400000) {
                // grey
                vehicleStatus = DownloadInfo.MORE_THAN_24_HOURS;
            } else {
                // green
                vehicleStatus = DownloadInfo.LESS_THAN_24_HOURS;
            }

        } else if (status == 0) {
            // green
            vehicleStatus = DownloadInfo.LESS_THAN_24_HOURS;
        }

        this.setVehicleStatus(vehicleStatus);
    }

    */
}
