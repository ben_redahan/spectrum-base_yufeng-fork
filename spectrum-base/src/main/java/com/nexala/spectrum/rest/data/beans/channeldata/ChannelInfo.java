package com.nexala.spectrum.rest.data.beans.channeldata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.nexala.spectrum.db.dao.charting.ChannelType;

/**
 * Channel object with less fields for channel data view.
 * @author brice
 *
 */
@JsonInclude(Include.NON_NULL)
public class ChannelInfo {

    private String id = null;
    
    private String description = null;
    
    private String name = null;
    
    private Boolean alwaysDisplayed = null;
    
    private ChannelType type = null;
    
    private String formatPattern = null;
    
    private String comment = null;

    /**
     * Getter for id.
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter for id.
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter for description.
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for description.
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for alwaysDisplayed.
     * @return the alwaysDisplayed
     */
    public Boolean getAlwaysDisplayed() {
        return alwaysDisplayed;
    }

    /**
     * Setter for alwaysDisplayed.
     * @param alwaysDisplayed the alwaysDisplayed to set
     */
    public void setAlwaysDisplayed(Boolean alwaysDisplayed) {
        this.alwaysDisplayed = alwaysDisplayed;
    }

    /**
     * Getter for type.
     * @return the type
     */
    public Integer getType() {
        if (type != null) {
            return type.ordinal();
        }
        
        return null;
    }

    /**
     * Setter for type.
     * @param type the type to set
     */
    public void setType(ChannelType type) {
        this.type = type;
    }

    /**
     * Getter for formatPattern.
     * @return the formatPattern
     */
    public String getFormatPattern() {
        return formatPattern;
    }

    /**
     * Setter for formatPattern.
     * @param formatPattern the formatPattern to set
     */
    public void setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
    }

	/**
	 * Getter for comment.
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Setter for comment.
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Getter for name.
	 * @return the name
	 */
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
}
