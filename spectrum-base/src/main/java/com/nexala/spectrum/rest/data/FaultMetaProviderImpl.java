package com.nexala.spectrum.rest.data;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.FaultExtraFieldDto;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultMetaDto;
import com.nexala.spectrum.db.beans.FaultMetaExtraField;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.FaultCategoryDao;
import com.nexala.spectrum.db.dao.FaultMetaDao;
import com.nexala.spectrum.db.dao.FaultMetaExtraFieldDao;

public class FaultMetaProviderImpl implements FaultMetaProvider {

    @Inject
    private FaultMetaDao fmDao;
    
    @Inject
    private FaultMetaExtraFieldDao fmemDao;
    
    @Inject
    private FaultCategoryDao fcDao;
    
    @Override
    public Integer updateFaultMeta(FaultMetaDto faultMetaDto) {
        if (faultMetaDto.getId() == null || faultMetaDto.getId() <= 0) {
            throw new FaultConfigServiceException("updateFaultMeta cannot be called with a Fault Meta with a null or less than 1 ID");
        }
        
        Integer faultMetaId = faultMetaDto.getId();
        String username = faultMetaDto.getUsername();
        Map<String, String> extraFieldsDtoMap = faultMetaDto.getExtraFieldValue();
        Map<String, String> overrideFieldsDtoMap = faultMetaDto.getOverrideFieldValue();
        Integer faultMetaFleetId = faultMetaDto.getFleetId();

        FaultMeta faultMeta = buildFaultMetaFromDto(faultMetaDto);

        // Update
        fmDao.update(faultMetaId, faultMeta, username);
        
        //Get the list of extra fields
        List<FaultMetaExtraField> extraFieldsList = fmemDao.get(faultMetaId);
        
        
        //need to update the override fields too
        FaultMeta overrideFaultMeta = fmDao.findSecondaryFieldByPrimaryId(faultMetaId, faultMetaFleetId);
        
        // Build a map of the extra fields to update, delete any extra fields that have been removed
        Map<String, FaultMetaExtraField> extraFieldsByName = new HashMap<String, FaultMetaExtraField>();
        for (FaultMetaExtraField current : extraFieldsList) {
            extraFieldsByName.put(current.getField(), current);
            
            if (!extraFieldsDtoMap.keySet().contains(current.getField())) {
                fmemDao.delete(faultMetaId, current.getField());
                
                // handles if we need to delete the extra field from the override fault meta
                if (overrideFaultMeta != null && !overrideFieldsDtoMap.keySet().contains(current.getField())) {
                    fmemDao.delete(overrideFaultMeta.getId(), current.getField());
                }
            }
        }
        
        // Create or update extra fields
        for (Entry<String, String> entry : extraFieldsDtoMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            
            if (extraFieldsByName.containsKey(key)) {
                FaultMetaExtraField current = extraFieldsByName.get(key);
                if (value == null || !value.equals(current.getValue())) {
                    fmemDao.update(faultMetaId, key, value);
                }
            } else {
                fmemDao.create(faultMetaId, key, value);
            }
        }
        
        if (overrideFaultMeta != null) {
            Integer overrideFaultMetaId = overrideFaultMeta.getId();
            
            //Get the list of override extra fields from the DB
            List<FaultMetaExtraField> overrideFieldsList = fmemDao.get(overrideFaultMetaId);
            Map<String, FaultMetaExtraField> overrideFieldsByName = new HashMap<String, FaultMetaExtraField>();
            
            for (FaultMetaExtraField current : overrideFieldsList) {
                overrideFieldsByName.put(current.getField(), current);
            }
            
            // Create or update the override's extra fields based on the normal extra fields
            // only apply if the key doesn't exist in the override
            for (Entry<String, String> entry : extraFieldsDtoMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                
                // let the next loop handle this
                if (!overrideFieldsDtoMap.containsKey(key)) {
                    if (overrideFieldsByName.containsKey(key)) {
                        FaultMetaExtraField current = overrideFieldsByName.get(key);
                        if (value == null || !value.equals(current.getValue())) {
                            fmemDao.update(overrideFaultMetaId, key, value);
                        }
                    } else {
                        fmemDao.create(overrideFaultMetaId, key, value);
                    }
                }
            }
            
            // Create of update the override based on override fields
            for (Entry<String, String> current : overrideFieldsDtoMap.entrySet()) {
                if (!current.getKey().equals("faultTypeId")) { //already in faultMeta table, not extrafields table
                    if (overrideFieldsByName.containsKey(current.getKey())) {
                        //update if we have a record for the overriding field, otherwise create one
                        fmemDao.update(overrideFaultMetaId, current.getKey(), current.getValue());
                    } else {
                        fmemDao.create(overrideFaultMetaId, current.getKey(), current.getValue()); 
                    }
                }
            }
        }
    
        return faultMetaId;
    }

    @Override
    public Integer createFaultMeta(FaultMetaDto faultMetaDto, Integer newFaultId) {
        faultMetaDto.setId(newFaultId);
        
        String username = faultMetaDto.getUsername();
        Map<String, String> faultMetaExtraFields = faultMetaDto.getExtraFieldValue();
        Map<String, String> faultMetaOverride = faultMetaDto.getOverrideFieldValue();
        Integer faultMetaFleetId = faultMetaDto.getFleetId();
        
        FaultMeta faultMeta = buildFaultMetaFromDto(faultMetaDto);
        Integer faultMetaId = fmDao.create(faultMeta, username); //creates the parent and child and returns the parentId
        
        FaultMeta overrideFaultMeta = fmDao.findSecondaryFieldByPrimaryId(faultMetaId, faultMetaFleetId); // ID of the overriding fault meta
        
        for (Entry<String, String> current : faultMetaExtraFields.entrySet()) {
            fmemDao.create(faultMetaId, current.getKey(), current.getValue());
            if (overrideFaultMeta != null) {
                if (faultMetaOverride.containsKey(current.getKey())) {
                    fmemDao.create(overrideFaultMeta.getId(), current.getKey(), faultMetaOverride.get(current.getKey())); 
                } else {
                    fmemDao.create(overrideFaultMeta.getId(), current.getKey(), current.getValue()); 
                }
            }
        }
        
        // loop through the override extra fields, and add the ones that weren't in the parent extra fields
        if (overrideFaultMeta != null) {
            for (Entry<String, String> current : faultMetaOverride.entrySet()) {
                if (!current.getKey().equals("faultTypeId") && !faultMetaExtraFields.containsKey(current.getKey())) { // already in faultMeta table, not extrafields table
                    fmemDao.create(overrideFaultMeta.getId(), current.getKey(), current.getValue()); 
                }
            }
        }
        
        return faultMetaId; //parentId
    }
    
    private FaultMeta buildFaultMetaFromDto(FaultMetaDto faultMetaDto) {
        validateFaultMetaDto(faultMetaDto);

        FaultMeta faultMeta = new FaultMeta();
        faultMeta.setId(faultMetaDto.getId());
        faultMeta.setCategoryId(faultMetaDto.getCategoryId());
        faultMeta.setDescription(faultMetaDto.getDescription());
        faultMeta.setFaultCode(faultMetaDto.getFaultCode());
        faultMeta.setHasRecovery(faultMetaDto.getHasRecovery());
        faultMeta.setRecoveryProcessPath(faultMetaDto.getRecoveryProcessPath());
        faultMeta.setSummary(faultMetaDto.getSummary());
        faultMeta.setAdditionalInfo(faultMetaDto.getAdditionalInfo());
        faultMeta.setUrl(faultMetaDto.getUrl());
        faultMeta.setTypeId(faultMetaDto.getType());
        faultMeta.setGroupId(faultMetaDto.getGroupId());
        faultMeta.setSourceId(faultMetaDto.getSourceId());
        faultMeta.setOptionalCategoryIdList(faultMetaDto.getOptionalCategoryIdList());
        faultMeta.setE2mFaultCode(faultMetaDto.getE2mFaultCode());
        faultMeta.setE2mFaultDescription(faultMetaDto.getE2mFaultDescription());
        faultMeta.setE2mPriorityCode(faultMetaDto.getE2mPriorityCode());
        faultMeta.setE2mPriority(faultMetaDto.getE2mPriority());
        faultMeta.setFromDate(new Timestamp(faultMetaDto.getOverrideStartTimestamp()));
        faultMeta.setToDate(new Timestamp(faultMetaDto.getOverrideEndTimestamp()));
        faultMeta.setFleetId(faultMetaDto.getFleetId());
        
        //  TODO Override fields should be in a nested FaultMeta/FaultMetaDto
        //  ie. instead of faultMetaDto.getOverrideFieldValue returning a Map, it should return an instance of FaultMetaDto
        faultMeta.setTypeIdOverride(
                faultMetaDto.getOverrideFieldValue().size() == 0
                        ? null
                        : Integer.valueOf(faultMetaDto.getOverrideFieldValue().get("faultTypeId")));
        
        return faultMeta;
    }
    
    protected void validateFaultMetaDto(FaultMetaDto faultMetaDto) {
        if (faultMetaDto.getCategoryId() == null) {
            throw new FaultConfigServiceException("Category ID is null");
        } else if (fcDao.findById(faultMetaDto.getCategoryId()) == null) {
            throw new FaultConfigServiceException("Category don't exist");
        }
        
        if (faultMetaDto.getDescription() == null) {
            throw new FaultConfigServiceException("Description is null");
        } else if (faultMetaDto.getDescription().length() > 100) {
            throw new FaultConfigServiceException("Description is longer than 100 characters");
        }
        
        if (faultMetaDto.getFaultCode() == null) {
            throw new FaultConfigServiceException("Fault code is null");
        } else if (faultMetaDto.getFaultCode().length() > 100) {
            throw new FaultConfigServiceException("Fault code is longer than 100 characters");
        }
        
        if (faultMetaDto.getUrl() != null && faultMetaDto.getUrl().length() > 500) {
            throw new FaultConfigServiceException("Fault Url is longer than 500 characters");
        }
        
        if (faultMetaDto.getType() == null) {
            throw new FaultConfigServiceException("Type is null");
        }
        
        if (faultMetaDto.getGroupByFleet() != null &&
        	faultMetaDto.getGroupByFleet() &&
        	faultMetaDto.getFleetId() == null) {
        	throw new FaultConfigServiceException("Fleet ID is null");
        }
        
        // Mandatory field validation in extra fields.
        if(faultMetaDto.getExtraFieldConfigs() != null && faultMetaDto.getExtraFieldConfigs().size() != 0) {
        	for(FaultExtraFieldDto extraField: faultMetaDto.getExtraFieldConfigs().values()) {
        		if(extraField.isMandatory()) {
        			if (faultMetaDto.getExtraFieldValue() == null || faultMetaDto.getExtraFieldValue().size() == 0
                			|| faultMetaDto.getExtraFieldValue().get(extraField.getName()) == null
                			|| faultMetaDto.getExtraFieldValue().get(extraField.getName()).equals("")) {
                		throw new FaultConfigServiceException(extraField.getName() + " is null");
                	}
        		}
        		
        	}
        }
        	
    }

	@Override
	public void deleteFaultMeta(FaultMetaDto faultMetaDto) {
		try {
            fmDao.delete(faultMetaDto.getId(), faultMetaDto.getE2mFaultCode());
            fmDao.delete(faultMetaDto.getParentId(), faultMetaDto.getE2mFaultCode());
        } catch (DataAccessException e) {
            if (e.getCause() != null && e.getCause().getMessage().equals("ERR_FAULT_EXIST")) {
                throw new FaultConfigServiceException("The fault configuration can't be deleted, some faults with this code already exist");
            }

            throw e;
        }  
	}
	
	@Override
	public void fillDeleteFaultMetaQueryAndParam(FaultMetaDto faultMetaDto, List<String> queries, List<Object> parameters) {
		fmDao.fillDeleteQueryAndParam(faultMetaDto.getId(), faultMetaDto.getE2mFaultCode(), queries, parameters);
		fmDao.fillDeleteQueryAndParam(faultMetaDto.getParentId(), faultMetaDto.getE2mFaultCode(), queries, parameters);
	}

	@Override
	public int deleteFaultMetaWithTransaction(List<String> queries, List<Object> parameters) {
		int result = 0;
		
		try {
			result = fmDao.executeQueriesInTransaction(queries, parameters);
		} catch (DataAccessException e) {
			if (e.getCause() != null && e.getCause().getMessage().equals("ERR_FAULT_EXIST")) {
				// TODO: need a better way to return the error message
				// The error msg should be : The fault configuration can't be deleted, some faults with this code already exist
                result = -1;
            }
            throw e;
		}
		
		return result;
	}
}