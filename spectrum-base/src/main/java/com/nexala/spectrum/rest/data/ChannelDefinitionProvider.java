/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelDefinition;
import com.nexala.spectrum.db.beans.ChannelDefinitionParam;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.validation.ChannelStatus;

@ImplementedBy(DefaultChannelDefinitionProvider.class)
public interface ChannelDefinitionProvider {
    List<ChannelDefinition> search(String keyword);

    ChannelDefinition getChannelDefinition(int channelId);

    List<ChannelConfig> getChannelConfigs();

    List<ChannelGroupConfig> getGroups();

    List<ChannelStatus> getStatus();

    boolean save(String params);
}
