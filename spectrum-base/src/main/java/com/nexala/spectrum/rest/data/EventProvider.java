/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.Date;
import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultGroup;
import com.nexala.spectrum.db.beans.FaultGroupInputParam;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventFormation;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;
import com.nexala.spectrum.rest.service.bean.EventTableBean;

@ImplementedBy(DefaultEventProvider.class)
public interface EventProvider {

    void update(Integer eventId, String action, String comment, String userId, Long timestamp, String method);

    List<EventComment> getComments(Integer eventId);
    
    List<EventComment> saveComment(Integer eventId, Long timestamp, String userId, String comment, String action);
    
    /**
     * Returns filtered list of events for current events panel
     * @param limit - number of results to return
     * @param searchCriteria - parameters to filter results in stored procedure
     * @return filtered list of events
     */
    public List<Event> getCurrentPanel(Integer limit, List<Object> searchCriteria);

    /**
     * Returns all the events for a unit. 
     */
    List<Event> getAllByUnit(Integer unitId);
    
    /**
     * Returns the live events for a unit. XXX rename to getLiveByStock
     */
    List<Event> getLiveByUnit(Integer unitId);

    /**
     * Returns a list of recent events for a unit
     */
    List<Event> getRecentByUnit(Integer unitId, int numberOf);
    
    /**
     * Returns a list of live and acknowledged events for a unit
     */
    List<Event> getAcknowledgedByUnit (Integer unitId);
    
    /**
     * Returns a list of live and not acknowledged events for a unit
     */
    List<Event> getNotAcknowledgedByUnit (Integer unitId);

    /**
     * Returns the events which were created after the specified timestamp
     * 
     * @param since
     * @return
     */
    List<Event> getEventsSince(long since);

    /**
     * Returns a list of events of an specific formation.
     * 
     * @param unitIds
     *            A list of unit number representing a formation.
     * @return List<Event>
     */
    List<Event> getEventsForFormation(List<Integer> unitIds);

    /**
     * Returns a event * @param eventId
     */
    Event getEventById(Long eventId, boolean displayDropdownLabels);

    /**
     * Returns a eventFormation for one event * @param eventId
     */
    List<EventFormation> getEventFormationByEventId(Long eventId);

    /**
     * Simple event search
     * 
     * @param dateFrom
     * @param dateTo
     * @param live
     * @param keyword
     * @return
     */
    List<Event> searchEvents(Date dateFrom, Date dateTo, String live, String keyword, Integer limit);

    List<Event> searchEvents(Date dateFrom, Date dateTo, String code, String type, String description, String category,
            String live, List<EventSearchParameter> parameters, Integer limit);
    

    ChannelData getEventChannelDataForStock(Integer eventId, Integer stockId, Integer groupId);
    
    /**
     * Returns extended events details for a specific event
     * @param eventId
     * @return a list containing the fields name, values and type
     */
    List<EventTableBean> getExtendedEventTableData(Long eventId);
    
    /**
     * Acknowledged an event from the Event Detail panel
     * @return
     */
    void acknowledgeEvent(int eventId, Long rowVersion, Integer eventGroupId);
    
    /**
     * Deactivated an event from the Event Detail panel
     * @return
     */
    void deactivateEvent(int eventId, String userName, Boolean isCurrent, Long rowVersion);

    /** Returns the number of live events */
    Integer getEventCountLive(List<Object> criteria);

    /** Returns the number of events (both live and not live) which have been created since the specified timestamp */
    Integer getEventCountSince(List<Object> criteria, long timestamp);
    
    /**
     * Return all fault categories
     * @return all fault categories
     */
    List<FaultCategory> getFaultCategories();
    
    List<FaultMeta> getFaultCodes();
    
    /**
     * Get a fault meta by fault code
     * @param faultCode the fault code
     * @return the fault meta for this fault code
     */
    FaultMeta getFaultMetaByCode(String faultCode);

    List<FaultType> getFaultTypes();
    
    List<FaultGroup> getFaultGroupNames();
    
    boolean areFaultsRetracted(Long timestamp);

    Long getLastLiveEventTimestamp();

    Long getLastRecentEventTimestamp();

    void sendFault(long eventId);

	Long createNewFault(Integer unitId, String faultCode, String fleetId, Long timestamp);
	
	void createFaultGroup(String username, List<FaultGroupInputParam> params, Long leadEventId);
	
	void updateFaultGroup(String username, List<FaultGroupInputParam> params, Long leadEventId);

	List<Event> getEventsByGroup(Integer groupId);
	
    Date getEndTime(Integer eventId);
    
    List<Event> getLiveFormationEventsByUnit(Integer unitId);
    
    List<Event> getRecentFormationEventsByUnit(Integer unitId, int numberOf);

    default ChannelData getEventChannelDataForStock(Integer eventId, Integer stockId, Integer groupId, int period, long timestamp) {
		return null;
	}

	default void saveFaultPeriod(Integer eventId, Integer vehicleId, Integer period, Long timestamp) {
	}
}
