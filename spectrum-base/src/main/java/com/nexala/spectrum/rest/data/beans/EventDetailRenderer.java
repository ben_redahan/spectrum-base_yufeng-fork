/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.rest.data.beans;

import java.util.List;

public class EventDetailRenderer {
    private final String rendererObject;
    private final String rendererFile;
    private final List<String> additionalCssFileList;
    private final List<String> additionalJsFileList;

    public EventDetailRenderer(String rendererObject, String rendererFile, List<String> additionalCssFileList,
            List<String> additionalJsFileList) {
        this.rendererObject = rendererObject;
        this.rendererFile = rendererFile;
        this.additionalCssFileList = additionalCssFileList;
        this.additionalJsFileList = additionalJsFileList;
    } 

    public String getRendererObject() {
        return rendererObject;
    }

    public String getRendererFile() {
        return rendererFile;
    }

    public List<String> getAdditionalCssFileList() {
        return additionalCssFileList;
    }

    public List<String> getAdditionalJsFileList() {
        return additionalJsFileList;
    }
}
