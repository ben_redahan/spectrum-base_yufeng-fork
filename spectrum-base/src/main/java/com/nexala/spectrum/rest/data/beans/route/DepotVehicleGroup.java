package com.nexala.spectrum.rest.data.beans.route;

/**
 * Define a vehicle group for a depot.
 * A vehicle group have a label
 * @author brice
 *
 */
public class DepotVehicleGroup {
    
    /**
     * Label of the group.
     */
    private String label = null;
    
    
    /**
     * The lower code of the group
     */
    private Integer lowerCode = null;
    
    /**
     * The upper code of the group
     */
    private Integer upperCode = null;

    
    /**
     * Constructor with label and codes in argument.
     * @param label the label
     * @param lowerCode the lower code
     * @param upperCode the upper code
     */
    public DepotVehicleGroup(String label, Integer lowerCode, Integer upperCode) {
        this.label = label;
        this.lowerCode = lowerCode;
        this.upperCode = upperCode;
    }
    
    /**
     * Default constructor.
     */
    public DepotVehicleGroup() {
    }
    
    /**
     * Getter for label.
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Setter for label.
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Getter for lowerCode.
     * @return the lowerCode
     */
    public Integer getLowerCode() {
        return lowerCode;
    }

    /**
     * Setter for lowerCode.
     * @param lowerCode the lowerCode to set
     */
    public void setLowerCode(Integer lowerCode) {
        this.lowerCode = lowerCode;
    }

    /**
     * Getter for upperCode.
     * @return the upperCode
     */
    public Integer getUpperCode() {
        return upperCode;
    }

    /**
     * Setter for upperCode.
     * @param upperCode the upperCode to set
     */
    public void setUpperCode(Integer upperCode) {
        this.upperCode = upperCode;
    }
    

}
