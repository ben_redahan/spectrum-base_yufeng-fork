/*
 * Copyright (c) Nexala Technologies 2017, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.mapping.Coordinate;
import com.nexala.spectrum.rest.data.beans.AdhesionReport;
import com.nexala.spectrum.view.conf.maps.AdhesionMarker;

public interface AdhesionProvider {

    public Map<Integer, LinkedList<AdhesionMarker>> getWspData(Coordinate upperLeft,
            Coordinate lowerRight, Object data, String fleetId);

    public List<AdhesionReport> getWspReport(Coordinate upperLeft,
            Coordinate lowerRight, List<Integer> timeRanges, String fleetId);

    public Map<Integer, LinkedList<AdhesionMarker>> getSandData(Coordinate upperLeft, 
            Coordinate lowerRight, Object data, String fleetId);

    public List<AdhesionReport> getSandReport(Coordinate upperLeft,
            Coordinate lowerRight, List<Integer> timeRanges, String fleetId);

	public Map<Integer, LinkedList<AdhesionMarker>> getOhlvData(Coordinate upperLeft, Coordinate lowerRight,
			Object data, String fleetId);
	
    public List<AdhesionReport> getOhlvReport(Coordinate upperLeft,
            Coordinate lowerRight, List<Integer> timeRanges, String fleetId);
}
