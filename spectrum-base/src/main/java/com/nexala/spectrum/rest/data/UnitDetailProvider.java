/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;
import java.util.Map;

import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.Diagram;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.UnitDetail;

public interface UnitDetailProvider {
    /**
     * Returns data for the Unit Detail, which will be displayed in the header. The returned dataset generally includes
     * summary information such as the Last Update Timestamp, Location, Leading Vehicle, # Faults / Warnings.
     * 
     * @return
     */
    DataSet getInfoData(int stockId);

    Map<Integer, Diagram> getDiagramData(int stockId);
    
    /**
     * Return timestamp for vehicles in this dataset.
     * Used on the unit summary to update vehicle timestamps
     * @param data list of datas
     * @return vehicles timestamps
     */
    // FIXME - should return Map<Integer, Long>
    DataSet getVehiclesTimestamp(List<DataSet> data);
    
    String getUnitInfoLabel(List<DataSet> data);
    
    UnitDetail getUnitDetail(int stockId, long timestamp);

    List<GenericBean> getRestrictions(int unitId);
    
    List<GenericBean> getWorkOrders(int unitId);
    
    String getWorkOrderComment(int woNumber);
}
