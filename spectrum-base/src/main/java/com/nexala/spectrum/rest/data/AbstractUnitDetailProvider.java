package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.dao.MaintenanceDao;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.data.beans.UnitDetail;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;

/**
 * Provide default methods implementations for {@link UnitDetailProvider}.
 * @author brice
 *
 */
public abstract class AbstractUnitDetailProvider implements UnitDetailProvider {

    protected UnitProvider unitProvider;

    protected UnitDetailConfiguration unitDetailConfiguration;

    @Inject
    private EventProvider eventProvider;
    
    @Inject
    protected MaintenanceDao woDao;

    public AbstractUnitDetailProvider(UnitProvider unitProvider,
            UnitDetailConfiguration unitDetailConfiguration) {
        
        this.unitProvider = unitProvider;
        this.unitDetailConfiguration = unitDetailConfiguration;
        
    }
    
    @Override
    public UnitDetail getUnitDetail(int stockId, long timestamp) {
        Unit stock = unitProvider.searchUnit(stockId);
        List<StockDesc> tabs = unitDetailConfiguration.getTabs(stock);
        
        List<Event> liveEvents = eventProvider.getLiveByUnit(stockId);
        
        List<Event> recentEvents = eventProvider.getRecentByUnit(stockId,
                Spectrum.UNIT_DETAIL_NUMBER_OF_RECENT_EVENTS);

        UnitDetail ud = new UnitDetail();
        ud.setUnit(stock);
        ud.setTabs(tabs);
        ud.setInfoData(getInfoData(stockId));
        ud.setDiagramData(getDiagramData(stockId));
        ud.setLive(liveEvents);
        ud.setRecent(recentEvents);

        return ud;
    }
    
    @Override
    public List<GenericBean> getWorkOrders(int unitId) {
        return woDao.getWorkOrdersByUnitId(unitId);
    }
    
    @Override
    public String getWorkOrderComment(int woNumber) {
        String result = new String();
        List<String> list =  woDao.getWorkOrderCommentByWONumber(woNumber);
        if(list.size() > 0){
            result =  list.get(0);
        }
        return result;
    }
    
    @Override
    public List<GenericBean> getRestrictions(int unitId) {
        return woDao.getRestrictionsByUnitId(unitId);
    }
}