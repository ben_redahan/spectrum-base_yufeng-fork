package com.nexala.spectrum.rest.service.bean.editor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fleet")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fleet {

	@XmlElement(name="fleet-code", required=true)
	private String fleetCode;

	public String getFleetCode() {
		return fleetCode;
	}

	public void setFleetCode(String fleetCode) {
		this.fleetCode = fleetCode;
	}
}
