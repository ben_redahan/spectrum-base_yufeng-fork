/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.FleetDataCache;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.GlobalFleetProvider;
import com.nexala.spectrum.rest.data.Response;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DrillDown;
import com.nexala.spectrum.rest.service.bean.FleetConfigurationUIBean;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;
import com.nexala.spectrum.view.conf.DefaultCommonFleetConfiguration;

@Path("/fleet")
@Singleton
public class FleetService {

    @Inject
    private FleetDataCache cache;

    @Inject
    private CommonFleetConfiguration commonFleetConf;

    @Inject
    private Map<String, FleetProvider> fleetProviders;
    
    @Inject
    private GlobalFleetProvider globalFleetProvider;

    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private ApplicationConfiguration appConfig;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;
    
    @GET
    @LoggingData
    @Path("/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Map<Integer, DataSet>> all(@PathParam("fleetId") String fleetId) {
        long roundedTime = FleetDataCache.roundedTimestamp();
        List<DataSet> dataSets = cache.get(roundedTime, fleetId);
        
        dataSets = fleetProviders.get(fleetId).getFleet(dataSets);

        return Response.of(roundedTime, DataUtils.toDataSetMap(dataSets));
    }

    @GET
    @LoggingData
    @Path("/drilldown/{fleetId}/{unitIds}/{group}/{config}")
    @Produces(MediaType.APPLICATION_JSON)
    public DrillDown drilldownGroup(@PathParam("fleetId") String fleetId, @PathParam("unitIds") String unitIds,
            @PathParam("group") String group, @PathParam("config") boolean config) {

        return fleetProviders.get(fleetId).getGroupData(config, fleetId, unitIds, group);
    }

    @GET
    @Cache
    @LoggingData
    @Path("/commonConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public CommonFleetConfiguration getCommonFleetConfiguration() {
        return commonFleetConf;
    }

    /*
     *Returns a list of Fleets ordered by the Fleet.Code from the DB
     */
    @GET
    @Cache
    @LoggingData
    @Path("/fleetList")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Fleet> getFleetList() {

        List<Fleet> fleets = new ArrayList<Fleet>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (Fleet f : globalFleetProvider.getFleets()) {
            String fleetLicence = fleetProviders.get(f.getCode()).getLicence(f.getCode());
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                fleets.add(f);
            }
        }
        
        Collections.sort(fleets, new Comparator<Fleet>() {
            @Override
            public int compare(Fleet f1, Fleet f2) {
                return f1.getCode().compareTo(f2.getCode());
            }
        });

        return fleets;
    }  
    
    /*
     * Returns a list of Fleets ordered by the "tabOrder" from the project.conf file 
     */
    @GET
    @Cache
    @LoggingData
    @Path("/fleetTabs")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Fleet> getFleetTabs() {

        List<Fleet> fleets = new ArrayList<Fleet>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        DefaultCommonFleetConfiguration commonFleetConfig = new DefaultCommonFleetConfiguration(appConfig);
        
        Map<String, Integer> fleetTabMap = commonFleetConfig.getFleetTabsOrdered();
        if (fleetTabMap.size() == 0) {
            return getFleetList();
        }
        
        Fleet fleetArray[] = new Fleet[fleetTabMap.size()];
        
        for (Fleet f : globalFleetProvider.getFleets()) {
            String fleetLicence = fleetProviders.get(f.getCode()).getLicence(f.getCode());
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                if (fleetTabMap.get(f.getCode()) != null) { 
                    fleetArray[fleetTabMap.get(f.getCode())-1] = f;
                }
            }
        }
        
        for (int i=0 ; i<fleetTabMap.size(); i++){
            if (fleetArray[i] != null) {
                fleets.add(fleetArray[i]);
            }
        }

        return fleets;
    }

    @GET
    @Cache
    @Path("/fleetConfiguration/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public FleetConfigurationUIBean getFleetConfiguration(@PathParam("fleetId") String fleetId) {

        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        FleetConfigurationUIBean config = new FleetConfigurationUIBean();

        config.setColumns(commonFleetConf.getColumns(fleetId, licence));
        config.setColumnsUnitIdentifier(commonFleetConf.getColumnsUnitIdentifier(fleetId));
        config.setFleetFilters(commonFleetConf.getFleetFilters(fleetId));
        config.setSorter(commonFleetConf.getDefaultSortingColumnName(fleetId, licence));
        config.setShowFleetFullNames(commonFleetConf.isShowFleetFullNames());
        config.setShowSingleFleetTab(commonFleetConf.isShowSingleFleetTab());
        config.setMouseOverHeaderEnabled(commonFleetConf.isMouseOverHeaderEnabled());
        config.setUserConfiguration(fleetProviders.get(fleetId).getUserConfiguration(licence, fleetId).getData());

        return config;
    }
    
}
