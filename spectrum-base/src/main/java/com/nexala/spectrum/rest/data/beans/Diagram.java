package com.nexala.spectrum.rest.data.beans;

import java.util.List;
import java.util.Map;

import com.nexala.spectrum.channel.ChannelCategory;

public class Diagram {
    private final int vehicleId;
    private final boolean leading;
    private final boolean forward;
    private final boolean reverse;
    private final String leadingCab;
    private final Map<String, ChannelCategory> categories;
    private final List<Event> events;
    private final List<Restriction> restrictions;
    private final Long timestamp;
    private final Map<String, Object> extraField;

    public Diagram(int vehicleId, boolean leading, boolean forward, boolean reverse, String leadingCab,
            Map<String, ChannelCategory> category, List<Event> events, List<Restriction> restrictions, Long timestamp, Map<String, Object> extraField) {
        this.vehicleId = vehicleId;
        this.leading = leading;
        this.forward = forward;
        this.reverse = reverse;
        this.leadingCab = leadingCab;
        this.categories = category;
        this.events = events;
        this.restrictions = restrictions;
        this.timestamp = timestamp;
        this.extraField = extraField;
    }

    public Diagram(int vehicleId, boolean leading, boolean forward, boolean reverse, String leadingCab,
            Map<String, ChannelCategory> category, List<Event> events, List<Restriction> restrictions, Long timestamp) {
        this(vehicleId, leading, forward, reverse, leadingCab, category, events, restrictions, timestamp, null);        
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public boolean isLeading() {
        return leading;
    }

    public boolean isForward() {
        return forward;
    }

    public boolean isReverse() {
        return reverse;
    }

    public String getLeadingCab() {
        return leadingCab;
    }

    public Map<String, ChannelCategory> getCategories() {
        return categories;
    }

    public List<Event> getEvents() {
        return events;
    }

    public List<Restriction> getRestrictions() {
        return restrictions;
    }

    public Long getTimestamp() {
        return timestamp;
    }
    
    public Map<String, Object> getExtraField() {
        return extraField;
    }

}
