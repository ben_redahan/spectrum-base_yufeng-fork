package com.nexala.spectrum.rest.data.beans;

import java.security.InvalidParameterException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Bean holding the event channel values for one source. 
 * @author BBaudry
 *
 */
public class SourceEventChannelData {
    
    private final Integer sourceId;
    
    private Date latestTimestamp;
    
    private Date lastInsertTime;
    
    private Map<Integer, Boolean> valueByChannelId = new HashMap<>();
    
    public SourceEventChannelData(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public Map<Integer, Boolean> getValueByChannelId() {
        return valueByChannelId;
    }
    
    /**
     * Add the channel value for the specified channel id and time.
     * @param timestamp the timestamp of the event channel data.
     * @param channelId the channel id
     * @param value the value
     * @return true if the data inserted is replacing a previous value, false otherwise
     */
    public Boolean put(Date timestamp, Integer channelId, Boolean value, Date insertTime) {
        updateTimestamp(timestamp, insertTime);
        
        return valueByChannelId.put(channelId, value);
    }

    public Date getLatestTimestamp() {
        return latestTimestamp;
    }
    
    public Date getLastInsertTime() {
        return lastInsertTime;
    }
    
    /**
     * Merge the SourceEventChannelData
     * @param updated
     */
    public void merge(SourceEventChannelData updated) {
        if (!updated.getSourceId().equals(sourceId)) {
            throw new InvalidParameterException("invalid source");
        }
        
        updateTimestamp(updated.getLatestTimestamp(), updated.getLastInsertTime());
        
        for (Entry<Integer, Boolean> current : updated.getValueByChannelId().entrySet()) {
            valueByChannelId.put(current.getKey(), current.getValue());
        }
        
    }
    
    /**
     * Update the timestamp and lastInsertTime if the one in parameter is more recent.
     * @param timestamp the timestamp we received
     * @param insertTime the insert time of the row in the database
     */
    private void updateTimestamp(Date timestamp, Date insertTime) {
        if (latestTimestamp == null || (timestamp != null && timestamp.after(latestTimestamp))) {
            latestTimestamp = timestamp;
        }
        
        if (lastInsertTime == null || (insertTime != null && insertTime.after(lastInsertTime))) {
            lastInsertTime = insertTime;
        }
    }

}
