/**
 * 
 */
package com.nexala.spectrum.rest.data;

import java.util.Collection;

import com.nexala.spectrum.db.beans.Configuration;

/**
 * Provide the application configurations items.
 * @author bbaudry
 *
 */
public interface ConfigurationProvider {
    
    /**
     * Return the list of all the configurations.
     * @return the list of all the configurations.
     */
    public Collection<Configuration> getAll();
    

}
