/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

public class UnitSummaryButton {

    private final String id;

    private final String displayName;

    private final boolean openUnitSummary;

    private final String unitSummaryTab;

    private final String alternativeAction;

    public UnitSummaryButton(String id, String displayName, boolean openUnitSummary, String unitSummaryTab,
            String alternativeAction) {
        this.id = id;
        this.displayName = displayName;
        this.openUnitSummary = openUnitSummary;
        this.unitSummaryTab = unitSummaryTab;
        this.alternativeAction = alternativeAction;
    }

    public String getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isOpenUnitSummary() {
        return openUnitSummary;
    }

    public String getUnitSummaryTab() {
        return unitSummaryTab;
    }

    public String getAlternativeAction() {
        return alternativeAction;
    }
}