/**
 * 
 */
package com.nexala.spectrum.rest.data;

import java.util.Collection;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.Configuration;
import com.nexala.spectrum.db.dao.ConfigurationDao;

/**
 * @author bbaudry
 *
 */
public class DefaultConfigurationProvider implements ConfigurationProvider {
    
    @Inject
    private ConfigurationDao configDao;

    /**
     * @see com.nexala.spectrum.rest.data.ConfigurationProvider#getAll()
     */
    @Override
    public Collection<Configuration> getAll() {
        return configDao.findAll();
    }

}
