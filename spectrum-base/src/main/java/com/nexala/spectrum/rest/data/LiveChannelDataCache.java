package com.nexala.spectrum.rest.data;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.caching.CacheWrapper;
import com.nexala.spectrum.caching.EhCacheWrapper;
import com.nexala.spectrum.rest.data.beans.ChannelDataKey;
import com.nexala.spectrum.rest.data.beans.DataSet;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.MemoryUnit;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;

/**
 * A cache for the live channel data.
 * Allow to cache the latest values for a source.
 * 
 * @author BBaudry
 *
 */
@Singleton
public class LiveChannelDataCache {
    
    public static final String CACHE_NAME = "channelDataCache";
    
    private CacheWrapper<ChannelDataKey, DataSet> cache;
    
    @Inject
    public LiveChannelDataCache(CacheManager cacheManager) {
        CacheConfiguration cacheConfig = new CacheConfiguration()
                .timeToLiveSeconds(3600).maxBytesLocalHeap(100,
                        MemoryUnit.MEGABYTES);
        cacheConfig.setName(CACHE_NAME);

        PersistenceConfiguration persistanceConfig = new PersistenceConfiguration();
        persistanceConfig.strategy(Strategy.NONE);
        cacheConfig.addPersistence(persistanceConfig);

        Cache cacheObj = new Cache(cacheConfig);
        cacheManager.addCache(cacheObj);
        cache = new EhCacheWrapper<ChannelDataKey, DataSet>(cacheObj);
    }
    
    /**
     * Add a dataset into the cache.
     * @param key the key for the element to cache
     * @param data the element to cache
     */
    public void add(ChannelDataKey key, DataSet data) {
        cache.acquireWriteLockOnKey(key);
        cache.put(key, data);
        cache.releaseWriteLockOnKey(key);
    }
    
    /**
     * Return the dataset for this key.
     * @param key the key
     * @return the dataset cached if any
     */
    public DataSet get(ChannelDataKey key) {
        DataSet result = null;
        cache.acquireReadLockOnKey(key);
        result = cache.get(key);
        cache.releaseReadLockOnKey(key);
        return result;
    }
    
}
