package com.nexala.spectrum.rest.data;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.dao.StockDao;
import com.nexala.spectrum.rest.data.beans.StockDesc;

public class DefaultStockProvider implements StockProvider {

    @Inject
    private StockDao dao;
    
    private static final Comparator<StockDesc> stockDescComparator = new Comparator<StockDesc>() {
        @Override
        public int compare(StockDesc stock1, StockDesc stock2) {
            if (stock1 == null || stock2 == null) {
                return 0;
            }
            
            return stock1.getPosition()-stock2.getPosition();
        }
    };

    @Override
    public List<StockDesc> getSiblings(int stockId) {
    	
    	List<StockDesc> result = dao.getSiblings(stockId);
    	
		Collections.sort(result, stockDescComparator);
    	
        return result;    
    }
    
    @Override
    public List<StockDesc> getStocksByEvent(int eventId) {
        
        List<StockDesc> result = dao.getStocksByEvent(eventId);
        
        Collections.sort(result, stockDescComparator);
        
        return result; 
    }
    
}
