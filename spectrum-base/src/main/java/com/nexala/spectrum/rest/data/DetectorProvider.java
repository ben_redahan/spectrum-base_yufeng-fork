/*
 * Copyright (c) Nexala Technologies 2017, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.view.conf.maps.DetectorSiteMarker;

public interface DetectorProvider {

    public List<DetectorSiteMarker> getDetectorData();
    
}
