package com.nexala.spectrum.rest.data.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.nexala.spectrum.db.dao.Record;

public class FaultHistory implements Record {
    private Long id;
    private String unitNumber;
    private String vehicleNumber;
    private String faultVehicle;
    private Long createTime;
    private Date timeStamp;
    private Date endTime;
    private String headCode;
    private String locationCode;
    private Double latitude;
    private Double longitude;
    private Integer speed;
    private String faultCode;
    private String description;
    private String faultSummary;
    private Double lineVoltage;
    private Double batteryVoltage;
    private String faultCategory;
    private Boolean isCurrent;
    private String type;
    private Integer recoveryID;
    private Long contextID;
    private Integer mainReservoirPressure;
    private String date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public Integer getMainReservoirPressure() {
        return mainReservoirPressure;
    }

    public void setMainReservoirPressure(Integer mainReservoirPressure) {
        this.mainReservoirPressure = mainReservoirPressure;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getFaultVehicle() {
        return faultVehicle;
    }

    public void setFaultVehicle(String faultVehicle) {
        this.faultVehicle = faultVehicle;
    }
    
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFaultSummary() {
        return faultSummary;
    }

    public void setFaultSummary(String faultSummary) {
        this.faultSummary = faultSummary;
    }

    public Double getLineVoltage() {
        return lineVoltage;
    }

    public void setLineVoltage(Double lineVoltage) {
        this.lineVoltage = lineVoltage;
    }

    public Double getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(Double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public String getFaultCategory() {
        return faultCategory;
    }

    public void setFaultCategory(String faultCategory) {
        this.faultCategory = faultCategory;
    }

    public Boolean getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(Boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRecoveryID() {
        return recoveryID;
    }

    public void setRecoveryID(Integer recoveryID) {
        this.recoveryID = recoveryID;
    }

    public Long getContextID() {
        return contextID;
    }

    public void setContextID(Long contextID) {
        this.contextID = contextID;
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((this.batteryVoltage == null) ? 0 : this.batteryVoltage
                        .hashCode());
        result = prime * result
                + ((this.createTime == null) ? 0 : this.createTime.hashCode());
        result = prime * result
                + ((this.endTime == null) ? 0 : this.endTime.hashCode());
        result = prime
                * result
                + ((this.faultCategory == null) ? 0 : this.faultCategory
                        .hashCode());
        result = prime * result
                + ((this.faultCode == null) ? 0 : this.faultCode.hashCode());
        result = prime
                * result
                + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime
                * result
                + ((this.faultSummary == null) ? 0 : this.faultSummary
                        .hashCode());
        result = prime * result
                + ((this.headCode == null) ? 0 : this.headCode.hashCode());
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result
                + ((this.isCurrent == null) ? 0 : this.isCurrent.hashCode());
        result = prime * result
                + ((this.latitude == null) ? 0 : this.latitude.hashCode());
        result = prime
                * result
                + ((this.lineVoltage == null) ? 0 : this.lineVoltage.hashCode());
        result = prime
                * result
                + ((this.locationCode == null) ? 0 : this.locationCode
                        .hashCode());
        result = prime * result
                + ((this.longitude == null) ? 0 : this.longitude.hashCode());
        result = prime * result
                + ((this.recoveryID == null) ? 0 : this.recoveryID.hashCode());
        result = prime * result
                + ((this.speed == null) ? 0 : this.speed.hashCode());
        result = prime * result
                + ((this.timeStamp == null) ? 0 : this.timeStamp.hashCode());
        result = prime * result
                + ((this.type == null) ? 0 : this.type.hashCode());
        result = prime * result
                + ((this.unitNumber == null) ? 0 : this.unitNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FaultHistory other = (FaultHistory) obj;
        if (this.batteryVoltage == null) {
            if (other.batteryVoltage != null)
                return false;
        } else if (!this.batteryVoltage.equals(other.batteryVoltage))
            return false;
        if (this.createTime == null) {
            if (other.createTime != null)
                return false;
        } else if (!this.createTime.equals(other.createTime))
            return false;
        if (this.endTime == null) {
            if (other.endTime != null)
                return false;
        } else if (!this.endTime.equals(other.endTime))
            return false;
        if (this.faultCategory == null) {
            if (other.faultCategory != null)
                return false;
        } else if (!this.faultCategory.equals(other.faultCategory))
            return false;
        if (this.faultCode == null) {
            if (other.faultCode != null)
                return false;
        } else if (!this.faultCode.equals(other.faultCode))
            return false;
        if (this.description == null) {
            if (other.description != null)
                return false;
        } else if (!this.description.equals(other.description))
            return false;
        if (this.faultSummary == null) {
            if (other.faultSummary != null)
                return false;
        } else if (!this.faultSummary.equals(other.faultSummary))
            return false;
        if (this.headCode == null) {
            if (other.headCode != null)
                return false;
        } else if (!this.headCode.equals(other.headCode))
            return false;
        if (this.id == null) {
            if (other.id != null)
                return false;
        } else if (!this.id.equals(other.id))
            return false;
        if (this.isCurrent == null) {
            if (other.isCurrent != null)
                return false;
        } else if (!this.isCurrent.equals(other.isCurrent))
            return false;
        if (this.latitude == null) {
            if (other.latitude != null)
                return false;
        } else if (!this.latitude.equals(other.latitude))
            return false;
        if (this.lineVoltage == null) {
            if (other.lineVoltage != null)
                return false;
        } else if (!this.lineVoltage.equals(other.lineVoltage))
            return false;
        if (this.locationCode == null) {
            if (other.locationCode != null)
                return false;
        } else if (!this.locationCode.equals(other.locationCode))
            return false;
        if (this.longitude == null) {
            if (other.longitude != null)
                return false;
        } else if (!this.longitude.equals(other.longitude))
            return false;
        if (this.recoveryID == null) {
            if (other.recoveryID != null)
                return false;
        } else if (!this.recoveryID.equals(other.recoveryID))
            return false;
        if (this.speed == null) {
            if (other.speed != null)
                return false;
        } else if (!this.speed.equals(other.speed))
            return false;
        if (this.timeStamp == null) {
            if (other.timeStamp != null)
                return false;
        } else if (!this.timeStamp.equals(other.timeStamp))
            return false;
        if (this.type == null) {
            if (other.type != null)
                return false;
        } else if (!this.type.equals(other.type))
            return false;
        if (this.unitNumber == null) {
            if (other.unitNumber != null)
                return false;
        } else if (!this.unitNumber.equals(other.unitNumber))
            return false;
        return true;
    }
    
	public Map<String, String> getLicensingAttributeMap() {
		Map<String, String> licensingAttributeMap = new HashMap<String, String>();
		licensingAttributeMap.put("headcode", this.getHeadCode());
		licensingAttributeMap.put("unitNumber", this.getUnitNumber());
		return licensingAttributeMap;
	}    
}
