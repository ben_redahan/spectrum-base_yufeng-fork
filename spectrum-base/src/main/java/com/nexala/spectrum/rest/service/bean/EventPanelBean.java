package com.nexala.spectrum.rest.service.bean;

import java.util.List;

/**
 * Bean for current event panel,
 * @author ben
 *
 */
public class EventPanelBean {
    private String name;
    private String label;
    private int position;
    private List<EventFilterBean> criteriaList;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public List<EventFilterBean> getCriteriaList() {
		return criteriaList;
	}
	public void setCriteriaList(List<EventFilterBean> criteriaList) {
		this.criteriaList = criteriaList;
	}

    
}
