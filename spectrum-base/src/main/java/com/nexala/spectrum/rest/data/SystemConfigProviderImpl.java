package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.ConfigurationDao;

public class SystemConfigProviderImpl implements SystemConfigProvider {

	@Inject 
	private ConfigurationDao configDao;
	
	@Override
	public String getDatabaseName() {
		String databaseName = "";
		
		String query = new QueryManager()
				.getQuery(Spectrum.DATABASE_NAMES);
		List<String> databaseNames = configDao.getStringListByQuery(query);
		
		if (databaseNames.size() == 1) {
			databaseName = databaseNames.get(0);
		}
		return databaseName;
	}
}
