/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.GroupProvider;

@Path("/groups")
@Singleton
public class GroupService {
    @Inject
    private Map<String, GroupProvider> groupProviders;
    
    @GET
    @Cache
    @LoggingData
    @Path("/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ChannelGroup> all(@PathParam("fleetId") String fleetId) {
        return groupProviders.get(fleetId).getVisibleGroupList();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/{fleetId}/{groupId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ChannelConfig> channels(@PathParam("fleetId") String fleetId, @PathParam("groupId") int groupId) {
        return groupProviders.get(fleetId).getChannels(groupId);
    }
}
