/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.DataSet;

public interface ChannelProvider {
    List<DataSet> group(String formation, String group);
}