package com.nexala.spectrum.rest.cache;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;

/**
 * Method interceptor called on method annotated with {@link Cache}. <br>
 * Should be a rest service, the {@link HttpServletResponse} header will be modified
 * depending on option passed.
 * Cache could be enable or disable with property {@link Spectrum#SPECTRUM_SERVICES_CACHE}
 * 
 * @author brice
 * 
 */
@Singleton
public class RestCacheInterceptor implements MethodInterceptor {

    private static final Logger LOGGER = Logger
            .getLogger(RestCacheInterceptor.class);

    private SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;
    
    @Inject
    private ConfigurationManager confManager;

    /**
     * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
     */
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        
        boolean cacheEnabled = confManager.getConfAsBoolean(Spectrum.SPECTRUM_SERVICES_CACHE, false);
        
        if (cacheEnabled) {
            
            Cache annotation = invocation.getMethod().getAnnotation(Cache.class);

            if (annotation != null) {
                try {
                   HttpServletResponse httpResponse = responseProvider.get();
                   
                   buildHeader(httpResponse, annotation);

                } catch (Exception ex) {
                    LOGGER.error("Error getting rest call parameters for cache");
                    ex.printStackTrace();
                }
            }
        }

        return invocation.proceed();
    }
    
    
    /**
     * Build the response header
     * @param httpResponse the http response
     * @param cache the {@link Cache} annotation
     */
    private void buildHeader(HttpServletResponse httpResponse, Cache cache) {
        long now = new Date().getTime();
        
        int cacheDuration = 0;
        
        httpResponse.setHeader("Pragma", "cache");
        
        List<String> cacheControlParams = new ArrayList<String>();
        
        if(cache.noTransform()) {
            cacheControlParams.add("no-transform");
        }
        
        if(cache.isPrivate()) {
            cacheControlParams.add("private");
        } else {
            cacheControlParams.add("public");
        }
        
        if (cache.mustRevalidate()) {
            cacheControlParams.add("must-revalidate");
        }
        
        if (cache.noStore()) {
            cacheControlParams.add("no-store");
        }
        
        if (cache.proxyRevalidate()) {
            cacheControlParams.add("proxy-revalidate");
        }
        
        if(cache.maxAge() != -1) {
            // max age have been specified
            cacheDuration = cache.maxAge();
            StringBuilder maxAge = new StringBuilder("max-age=");
            maxAge.append(cacheDuration);
            
            cacheControlParams.add(maxAge.toString());
        } else {
            // Otherwise we are using s-maxAge
            cacheDuration = cache.sMaxAge();
            StringBuilder sMaxAge = new StringBuilder("s-maxage=");
            sMaxAge.append(cacheDuration);
            
            cacheControlParams.add(sMaxAge.toString());            
        }
        
        // Build cache-control params
        StringBuilder cacheControl = new StringBuilder();
        
        if(cacheControlParams.size() > 0) {
            Iterator<String> it = cacheControlParams.iterator();
            
            // we have at least 1 elem
            cacheControl.append(it.next());
            
            while (it.hasNext()) {
                String current = it.next();
                cacheControl.append(",");
                cacheControl.append(current);
            }
        }
        
        httpResponse.setHeader("Cache-control", cacheControl.toString());
        httpResponse.setHeader("Expires", formatter.format(new Date(now+cacheDuration*1000)));
        
    }
    

}
