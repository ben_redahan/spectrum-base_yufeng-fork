/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

import java.util.ArrayList;
import java.util.List;

public class UnitDescList {
    private final String[] headers;
    private List<UnitDesc> units = new ArrayList<UnitDesc>();
    private boolean truncated = false;

    /**
     * @param Whether there are more results or not.
     * @param header
     *            The first header, e.g "Headcode"
     * @param otherHeaders
     *            The remaining headers, e.g. "Unitcode", "Formation", etc.
     */
    public UnitDescList(String header, String... otherHeaders) {
        headers = new String[otherHeaders.length + 1];
        headers[0] = header;
        System.arraycopy(otherHeaders, 0, headers, 1, otherHeaders.length);
    }

    /**
     * @param id
     *            The db record id of the "Unit"
     * @param firstDesc
     *            the first unit descriptor, i.e. unitCode, headCode, etc.
     * @param otherDescs
     *            the remaining unit descriptors, i.e. unitCode, headCode, etc.
     */
    public void addRecord(long id, String displayName, String fleetId, String firstDesc,
            String... otherDescs) {
        if (displayName == null) {
            throw new IllegalArgumentException("displayName may not be null");
        }

        UnitDesc desc = new UnitDesc(id, displayName, fleetId);
        desc.addDescriptor(firstDesc);

        for (String nextDesc : otherDescs) {
            desc.addDescriptor(nextDesc);
        }

        units.add(desc);
    }

    public String[] getHeaders() {
        return headers;
    }

    public List<UnitDesc> getUnitDescs() {
        return units;
    }
    
    public void truncate(int maxResults) {
        if (maxResults < units.size()) {
            units = units.subList(0, maxResults - 1);
            truncated = true;
        } else {
            truncated = false;
        }
    }
    
    public boolean isTruncated() {
        return truncated;
    }
    
    public int getResultCount() {
        return units.size();
    }

    public void setUnitDescs(List<UnitDesc> units) {
        this.units = units;
    }
}