package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.Fleet;

@ImplementedBy(value=DefaultGlobalFleetProvider.class)
public interface GlobalFleetProvider {
	
	public List<Fleet> getFleets();

}
