/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.ParameterDto;
import com.nexala.spectrum.rest.data.beans.DownloadInfo;

@ImplementedBy(DefaultDownloadProvider.class)
public interface DownloadProvider {
    List<DownloadInfo> searchDownload(Date dateFrom, Date dateTo, String unit,
            String vehicle, String fileType);

    void insertDownload(String vehicleId, String fileType, String startDate, String startTime, String endTime);

    // TODO make insert generic - requestTypeId and devices are railhead specific
    void insertDownload(String vehicleId, String fileType, String startDate, String startTime, String endTime
    		, String requestTypeId, String devices);
    
    Boolean checkCurrentDownload(String vehicleId, String fileType);
    String downloadFile(String fileName) throws IOException ;

	void insertDownload(List<ParameterDto> parameters);
}