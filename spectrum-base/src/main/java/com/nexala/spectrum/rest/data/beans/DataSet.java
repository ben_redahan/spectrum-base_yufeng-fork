/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.nexala.spectrum.channel.ChannelCollection;

public interface DataSet {
    //public Long getRowId();
    
    public Integer getSourceId();

    public Long getTimestamp();

    @JsonUnwrapped
    public ChannelCollection getChannels();
    
    public void setChannels(ChannelCollection cc);
}
