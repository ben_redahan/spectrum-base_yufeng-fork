package com.nexala.spectrum.rest.service.bean.editor;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "variables")
@XmlAccessorType(XmlAccessType.FIELD)
public class Variables {
    
    @XmlAnyElement
    @XmlElementRefs({
        @XmlElementRef(type = Variable.class)
    })
    private List<Variable> variables;

    /**
     * Return the variables.
     * @return the variables
     */
    public List<Variable> getVariables() {
        return variables;
    }

    /**
     * Setter for the variablesvariables.
     * @param packages the packages to set
     */
    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

}
