/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

import java.util.List;

/**
 * Bean for a single Event field
 * @author Fulvio
 */
public class EventField extends BeanField {

    private List<String> fleets;

    private List<String> screens;

    public List<String> getFleets() {
        return fleets;
    }

    public void setFleets(List<String> fleets) {
        this.fleets = fleets;
    }

    public List<String> getScreens() {
        return screens;
    }

    public void setScreens(List<String> screens) {
        this.screens = screens;
    }
}
