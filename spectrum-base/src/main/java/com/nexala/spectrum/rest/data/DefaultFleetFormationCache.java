package com.nexala.spectrum.rest.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.FleetFormation;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;

/**
 * Cache for fleetformation.
 * @author BBaudry
 *
 */
public class DefaultFleetFormationCache implements FleetFormationCache {
    
    private Map<String, Long> lastLoadPerDatabase = new HashMap<>();
    
    private Map<String, List<FleetFormation>> formationsPerFleet = new HashMap<>();
    
    private ListMultimap<String, String> fleetsPerDatabase = ArrayListMultimap.create();
    
    private static final Logger LOGGER = Logger.getLogger(DefaultFleetFormationCache.class);

    @Inject
    private Map<String, UnitProvider> unitProvider;
    
    public final int cacheInterval;
    
    @Inject
    public DefaultFleetFormationCache(CommonFleetConfiguration fleetConf, ApplicationConfiguration appConf) {

        fleetsPerDatabase = fleetConf.getFleetsPerDatabase();
        cacheInterval = appConf.get().getInt("r2m.fleetFormationCacheInterval");
        
    }
    
    /**
     * In charge of getting the fleet formation.
     * If the cache is too old will reload it
     * @param fleet the fleet id
     */
    public List<FleetFormation> getValidFleetFormations(String fleet) {

        for (String currentdb : fleetsPerDatabase.keySet()) {
            if (fleetsPerDatabase.containsEntry(currentdb, fleet)) {
                Long currentTime = System.currentTimeMillis();
                Long lastLoad = lastLoadPerDatabase.get(currentdb);
                if (lastLoad == null || currentTime - lastLoad  > cacheInterval) {
                    List<FleetFormation> formations = unitProvider.get(fleet).getValidFleetFormations();
                    for (String current : fleetsPerDatabase.get(currentdb)) {
                        formationsPerFleet.put(current, formations);
                        LOGGER.info("Loaded "+formations.size()+" formations for fleet "+current);
                    }
                    lastLoadPerDatabase.put(currentdb, System.currentTimeMillis());
                    
                }
                break;
            }
        }
        
        return formationsPerFleet.get(fleet);
    }


}
