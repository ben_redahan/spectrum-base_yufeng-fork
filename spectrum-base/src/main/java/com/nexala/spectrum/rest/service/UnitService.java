/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.view.conf.unit.UnitSearchConfiguration;

@Path("/unit")
@Singleton
public class UnitService {

    @Inject
    private Map<String, UnitProvider> unitProviders;
    
    @Inject
    private Map<String, ChannelDataProvider> channelDataProviders;

    @Inject
    private UnitSearchConfiguration searchConf;

    @Inject
    private DataLicensor dataLicensor;

    @Inject
    private Map<String, FleetProvider> fleetProviders;

    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;
    
    @GET
    @Cache
    @LoggingData
    @Path("/unitSearchConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public UnitSearchConfiguration unitSearchConfiguration() {
        return searchConf;
    }

    @GET
    @LoggingData
    @Path("/find/{substr: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericBean> searchStock(@PathParam("substr") String substr) {
        // XXX possibly a unit cache is required. thought at the moment
        // seems to be reasonably performant.

        List<GenericBean> results = null;

        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (String fleetId : unitProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                // FIXME - Nasty
                List<GenericBean> resultsAux = unitProviders.get(fleetId).searchUnitsByString(substr);
                if (results == null) {
                    results = resultsAux;
                } else {
                    results.addAll(resultsAux);
                }
            }
        }
        
        results = dataLicensor.getLicenced(results);

        return results;
    }

    @GET
    @LoggingData
    @Path("/{unitId: [0-9]+}/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<StockDesc> stock(@PathParam("unitId") int unitId,
            @PathParam("fleetId") String fleetId) {
        return unitProviders.get(fleetId).getStockForUnit(unitId);
    }

    @GET
    @LoggingData
    @Path("/allUnits")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Unit> unit() {
        List<Unit> results = new ArrayList<Unit>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (String fleetId : unitProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                results.addAll(unitProviders.get(fleetId).getAllUnits());
            }
        }

        return dataLicensor.getLicenced(results);
    }
    
    
    /**
     * Return the last available timestamp of this unit
     * @param fleetId the fleetId
     * @param unitId the unit id
     * @return the last available timestamp of this unit
     */
    @GET
    @LoggingData
    @Path("/timestamp/{fleetId}/{unitId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Long getLiveTimestamp(@PathParam("fleetId") String fleetId,
    		@PathParam("unitId") Integer unitId) {
        return channelDataProviders.get(fleetId).getUnitTimestamp(unitId, null);
    }
    
    /**
     * Return the last available maintenance timestamp of this unit
     * @param fleetId the fleetId
     * @param unitId the unit id
     * @return the last available maintenance timestamp of this unit
     */
    @GET
    @LoggingData
    @Path("/lastMaintenance/{fleetId}/{unitId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Long getLastMaintenance(@PathParam("fleetId") String fleetId,
    		@PathParam("unitId") Integer unitId) {
        return channelDataProviders.get(fleetId).getUnitLastMaintTimestamp(unitId);
    }

    @GET
    @Path("/formationUnits/{unitId: [0-9]+}/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Unit> formationUnits(@PathParam("unitId") int unitId,
            @PathParam("fleetId") String fleetId) {
        return unitProviders.get(fleetId).getFormationUnits(unitId);
    }
    
    @GET
    @LoggingData
    @Path("/fleetUnits/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Unit> fleetUnits(@PathParam("fleetId") String fleetId) {
        List<Unit> results = new ArrayList<Unit>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
        if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
            results.addAll(unitProviders.get(fleetId).getAllUnits());
        }

        return dataLicensor.getLicenced(results);
    }
}

