package com.nexala.spectrum.rest.service.bean.editor;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "faultConfigurationMeta")
@XmlAccessorType(XmlAccessType.FIELD)
public class FaultConfigurationMeta {
	
	@XmlElement(name = "group-by-fleet")
	private Boolean groupByFleet;
	
	@XmlAnyElement
	@XmlElementWrapper(name = "fleets")
	@XmlElementRefs({
		@XmlElementRef(type = Fleet.class)
	})
	private List<Fleet> fleets;

	public Boolean getGroupByFleet() {
		return groupByFleet;
	}

	public void setGroupByFleet(Boolean groupByFleet) {
		this.groupByFleet = groupByFleet;
	}

	public List<Fleet> getFleets() {
		return fleets;
	}
	
	public void setFleets(List<Fleet> fleets) {
		this.fleets = fleets;
	}
}
