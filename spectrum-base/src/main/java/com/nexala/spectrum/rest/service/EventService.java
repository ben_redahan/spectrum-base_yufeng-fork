/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultGroupInputParam;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.CommonEventProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.EventSearchProvider;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.Dropdown;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventFormation;
import com.nexala.spectrum.rest.data.beans.EventNotification;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.security.TimeoutFree;
import com.nexala.spectrum.rest.service.bean.EventFilterBean;
import com.nexala.spectrum.rest.service.bean.EventHistoryConfiguration;
import com.nexala.spectrum.rest.service.bean.EventPanelBean;
import com.nexala.spectrum.rest.service.bean.EventTableBean;
import com.nexala.spectrum.rest.service.bean.FilterOptionBean;
import com.nexala.spectrum.rest.service.bean.LicenceConfigurationBean;
import com.nexala.spectrum.utils.DateUtils;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.CurrentEventsConfiguration;
import com.nexala.spectrum.view.conf.CurrentEventsPanel;
import com.nexala.spectrum.view.conf.EventConfiguration;
import com.nexala.spectrum.view.conf.EventDetailConfiguration;
import com.nexala.spectrum.view.conf.EventGroupConfiguration;
import com.nexala.spectrum.view.conf.ExportConfiguration;
import com.nexala.spectrum.view.conf.Field;

import au.com.bytecode.opencsv.CSVWriter;

@Path("/event")
@Singleton
public class EventService {
	
	@Inject
    private AppConfiguration appConf;

    @Inject
    private DataLicensor dataLicensor;

    @Inject
    private EventDetailConfiguration detailConfiguration;

    @Inject
    private EventGroupConfiguration groupConfiguration;
    
    @Inject
    private EventConfiguration eventConfiguration;

    @Inject
    private CurrentEventsConfiguration currentEventsConfiguration;

    @Inject
    private Map<String, EventProvider> eventProviders;

    @Inject
    private CommonEventProvider commonEventProvider;

    @Inject
    private EventSearchProvider eventSearchProvider;
    
    @Inject 
    private ConfigurationManager configurationManager;

    @Inject
    private Map<String, FleetProvider> fleetProviders;
    
    @Inject
    private Map<String, UnitProvider> unitProvider;

    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;

    @POST
    @LoggingData
    @Path("/update/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean update(Map<String, String> parameters, @Context HttpServletRequest request) {
        String fleetId = parameters.get("fleetId");
        int eventId = Integer.parseInt(parameters.get("eventId"));
        String action = parameters.get("action");
        String comment = parameters.get("comment");
        String method = parameters.get("method");
        String timestamp = parameters.get("timestamp");

        Licence licence = Licence.get(request, responseProvider.get(), false);

        eventProviders.get(fleetId).update(eventId, action, comment, licence.getLoginUser().getId(),
                (timestamp != null ? Long.parseLong(timestamp) : null), method);

        return true;
    }

    @GET
    @LoggingData
    @Path("/comments/{fleetId}/{eventId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventComment> getComments(@PathParam("fleetId") String fleetId, @PathParam("eventId") int eventId) {
        return eventProviders.get(fleetId).getComments(eventId);
    }
    
    @POST
    @LoggingData
    @Path("/saveComment")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<EventComment> saveComment(HashMap<String, String> parameters) {
        
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        
    	String fleetId = parameters.get("fleetId");
    	Integer eventId = Integer.valueOf(parameters.get("eventId"));
    	String userName = licence.getLoginUser().getLogin();
    	String comment = parameters.get("comment");
    	String action = parameters.get("action");
    	
    	Long timestamp;
    	if (parameters.get("timestamp") == null) {
            timestamp = new Date().getTime();
        } else {
            timestamp = Long.valueOf(parameters.get("timestamp"));
        }
    	return eventProviders.get(fleetId).saveComment(eventId, timestamp, userName, comment, action);
    }

    @GET
    @Cache
    @LoggingData
    @Path("/additionalSearchFields")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Field> additionalSearchFields() {
        return eventConfiguration.getAdditionalAdvancedSearchFields();
    }

    private List<Event> advanced(Map<String, String> searchParametersUnordered) {
    	TreeMap<String, String> searchParameters = new TreeMap<String, String>();
    	searchParameters.putAll(searchParametersUnordered);
    	
        String fleetId = searchParameters.remove("fleetId");
        Integer limit = Integer.parseInt(searchParameters.remove("limitHistory"));
        Date startDate = new Date(Long.parseLong(searchParameters.remove("startDate")));
        Date endDate = new Date(Long.parseLong(searchParameters.remove("endDate")));
        String live = searchParameters.remove("live");
        String code = searchParameters.remove("code");
        String type = searchParameters.remove("type");
        String description = searchParameters.remove("description");
        String category = searchParameters.remove("category");

        // Get the remaining project specific parameters..

        List<EventSearchParameter> additionalParameters = new ArrayList<EventSearchParameter>();


        for (Map.Entry<String, String> entry : searchParameters.entrySet()) {
            if (entry.getKey().equals("unitType") && !entry.getValue().isEmpty()) {
            	List<String> unitTypesList = getUnitTypes(getFleets());
            	additionalParameters.add(new EventSearchParameter(entry.getKey(), unitTypesList.get(Integer.parseInt(entry.getValue()))));
            } else {
            	additionalParameters.add(new EventSearchParameter(entry.getKey(), entry.getValue()));
            }
        }

        List<Event> events;
        if (fleetId != null && !fleetId.equals("")) {
            events = eventSearchProvider.searchFleetEvents(eventProviders.get(fleetId), startDate, endDate, live, code,
                    type, description, category, additionalParameters, limit);
        } else {
            Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
            events = eventSearchProvider.searchEvents(commonEventProvider, startDate, endDate, live, code, type,
                    description, category, additionalParameters, limit, licence);
        }

        return dataLicensor.getLicenced(events);
    }

    @POST
    @LoggingData
    @Path("/advancedSearch/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Event> advancedSearch(Map<String, String> searchParameters) {
        return advanced(searchParameters);
    }

    @POST
    @LoggingData
    @Path("/advancedDownload")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/csv")
    public Response advancedSearchDownload(@Context HttpServletResponse response, @FormParam("data") String data,
            @FormParam("timezone") String timezone) throws IOException {
        
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        // FIXME doesn't handle summerTime use the jstz.js instead
        TimeZone tz = DateUtils.getClientTimeZone(timezone);

        TreeMap<String, String> searchParameters = new ObjectMapper().readValue(data,
                TypeFactory.defaultInstance().constructMapType(TreeMap.class, String.class, String.class));

        List<Event> events = advanced(searchParameters);
        createFile(events, tz, response, licence);
        return Response.ok().build();
    }

    private void createFile(List<Event> eventList, TimeZone timezone, HttpServletResponse response, Licence licence) throws IOException {
    	ExportConfiguration exportConf = appConf.getExportConfig();
        OutputStream outputStream = response.getOutputStream();
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(licence.getUserLocale());
        String fileName = bundle.getString(Spectrum.FAULT_EXPORT_FILE_NAME);
        
        response.setContentType("text/" + exportConf.getFileExtension());
        response.addHeader("Content-disposition", "attachment; filename=" + fileName + "." + exportConf.getFileExtension());

        PrintWriter writer = new PrintWriter(outputStream);
        CSVWriter csvWriter = new CSVWriter(writer, exportConf.getSeparator());

        csvWriter.writeNext(eventConfiguration.getHeaders(licence));

        for (Iterator<Event> it = eventList.iterator(); it.hasNext();) {
            Event event = it.next();
            csvWriter.writeNext(eventConfiguration.getRow(event, timezone));
        }

        csvWriter.close();
        writer.close();
        outputStream.close();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventCategories/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FaultCategory> eventCategories(@PathParam("fleetId") String fleetId) throws IOException {
        return eventProviders.get(fleetId).getFaultCategories();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventCategories")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<FaultCategory> eventCategories() throws IOException {
        Map<String, FaultCategory> categories = new LinkedHashMap<String, FaultCategory>();

        for (FaultCategory s : commonEventProvider.getLiveAndRecentCategories()) {
            if (!categories.containsKey(s)) {
                categories.put(s.getCategory(), s);
            }
        }

        return categories.values();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventCodes")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<FaultMeta> eventCodes() throws IOException {
        Map<String, FaultMeta> faultMeta = new LinkedHashMap<String, FaultMeta>();

        for (FaultMeta fc : commonEventProvider.getAllFleetsFaultCodes()) {
            if (!faultMeta.containsKey(fc.getFaultCode())) {
                faultMeta.put(fc.getFaultCode(), fc);
            }
        }

        return faultMeta.values();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventTypes")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<FaultType> eventTypes() throws IOException {
        Map<Integer, FaultType> faultTypes = new LinkedHashMap<Integer, FaultType>();

        for (FaultType ft : commonEventProvider.getAllFleetsFaultTypes()) {
            if (!faultTypes.containsKey(ft.getId())) {
                faultTypes.put(ft.getId(), ft);
            }
        }

        return faultTypes.values();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventColumns")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Column> eventColumns() throws IOException {
        return eventConfiguration.getColumns();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventHistoryConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public EventHistoryConfiguration eventConfiguration() throws IOException {
        EventHistoryConfiguration result = new EventHistoryConfiguration();
        result.setAdditionnalFields(additionalSearchFields());
        result.setAdditionalDropDownValues(commonEventProvider.getAdditionnalDropDownValues(getFleets()));
        result.setEventColumns(eventColumns());
        result.setSearchLimit(configurationManager.getConfAsInteger(Spectrum.SPECTRUM_EVENT_HISTORY_SEARCH_LIMIT, 3000));
        result.setExportLimit(configurationManager.getConfAsInteger(Spectrum.SPECTRUM_EVENT_HISTORY_ADVANCED_EXPORT_LIMIT, 30000));
        result.setDefinedFiltersVisible(eventConfiguration.isDefinedFiltersVisible());
        result.setDefinedFilters(eventConfiguration.getDefinedFilters());
        result.setGroupButtonVisible(eventConfiguration.isGroupButtonVisible());
        result.setDisplayGroupsInBlock(eventConfiguration.isDisplayGroupsInBlock());
        result.setDefaultSortColumn(eventConfiguration.getDefaultSortColumn());
        
        return result;
    }

    @GET
    @LoggingData
    @Path("/eventData/{fleetId}/{eventId: [0-9]+}/{groupId: [0-9]+}/{vehicleId: [0-9]+}/{period}/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public ChannelData eventData(@PathParam("eventId") int eventId, @PathParam("groupId") int groupId,
            @PathParam("vehicleId") int vehicleId, @PathParam("fleetId") String fleetId , @PathParam("period") int period,@PathParam("timestamp") long timestamp) {
    			
    			if(period != 0) {
    				 return eventProviders.get(fleetId).getEventChannelDataForStock(eventId, vehicleId, groupId, period,timestamp);
    			}
    		
        return eventProviders.get(fleetId).getEventChannelDataForStock(eventId, vehicleId, groupId);
    }

    @GET
    @LoggingData
    @Path("/eventDetail/{fleetId}/{eventId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Event eventDetail(@PathParam("eventId") long eventId, @PathParam("fleetId") String fleetId) {
        return eventProviders.get(fleetId).getEventById(eventId, true);
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventDetailConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public EventDetailConfiguration eventDetailConfiguration() {
        return detailConfiguration;
    }

    @GET
    @LoggingData
    @Path("/extendedEventDetail/{fleetId}/{eventId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventTableBean> getExtendedEventDetailData(@PathParam("eventId") long eventId,
            @PathParam("fleetId") String fleetId) {
        return eventProviders.get(fleetId).getExtendedEventTableData(eventId);
    }
    
    @GET
    @LoggingData
    @Path("/eventsByGroup/{fleetId}/{faultGroupId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getEventsByGroup(@PathParam("faultGroupId") int faultGroupId, @PathParam("fleetId") String fleetId) {
        return eventProviders.get(fleetId).getEventsByGroup(faultGroupId);
    }

    @GET
    @Path("/getLicences")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Boolean> hasUpdateStatusLicence(@Context HttpServletRequest request) {
        Map<String, Boolean> licences = new HashMap<String, Boolean>();
    	List<LicenceConfigurationBean> configLicences = this.eventDetailConfiguration().getLicences();
    	Licence licence = Licence.get(request, responseProvider.get(), true);
    	
    	for (LicenceConfigurationBean configLicence: configLicences) {
    		licences.put(configLicence.getPermission(), licence.hasResourceAccess(configLicence.getLicenceKey()));
    	}

        return licences;
    }
    
    @GET
    @LoggingData
    @Path("/eventFormation/{fleetId}/{eventId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventFormation> eventFormation(@PathParam("fleetId") String fleetId, @PathParam("eventId") long eventId) {
        return eventProviders.get(fleetId).getEventFormationByEventId(eventId);
    }

    @GET
    @LoggingData
    @Path("/formation/{fleetId}/{formation}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> eventsForFormation(@PathParam("fleetId") String fleetId, @PathParam("formation") String formation) {
        String[] units = formation.split(" ");
        List<Integer> unitIds = Lists.newArrayList();

        for (String unitId : units) {
            unitIds.add(Integer.parseInt(unitId));
        }

        List<Event> events = eventProviders.get(fleetId).getEventsForFormation(unitIds);

        return dataLicensor.getLicenced(events);
    }

    /**
     * Returns a list of the columns required for the fleet summary events
     * overview & common (popup) events overview [all other screens]
     * 
     * @return
     */
    @GET
    @Cache
    @LoggingData
    @Path("/panelConfiguration/{isCurrentScreenFleetSummary}")
    @Produces(MediaType.APPLICATION_JSON)
    public CurrentEventsPanel eventPanelConfiguration(
            @PathParam("isCurrentScreenFleetSummary") Boolean isCurrentScreenFleetSummary) throws IOException {
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        CurrentEventsPanel result = currentEventsConfiguration.getConfig(isCurrentScreenFleetSummary);
        // Always refresh filter options as they might have changed
        setFilterOptions(result);
        
        // Add the user configuration
        UserConfiguration userConfiguration = commonEventProvider.getEventPanelUserConfiguration(licence);
        result.setUserConfiguration(userConfiguration.getData());
        
        return result;
    }
    
    private void setFilterOptions(CurrentEventsPanel panel) throws IOException {
        Map<String, List<FilterOptionBean>> uniqueFilterOptions = new HashMap<String, List<FilterOptionBean>>();
        
        for (EventPanelBean eventPanel : panel.getEventPanels()) {
            for (EventFilterBean eventFilterBean : eventPanel.getCriteriaList()) {
                uniqueFilterOptions.put(eventFilterBean.getName(), new ArrayList<FilterOptionBean>());
            }
        }
        
        for (Map.Entry<String, List<FilterOptionBean>> filterOption : uniqueFilterOptions.entrySet()) {
            if (filterOption.getKey().equals(Spectrum.EVENT_CATEGORY_FILTER)) {
                filterOption.getValue().addAll(getCategoriesAsFilters(eventCategories()));
            } else if (filterOption.getKey().equals(Spectrum.EVENT_SEVERITY_FILTER)) {
                filterOption.getValue().addAll(getSeveritiesAsFilters(getEventSeverities()));
            } else if (filterOption.getKey().equals(Spectrum.EVENT_FLEET_FILTER)) {
                filterOption.getValue().addAll(getFleetsAsFilters(getFleets()));
            } else if (filterOption.getKey().equals(Spectrum.EVENT_UNIT_TYPE_FILTER)) {
                filterOption.getValue().addAll(getUnitTypesAsFilters(getFleets()));
            }
        }
        
        for (EventPanelBean eventPanel : panel.getEventPanels()) {
            for (EventFilterBean eventFilterBean : eventPanel.getCriteriaList()) {
                eventFilterBean.setOptions(uniqueFilterOptions.get(eventFilterBean.getName()));
            }
        }
    }
    
    private Collection<FilterOptionBean> getCategoriesAsFilters(Collection<FaultCategory> categories) throws IOException {
        List<FilterOptionBean> result = new ArrayList<FilterOptionBean>();
        for (FaultCategory category : categories) {
            FilterOptionBean fob = new FilterOptionBean();
            fob.setDisplayName(category.getCategory());
            fob.setId(category.getId().toString());
            result.add(fob);                
        }
        
        return result;
    }
    
    private Collection<FilterOptionBean> getSeveritiesAsFilters(Collection<FaultType> severities) throws IOException {
        List<FilterOptionBean> result = new ArrayList<FilterOptionBean>();
        for (FaultType fc : severities) {
            FilterOptionBean fob = new FilterOptionBean();
            fob.setDisplayName(fc.getName());
            fob.setId(fc.getId().toString());
            result.add(fob);             
        }
        
        return result;
    }
    
    private Collection<FilterOptionBean> getFleetsAsFilters(Collection<Fleet> fleets) throws IOException {
        List<FilterOptionBean> result = new ArrayList<FilterOptionBean>();
        for (Fleet fleet : fleets) {
            FilterOptionBean fob = new FilterOptionBean();
            fob.setDisplayName(fleet.getCode());
            fob.setId(fleet.getCode());
            result.add(fob);                
        }
        
        return result;
    }
    
    private Collection<FilterOptionBean> getUnitTypesAsFilters(Collection<Fleet> fleets) throws IOException {
    	List<FilterOptionBean> result = new ArrayList<FilterOptionBean>();
        List<String> unitTypeList = getUnitTypes(fleets);
    	for (String unitType : unitTypeList) {
			FilterOptionBean fob = new FilterOptionBean();
            fob.setDisplayName(unitType);
            fob.setId(unitType);
            result.add(fob); 
    	}
        return result;
    }
    
    @GET
    @Cache
    @LoggingData
    @Path("/eventSeverities/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FaultType> getEventSeverities(@PathParam("fleetId") String fleetId) throws IOException {
        return eventProviders.get(fleetId).getFaultTypes();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventSeverities")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<FaultType> getEventSeverities() throws IOException {
        Map<Integer, FaultType> result = new HashMap<Integer, FaultType>();

        for (FaultType fc : commonEventProvider.getLiveAndRecentSeverities()) {
            if (!result.containsKey(fc.getId())) {
                result.put(fc.getId(), fc);
            }
        }

        return result.values();
    }
    
    @POST
    @LoggingData
    @Path("/current")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Event> getCurrent(HashMap<String, String> criteriaMap) {
    	    
    	List<Object> criteria = getCriteria(criteriaMap);
    	
    	return getCurrentEvents(criteria);
    }
    
	@POST
    @Path("/livecount")
    @Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public EventNotification getLiveCount(HashMap<String, String> criteriaMap) {
    	
		List<Object> criteria = getCriteria(criteriaMap);
		
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        Integer liveEvents = commonEventProvider.countAllFleetsLiveEvents(criteria, licence);
        
        Long lastTimeStamp = commonEventProvider.getLastEventTimestamp(true);
        
        return new EventNotification(liveEvents, false, lastTimeStamp);
    }

	@POST
    @TimeoutFree
    @Path("/livecount/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public EventNotification getLiveCount(@PathParam("timestamp") long timestamp, HashMap<String, String> criteriaMap) {
    	
    	List<Object> criteria = getCriteria(criteriaMap);
        
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        Integer liveEvents = commonEventProvider.countAllFleetsLiveEvents(criteria, licence);
        
        Long lastTimeStamp = commonEventProvider.getLastEventTimestamp(true);
        // If an event ended after the last live, we get this timestamp.
        Long lastEventEnd = commonEventProvider.getLastEventTimestamp(false);
        
        if (lastEventEnd != null && lastTimeStamp != null && lastEventEnd.compareTo(lastTimeStamp) >0) {
            lastTimeStamp = lastEventEnd;
        }
        
        boolean hasChanged = !(lastTimeStamp != null && lastTimeStamp.longValue() <= timestamp);
        
        return new EventNotification(liveEvents, hasChanged, lastTimeStamp);
    }

    @GET
    @Path("/newEvents/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getNewEvents(@PathParam("timestamp") long timestamp) {
        return dataLicensor.getLicenced(eventSearchProvider.getEventsSince(commonEventProvider, timestamp));
    }
    
    @POST
    @LoggingData
    @Path("/acknowledge")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response acknowledge(HashMap<String, String> parameters) {
    	
        String fleetId = parameters.get("fleetId");
        Integer eventId = Integer.valueOf(parameters.get("eventId"));
        Long rowVersion = Long.valueOf(parameters.get("rowVersion"));
        Integer eventGroupId = null;
        
        if(parameters.get("eventGroupId") != null){
            eventGroupId = Integer.valueOf(parameters.get("eventGroupId"));
        }
        
    	eventProviders.get(fleetId).acknowledgeEvent(eventId, rowVersion, eventGroupId);
    	
    	return Response.ok().build();
    }
    

    
    @POST
    @LoggingData
    @Path("/deactivate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deactivate(HashMap<String, String> parameters) {
    	
        String fleetId = parameters.get("fleetId");
        Integer eventId = Integer.valueOf(parameters.get("eventId"));
        String userName = parameters.get("userName");
        Boolean isCurrent = Boolean.valueOf(parameters.get("isCurrent"));
        Long rowVersion = Long.valueOf(parameters.get("rowVersion"));
        
        eventProviders.get(fleetId).deactivateEvent(eventId, userName, isCurrent, rowVersion);
        
        return Response.ok().build();
    }

    @GET
    @LoggingData
    @Path("/getEndTime/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Long getEndTime(@PathParam("fleetId") String fleetId,
            @PathParam("unitId") int unitId) {
        return eventProviders.get(fleetId).getEndTime(unitId).getTime();
    }
    
    @GET
    @LoggingData
    @Path("/createNewFault/{fleetId}/{unitId}/{faultCode}/{timestamp}")
    @Produces(MediaType.APPLICATION_JSON)
    public Long createNewFault(@PathParam("fleetId") String fleetId,
    		@PathParam("unitId") int unitId,
    		@PathParam("faultCode") String faultCode,
    		@PathParam("timestamp") long timestamp) {
    	    	
    	return eventProviders.get(fleetId).createNewFault(unitId, faultCode, fleetId, timestamp);
    }
    
    @GET
    @LoggingData
    @Path("/newFaultDropDowns")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, List<Dropdown>> getCreateFaultDropDowns() {
    	return commonEventProvider.getNewFaultDropDownValues();
    }

    private List<Event> simple(Map<String, String> searchParameters) {
        String fleetId = searchParameters.remove("fleet");
        Integer limit = Integer.parseInt(searchParameters.remove("limitHistory"));
        Date fromDate = new Date(Long.parseLong(searchParameters.remove("startDate")));
        Date toDate = new Date(Long.parseLong(searchParameters.remove("endDate")));
        String live = searchParameters.remove("live");
        String keyword = searchParameters.remove("keyword");

        List<Event> events;
        if (fleetId != null) {
            events = eventSearchProvider
                    .searchFleetEvents(eventProviders.get(fleetId), fromDate, toDate, live, keyword, limit);
        } else {
            Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
            events = eventSearchProvider.searchEvents(commonEventProvider, fromDate, toDate, live, keyword,
                    limit, licence);
        }

        return dataLicensor.getLicenced(events);
    }

    @POST
    @LoggingData
    @Path("/search/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Event> simpleSearch(Map<String, String> searchParameters) {
        return simple(searchParameters);
    }

    @POST
    @LoggingData
    @Path("/download")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("{text/csv, text/xlsx}")
    public Response simpleSearchDownload(@Context HttpServletResponse response, @FormParam("data") String data,
            @FormParam("timezone") String timezone) throws IOException {
        
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        // FIXME doesn't handle summerTime use the jstz.js instead
        TimeZone tz = DateUtils.getClientTimeZone(timezone);

        TreeMap<String, String> searchParameters = new ObjectMapper().readValue(data,
                TypeFactory.defaultInstance().constructMapType(TreeMap.class, String.class, String.class));

        List<Event> events = simple(searchParameters);
        createFile(events, tz, response, licence);
        return Response.ok().build();
    }

    @GET
    @Path("/panelLicence")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean hasEventsPanelLicence(@Context HttpServletRequest request) {
        boolean hasEventsPanelLicence = false;
        Licence licence = Licence.get(request, responseProvider.get(), true);

        if (licence.hasResourceAccess(Constants.EVENT_PANEL_LIC_KEY)) {
            hasEventsPanelLicence = true;
        }

        return hasEventsPanelLicence;
    }
    
    @POST
    @LoggingData
    @Path("/searchCount")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Integer simpleSearchCount(Map<String, String> searchParameters) {
        return simple(searchParameters).size();
    }
    
    @POST
    @LoggingData
    @Path("/searchAdvancedCount")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Integer advancedSearchCount(Map<String, String> searchParameters) {
        return advanced(searchParameters).size();
    }
    
    @GET
    @LoggingData
    @Path("/sendFault/{fleetId}/{eventId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Event sendFault(@PathParam("fleetId") String fleetId, @PathParam("eventId") long eventId) {
        eventProviders.get(fleetId).sendFault(eventId);

        return eventProviders.get(fleetId).getEventById(eventId, true);
    }

    private List<Fleet> getFleets() {
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        List<Fleet> fleetList = new ArrayList<Fleet>();
        
        for (Fleet f : commonEventProvider.getLiveAndRecentFleets()) {
            String fleetLicence = fleetProviders.get(f.getCode()).getLicence(f.getCode());
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                fleetList.add(f);
            }
        }
        
        return fleetList;
    }
    
    private List<String> getUnitTypes(Collection<Fleet> fleets) {
        List<String> unitTypeList = new ArrayList<String>();
       
        for (Fleet fleet : fleets) {
        	unitTypeList.addAll(unitProvider.get(fleet.getCode()).getUnitTypes());
        }
        
        return unitTypeList;
    }
    
    private List<Event> getCurrentEvents(List<Object> searchCriteria) {
    	Integer limit = currentEventsConfiguration.getEventsLimit();
    	
    	Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
    	
    	List<Event> result = commonEventProvider.getCurrentEvents(limit, searchCriteria, licence);
    	
    	return result;
    }
    
    private List<Object> getCriteria(HashMap<String, String> criteriaMap) {
    	
    	List<Object> criteria = new ArrayList<Object>();
    	
    	criteria.add(criteriaMap.containsKey("isLive") ? Boolean.valueOf(criteriaMap.get("isLive")) : null);
    	criteria.add(criteriaMap.containsKey("isAcknowledged") ? Boolean.valueOf(criteriaMap.get("isAcknowledged")) : null);
    	criteria.add(criteriaMap.containsKey("fleet") ? criteriaMap.get("fleet") : null);
        criteria.add(criteriaMap.containsKey("severity") ? criteriaMap.get("severity") : null);
        criteria.add(criteriaMap.containsKey("category") ? criteriaMap.get("category") : null);
        criteria.add(criteriaMap.containsKey("hasRecovery") ? Boolean.valueOf(criteriaMap.get("hasRecovery")) : null);
        criteria.add(criteriaMap.containsKey("unitType") ? criteriaMap.get("unitType") : null);
        criteria.add(criteriaMap.containsKey("unitStatus") ? Boolean.valueOf(criteriaMap.get("unitStatus")) : null);

        
        return criteria;
	}
    
    @POST
    @LoggingData
    @Path("/createFaultGroup/{fleetId}/{username}/{leadEventId: [0-9]+}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFaultGroup(
            @PathParam("fleetId") String fleetId,
            @PathParam("username") String username, 
            @PathParam("leadEventId") long leadEventId,
            List<FaultGroupInputParam> params) {
        eventProviders.get(fleetId).createFaultGroup(username, params, leadEventId);
        return Response.ok().build();
    }
    
    @POST
    @LoggingData
    @Path("/updateFaultGroup/{fleetId}/{username}/{leadEventId: [0-9]+}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateFaultGroup(
    		@PathParam("fleetId") String fleetId,
    		@PathParam("username") String username,
    		@PathParam("leadEventId") long leadEventId,
    		List<FaultGroupInputParam> params) {
    	eventProviders.get(fleetId).updateFaultGroup(username, params, leadEventId);
    	return Response.ok().build();
    }

    @GET
    @Cache
    @LoggingData
    @Path("/eventGroupConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public EventGroupConfiguration eventGroupConfiguration() {
        return groupConfiguration;
    }
    


    @GET
    @LoggingData
    @Path("/find/{substr: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericBean> searchStock(@PathParam("substr") String substr) {

        List<GenericBean> results = null;

        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (String fleetId : fleetProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                // FIXME - Nasty
                List<GenericBean> resultsAux = fleetProviders.get(fleetId).searchUnitsByString(fleetId, substr);
                if (results == null) {
                    results = resultsAux;
                } else {
                    results.addAll(resultsAux);
                }
            }
        }
        
        results = dataLicensor.getLicenced(results);

        return results;
    }
    
    
    @POST
    @LoggingData
    @Path("/saveFaultPeriod/{fleetId}/{periodSelected}/{eventId: [0-9]+}/{timeStamp: [0-9]+}/{vehicleId: [0-9]+}/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveFaultPeriod(@PathParam("eventId") Integer eventId,@PathParam("vehicleId") Integer vehicleId, @PathParam("fleetId") String fleetId , @PathParam("periodSelected") Integer period,
    		@PathParam("timeStamp") Long timestamp) {
    			
    			if(period != 0) {
    				  eventProviders.get(fleetId).saveFaultPeriod(eventId, vehicleId, period,timestamp);
    			}
    			
    		
    	 return Response.ok().build();
    }
    
    
}
