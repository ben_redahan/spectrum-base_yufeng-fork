package com.nexala.spectrum.rest.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.recovery.beans.RecoveryResumePayload;
import com.nexala.spectrum.recovery.beans.RecoveryStepFrontend;
import com.nexala.spectrum.rest.data.RecoveryProvider;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.unit.RecoveryConfiguration;

@Path("/recovery/")
@Singleton
public class RecoveryService {

    @Inject
    private RecoveryConfiguration recoveryConf;

    @Inject
    private Map<String, RecoveryProvider> recoveryProviders;
    
    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;
    
    @GET
    @Path("columns")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Column> getColumns() {
        return recoveryConf.getFaultColumns();
    }

    @GET
    @Path("execute/{fleetId}/{faultCode}/{faultId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Long executeRecovery(
            @PathParam("fleetId") String fleetId,
            @PathParam("faultCode") String faultCode,
            @PathParam("faultId") Long faultId,
            @Context HttpServletRequest request) {
    	
    	Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
    	String userName = licence.getLoginUser().getLogin();
    	
        return recoveryProviders.get(fleetId).executeRecovery(faultCode, faultId, userName);
    }
    
    @GET
    @Path("nextStep/{fleetId}/{contextId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public RecoveryStepFrontend getNextStep(
            @PathParam("fleetId") String fleetId,
            @PathParam("contextId") long contextId, 
            @Context HttpServletRequest request) {
    	
    	Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
    	String userName = licence.getLoginUser().getLogin();
    	
        return recoveryProviders.get(fleetId).getNextStep(contextId, userName);
    }
    
    @GET
    @Path("worstCaseScenario/{fleetId}/{contextId: [0-9]+}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getWorstCaseScenario(
            @PathParam("fleetId") String fleetId,
            @PathParam("contextId") long contextId) {
        return recoveryProviders.get(fleetId).getWorstCaseScenario(contextId);
    }

    @GET
    @Path("setStepOutcome/{fleetId}/{contextId: [0-9]+}/{faultId: [0-9]+}/{answerId: [0-9]+}/{answer}/{worked}/{completed}")
    @Produces(MediaType.APPLICATION_JSON)
    public void setStepOutcome(
            @PathParam("fleetId") String fleetId,
            @PathParam("contextId") long contextId,
            @PathParam("faultId") long faultId,
            @PathParam("answerId") int answerId,
            @PathParam("answer") String answer,
            @PathParam("worked") boolean worked,
            @PathParam("completed") boolean completed,
            @QueryParam("notes") String notes,
            @QueryParam("action") String action,
            @Context HttpServletRequest request) {

    	Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
    	String userName = licence.getLoginUser().getLogin();
    	
        if (!completed) {
            recoveryProviders.get(fleetId).updateRecoveryStatus(faultId, "Started");
        }
        recoveryProviders.get(fleetId).setStepOutcome(contextId, answerId, answer, action,
                worked, notes, userName);
    }

    @GET
    @Path("completeRecovery/{fleetId}/{contextId: [0-9]+}/{faultId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public int completeRecovery(
            @PathParam("fleetId") String fleetId,
            @PathParam("contextId") long contextId,
            @PathParam("faultId") long faultId,
            @Context HttpServletRequest request) {

    	Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
    	String userName = licence.getLoginUser().getLogin();
    	
        recoveryProviders.get(fleetId).updateRecoveryStatus(faultId, "Completed");
        return recoveryProviders.get(fleetId).completeRecovery(contextId, userName);
    }
    
    @GET
    @Path("resumeRecovery/{fleetId}/{contextId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public RecoveryResumePayload resumeRecovery(
            @PathParam("fleetId") String fleetId,
            @PathParam("contextId") long contextId) {
        return recoveryProviders.get(fleetId).resumeRecovery(contextId);
    }

    @GET
    @Path("isActive/{fleetId}/{faultId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Long isRecoveryActive(
            @PathParam("fleetId") String fleetId,
            @PathParam("faultId") long faultId) {
        Long contextId = recoveryProviders.get(fleetId).getContextId(faultId);
        boolean active = false;
        if (contextId != null) {
            active = recoveryProviders.get(fleetId).isActive(contextId);
        }
        return active ? contextId : null;
    }
    
    @GET
    @Path("auditItems/{fleetId}/{faultId: [0-9]+}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getAuditItems(
            @PathParam("fleetId") String fleetId,
            @PathParam("faultId") long faultId) {
        return recoveryProviders.get(fleetId).getRecoveryAuditItems(faultId);
    }
}
