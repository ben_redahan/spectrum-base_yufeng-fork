/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$ $Date$ $Revision$ $Source$
 */
package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.db.dao.FleetDao;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.Dropdown;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;

public class DefaultCommonEventProvider implements CommonEventProvider {

    private Map<String, EventProvider> eventProviders;
    
    @Inject
    private Map<String, UnitProvider> unitProviders;

    @Inject
    protected Map<String, FleetProvider> fleetProviders;

    @Inject
    private FleetDao fleetDao;
    
    @Inject
    private UserConfigurationProvider userConfigProvider;

    private Comparator<Event> eventComparator = new Comparator<Event>() {
        @Override
        public int compare(Event o1, Event o2) {
            if (o1.getField(Spectrum.EVENT_CREATE_TIME) == null || o2.getField(Spectrum.EVENT_CREATE_TIME) == null)
                return 0;
            return ((Long) o2.getField(Spectrum.EVENT_CREATE_TIME)).compareTo(((Long) o1.getField(Spectrum.EVENT_CREATE_TIME)));
        }
    };
    
    @Inject
    public DefaultCommonEventProvider(Map<String, EventProvider> eventProviders,
            CommonFleetConfiguration fleetConf) {
        this.eventProviders = new HashMap<String, EventProvider>();
        // Get only one provider for fleets which share the same db
        ListMultimap<String, String> fleetsPerDatabase = fleetConf.getFleetsPerDatabase();
        for (String db : fleetsPerDatabase.keySet()) {
            String fleetId = fleetsPerDatabase.get(db).get(0);
            this.eventProviders.put(fleetId, eventProviders.get(fleetId));
        }
    }
    
    @Override
	public List<Event> getCurrentEvents(Integer limit, List<Object> searchCriteria, Licence licence) {
		List<Event> result = new ArrayList<Event>();
        
        for (String fleetId : eventProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                result.addAll(eventProviders.get(fleetId).getCurrentPanel(limit, searchCriteria));
            }
        }

        Collections.sort(result, eventComparator);
        return result;
	}

    @Override
    public List<FaultType> getAllFleetsFaultTypes() {
        List<FaultType> result = new ArrayList<FaultType>();
        for (EventProvider current : eventProviders.values()) {
            result.addAll(current.getFaultTypes());
        }
        return result;
    }

    @Override
    public List<FaultMeta> getAllFleetsFaultCodes() {
        List<FaultMeta> result = new ArrayList<FaultMeta>();
        for (EventProvider current : eventProviders.values()) {
            result.addAll(current.getFaultCodes());
        }
        return result;
    }

    @Override
    public List<Event> getAllFleetsEventsSince(long since) {
        List<Event> result = new ArrayList<Event>();
        for (EventProvider current : eventProviders.values()) {
            result.addAll(current.getEventsSince(since));
        }
        Collections.sort(result, eventComparator);
        return result;
    }

    @Override
    public List<Event> searchAllFleetsEvents(Date dateFrom, Date dateTo,
            String live, String keyword, Integer limit, Licence licence) {
        List<Event> result = new ArrayList<Event>();
        for (String fleetId : eventProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                result.addAll(eventProviders.get(fleetId).searchEvents(dateFrom, dateTo, live, keyword, limit));
            }
        }

        Collections.sort(result, eventComparator);
        return result;
    }

    @Override
    public List<Event> searchAllFleetsEvents(Date dateFrom, Date dateTo, String code, String type, String description,
            String category, String live, List<EventSearchParameter> parameters, Integer limit, Licence licence) {
        List<Event> result = new ArrayList<Event>();
        for (String fleetId : eventProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                result.addAll(eventProviders.get(fleetId).searchEvents(dateFrom, dateTo, code, type, description,
                        category, live, parameters, limit));
            }
        }

        Collections.sort(result, eventComparator);
        return result;
    }

    @Override
    public boolean areFaultsRetracted(Long timestamp) {
        boolean faultsRetracted = false;
        for (EventProvider current : eventProviders.values()) {
            faultsRetracted = current.areFaultsRetracted(timestamp);
            if (faultsRetracted)
                break;
        }
        return faultsRetracted;
    }

    @Override
    public Integer countAllFleetsLiveEvents(List<Object> criteria, Licence licence) {
        Integer eventCount = 0;

        for (String fleetId : eventProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                eventCount += eventProviders.get(fleetId).getEventCountLive(criteria);
            }
        }

        return eventCount;
    }

    @Override
    public Long getLastEventTimestamp(boolean live) {
        Long timestamp = 0L;

        for (EventProvider current : eventProviders.values()) {
            Long newTimestamp = null;
            if (live) {
                newTimestamp = current.getLastLiveEventTimestamp();
            } else {
                newTimestamp = current.getLastRecentEventTimestamp();
            }

            if (newTimestamp != null && newTimestamp > timestamp) {
                timestamp = newTimestamp;
            }
        }

        return timestamp;
    }

    public List<Dropdown> getCodeDropDownList() {
        List<FaultMeta> codeList = new ArrayList<FaultMeta>();
        List<Dropdown> dropdownListCode = new ArrayList<Dropdown>();
        // Get only one provider as fault codes are the same across all database
        EventProvider provider = Iterables.get(eventProviders.values(), 0);
        codeList.addAll(provider.getFaultCodes());
        for (FaultMeta faultMeta : codeList) {
            dropdownListCode.add(new Dropdown(faultMeta.getId(), faultMeta.getFaultCode()));
        }
        return dropdownListCode;
    }

    public List<Dropdown> getCategoryDropDownList() {
        List<FaultCategory> catList = new ArrayList<FaultCategory>();
        List<Dropdown> dropdownListCategory = new ArrayList<Dropdown>();
        // Get only one provider as fault categories are the same across all database
        EventProvider provider = Iterables.get(eventProviders.values(), 0);
        catList.addAll(provider.getFaultCategories());
        for (FaultCategory faultCategory : catList) {
            dropdownListCategory.add(new Dropdown(faultCategory.getId(), faultCategory.getCategory()));
        }
        return dropdownListCategory;
    }

    public List<Dropdown> getTypeDropDownList() {
        List<FaultType> typeList = new ArrayList<FaultType>();
        List<Dropdown> dropdownListType = new ArrayList<Dropdown>();
        // Get only one provider as fault types are the same across all database
        EventProvider provider = Iterables.get(eventProviders.values(), 0);
        typeList.addAll(provider.getFaultTypes());
        for (FaultType faultType : typeList) {
            dropdownListType.add(new Dropdown(faultType.getId(), faultType.getName()));
        }

        return dropdownListType;
    }
    
    public List<Dropdown> getUnitTypeDropDownList(List<Fleet> fleets) {
        List<String> unitTypeList = new ArrayList<String>();
        List<Dropdown> dropdownListUnitType = new ArrayList<Dropdown>();
        
        for (Fleet fleet : fleets) {
            unitTypeList.addAll(unitProviders.get(fleet.getCode()).getUnitTypes());
        }
        
        for (int i = 0; i < unitTypeList.size(); i++){
            dropdownListUnitType.add(new Dropdown(i, unitTypeList.get(i)));
        }
        return dropdownListUnitType;
    }
    
    public List<Dropdown> getHasRecoveryDropDownList() {
        List<Dropdown> dropdownListHasRecovery = new ArrayList<Dropdown>();

        dropdownListHasRecovery.add(new Dropdown(0, "No"));
        dropdownListHasRecovery.add(new Dropdown(1, "Yes"));
 
        return dropdownListHasRecovery;
    }
    
    public List<Dropdown> getHasExternalReferenceDropDownList() {
    	List<Dropdown> dropdownListHasExternalReference = new ArrayList<Dropdown>();
    	
    	dropdownListHasExternalReference.add(new Dropdown(0, "No"));
    	dropdownListHasExternalReference.add(new Dropdown(1, "Yes"));
    	
    	return dropdownListHasExternalReference;
    }
    
    public List<Dropdown> getBooleanDropDownList() {
    	List<Dropdown> dropdownListBoolean = new ArrayList<Dropdown>();
    	
    	dropdownListBoolean.add(new Dropdown(0, "No"));
    	dropdownListBoolean.add(new Dropdown(1, "Yes"));
    	
    	return dropdownListBoolean;
    }
    
    public List<Dropdown> getFleetDropDownList(List<Fleet> fleets) {
        List<Dropdown> dropdownListFleet = new ArrayList<Dropdown>();
        
        for (Fleet fleet : fleets) {
            dropdownListFleet.add(new Dropdown(-1, fleet.getName()));
        }
        
        return dropdownListFleet;
    }

    @Override
    public Map<String, List<Dropdown>> getAdditionnalDropDownValues(List<Fleet> fleets) {
        Map<String, List<Dropdown>> mapDropDown = new HashMap<String, List<Dropdown>>();
        
        mapDropDown.put("code", getCodeDropDownList());
        mapDropDown.put("type", getTypeDropDownList());
        mapDropDown.put("category", getCategoryDropDownList());
        mapDropDown.put("unitType", getUnitTypeDropDownList(fleets));
        mapDropDown.put("hasRecovery", getHasRecoveryDropDownList());
        mapDropDown.put("hasExternalReference", getHasExternalReferenceDropDownList());
        mapDropDown.put("isAcknowledged", getBooleanDropDownList());
        mapDropDown.put("createdByRule", getBooleanDropDownList());
        mapDropDown.put("unitStatus", getBooleanDropDownList());
        return mapDropDown;
    }
    
    @Override
    public Map<String, List<Dropdown>> getNewFaultDropDownValues() {
        Map<String, List<Dropdown>> mapDropDown = new HashMap<String, List<Dropdown>>();
        mapDropDown.put("newFaultCode", getCodeDropDownList());
        mapDropDown.put("newFaultCategory", getCategoryDropDownList());
        return mapDropDown;
    }

    @Override
    public List<FaultCategory> getLiveAndRecentCategories() {
        // Get only one provider as fault categories are the same across all database
        EventProvider provider = Iterables.get(eventProviders.values(), 0);
        return provider.getFaultCategories();
    }

    @Override
    public List<FaultType> getLiveAndRecentSeverities() {
        // Get only one provider as fault types are the same across all database
        EventProvider provider = Iterables.get(eventProviders.values(), 0);
        return provider.getFaultTypes();
    }

    @Override
    public List<Fleet> getLiveAndRecentFleets() {
        // Get the list of fleets from the main database
        return fleetDao.getFleetList();
    }
    
    @Override
    public UserConfiguration getEventPanelUserConfiguration(Licence licence) {
        return userConfigProvider.getUserConfiguration(licence.getLoginUser().getLogin(), "eventFilters");
    }

}
