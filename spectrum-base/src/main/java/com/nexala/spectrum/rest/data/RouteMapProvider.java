package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.route.Route;
import com.nexala.spectrum.rest.data.beans.route.RouteView;

/**
 * Provide datas for the route map view.
 * @author brice
 *
 */
public interface RouteMapProvider {
    
    /**
     * Return the route with this id.
     * @param id id of the route
     * @return null if no route with this id, otherwise the route.
     */
    RouteView getRouteView(String id);
    
    /**
     * Return list of availables routes.
     * Each route contain his list of stations and depots.
     * @return list of availables routes
     */
    List<Route> getAllRoutes();

}
