package com.nexala.spectrum.rest.service.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ColumnSorterBean;
import com.nexala.spectrum.view.conf.FleetFilter;

/**
 * Contain the fleet configuration for the fleet summary screen.
 * @author brice
 *
 */
@JsonInclude(Include.NON_NULL)
public class FleetConfigurationUIBean {
    
    private List<Column> columns = null;

    private List<FleetFilter> fleetFilters = null;

    private List<String> columnsUnitIdentifier = null;
    
    private ColumnSorterBean sorter = null;

    private boolean showFleetFullNames = false;
    
    private boolean showSingleFleetTab = false;
    
    private String userConfiguration  = null;

    private boolean mouseOverHeaderEnabled;

    /**
     * Getter for columns.
     * @return the columns
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * Setter for columns.
     * @param columns the columns to set
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    /**
     * Getter for fleetFilters.
     * @return the fleetFilters
     */
    public List<FleetFilter> getFleetFilters() {
        return fleetFilters;
    }

    /**
     * Setter for fleetFilters.
     * @param fleetFilters the fleetFilters to set
     */
    public void setFleetFilters(List<FleetFilter> fleetFilters) {
        this.fleetFilters = fleetFilters;
    }

    /**
     * Getter for columnsUnitIdentifier.
     * @return the columnsUnitIdentifier
     */
    public List<String> getColumnsUnitIdentifier() {
        return columnsUnitIdentifier;
    }

    /**
     * Setter for columnsUnitIdentifier.
     * @param columnsUnitIdentifier the columnsUnitIdentifier to set
     */
    public void setColumnsUnitIdentifier(List<String> columnsUnitIdentifier) {
        this.columnsUnitIdentifier = columnsUnitIdentifier;
    }

    /**
     * Getter for sorter.
     * @return the sorter
     */
    public ColumnSorterBean getSorter() {
        return sorter;
    }

    /**
     * Setter for sorter.
     * @param sorter the sorter to set
     */
    public void setSorter(ColumnSorterBean sorter) {
        this.sorter = sorter;
    }

    /**
     * Getter for ShowFleetFullNames.
     * @return the showFleetFullNames
     */
    public boolean isShowFleetFullNames() {
        return showFleetFullNames;
    }

    /**
     * Setter for ShowFleetFullNames.
     * @param showFleetFullNames the showFleetFullNames to set
     */
    public void setShowFleetFullNames(boolean showFleetFullNames) {
        this.showFleetFullNames = showFleetFullNames;
    }

    /**
     * Getter for ShowSingleFleetTab.
     * @return the showSingleFleetTab
     */
    public boolean isShowSingleFleetTab() {
        return showSingleFleetTab;
    }

    /**
     * Setter for ShowSingleFleetTab.
     * @param showSingleFleetTab the showSingleFleetTab to set
     */
    public void setShowSingleFleetTab(boolean showSingleFleetTab) {
        this.showSingleFleetTab = showSingleFleetTab;
    }

    public String getUserConfiguration() {
        return userConfiguration;
    }

    public void setUserConfiguration(String userConfiguration) {
        this.userConfiguration = userConfiguration;
    }
    
    /**
     * Setter for mouseOverHeaderEnabled.
     * @param mouseOverHeaderEnabled to set
     */
    public void setMouseOverHeaderEnabled(boolean mouseOverHeaderEnabled) {
        this.mouseOverHeaderEnabled = mouseOverHeaderEnabled;
    }
    
    /**
     * Getter for mouseOverHeaderEnabled.
     * @return boolean
     */
    public boolean getMouseOverHeaderEnabled(){
        return mouseOverHeaderEnabled;
    }

}
