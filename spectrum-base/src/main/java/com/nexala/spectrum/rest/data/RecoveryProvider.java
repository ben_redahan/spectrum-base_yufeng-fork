package com.nexala.spectrum.rest.data;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.recovery.beans.RecoveryResumePayload;
import com.nexala.spectrum.recovery.beans.RecoveryStepFrontend;

@ImplementedBy(DefaultRecoveryProvider.class)
public interface RecoveryProvider {

    public Long executeRecovery(String faultCode, Long faultInstanceId, String username);
    
    public RecoveryStepFrontend getNextStep(Long contextId, String username);
    
    public String getWorstCaseScenario(Long contextId);

    public void setStepOutcome(Long contextId, int answerId, String answer, String action,
            boolean worked, String notes, String username);

    public int completeRecovery(Long contextId, String username);

    public RecoveryResumePayload resumeRecovery(Long contextId);

    public void updateRecoveryStatus(Long faultId, String recoveryStatus);

    public Long getContextId(Long faultId);

    public boolean isActive(Long contextId);

    public String getRecoveryAuditItems(Long faultId);
}
