package com.nexala.spectrum.rest.data;

import java.util.List;
import java.util.Map;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.data.beans.channeldata.ChannelsGroup;

public interface GroupProvider {
    
    ChannelData getChannelData(List<DataSet> data, int groupId, boolean showAll, Licence licence);

    Map<Integer, Channel<?>> getGroupCategories(List<DataSet> data);
    
    List<ChannelGroup> getVisibleGroupList();
    
    List<ChannelConfig> getChannels(Integer groupId);
    
    /**
     * Return the data for a group. 
     * Contain columns to display and the group channels.
     * @param groupId the group id
     * @param stocks list of stocks
     * @return a {@link ChannelsGroup} object
     */
    ChannelsGroup getGroupData(int groupId, List<StockDesc> stocks, Licence licence);
}
