package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.RecoveryAuditLogManager;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.RecoveryAuditItem;
import com.nexala.spectrum.db.beans.RecoveryAuditItemType;
import com.nexala.spectrum.db.dao.FaultDao;
import com.nexala.spectrum.db.dao.FaultMetaDao;
import com.nexala.spectrum.db.dao.RecoveryAuditItemDao;
import com.nexala.spectrum.db.dao.RecoveryAuditItemTypeDao;
import com.nexala.spectrum.recovery.RecoveryAnswer;
import com.nexala.spectrum.recovery.RecoveryException;
import com.nexala.spectrum.recovery.RecoveryManager;
import com.nexala.spectrum.recovery.RecoveryOutcome;
import com.nexala.spectrum.recovery.RecoveryStep;
import com.nexala.spectrum.recovery.RecoveryVertex;
import com.nexala.spectrum.recovery.beans.RecoveryAnswerFrontend;
import com.nexala.spectrum.recovery.beans.RecoveryResumePayload;
import com.nexala.spectrum.recovery.beans.RecoveryStepFrontend;
import com.nexala.spectrum.recovery.workflow.Activity;
import com.nexala.spectrum.recovery.workflow.WorkItem;
import com.trimble.rail.security.client.HttpClient;

public class DefaultRecoveryProvider implements RecoveryProvider {
    
    @Inject
    protected FaultMetaDao fmDao;

    @Inject
    protected RecoveryAuditItemDao itemDao;

    @Inject
    protected RecoveryAuditItemTypeDao itemTypeDao;

    @Inject
    protected FaultDao faultDao;
    
    @Inject
    protected RecoveryAuditLogManager recoveryAuditLogManager;
    
    protected HttpClient httpClient;

    protected ApplicationConfiguration appConf;

    @Inject
    public DefaultRecoveryProvider(HttpClient httpClient, FaultMetaDao fmDao, RecoveryAuditItemDao itemDao, RecoveryAuditItemTypeDao itemTypeDao,
            RecoveryAuditLogManager recoveryAuditLogManager, ApplicationConfiguration applicationConfiguration) {
        this.fmDao = fmDao;
        this.itemDao = itemDao;
        this.itemTypeDao = itemTypeDao;
        this.recoveryAuditLogManager = recoveryAuditLogManager;
        this.appConf = applicationConfiguration;
        this.httpClient = httpClient;
    }
    
    private static final Logger logger = Logger.getLogger(DefaultRecoveryProvider.class);
    
    /**
     * Starts a recovery workflow on a fault definition.
     * 
     * @param faultCode
     *            Fault code to recover from
     * @param faultInstanceId
     *            The fault instance ID (used for audit logging).
     * @return The context ID of the new recovery process.
     * @throws ClientException
     */
    @Override
    public Long executeRecovery(String faultCode, Long faultInstanceId, String username) {

        Long contextId = -1L;

        RecoveryManager manager = RecoveryManager.instance(appConf);

        logger.debug("Executing recovery for fault code: " + faultCode);

        FaultMeta fm = fmDao.findByFaultCode(faultCode);

        if (fm != null) {
            contextId = (Long) manager.executeProcess(fm.getRecoveryProcessPath(), username, httpClient);

            if (contextId == null) {
                logger.error("Could not load recovery process. The recovery process could not be"
                        + " started as it could not be loaded from the recovery editor.");
            }

            recoveryAuditLogManager.logExecuteProcess(faultInstanceId, ""
                    + contextId, username);
        } else {
            logger.error("Cannot find fault definition. The recovery cannot be executed as the"
                    + " fault definition cannot be found.");
        }

        return contextId;
    }

    /**
     * Gets the next recovery step in the process instance
     * 
     * @return the next step
     */
    @Override
    public RecoveryStepFrontend getNextStep(Long contextId, String username) {
        
        RecoveryStepFrontend stepFrontend = new RecoveryStepFrontend();

        RecoveryManager manager = RecoveryManager.instance(appConf);

        RecoveryVertex v = null;

        try {
            v = manager.getNextStep(contextId);

            if (v == null) {
                logger.error("Cannot get next step information: Unknown problem");
            }
        } catch (RecoveryException re) {
            logger.error("Cannot get next step information: " + re.getMessage(), re);
            logger.error("Caused by: " + re.getCause().getMessage());
        }

        if (v instanceof RecoveryStep) {
            RecoveryStep step = (RecoveryStep) v;
            recoveryAuditLogManager.logNextStep("" + contextId, step.getQuestion(), username);

            stepFrontend = new RecoveryStepFrontend();
            stepFrontend.setType(RecoveryStepFrontend.STEP_TYPE_LOGIC_BLOCK);

            if (step.getStepId() instanceof String) {
                stepFrontend.setStepId((String) step.getStepId());
            } else {
                stepFrontend.setStepId(null);
            }

            stepFrontend.setQuestion(step.getQuestion());
            stepFrontend.setQuestionUrl(step.getQuestionUrl());

            ArrayList<RecoveryAnswerFrontend> answerFrontends = new ArrayList<RecoveryAnswerFrontend>();

            for (RecoveryAnswer answer : step.getAnswers()) {
                RecoveryAnswerFrontend af = new RecoveryAnswerFrontend();
                af.setAction(answer.getAction());
                af.setAnswer(answer.getAnswer());

                af.setAnswerId((String) answer.getAnswerId());
                af.setHasDataCheck(answer.hasDataCheck());
                af.setActionUrl(answer.getActionUrl());

                if (answer.getDataCheck() != null
                        && answer.getDataCheck().length() > 0) {
                    af.setDataCheck(answer.getDataCheck());
                } else {
                    // there's no data check, so send one that'll be false
                    af.setDataCheck("");
                }

                answerFrontends.add(af);
            }
            stepFrontend.setAnswers(answerFrontends);

        } else if (v instanceof RecoveryOutcome) {
            RecoveryOutcome outcome = (RecoveryOutcome) v;

            recoveryAuditLogManager.logOutcome("" + contextId, outcome.getName(), username);

            stepFrontend.setType(RecoveryStepFrontend.STEP_TYPE_OUTCOME);
            stepFrontend.setQuestion(outcome.getName());
            stepFrontend.setOutcomeAction(outcome.getAction());
            return stepFrontend;
        } else {
            logger.error("Unknown activity type. An unknown next step was retrieved."
                    + " This cannot be processed.");
        }

        return stepFrontend;
    }

    @Override
    public String getWorstCaseScenario(Long contextId) {

        RecoveryManager manager = RecoveryManager.instance(appConf);

        String wcs = "";

        try {
            wcs = manager.getWorstCaseScenario(contextId);
            logger.debug("Got worst case scenario: " + wcs);
        } catch (RecoveryException re) {
            logger.error("Cannot get worst case scenario: " + re.getMessage(), re);
            logger.error("Caused by: " + re.getCause().getMessage());
        }

        return wcs;
    }
    
    @Override
    public void setStepOutcome(Long contextId, int answerId, String answer, String action,
            boolean worked, String notes, String username) {
        
        RecoveryManager manager = RecoveryManager.instance(appConf);

        recoveryAuditLogManager.logQuestionAnswer("" + contextId, null, answer, notes, username);
        recoveryAuditLogManager.logActionResult("" + contextId, action, worked ? "Worked" : "Didn't Work",
                notes, username);

        try {
            manager.setStepOutcome(contextId, answerId, worked, notes);
        } catch (RecoveryException re) {
            logger.error("Could not set step outcome: " + re.getMessage(), re);
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage(), e);
        }
    }

    @Override
    public int completeRecovery(Long contextId, String username) {
        int result = 1;
        RecoveryManager manager = RecoveryManager.instance(appConf);

        try {
            if(manager.isActive(contextId)
                    && manager.isReadyForCompletion(contextId)) {
                manager.removeProcess(contextId);
            }
            
            recoveryAuditLogManager.logCompletion("" + contextId, username);
        }
        catch(RecoveryException re) {
            logger.error("Could not complete process: " + re.getMessage(), re);
            logger.error("Caused by: " + re.getCause().getMessage());
            result = 0;
        }

        return result;
    }

    @Override
    public RecoveryResumePayload resumeRecovery(Long contextId) {
        
        RecoveryManager manager = RecoveryManager.instance(appConf);
        RecoveryResumePayload payload = new RecoveryResumePayload();
        
        try {
            synchronized(manager) {
                List<WorkItem> workItems = manager.getCompletedWorkItems(contextId);
                payload.setWorkItems(workItems);
                
                List<Activity> activities = new ArrayList<Activity>();
                for(WorkItem item:workItems) {
                    activities.add(manager.getActivityFromWorkItem(contextId, item));
                }
                
                // append the current activity to the activities list
                activities.add(manager.getCurrentActivity(contextId));
                
                payload.setActivities(activities);
            }
        }
        catch(RecoveryException e) {
            logger.error("There was an error resuming the recovery", e);
        }
        
        return payload;
    }
    
    @Override
    public void updateRecoveryStatus(Long faultId, String recoveryStatus) {
        faultDao.updateRecoveryStatus(faultId, recoveryStatus);
    }

    @Override
    public Long getContextId(Long faultId) {
        Long result = null;
        List<RecoveryAuditItem> items = itemDao.findByFaultId(faultId, true);
        RecoveryAuditItem lastItem = null;
        if (items.size() > 0) {
            lastItem = items.get(items.size() - 1);
            result = Long.valueOf(lastItem.getContextId());
        }
        return result;
    }

    @Override
    public boolean isActive(Long contextId) {
        RecoveryManager manager = RecoveryManager.instance(appConf);

        return manager.isActive(contextId);
    }

    @Override
    public String getRecoveryAuditItems(Long faultId) {

        List<RecoveryAuditItem> items = itemDao.findByFaultId(faultId, true);

        StringBuilder builder = new StringBuilder(200);

        if (items.size() == 0) {
            builder.append("A recovery process has not been started for this fault.");
            return builder.toString();
        }

        HashMap<Long, RecoveryAuditItemType> itemTypes = new HashMap<Long, RecoveryAuditItemType>(); // prevent
                                                                                                     // unneeded
                                                                                                     // SQL
                                                                                                     // queries
        for (RecoveryAuditItem item : items) {
            RecoveryAuditItemType itemType = itemTypes.get(item
                    .getAuditItemTypeId());
            if (itemType == null) {
                itemType = itemTypeDao.findById(item.getAuditItemTypeId());
            }

            builder.append("------------------------------<br>");
            builder.append("<b>Audit log item at "
                    + item.getTimestamp().toString() + "</b><br>");

            if (itemType == null) {
                builder.append("Cannot find information about this item.<br>");
                continue;
            }

            builder.append("Username: " + item.getUserName() + "<br>");
            builder.append("User action: " + itemType.isUserAction() + "<br>");
            builder.append("<br>");

            builder.append("Action details:<br>");

            if (itemType.isExecution()) {
                builder.append("Recovery process execution start");
            } else if (itemType.isNextStep()) {
                builder.append("Access next step: \"" + itemType.getItemText()
                        + "\"");
            } else if (itemType.isOutcome()) {
                builder.append("Retrieve outcome: \"" + itemType.getItemText()
                        + "\"");
            } else if (itemType.isReversion()) {
                builder.append("Reversion to \"" + itemType.getItemText()
                        + "\"");
            } else if (itemType.isQuestion()) {
                builder.append("Answer question with \"" + itemType.getAnswer()
                        + "\"");
            } else {
                builder.append("Outcome of action \"" + itemType.getItemText()
                        + "\" was \"" + itemType.getAnswer() + "\"");
            }

            builder.append("<br>");
        }

        return builder.toString();
    }
}
