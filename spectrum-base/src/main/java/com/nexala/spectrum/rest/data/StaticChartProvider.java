package com.nexala.spectrum.rest.data;

import java.io.IOException;
import java.util.List;

/**
 * A static chart provider.
 * @author brice
 *
 */
public interface StaticChartProvider {
	
	
	/**
	 * 
	 * Return the static chart.
	 * @param fleetId
	 * @param vehicleId
	 * @param interval
	 * @param endTimeParam
	 * @param timezone
	 * @param chartId
	 * @param height
	 * @param width
	 * @return
	 */
	public byte[] getChart(String fleetId, int vehicleId, int interval, Long endTime,
			String timezone, Integer chartId, Integer height, Integer width) throws IOException;
	
	
	
	/**
	 * Compute charts and then put it on cache.
	 * @param fleetId
	 * @param vehicleId
	 * @param interval
	 * @param endTime
	 * @param timezone
	 * @param height
	 * @param width
	 * @throws IOException
	 */
	public void computeChart(String fleetId, int vehicleId, int interval, Long endTime,
			String timezone, Integer chartId, Integer width, Integer height) throws IOException;
	
	public List<com.nexala.spectrum.charting.StaticChartConfig> getAllChannelsConfig(String fleetId);

}
