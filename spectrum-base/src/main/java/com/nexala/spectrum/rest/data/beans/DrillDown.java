package com.nexala.spectrum.rest.data.beans;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.view.conf.Column;

public class DrillDown {
    private List<DataSet> data = new ArrayList<DataSet>();
    private List<Column> columns = new ArrayList<Column>();
    private List<String> channelNameList = new ArrayList<String>();
    private String activeVehicle;

    private DrillDown() {
    }

    public static DrillDown get() {
        return new DrillDown();
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public void setChannelNameList(List<String> channelNameList) {
        this.channelNameList = channelNameList;
    }

    public List<String> getChannelNameList() {
        return channelNameList;
    }

    public void setData(List<DataSet> data) {
        this.data = data;
    }

    public List<DataSet> getData() {
        return data;
    }

    public String getActiveVehicle() {
        return activeVehicle;
    }

    public void setActiveVehicle(String activeVehicle) {
        this.activeVehicle = activeVehicle;
    }
}
