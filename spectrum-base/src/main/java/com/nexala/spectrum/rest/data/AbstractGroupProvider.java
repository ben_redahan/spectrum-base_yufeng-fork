package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.data.beans.channeldata.ChannelInfo;
import com.nexala.spectrum.rest.data.beans.channeldata.ChannelsGroup;
import com.nexala.spectrum.validation.ChannelStatusUtil;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.unit.ChannelDataConfiguration;

public abstract class AbstractGroupProvider implements GroupProvider {

    private static final Logger LOGGER = Logger.getLogger(AbstractGroupProvider.class);

    /**
     * Implementation:
     *
     * <code>
     *
     * @Inject @FleetName ChannelConfiguration config; public ChannelConfiguration getChannelConfiguration() { return
     *         config; } </code>
     *
     * @return
     */
    public abstract ChannelConfiguration getChannelConfiguration();

    @Override
    public ChannelData getChannelData(List<DataSet> data, int groupId, boolean showAll, Licence licence) {
        ChannelData channelData = new ChannelData();

        // TODO : change the ChannelData object, "groups" in this case doesn't
        // represent the groups but the group categories.
        // Change to groupCategories and the map to <Integer, ChannelCategory>
        // instead of <Integer, Channel<?>>, also change
        // DataUtils.getGroupCategories to return the correct <Integer,
        // ChannelCategory> map?

        ChannelConfiguration channelConf = getChannelConfiguration();
        channelData.setGroups(getGroupCategories(data));

        ChannelGroupConfig groupConfig = channelConf.getGroup(groupId);

        if (groupConfig != null) {
            List<DataSet> allData = new ArrayList<DataSet>();

            allData.addAll(getCommonGroupChannels(groupConfig, data));

            DataUtils.loadLookupValues(getChannelLookupProvider(), channelConf, data);

            allData.addAll(DataUtils.getVehiclesInColumn(data, channelConf,
                    groupConfig.getChannelIdList(), showAll, false));

            channelData.setData(allData);
        }

        return channelData;
    }

    // @Override
    public ChannelGroupConfig getGroup(Integer groupId) {
        return getChannelConfiguration().getGroup(groupId);
    }

    public abstract ChannelDataConfiguration getChannelDataConfiguration();

    public abstract ChannelLookupProvider getChannelLookupProvider();

    public abstract ChannelStatusUtil getChannelStatusUtil();

    @Override
    public Map<Integer, Channel<?>> getGroupCategories(List<DataSet> data) {
        Map<Integer, Channel<?>> result = new HashMap<Integer, Channel<?>>();

        for (ChannelGroupConfig cgc : getChannelConfiguration().getGroupList()) {
            ChannelCategory groupCat = getGroupCategory(data, cgc, null);
            ChannelString category = new ChannelString(Integer.toString(cgc.getId()), cgc.getDescription(), groupCat);
            result.put(cgc.getId(), category);
        }

        return result;
    }

    protected ChannelCategory getGroupCategory(List<DataSet> data, ChannelGroupConfig config, String fleetId) {
        ChannelCategory highestCategory = ChannelCategory.NODATA;

        for (ChannelConfig cc : config.getChannelConfigList()) {
            for (DataSet ds : data) {
                Channel<?> channel = ds.getChannels().getById(cc.getId());
                if (channel != null) {
                    ChannelCategory ccat = channel.getCategory();

                    if (ccat == null) {
                        LOGGER.warn("Channel does not have a category..");
                    } else {
                        Integer chKey = getChannelStatusUtil().getChannelCategoryPriority(channel.getCategory());
                        Integer maxKey = getChannelStatusUtil().getChannelCategoryPriority(highestCategory);

                        if (chKey == null || maxKey == null) {
                            LOGGER.warn("ChannelCategory not found in categories map");
                        } else if (chKey > maxKey) {
                            highestCategory = ccat;
                        }
                    }
                }
            }
        }

        return highestCategory;
    }

    /**
     * Gets the Channel Values, such as ID, Timestamp, Vehicle Number, etc. Which should be included in all channel
     * group drilldowns.
     *
     * @param groupConfig
     * @param data
     * @return
     */
    protected List<DataSet> getCommonGroupChannels(ChannelGroupConfig groupConfig, List<DataSet> data) {
        List<DataSet> result = new ArrayList<DataSet>();

        for (ChannelConfig config : getChannelDataConfiguration().getNonGroupedChannelNames(groupConfig)) {

            ChannelCollection channels = new ChannelCollection();

            channels.put(new ChannelString(Spectrum.ID, config.getDescription(),
                    ChannelCategory.NORMAL));

            for (DataSet ds : data) {
                ChannelCollection cc = ds.getChannels();
                Channel<?> channel = cc.getByConfig(config);

                if (channel != null) {
                    String val = channel.getStringValue();

                    ChannelCategory cat = channel.getValue() != null ? channel.getCategory() : ChannelCategory.NODATA;

                    channels.put(new ChannelString(String.valueOf(ds.getSourceId()), val, cat));
                }
            }
            result.add(new DataSetImpl(null, channels));

        }

        return result;
    }

    public List<ChannelGroup> getVisibleGroupList() {
        return getChannelConfiguration().getVisibleGroupList();
    }

    public List<ChannelConfig> getChannels(Integer groupId) {
        ChannelGroupConfig groupConfig = getChannelConfiguration().getGroup(groupId);

        if (groupConfig != null) {
            List<ChannelConfig> channelsResult = new ArrayList<ChannelConfig>();
            List<ChannelConfig> channels = groupConfig.getChannelConfigList();
            for (ChannelConfig channel : channels) {
                if (channel.getShortName() != null) {
                    channelsResult.add(channel);
                }
            }
            return channelsResult;
        }

        return null;
    }



    /**
     * Return informations for a group. Used on the channel data view.
     * Contain the list of channels in the group and columns of the table
     * @param groupId the group id
     * @param stocks list of stocks in this unit
     * @return a {@link ChannelsGroup} object
     */
    @Override
    public ChannelsGroup getGroupData(int groupId, List<StockDesc> stocks, Licence licence) {
        ChannelsGroup result = new ChannelsGroup();
        List<ChannelInfo> channels = new ArrayList<ChannelInfo>();

        ChannelGroupConfig group = getChannelConfiguration().getGroup(groupId);

        if (group != null) {
            for (Integer channelId : group.getChannelIdList()) {

                ChannelConfig config = getChannelConfiguration().getConfig(channelId);

                channels.add(convertChannelConfigToInfo(config));

            }
        }

        List<ChannelConfig> nonGroupedChans = getChannelDataConfiguration().getNonGroupedChannelNames(group);
        for(ChannelConfig chan : nonGroupedChans) {
            ChannelInfo current = convertChannelConfigToInfo(chan);

            // Every non grouped channel don't have an id, set the description instead
            current.setId(chan.getDescription());

            channels.add(current);
        }

        List<Column> cols = getChannelDataConfiguration().getColumns(stocks, licence);

        result.setChannels(channels);
        result.setColumns(cols);

        return result;

    }

    /** Convert a {@link ChannelConfig} into a {@link ChannelInfo}
     * @param chanConf the channel config to convert
     * @return the channel info object
     */
    private ChannelInfo convertChannelConfigToInfo(ChannelConfig chanConf) {

        ChannelInfo chan = new ChannelInfo();
        if (chanConf.getId() != null) {
            chan.setId(chanConf.getId().toString());
        }

        chan.setDescription(chanConf.getDescription());
        chan.setAlwaysDisplayed(chanConf.isAlwaysDisplayed());
        chan.setComment(chanConf.getComment());
        chan.setName(chanConf.getName());

        if (ChannelType.TIMESTAMP.equals(chanConf.getType())) {
            // Add timestamp related fields only when it's a timestamp to send less data
            chan.setType(chanConf.getType());
            chan.setFormatPattern(chanConf.getFormatPattern());
        }

        return chan;

    }
}
