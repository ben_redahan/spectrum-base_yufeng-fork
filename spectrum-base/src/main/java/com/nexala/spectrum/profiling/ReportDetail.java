/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.profiling;

abstract class ReportDetail implements Comparable<ReportDetail> {

    protected long time;

    public ReportDetail(long initialTime) {
        this.time = initialTime;
    }

    public void addTime(long time) {
        this.time += time;
    }

    public int compareTo(ReportDetail o) {
        return 0;
    }

    public long getExecTime() {
        return time;
    }
}