/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.licensing;

import javax.servlet.http.HttpServletRequest;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.nexala.spectrum.view.Constants;

public class ServletLogResourceInterceptor implements MethodInterceptor {
    @Inject
    private ActivityLogProvider logProvider;

    @Inject
    private Provider<HttpServletRequest> requestProvider;

    public Object invoke(MethodInvocation invocation) throws Throwable {
        String logging = invocation.getMethod()
                .getAnnotation(LoggingResource.class).value();

        logProvider.logActivity(Constants.RESOURCE_ACCESS_LOG_TYPE, logging,
                requestProvider.get());

        return invocation.proceed();
    }
}
