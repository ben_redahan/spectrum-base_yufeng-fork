package com.nexala.spectrum.licensing;

import java.util.Map;

public interface LicenceValidatorProvider {
    Map<Class<?>, LicenceValidator<?>> getValidators();
}
