package com.nexala.spectrum.licensing;

import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nexala.spectrum.view.Constants;
import com.trimble.rail.security.servlet.J2EUserProfileFacade;

public class Licence {
    private final String appName;
    private Set<String> userRoles;
    private LoginUser loginUser;
    private Locale userLocale;

    public Licence(String appName, J2EUserProfileFacade profile) {
        this.appName = appName;
        this.userRoles = profile.getRoles();
        Boolean userAdmin = userRoles != null && userRoles.contains(Constants.ADMINISTRATION_USERS_LIC_KEY);
        
        if (profile.getLocale() != null) {
            userLocale = profile.getLocale();
        } else {
            userLocale = Locale.ENGLISH;
        }
        
        this.loginUser = LoginUser.get(profile.getUsername(), profile.getUsername(), profile.getName(),
                profile.getLogoutURL(), profile.getAccountURL(), userAdmin, profile.getRealmManagementUrl(), userLocale.getLanguage());
    }

    public LoginUser getLoginUser() {
        return loginUser;
    }

    public String getAppName() {
        return appName;
    }
    
    public Locale getUserLocale() {
        return userLocale;
    }
    
    public Set<String> getUserRoles() {
        return userRoles;
    }
    
    public boolean hasResourceAccess(String resourceKey) {
        return userRoles.contains(resourceKey);
    }

    public static final Licence get(HttpServletRequest request, HttpServletResponse response, boolean reuse) {
        if (reuse) {
            Object licence = request.getSession().getAttribute(Constants.LICENCE);

            if (licence != null) {
                return (Licence) licence;
            }
        }

        final J2EUserProfileFacade profile = new J2EUserProfileFacade(request, response);

        Licence licence = new Licence(Constants.APP_NAME, profile);

        request.getSession().setAttribute(Constants.LICENCE, licence);

        return licence;
    }

    

}
