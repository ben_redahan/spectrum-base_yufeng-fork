package com.nexala.spectrum.licensing;

public interface LicenceValidator<T> {
    LicenceResult getLicenceResult(T obj, Licence licence);
}
