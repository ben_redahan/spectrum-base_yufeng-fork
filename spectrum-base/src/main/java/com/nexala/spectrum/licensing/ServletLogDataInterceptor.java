/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.licensing;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.nexala.spectrum.view.Constants;

public class ServletLogDataInterceptor implements MethodInterceptor {
    private static final Logger LOGGER = Logger
            .getLogger(ServletLogDataInterceptor.class);

    @Inject
    private ActivityLogProvider logProvider;

    @Inject
    private Provider<HttpServletRequest> requestProvider;

    public Object invoke(MethodInvocation invocation) throws Throwable {
        HttpServletRequest request = requestProvider.get();

        StringBuffer logInfo = new StringBuffer();
        logInfo.append("service="
                + invocation.getMethod().getDeclaringClass().getName());
        logInfo.append(", method=" + invocation.getMethod().getName());
        logInfo.append(", uri=" + request.getRequestURI());
        logInfo.append(", referer=" + request.getHeader("referer"));
        logInfo.append(", params={" + getParameters(invocation) + "}");

        logProvider.logActivity(Constants.DATA_ACCESS_LOG_TYPE,
                logInfo.toString(), request);

        return invocation.proceed();
    }

    private String getParameters(MethodInvocation invocation) {
        // TODO extract other parameter annotations like MatrixParam,
        // FormParam and so forth. At moment it extracts PathParam with
        // getPathParameters and QueryParam with getQueryParameters

        Annotation[][] paramAnnotations = invocation.getMethod()
                .getParameterAnnotations();
        Object[] paramValues = invocation.getArguments();

        StringBuffer params = new StringBuffer();
        String sep = "";
        for (int i = 0; i < paramAnnotations.length; i++) {
            try {
                if (paramAnnotations[i].length > 0) {
                    Annotation annotation = paramAnnotations[i][0];
                    String paramName = null;
                    if (annotation instanceof PathParam) {
                        paramName = ((PathParam) annotation).value();
                    } else if (annotation instanceof QueryParam) {
                        paramName = ((QueryParam) annotation).value();
                    }
    
                    params.append(sep);
                    params.append(paramName + "=" + paramValues[i]);
                    sep = ", ";
                }
            } catch (Exception ex) {
                LOGGER.error("Error getting rest call parameters for logging");
                ex.printStackTrace();
            }
        }

        return params.toString();
    }
}
