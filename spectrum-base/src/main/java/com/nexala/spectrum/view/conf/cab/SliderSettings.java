/*
 * Copyright (c) Nexala Technologies 2014, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class SliderSettings implements Settings {

    private Double majorIncrement;
    private Double minorIncrement;
    private Double minValue;
    private Double maxValue;

    public Double getMajorIncrement() {
        return majorIncrement;
    }

    public SliderSettings setMajorIncrement(Double majorIncrement) {
        this.majorIncrement = majorIncrement;
        return this;
    }

    public Double getMinorIncrement() {
        return minorIncrement;
    }

    public SliderSettings setMinorIncrement(Double minorIncrement) {
        this.minorIncrement = minorIncrement;
        return this;
    }

    public Double getMinValue() {
        return minValue;
    }

    public SliderSettings setMinValue(Double minValue) {
        this.minValue = minValue;
        return this;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public SliderSettings setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
        return this;
    }

}
