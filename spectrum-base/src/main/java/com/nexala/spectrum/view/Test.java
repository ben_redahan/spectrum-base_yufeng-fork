/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.regex.Pattern;

public class Test {
    public static void main(String [] args) {
        
        String input = "    js/nexala.hub.js,    js/nexala.timepicker.js,    js/nexala.togglebutton.js,    js/nexala.unit.unitsummary.js,    js/nexala.unit.stocktabs.js,    js/nexala.unit.tab.js,    js/nexala.unit.live.js,    js/nexala.unit.timeaware.js,    js/nexala.unitselector.js,    js/jquery.fadeto.js,    js/nexala.dataplots.js,    js/nexala.unit.maps.js,    js/nexala.maps.js,    js/nexala.channeldata.js,    js/raphael-2.1.min.js,    js/nexala.raphael.js,    js/nexala.unit.unitdetail.js,    js/nexala.unit.diagram.js,    js/nexala.cab.widget.js,    js/nexala.cab.js,    js/nexala.cab.gauge.js,    js/nexala.cab.button.js,    js/nexala.cab.uncouple.js,    js/nexala.cab.led.js,    js/nexala.cab.lever.js,    js/nexala.cab.caws.js,    js/nexala.cab.simple-lever.js,            js/nexala.cab.mmi.js,        js/nexala.ir.diagram.js,        js/nexala.ir.diagram.cab22000.js,        js/nexala.ir.diagram.coach22000.js,    ";
        Pattern VALIDATION = Pattern
                .compile("^\\s*[^,]+\\.(js|css)(\\s*,\\s*([^,]+\\.(js|css)))*\\s*,?\\s*$");
        
        System.out.println(VALIDATION.matcher(input).matches());
    }
}