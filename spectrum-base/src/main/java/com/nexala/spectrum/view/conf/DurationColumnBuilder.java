/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class DurationColumnBuilder extends ColumnBuilder{
    public DurationColumnBuilder() {
        super();
        type(ColumnType.DURATION);
        width(30);
    }
}

