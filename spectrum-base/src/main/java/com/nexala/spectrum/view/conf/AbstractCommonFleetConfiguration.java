package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.GlobalFleetProvider;

public abstract class AbstractCommonFleetConfiguration implements CommonFleetConfiguration {
    
    @Inject
    private GlobalFleetProvider globalFleetProvider;
    
    @Inject
    private Map<String, FleetProvider> fleetProviders;
    
    @Inject
    private ApplicationConfiguration applicationConfiguration;

    public AbstractCommonFleetConfiguration() {
        
    }
    
    @Override
    public boolean isShowSingleFleetTab() {
        String path = "r2m.fleetSummary.ShowSingleFleetTab";

        if (applicationConfiguration.get().hasPath(path)) {
            return applicationConfiguration.get().getBoolean(path);
        }
        return false;
    }
    
    @Override
    public List<Fleet> getFleetList(Licence licence) {
        List<Fleet> result = new ArrayList<Fleet>();

        for (Fleet f : globalFleetProvider.getFleets()) {
            String fleetLicence = fleetProviders.get(f.getCode()).getLicence(f.getCode());
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                result.add(f);
            }
        }
        
        return result;
    }

    @Override
    public String getScreenName() {
        return "Fleet Summary";
    }

    @Override
    public boolean isShowFleetFullNames() {
        String path = "r2m.fleetSummary.ShowFleetFullNames";

        if (applicationConfiguration.get().hasPath(path)) {
            return applicationConfiguration.get().getBoolean(path);
        }
        return false;
    }

    @Override
    public boolean isMouseOverHeaderEnabled() {
        String path = "r2m.fleetSummary.mouseOverHeaderEnabled";
        return applicationConfiguration.get().getBoolean(path);
    }
}
