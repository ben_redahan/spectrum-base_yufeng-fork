package com.nexala.spectrum.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nexala.spectrum.licensing.ActivityLogProvider;
import com.nexala.spectrum.licensing.Licence;


/**
 * Abstract servlet for external apps. These app are displayed on an Iframe with the header and footer of the application.
 * @author brice
 *
 */
public abstract class ExternalAppTemplate extends TemplateServlet {
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    private ActivityLogProvider logProvider;

    @Override
    protected final void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Licence licence = Licence.get(req, resp, false);
        
        if (hasAccess(licence)) {
            logAccess(logProvider, req);
            Map<String, Object> rootMap = new HashMap<String, Object>();
            rootMap.put("url", getExternalUrl());
            rootMap.put("includes", new ArrayList<String>());
            rootMap.put("screenName", getScreenName());

            doRender(req, resp, "externalApp.ftl", rootMap, licence);
            
        } 
    }
    

    /**
     * If access was allow, log this access.
     * @param logProvider the activity log provider
     * @param request the http request
     */
    protected abstract void logAccess(ActivityLogProvider logProvider, HttpServletRequest request);
    
    /**
     * Return the URL of the external app that should be displayed on the Iframe.
     * @return the URL of the external app that should be displayed on the Iframe.
     */
    protected abstract String getExternalUrl();
    
    /**
     * Return true if we have access to the screen, false otherwise.
     * 
     * @param licence the licence
     * @return true if we have access to the screen, false otherwise
     */
    protected abstract boolean hasAccess(Licence licence);

    /**
     * @return the name of the screen
     */
    protected abstract String getScreenName();
}
