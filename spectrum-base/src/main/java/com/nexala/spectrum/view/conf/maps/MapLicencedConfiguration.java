/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class MapLicencedConfiguration {

    private List<NamedMapView> namedViews;
    
    private List<Station> stations;

    private List<RouteTrack> routes;
    
    private List<ExtraLocationDropdown> extraLocationDropdown;

    public MapLicencedConfiguration(List<NamedMapView> namedViews,
            List<Station> stations, List<RouteTrack> routes,
            List<ExtraLocationDropdown> extraLocationDropdown) {
        this.namedViews = namedViews;
        this.stations = stations;
        this.routes = routes;
        this.extraLocationDropdown = extraLocationDropdown;
    }
    
    public List<ExtraLocationDropdown> getExtraLocationDropdown() {
    	return extraLocationDropdown;
    }

    public List<NamedMapView> getNamedViews() {
        return namedViews;
    }

    public List<Station> getStations() {
        return stations;
    }

    public List<RouteTrack> getRoutes() {
        return routes;
    }
}
