package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.nexala.spectrum.licensing.Licence;

/**
 * Represent an element into the application menu.
 * @author brice
 *
 */
public class MenuEntry {
    
    private final String label;
    
    private final List<App> screens;
    
    
    /**
     * Create a menu entry. The menu entry label is the description of the app
     * @param app the app contained in the menu
     */
    public MenuEntry(App app) {
        this.label = app.getDesc();
        this.screens = Arrays.asList(app);
    }
    
    /**
     * Create a menu entry.
     * @param label label of this menu entry
     * @param apps list of sub menu entries
     */
    public MenuEntry(String label, App... apps) {
        this.label = label;
        this.screens = Arrays.asList(apps);
    }

    /**
     * Getter for label.
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Getter for screens.
     * @return the screens
     */
    public List<App> getScreens() {
        return screens;
    }
    
    
    /**
     * Getter for screens.
     * @return the screens
     */
    public List<App> getScreens(Licence licence) {
        
        LicencePredicate predicate = new LicencePredicate(licence);
        ArrayList<App> apps = Lists.newArrayList(getScreens());
        Iterable<App> filtered = Iterables.filter(apps, predicate);
        
        return Lists.newArrayList(filtered);
    }
    
    

}
