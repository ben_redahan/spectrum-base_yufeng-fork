/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import com.nexala.spectrum.view.conf.maps.Coordinate;
import com.nexala.spectrum.view.conf.maps.Station;

public class StationXmlParser extends DefaultHandler {
    private static final Logger LOGGER = Logger
            .getLogger(StationXmlParser.class);

    private static class StationXmlParserHandler extends DefaultHandler {
        private static final String RECORD = "record";
        private static final String ATTR_ID = "id";
        private static final String ATTR_TEXT = "text";
        private static final String ATTR_LATITUDE = "latitude";
        private static final String ATTR_LONGITUDE = "longitude";
        private static final String ATTR_STATIONCODE = "stationCode";
        private static final String ATTR_TYPE = "type";

        private List<Station> stationElements = new ArrayList<Station>();

        @Override
        public void startElement(String uri, String localName, String qName,
                Attributes attributes) {
            if (qName.equals(RECORD)) {
                try {
                    Integer id = Integer.parseInt(attributes.getValue(ATTR_ID));
                    String text = attributes.getValue(ATTR_TEXT);
                    Double latitude = Double.parseDouble(attributes
                            .getValue(ATTR_LATITUDE));
                    Double longitude = Double.parseDouble(attributes
                            .getValue(ATTR_LONGITUDE));
                    String stationCode = attributes.getValue(ATTR_STATIONCODE);
                    String type = attributes.getValue(ATTR_TYPE);

                    stationElements.add(new Station(id, text, stationCode,
                            new Coordinate(latitude, longitude), type));
                } catch (Exception ex) {
                    LOGGER.warn("Error getting station (" + attributes + ")");
                    ex.printStackTrace();
                }
            }
        }

        public List<Station> getStationElements() {
            return stationElements;
        }
    }

    public static List<Station> parse(String xml) {
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            SAXParser sp = SAXParserFactory.newInstance().newSAXParser();
            StationXmlParserHandler handler = new StationXmlParserHandler();
            sp.parse(cl.getResourceAsStream(xml), handler);

            return handler.getStationElements();
        } catch (Exception e) {
            LOGGER.warn("Error loading Stations file (" + xml + ")");
            e.printStackTrace();

            return new ArrayList<Station>();
        }
    }
}