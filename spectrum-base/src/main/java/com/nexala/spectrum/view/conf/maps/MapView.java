/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

/** A MapView specifies the center position for the map and the zoom level */
public class MapView {
    private Coordinate northWest;
    private Coordinate southEast;
    private MapType mapType;
    private Coordinate center;
    private int zoom;

    public MapView(Coordinate northWest, Coordinate southEast, MapType mapType) {
        this.mapType = mapType;
        this.northWest = northWest;
        this.southEast = southEast;
    }

    public MapView(Coordinate center, int zoom, MapType mapType) {
        this.mapType = mapType;
        this.center = center;
        this.zoom = zoom;
    }

    public Coordinate getNorthWest() {
        return northWest;
    }

    public Coordinate getSouthEast() {
        return southEast;
    }

    public String getMapType() {
        return ((HasMicrosoftMapsName) mapType).getName();
    }

    public Coordinate getCenter() {
        return center;
    }

    public int getZoom() {
        return zoom;
    }
}
