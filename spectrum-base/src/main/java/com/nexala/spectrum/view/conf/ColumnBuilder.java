/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.channel.ChannelCategory;

public class ColumnBuilder {
    
    private String displayName = null;
    
    private String format = null;

    private String handler = null;
    
    private String licence = null;

    private String name = null;
    
    private String cssClass = null;

    private boolean resizable;

    private boolean sortable = false;
    
    private String sortByColumn = null;

    private Map<ChannelCategory, String> styles = new HashMap<ChannelCategory, String>();
    
    /** Set some reasonable defaults */
    private ColumnType type = ColumnType.DIGITAL;
    
    private boolean visible = true;
    
    private boolean autosize = false;
    
    private Integer width = 20;
    
    private boolean exportable = false;

    private String constant = null;

    private List<MouseOverText> mouseOverContent = new ArrayList<MouseOverText>();
    
    private String mouseOverHeader = null;
    
    private String displayField;
    
    private List<String> fleets;

    public ColumnBuilder() {}

    public Column build() {
        return new Column(this);
    }

    public ColumnBuilder displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
    
    public ColumnBuilder format(String format) {
        this.format = format;
        return this;
    }

    public String getDisplayName() {
        if (displayName != null) {
            return displayName;
        }
        else {
            return name;
        }
    }

    public String getFormat() {
        return format;
    }

    public String getHandler() {
        return handler;
    }

    public String getLicence() {
        return licence;
    }

    public String getName() {
        return name;
    }

    public Map<ChannelCategory, String> getStyles() {
        return styles;
    }

    public ColumnType getType() {
        return type;
    }

    public Integer getWidth() {
        return width;
    }

    public ColumnBuilder handler(String handler) {
        this.handler = handler;
        return this;
    }

    public boolean isResizable() {
        return resizable;
    }

    public boolean isSortable() {
        return sortable;
    }
    
    public String getSortByColumn() {
        return sortByColumn;
    }
    
    public boolean isVisible() {
        return visible;
    }

    public boolean isAutosize() {
        return autosize;
    }
    
    public boolean isExportable() {
        return exportable;
    }

    public String getConstant() {
        return constant;
    }

    public List<MouseOverText> getMouseOverContent() {
        return mouseOverContent;
    }
    
    public String getMouseOverHeader() {
        return mouseOverHeader;
    }

    public ColumnBuilder licence(String licence) {
        this.licence = licence;
        return this;
    }

    public ColumnBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ColumnBuilder resizable(boolean resizable) {
        this.resizable = resizable;
        return this;
    }
    
    public ColumnBuilder sortable(boolean sortable) {
        this.sortable = sortable;
        return this;
    }
    
    public ColumnBuilder sortByColumn(String columnName) {
        this.sortByColumn = columnName;
        return this;
    }

    public ColumnBuilder styles(Map<ChannelCategory, String> styles) {
        this.styles = styles;
        return this;
    }
    
    public ColumnBuilder type(ColumnType type) {
        this.type = type;
        return this;
    }
    
    public ColumnBuilder visible(boolean visible) {
        this.visible = visible;
        return this;
    }
    
    public ColumnBuilder autosize(boolean autosize) {
        this.autosize = autosize;
        return this;
    }
    
    public ColumnBuilder width(Integer width) {
        this.width = width;
        return this;
    }

    public ColumnBuilder exportable(boolean exportable) {
        this.exportable = exportable;
        return this;
    }

    public ColumnBuilder constant(String constant) {
        this.constant = constant;
        return this;
    }

    public ColumnBuilder mouseOverContent(List<MouseOverText> mouseOverContent) {
        this.mouseOverContent = mouseOverContent;
        return this;
    }
    
    public ColumnBuilder mouseOverHeader(String mouseOverHeader) {
        this.mouseOverHeader = mouseOverHeader;
        return this;
    }

    public String getCssClass() {
        return cssClass;
    }

    public ColumnBuilder cssClass(String cssClass) {
        this.cssClass = cssClass;
        return this;
    }
    
    public List<String> getFleets() {
        return fleets;
    }
    
    public ColumnBuilder fleets(List<String> fleets) {
        this.fleets = fleets;
        return this;
    }

    public String getDisplayField() {
        return displayField;
    }

    public void setDisplayField(String displayField) {
        this.displayField = displayField;
    }
}
