/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class GaugeSettings implements Settings {
    private String borderColor;
    private Double borderWidth;
    private Double cx;
    private Double cy;
    private List<DialSettings> dials;
    private Double endValue;
    private String faceFill;
    private Double faceRadius;
    private String faceRenderer;
    private String faceShadowFill;
    private String faceShadowStroke;
    private Integer fontSize;
    private List<LabelSettings> labels;
    private Double radius;
    private Double pivotRadius;
    private String rimFill;
    private Double rimWidth;
    private String rimHlFill;
    private Double startValue;
    private List<Slice> slices;
    private Boolean halfGauge;
    /** Height in pixel of the space available at the bottom of the component */
    private Double halfGaugeBottomSpacer;
    
    

    public Boolean getHalfGauge() {
        return halfGauge;
    }

    public GaugeSettings setHalfGauge(Boolean halfGauge) {
        this.halfGauge = halfGauge;
        return this;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public GaugeSettings setBorderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Double getBorderWidth() {
        return borderWidth;
    }

    public GaugeSettings setBorderWidth(Double borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Double getCx() {
        return cx;
    }

    public GaugeSettings setCx(Double cx) {
        this.cx = cx;
        return this;
    }

    public Double getCy() {
        return cy;
    }

    public GaugeSettings setCy(Double cy) {
        this.cy = cy;
        return this;
    }

    public List<DialSettings> getDials() {
        return dials;
    }

    public GaugeSettings setDials(List<DialSettings> dials) {
        this.dials = dials;
        return this;
    }

    public String getFaceFill() {
        return faceFill;
    }

    public GaugeSettings setFaceFill(String faceFill) {
        this.faceFill = faceFill;
        return this;
    }

    public Double getFaceRadius() {
        return faceRadius;
    }

    public GaugeSettings setFaceRadius(Double faceRadius) {
        this.faceRadius = faceRadius;
        return this;
    }

    public String getFaceRenderer() {
        return faceRenderer;
    }

    public GaugeSettings setFaceRenderer(String faceRenderer) {
        this.faceRenderer = faceRenderer;
        return this;
    }

    public String getFaceShadowFill() {
        return faceShadowFill;
    }

    public GaugeSettings setFaceShadowFill(String faceShadowFill) {
        this.faceShadowFill = faceShadowFill;
        return this;
    }

    public String getFaceShadowStroke() {
        return faceShadowStroke;
    }

    public GaugeSettings setFaceShadowStroke(String faceShadowStroke) {
        this.faceShadowStroke = faceShadowStroke;
        return this;
    }

    public Integer getFontSize() {
        return fontSize;
    }

    public GaugeSettings setFontSize(Integer fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public List<LabelSettings> getLabels() {
        return labels;
    }

    public GaugeSettings setLabels(List<LabelSettings> labels) {
        this.labels = labels;
        return this;
    }

    public Double getRadius() {
        return radius;
    }

    public GaugeSettings setRadius(Double radius) {
        this.radius = radius;
        return this;
    }

    public String getRimFill() {
        return rimFill;
    }

    public GaugeSettings setRimFill(String rimFill) {
        this.rimFill = rimFill;
        return this;
    }

    public Double getRimWidth() {
        return rimWidth;
    }

    public GaugeSettings setRimWidth(Double rimWidth) {
        this.rimWidth = rimWidth;
        return this;
    }

    public String getRimHlFill() {
        return rimHlFill;
    }

    public GaugeSettings setRimHlFill(String rimHlFill) {
        this.rimHlFill = rimHlFill;
        return this;
    }
    
    public List<Slice> getSlices() {
        return slices;
    }
    
    public GaugeSettings setSlices(List<Slice> slices) {
        this.slices = slices;
        return this;
    }


    public Double getHalfGaugeBottomSpacer() {
        return halfGaugeBottomSpacer;
    }

    public GaugeSettings setHalfGaugeBottomSpacer(Double halfGaugeBottomSpace) {
        this.halfGaugeBottomSpacer = halfGaugeBottomSpace;
        return this;
    }
    
    public Double getPivotRadius() {
        return pivotRadius;
    }
    
    public GaugeSettings setPivotRadius(Double pivotRadius) {
        this.pivotRadius = pivotRadius;
        return this;
    }
}