package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.Plugin;
import com.typesafe.config.Config;

public class DefaultCommonFleetConfiguration extends AbstractCommonFleetConfiguration {

    private ListMultimap<String, String> fleetsPerDatabase = ArrayListMultimap.create();
    
    private ApplicationConfiguration applicationConfiguration;
    
    private final Map<String, Integer> fleetTabs;
    
    private class LicencePredicate implements Predicate<Column> {
        
        private final Licence licence;
        
        public LicencePredicate(Licence licence) {
            this.licence = licence;
        }
        
        @Override
        public boolean apply(Column column) {
            if (column.getLicence() == null) {
                return true;
            } else if (licence.hasResourceAccess(column.getLicence())) {
                return true;
            } else {
                return false;
            }
        }
    };
    
    @Inject
    public DefaultCommonFleetConfiguration(ApplicationConfiguration applicationConfiguration) {
        
        Map<String, Config> fleets = applicationConfiguration.getFleetConfigList();
        fleetTabs = new HashMap<String, Integer>();
        
        for (Map.Entry<String, Config> fleetConfig : fleets.entrySet()) {
            fleetsPerDatabase.put(fleetConfig.getValue().getString("datasource"), fleetConfig.getKey());
            if (fleetConfig.getValue().hasPath("tabOrder")){
                fleetTabs.put(fleetConfig.getKey(), fleetConfig.getValue().getInt("tabOrder"));
            }
        }
        
        this.applicationConfiguration = applicationConfiguration;
    }
    
    @Override
    public ListMultimap<String, String> getFleetsPerDatabase() {
        return fleetsPerDatabase;
    }

    @Override
    public List<Column> getColumns(String fleetId, Licence licence) {
        LicencePredicate predicate = new LicencePredicate(licence);
        List<? extends Config> columnConfigs = applicationConfiguration.get().getConfigList("r2m.fleets." + fleetId + ".columns");
        
        List<Column> result = new ArrayList<Column>();
        for (Config columnConfig : columnConfigs) {
            ColumnBuilder columnBuilder = applicationConfiguration.columnBuilder(columnConfig);
            
            if (columnBuilder.getType().equals(ColumnType.GROUP)) {
                GroupColumn groupColumn = new GroupColumn(columnBuilder);
                
                result.add(groupColumn);
                
                for (Config childColumnConfig : columnConfig.getConfigList("columns")) {
                    ColumnBuilder childColumnBuilder = applicationConfiguration.columnBuilder(childColumnConfig);
                    if(childColumnBuilder.getLicence() == null || licence.hasResourceAccess(childColumnBuilder.getLicence())) {
                        groupColumn.addColumn(new Column(childColumnBuilder));
                    }
                }
                
                result.addAll(groupColumn.getColumns());
                
            } else {
                result.add(new Column(columnBuilder));
            }
            
        }
        return Lists.newArrayList(Iterables.filter(result, predicate));
    }

    @Override
    public List<Column> getColumns(String fleetId) {
        throw new UnsupportedOperationException();   
    }
    
    @Override
    public List<FleetFilter> getFleetFilters(String fleetId) {
        List<? extends Config> fleetFilterConfigs = applicationConfiguration.get().getConfigList("r2m.fleets." + fleetId + ".filters");
        
        List<FleetFilter> result = new ArrayList<FleetFilter>();
        for (Config filterConfig : fleetFilterConfigs) {
            String id = null;
            String label = null;
            String type = null;
            Boolean optional = false;
            List<String> columnIds = new ArrayList<>();
            
            if (filterConfig.hasPath("id")) {
                id = filterConfig.getString("id");
            }
            if (filterConfig.hasPath("label")) {
                label = filterConfig.getString("label");
            }
            if (filterConfig.hasPath("type")) {
                type = filterConfig.getString("type");
            }
            if (filterConfig.hasPath("columns")) {
                columnIds = filterConfig.getStringList("columns");
            }
            if (filterConfig.hasPath("optional")) {
                optional = filterConfig.getBoolean("optional");
            }
            
            result.add(new FleetFilter(id, label, type, optional, columnIds.toArray(new String[0])));
        }
        
        return result;
    }

    @Override
    public List<String> getColumnsUnitIdentifier(String fleetId) {
        if (applicationConfiguration.get().hasPath("r2m.fleets." + fleetId + ".columnUnitIdentifier")) {
            return applicationConfiguration.get().getStringList("r2m.fleets." + fleetId + ".columnUnitIdentifier");
        }
        return new ArrayList<String>();
    }

    @Override
    public ColumnSorterBean getDefaultSortingColumnName(String fleetId, Licence licence) {       
        Config fleetConfig = applicationConfiguration.get().getConfig("r2m.fleets." + fleetId);
        
        String columnName = null;
        boolean sortDesc = false;
        
        if (fleetConfig.hasPath("defaultSortingColumns")) {
            List<String> defaultSortingColumns = fleetConfig.getStringList("defaultSortingColumns");
            for (Column licencedColumn : getColumns(fleetId, licence)) {
                if (defaultSortingColumns.contains(licencedColumn.getName())) {
                    columnName = licencedColumn.getName();
                    break;
                }
            }
        } else {
            // Sort by the first column that is not a group column
            List<Column> licencedColumns = getColumns(fleetId, licence);
            
            for (Column column : licencedColumns) {
                if (!column.getType().equals(ColumnType.GROUP.toString().toLowerCase())) {
                    columnName = column.getName();
                    break;
                }
            }
        }
        
        if (fleetConfig.hasPath("defaultSortingDescending")) {
            sortDesc = fleetConfig.getBoolean("defaultSortingDescending");
        }
        
        return new ColumnSorterBean(columnName, sortDesc);
    }

    @Override
    public List<Plugin> getPlugins() {
        return applicationConfiguration.getPlugins(Spectrum.FLEET_SUMMARY_ID);
    }

    @Override
    public Plugin getParentPlugin() {
        return applicationConfiguration.getParentPlugin(Spectrum.FLEET_SUMMARY_ID);
    }
    
    @Override
    public Map<String, Integer> getFleetTabsOrdered() {
        return fleetTabs;
    }
}