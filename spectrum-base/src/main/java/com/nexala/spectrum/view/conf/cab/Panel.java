/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;


public class Panel extends Component {
    
    
    private static final String CLASS_BLACK_PANEL = "panel";
    
    private static final String CLASS_WHITE_PANEL = "whitepanel";
    
    private static final String CLASS_GREY_PANEL = "greypanel";
    
    
    private String className = CLASS_BLACK_PANEL; 
    
    
    public static enum COLOR {BLACK, WHITE, GREY};
    
    

    public Panel(String domId, int x, int y, int height, int width) {
        super(domId, x, y, height, width);
    }
    
    public String getClassName() {
        return className;
    }
    
    /**
     * Change the color of the panel.
     * @param color the color choose. Could be white or black.
     */
    public Panel setColor(COLOR color) {
        switch(color) {
        case BLACK :
            this.className = CLASS_BLACK_PANEL;
            break;
        case WHITE :
            this.className = CLASS_WHITE_PANEL;
            break;
        case GREY :
            this.className = CLASS_GREY_PANEL;
            break;
        }
        
        
        
        return this;
    }
    
    @Override
    public String getWidgetClassName() {
        return "panel";
    }
}