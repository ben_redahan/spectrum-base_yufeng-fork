/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import java.util.List;

public interface CabConfiguration {
    public List<Component> getLayout();

    public List<Label> getLabels();

    public int getWidth();

    public int getHeight();

    public List<Integer> getChannelIds();

    /**
     * Returns true if the cab implementation needs to use the digital default
     * value. With this feature on, instead of query 1 single record, the latest
     * x seconds of data will be queried and the "non" default value for each
     * digital channel will be returned.
     */
    public boolean hasTransientStates();

    public int getDataInterval();
    
    public int getDataIntervalLimit();
    
    /**
     * Return true if this cab config is the only one on this project 
     * and don't need to be reload on the cab view.
     * 
     * @return true if this cab config is the only one, false otherwise
     * 
     * FIXME - this looks a bit strange. Can't this be determined on the client side? i.e. [[ if config count == 1 ]]
     */
    public boolean isOnlyOneCabConfig();
}
