/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

/**
 * Widget to display the time.
 * @author brice
 *
 */
public class Clock extends Widget {
    
    
    public Clock(String domId, int x, int y, int h, int w, 
            String... channels) {
        super(domId, x, y, h, w, channels);
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Clock";
    }
    
}