package com.nexala.spectrum.view.conf;

import java.util.List;

import com.nexala.spectrum.view.Plugin;

public interface HasPlugins {
    /** Returns the child plugins, if any */
    public List<Plugin> getPlugins();
}
