package com.nexala.spectrum.view.conf.cab;

public class Arrow extends Widget {


    public Arrow(String domId, int x, int y, int height, int width,
            String... channels) {
        super(domId, x, y, height, width, channels);
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Arrow";
    }

}
