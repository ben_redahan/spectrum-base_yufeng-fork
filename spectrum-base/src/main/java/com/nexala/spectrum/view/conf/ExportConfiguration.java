package com.nexala.spectrum.view.conf;

public class ExportConfiguration {
	
	private char separator;
	private String encoding;
	private String fileExtension;
		
	public char getSeparator() {
		return separator;
	}
	
	public void setSeparator(char separator) {
		this.separator = separator;
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}
	
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

}
