/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class AnalogueColumnBuilder extends ColumnBuilder {
    public AnalogueColumnBuilder() {
        super();
        type(ColumnType.ANALOGUE);
        width(30);
    }
}
