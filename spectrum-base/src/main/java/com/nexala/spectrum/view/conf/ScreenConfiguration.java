package com.nexala.spectrum.view.conf;

import com.nexala.spectrum.view.Plugin;

/**
 * Represent a screen configuration
 * @author brice
 *
 */
public interface ScreenConfiguration extends HasPlugins {
	
	/**
	 * Return a plugin containing all scripts and stylesheet the screen need to work.
	 * @return a plugin containing all scripts and stylesheet the screen need to work.
	 */
	public Plugin getParentPlugin();
	
    /**
     * Return the name of the screen.
     * @return the name of the screen.
     */	
	public String getScreenName();

}
