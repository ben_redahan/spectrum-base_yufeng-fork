/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.Plugin;

@ImplementedBy(DefaultUnitConfiguration.class)
public interface UnitConfiguration extends ScreenConfiguration {
    /**
     * Returns a licenced UnitConfiguration object
     * 
     * @param licence
     */
    //UnitConfiguration get(Licence licence);

    /**
     * Gets the width of the unit selector and results dropdown (in pixels)
     */
    int getUnitSelectorWidth();

    /** Returns a list of the screens, which should be included in the unit summary */
    List<DetailScreen> getScreens(Licence licence);

    List<Plugin> getPlugins();
    
    Boolean isUnitSummaryHeaderCombined();
}
