<#include "header.ftl"/>

<div id = 'mapHeader'>
    <div id = 'right'>
        <div id = 'timeController' style = 'float:right'>
            <div id = 'playPauseButton'></div>
            <div id = 'timePicker' class = 'timePicker'>
                <div class = "leftButton"><a>-</a></div>
                <div class = "inputDiv"></div>
                <div class = 'nowButton'><a>LIVE</a></div> 
                <div class = "rightButton"><a>+</a></div>
            </div>
        </div>
    </div>
</div>

<div id = 'mapWrapper'>
    <div id = 'mapContainer' class = 'ui-widget' >
        <div id ='mapToolbar' class = 'ui-widget-header'></div>
        <div id = 'mapUnitSearchPanel'>
            <div class = 'peak-up'></div>
            <div class = 'unitSearchBody darkbg ui-corner-all'>
                <div id='mapUnitSelector' >
                    <input class="textBox" type="text">
                </div>
            </div>
        </div>
        
        <div id='map'>
            <div class = 'zoomer'>
                <div class = 'zoomout ui-corner-left ui-widget-header'>&#8722;</div>
                <div class = 'zoomin ui-corner-right ui-widget-header'>&#43;</div>
            </div>
            <div id='mapInfoOverlay' class = 'darkbg'></div>
        </div>
        <div id='routeMap' class = 'ui-widget' style="display : none; background-color  : white; width : 100%; position : absolute; top : 40px; bottom : 0px; overflow : auto; z-index : 2;">
            <div id='routeMapCmp' class = 'ui-widget'></div>
        </div>
        
    </div>
</div>

<#include "footer.ftl"/>