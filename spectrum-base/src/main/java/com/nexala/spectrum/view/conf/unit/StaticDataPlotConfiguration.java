/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;

import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.view.conf.DetailScreen;

public interface StaticDataPlotConfiguration extends DetailScreen, ScreenRequiresFeature {
    
    /**
     * Return list of channel groups.
     * @return list of channel groups.
     */
    List<ChannelGroup> getChannelGroups();
    
    
}
