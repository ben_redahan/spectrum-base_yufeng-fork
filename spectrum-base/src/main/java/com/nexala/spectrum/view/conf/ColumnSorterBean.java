package com.nexala.spectrum.view.conf;

/**
 * Bean represention a column sorter.
 * Include the column name and the order of sort.
 * @author brice
 *
 */
public class ColumnSorterBean {
    public ColumnSorterBean() {
    }

    public ColumnSorterBean(String columnName, boolean desc) {
        this.columnName = columnName;
        this.desc = desc;
    }

    /**
     * The column name
     */
    String columnName = null;
    
    /**
     * If the column should be order asc or desc.
     */
    boolean desc = false;

    /**
     * Getter for columnName.
     * @return the columnName
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Setter for columnName.
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * Getter for desc.
     * @return the desc
     */
    public boolean isDesc() {
        return desc;
    }

    /**
     * Setter for desc.
     * @param desc the desc to set
     */
    public void setDesc(boolean desc) {
        this.desc = desc;
    }

}
