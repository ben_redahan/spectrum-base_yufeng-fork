/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import static com.nexala.spectrum.view.conf.DrillDownGridStyle.DRILLDOWN_CHANNELNAME;
import static com.nexala.spectrum.view.conf.DrillDownGridStyle.DRILLDOWN_COMMON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.beans.Vehicle;
import com.nexala.spectrum.rest.data.beans.Formation;
import com.typesafe.config.Config;

public class DefaultDrillDownConfiguration implements DrillDownConfiguration {

    private final Map<String, Boolean> vehicleLevelMap;
    private final Boolean showUnitRow; 
        
    @Inject
    DefaultDrillDownConfiguration(ApplicationConfiguration applicationConfiguration) {
        this.showUnitRow = applicationConfiguration.get().getBoolean("r2m.fleetSummary.drillDown.displayUnitRow");
        
        this.vehicleLevelMap = new HashMap<String, Boolean>();
        Map<String, Config> fleets = applicationConfiguration.getFleetConfigList();
        
        for (Map.Entry<String, Config> fleetConfig : fleets.entrySet()) {
            this.vehicleLevelMap.put(fleetConfig.getKey(), fleetConfig.getValue().getBoolean("vehicleLevel"));
        }
    }
    
    @Override
    public List<Column> getColumns(String fleetId, List<Formation> formationList) {
        List<Column> columns = new ArrayList<Column>();

        if (fleetId != null) {

            Boolean vehicleLevel = vehicleLevelMap.get(fleetId);

            columns.add(new TextColumnBuilder().name(Spectrum.COLUMN_CHANNEL_NAME)
                    .width(255).styles(DRILLDOWN_CHANNELNAME).sortable(false)
                    .build());

            columns.add(new TextColumnBuilder()
                    .name(Spectrum.COLUMN_ALWAYS_DISPLAYED).visible(false).build());

            for (Formation formation : formationList) {
                Unit unit = formation.getUnit();
                
                if (vehicleLevel) {
                    if (unit != null && showUnitRow) {
                        GroupColumnBuilder gc = new GroupColumnBuilder();

                        gc.name(unit.getUnitNumber());
                        gc.displayName(unit.getUnitNumber());

                        GroupColumn group = gc.buildGroup();

                        for (Vehicle vehicle : formation.getVehicles()) {
                            group.addColumn(createVehicleColumn(vehicle));
                        }

                        columns.add(group);
                    } else {
                        for (Vehicle vehicle : formation.getVehicles()) {
                            columns.add(createVehicleColumn(vehicle));
                        }
                    }
                } else {
                    if (unit != null) {
                        Column col = createUnitColumn(unit);
                        columns.add(col);
                    }
                }
            }
        }

        return columns;
    }

    private Column createVehicleColumn(Vehicle vehicle) {
        return new TextColumnBuilder().displayName(vehicle.getVehicleNumber())
                .name(Long.toString(vehicle.getId())).width(55)
                .styles(DRILLDOWN_COMMON).sortable(false).build();
    }

    private Column createUnitColumn(Unit unit) {
        return new TextColumnBuilder().displayName(unit.getUnitNumber())
                .name(unit.getId().toString()).width(55)
                .styles(DRILLDOWN_COMMON).sortable(false).build();
    }
}
