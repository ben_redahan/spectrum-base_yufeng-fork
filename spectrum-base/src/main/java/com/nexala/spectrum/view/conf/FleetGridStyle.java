/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.HashMap;
import java.util.Map;

import com.nexala.spectrum.channel.ChannelCategory;

// TODO : add new ChannelCategory/ChannelStatus EVENT

public class FleetGridStyle {
    // XXX why do we include FLEET in name?
    public static final Map<ChannelCategory, String> FLEET_COMMON = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 4493094068501372290L;
        {
            put(ChannelCategory.INVALID, "fleet_purple_black");
            put(ChannelCategory.NODATA, "fleet_white_white");
            put(ChannelCategory.NORMAL, "fleet_green_white");
            put(ChannelCategory.ON, "fleet_yellow_black");
            put(ChannelCategory.OFF, "fleet_blue_black");
            put(ChannelCategory.WARNING, "fleet_orange_white");
            put(ChannelCategory.WARNING1, "fleet_orange_white");
            put(ChannelCategory.WARNING2, "fleet_orange_white");
            put(ChannelCategory.FAULT, "fleet_red_white");
            put(ChannelCategory.FAULT1, "fleet_red_white");
            put(ChannelCategory.FAULT2, "fleet_red_white");
        }
    };

    // XXX why do we include FLEET in name?
    public static final Map<ChannelCategory, String> FLEET_GROUP = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 9215077496854465908L;
        {
            put(ChannelCategory.INVALID, "fleet_purple_purple");
            put(ChannelCategory.NODATA, "fleet_white_white");
            put(ChannelCategory.NORMAL, "fleet_green_green");
            put(ChannelCategory.ON, "fleet_yellow_yellow");
            put(ChannelCategory.OFF, "fleet_blue_blue");
            put(ChannelCategory.WARNING, "fleet_orange_orange");
            put(ChannelCategory.WARNING1, "fleet_orange_orange");
            put(ChannelCategory.WARNING2, "fleet_orange_orange");
            put(ChannelCategory.FAULT, "fleet_red_red");
            put(ChannelCategory.FAULT1, "fleet_red_red");
            put(ChannelCategory.FAULT2, "fleet_red_red");
        }
    };
    
    // XXX why do we include FLEET in name?
    public static final Map<ChannelCategory, String> FLEET_UNIT_ACTIVE = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = -5680375858579338411L;
        {
            put(ChannelCategory.ON, "fleet_yellow_black");
            put(ChannelCategory.OFF, "fleet_white_black");
        }
    };
    
    public static final Map<ChannelCategory, String> NON_VALIDATED = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 3905701591325977178L;
        {
            put(ChannelCategory.NODATA, "fleet_white_white");
            put(ChannelCategory.NORMAL, "fleet_white_black");
        }
    };
    
    // XXX why do we include FLEET in name?
    public static final Map<ChannelCategory, String> FLEET_TIMESTAMP = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 3905701591325977178L;
        {
            put(ChannelCategory.NODATA, "fleet_white_white");
            put(ChannelCategory.NORMAL, "fleet_white_black");
            put(ChannelCategory.WARNING, "fleet_yellow_black");
            put(ChannelCategory.FAULT, "fleet_orange_white");
        }
    };
    
    public static final Map<ChannelCategory, String> BLUE_YELLOW_GROUP = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 9215077496854465908L;
        {
        	put(ChannelCategory.NODATA, "fleet_white_white");
            put(ChannelCategory.OFF, "fleet_yellow_yellow");
            put(ChannelCategory.ON, "fleet_blue_blue");
        }
    };
    
    public static final Map<ChannelCategory, String> BLUE_YELLOW_DATA_GROUP = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 9215077496854465908L;
        {
        	put(ChannelCategory.NODATA, "fleet_white_white");
            put(ChannelCategory.OFF, "fleet_yellow_black");
            put(ChannelCategory.ON, "fleet_blue_black");
        }
    };
}