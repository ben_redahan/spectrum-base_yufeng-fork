/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;
import java.util.Map;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.view.conf.ChartConfiguration;
import com.nexala.spectrum.view.conf.DetailScreen;
import com.nexala.spectrum.view.conf.LabelValue;

@ImplementedBy(DefaultDataPlotConfiguration.class)
public interface DataPlotConfiguration extends DetailScreen, ScreenRequiresFeature {
    /**
     * Return true if we should have a virtual scroll bar to be able to
     * scroll between configurations, false otherwise
     * @return true if virtual scrollbar, false otherwise
     */
    public boolean getVirtualScrollBar();
    
    
    /**
     * Return list of channel groups.
     * @return list of channel groups.
     */
    List<ChannelGroup> getChannelGroups();
    
    
    /**
     * Custom Data View renders <b>systems</b> and <b>users</b> ( user defined ) graphs. 
     * This option controls whether system charts should be included or no. 
     * 
     * @return true if <b>system</b> charts are included otherwise no
     */
    public boolean isSystemChartsIncluded();

    public Map<String, Boolean> getChartEnabledByFleet();

	public ChartConfiguration getChartConfiguration();
	
	public LabelValue getChannelLabelConfig();
	
	public LabelValue getTableChannelLabelConfig();
}