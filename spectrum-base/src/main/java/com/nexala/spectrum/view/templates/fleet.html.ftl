<#include 'header.ftl'/>

<div id = 'fleetSummary'>
    <!-- fleet summary matrix -->
    <div id = 'fleetContainer'>
        <div id = 'fleetGrid' class = 'tabularVertical'>
            <table class = 'ui-corner-all'></table>
        </div>
    </div>
</div>

<!-- drilldown dialog -->
<div id ='drilldown_dialog' style='display: none'>
    <div class = 'drilldownTable'>
        <table></table>
    </div>
</div>

<#include 'footer.ftl'/>