var inject = function() {
    var includes = [];
    
    for (var i = 0; i < arguments.length; i++) {
        var resource = arguments[i];
        var l = resource.length;
        
        if (resource.indexOf('.js') == l - 3) {
            includes.push('\x3Cscript charset="UTF-8" type = "text/javascript" src = "' + resource + '">\x3C/script>');
        }
    }
    
    document.write(includes.join(''));
};