/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

/**
 * Widget for indicators.
 * @author brice
 *
 */
public class Indicator extends Widget {

    IndicatorSettings settings;

    public Indicator(String domId, int x, int y, int h, int w,
            IndicatorSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }

    @Override
    public String getWidgetClassName() {
        return "nx.cab.Indicator";
    }
    
    @Override
    public Settings getSettingsObject() {
        return settings;
    }

}