/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class Component {
    private final String domId;
    private final int height;
    private final int width;
    private int x;
    private int y;

    public Component(String domId, int x, int y, int height, int width) {
        this.domId = domId;
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    public abstract String getClassName();

    @JsonIgnore
    public String getSettings() {
        Settings settings = getSettingsObject();

        if (settings == null) {
            return null;
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(Include.NON_NULL);
            return mapper.writeValueAsString(settings);
        } catch (JsonGenerationException e) {
            return null;
        } catch (JsonMappingException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public String getWidgetClassName() {
        return null;
    }

    public Settings getSettingsObject() {
        return null;
    }

    public String getId() {
        return domId;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Setter for x.
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }
}
