package com.nexala.spectrum.view.conf.maps;

/**
 * Marker for Detectors.
 *
 */
public class Detector {
    
    private Integer id;
    private Integer siteId;
    private String from;
    private String to;
    
    /**
     * Default constructor
     */
    public Detector(Integer id, Integer siteId, String from, String to) {
        setId(id);
        setSiteId(siteId);
        setFrom(from);
        setTo(to);
    }
    
    public void setId(Integer id){
        this.id = id;
    }  
    
    public void setSiteId(Integer siteId){
        this.siteId = siteId;
    }  
    
    public void setFrom(String from){
        this.from = from;
    }  
    
    public void setTo(String to){
        this.to = to;
    }  
    
    public Integer getId(){
        return id;
    }  
    
    public Integer getSiteId(){
        return siteId;
    }  
    
    public String getFrom(){
        return from;
    }  
    
    public String getTo(){
        return to;
    }  
}
