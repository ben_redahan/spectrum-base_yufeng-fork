/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.rest.data.GroupProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.UnitDesc;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.DetailScreen;
import com.nexala.spectrum.view.conf.UnitConfiguration;

@Singleton
public class UnitView extends TemplateServlet {

    @Inject
    private Map<String, UnitConfiguration> confs;

    @Inject
    private Map<String, UnitProvider> unitProviders;

    @Inject
    private Map<String, GroupProvider> groupProviders;

    @Inject
    private AppConfiguration appConf;

    private static final long serialVersionUID = 7743892486241418025L;

    private void handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        String fleetId = request.getParameter("fleetId");
        String unitId = request.getParameter("unitId");
        String initialTab = request.getParameter("tab");
        String timestamp = request.getParameter("timestamp");
        String faultId = request.getParameter("faultId");
        UnitDesc unitDesc = null;
        Unit unit = null;

        if (fleetId == null && confs.size() > 0) {
            // Get the first conf available
            fleetId = confs.keySet().iterator().next();
        }

        if (unitId != null) {
            unit = unitProviders.get(fleetId).searchUnit(
                    Integer.parseInt(unitId));
        } else {
            // Notice: for GA unitNumber = last three digits from original db value
            // The value is re-composed in Unit fleet providers 
            String unitNumber = request.getParameter("unitNumber");
            unit = unitProviders.get(fleetId).searchUnit(unitNumber);
        }

        unitDesc = (unit == null) ? null : new UnitDesc(unit.getId(),
                unit.getUnitNumber(), fleetId);
        proceed(request, response, unitDesc, fleetId, initialTab, timestamp, faultId);
    }

    @RestrictedResource(Constants.UNIT_LIC_KEY)
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        handleRequest(request, response);
    }

    @RestrictedResource(Constants.UNIT_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        handleRequest(request, response);
    }

    private void proceed(HttpServletRequest request, HttpServletResponse response, UnitDesc unitDesc, String fleetId,
            String initialTab, String timestamp, String faultId) throws ServletException {

        Licence licence = Licence.get(request, response, true);

        if (fleetId == null && confs.size() > 0) {
            // Get the first conf available
            fleetId = confs.keySet().iterator().next();

        }

        List<String> includes = new ArrayList<String>();
        List<Plugin> plugins = new ArrayList<Plugin>();
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "unit_head.html.ftl");
        rootMap.put("includes", includes);
        rootMap.put("channelGroups", groupProviders.get(fleetId).getVisibleGroupList());
        rootMap.put("initialTab", initialTab);
        rootMap.put("timestamp", timestamp);
        rootMap.put("faultId", faultId);

        Set<String> jsIncludes = new LinkedHashSet<String>();
        Set<String> cssIncludes = new LinkedHashSet<String>();

        for (DetailScreen screen : confs.get(fleetId).getScreens(licence)) {
            Plugin parent = screen.getParentPlugin();

            if (parent != null) {
                if (parent.getSources() != null) {
                    jsIncludes.addAll(parent.getSources());
                }

                if (parent.getStylesheets() != null) {
                    cssIncludes.addAll(parent.getStylesheets());
                }
            }

            for (Plugin plugin : screen.getPlugins()) {
                if (plugin.getSources() != null) {
                    jsIncludes.addAll(plugin.getSources());
                }

                if (plugin.getStylesheets() != null) {
                    cssIncludes.addAll(plugin.getStylesheets());
                }
            }
        }

        plugins = confs.get(fleetId).getPlugins();
        for (Plugin plugin : plugins) {
            if (plugin.getSources() != null) {
                jsIncludes.addAll(plugin.getSources());
            }

            if (plugin.getStylesheets() != null) {
                cssIncludes.addAll(plugin.getStylesheets());
            }
        }

        rootMap.put("jsIncludesList", Lists.partition(new ArrayList<String>(jsIncludes), 10));
        rootMap.put("cssIncludesList", Lists.partition(new ArrayList<String>(cssIncludes), 10));
        rootMap.put("screens", confs.get(fleetId).getScreens(licence));
        rootMap.put("unit", unitDesc);
        rootMap.put("plugins", plugins);
        rootMap.put("screenName", confs.get(fleetId).getScreenName());
        doRender(request, response, "unit.html.ftl", rootMap, licence);
    }

    public App getView() {
        return appConf.getApp(Constants.UNIT_SUMMARY);
    }

}
