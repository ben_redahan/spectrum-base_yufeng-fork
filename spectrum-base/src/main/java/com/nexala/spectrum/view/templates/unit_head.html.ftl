<!-- Unit Summary includes -->

<link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
    css/nexala.unitsummary.css,
    css/nexala.unitselector.css,
    css/nexala.timepicker.css,
    css/nexala.togglebutton.css
    &view=unit
'>

<#list cssIncludesList as cssIncludes>
    <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
        <#list cssIncludes as cssInclude>${cssInclude},</#list>
        &view=unit
    '>
</#list>

<script type = 'text/javascript' src = 'js/merge?resources=
    js/i18n/datepicker-nl.js,
    js/i18n/datepicker-en-GB.js,
    js/nexala.timepicker.js,
    js/nexala.togglebutton.js,
    js/nexala.unit.tab.js,
    js/nexala.unit.live.js,
    js/nexala.unit.timeaware.js,
    js/nexala.unitselector.js,
    js/nexala.unit.unitsummary.js
    &view=unit
'></script>

<#list jsIncludesList as jsIncludes>
    <script type = 'text/javascript' src = 'js/merge?resources=
        <#list jsIncludes as jsInclude>${jsInclude},</#list>
        &view=unit
    '></script>
</#list>

<script type = 'text/javascript'>
    $(document).ready(function() {
        var unitSummary = new nx.unit.UnitSummary();
        <#list screens as screen>
            <#if screen??>
            	<#if screen.parentPlugin??>
	                <#if (screen.screenType!"TAB") == "TAB">
	                    
	                    
	                    unitSummary.bind('#${screen.link!"us_tab${screen_index + 1}"}', ${screen.parentPlugin.pluginClass}, '${screen.dataNavigationType}');
	                    
	                    <#list screen.plugins as plugin>
	                        <#if plugin.pluginClass?has_content>
	                            unitSummary.bindTabPlugin('#${screen.link!"us_tab${screen_index + 1}"}', ${plugin.pluginClass});
	                        </#if>
	                    </#list>
	                <#else>
	                    unitSummary.bindDialog('#us_dialog${screen.displayName?replace(" ", "")}', ${screen.parentPlugin.pluginClass});
	                </#if>
                </#if>
            </#if>
        </#list>
        
        <#list plugins as plugin>
            <#if plugin??>
                unitSummary.bindPlugin(${plugin.pluginClass});
            </#if>
        </#list>
        
        var unitDesc = {};
        
        <#if unit??>
            unitDesc.id = ${unit.id?c};
            unitDesc.displayName = "${unit.displayName}";
            unitDesc.fleetId = "${unit.fleetId}";
        </#if>
        
        var initialTab = null;
        <#if initialTab??>
            initialTab = '${initialTab}';
        </#if>
        
        var timestamp = null;
        <#if timestamp??>
            timestamp = ${timestamp};
        </#if>
        
        var faultId = null;
        <#if faultId??>
            faultId = ${faultId};
        </#if>
        
        unitSummary.initialize(unitDesc, initialTab, timestamp, faultId);
    });
</script>