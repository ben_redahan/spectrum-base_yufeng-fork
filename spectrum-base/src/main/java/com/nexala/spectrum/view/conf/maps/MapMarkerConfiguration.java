/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

public class MapMarkerConfiguration {

    private String jsObject;

    private String markerType;

    private int zIndex;

    private boolean redrawOnViewChange;

    private String settingLabel;

    private boolean enabledByDefault;

    private MapMarkerLegend legend;
    
    private String config;
    
    private String datasource;

    /**
     * Return the name of the js object for the marker.
     * 
     * @return the name of the js object.
     */
    public String getJsObject() {
        return jsObject;
    }

    public void setJsObject(String jsObject) {
        this.jsObject = jsObject;
    }

    /**
     * Return Id for this type of marker. Should be unique
     * 
     * @return
     */
    public String getMarkerType() {
        return markerType;
    }

    public void setMarkerType(String markerType) {
        this.markerType = markerType;
    }

    /**
     * Return the z-index for this marker type.
     * Value between 0 and 999, 999 is at the top. 
     * @return the z-index
     */
    public int getzIndex() {
        return zIndex;
    }

    public void setzIndex(int zIndex) {
        this.zIndex = zIndex;
    }

    /**
     * True if the markers provided need to be redraw when view changed, false otherwise.
     * @return if markers displayed need to be redraw when view change
     */
    public boolean isRedrawOnViewChange() {
        return redrawOnViewChange;
    }

    public void setRedrawOnViewChange(boolean redrawOnViewChange) {
        this.redrawOnViewChange = redrawOnViewChange;
    }

    /**
     * Return the label displayed on the setting panel to enable or disable. 
     * this marker provider. 
     * @return the label displayed on the setting panel
     */
    public String getSettingLabel() {
        return settingLabel;
    }

    public void setSettingLabel(String settingLabel) {
        this.settingLabel = settingLabel;
    }

    /**
     * The state of the marker when loading screen.
     * @return true if the marker is enabled by default, false otherwise
     */
    public boolean isEnabledByDefault() {
        return enabledByDefault;
    }

    public void setEnabledByDefault(boolean enabledByDefault) {
        this.enabledByDefault = enabledByDefault;
    }

    /**
     * Return the legend for this marker.
     * @return the legend for this marker
     */
    public MapMarkerLegend getLegend() {
        return legend;
    }

    public void setLegend(MapMarkerLegend legendId) {
        this.legend = legendId;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }
}
