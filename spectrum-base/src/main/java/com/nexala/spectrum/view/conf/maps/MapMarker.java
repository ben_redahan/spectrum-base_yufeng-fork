package com.nexala.spectrum.view.conf.maps;

import com.nexala.spectrum.mapping.Coordinate;

/**
 * Represent a marker on the map. Could be extend to add additional informations
 * used to render it.
 * 
 * @author brice
 * 
 */
public class MapMarker extends Coordinate {

    protected String markerType = null;
    

    /**
     * Getter for markerType.
     * 
     * @return the markerType
     */
    public String getMarkerType() {
        return markerType;
    }

    /**
     * Setter for markerType.
     * 
     * @param markerType
     *            the markerType to set
     */
    public void setMarkerType(String markerType) {
        this.markerType = markerType;
    }

}
