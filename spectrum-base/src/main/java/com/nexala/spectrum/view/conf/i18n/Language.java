package com.nexala.spectrum.view.conf.i18n;

import java.util.List;

/**
 * Represent a language supported in the application.
 * @author BBaudry
 *
 */
public class Language {
    
    private String localCode;
    
    private List<TranslationFile> translationFiles;

    public String getLocalCode() {
        return localCode;
    }

    public void setLocalCode(String localCode) {
        this.localCode = localCode;
    }

    public List<TranslationFile> getTranslationFiles() {
        return translationFiles;
    }

    public void setTranslationFiles(List<TranslationFile> translationFiles) {
        this.translationFiles = translationFiles;
    }

}
