/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.templates;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MruCacheStorage;
import freemarker.cache.NullCacheStorage;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;

@Singleton
public class TemplateConfiguration {

    private final Configuration conf;

    @Inject
    public TemplateConfiguration(ConfigurationManager confManager) {
        conf = new Configuration();

        boolean cache = confManager.getConfAsBoolean(Spectrum.SPECTRUM_TEMPLATES_CACHE, false);

        if (cache) {
            conf.setCacheStorage(new MruCacheStorage(20, 50));
        } else {
            conf.setTemplateUpdateDelay(0);
            conf.setCacheStorage(new NullCacheStorage());
        }

        conf.setTemplateLoader(getTemplateLoader());
    }

    /**
     * return the configuration that will be used. Could be override.
     * 
     * @param properties
     *            the properties
     * @return the freemarker configuration
     */
    protected TemplateLoader getTemplateLoader() {
        return new ClassTemplateLoader(TemplateConfiguration.class, "");
    }

    /**
     * Return the freemarker configuration. Should NOT be override.
     * 
     * @return the freemarker configuration
     */
    public final Configuration get() {
        return conf;
    }
}