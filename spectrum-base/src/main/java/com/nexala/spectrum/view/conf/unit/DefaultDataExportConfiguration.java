/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.inject.Inject;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;


public class DefaultDataExportConfiguration implements DataExportConfiguration {
    
    @Inject
    private Map<String, ChannelDataProvider> cdProviders;
    
    @Override
    public String[] getHeader(String vehicleNumber, long start, long end, 
            TimeZone tz, String fleetId, Licence licence) {
        
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(licence.getUserLocale());
        
        // fleetId could be used in specific projects where this method is overridden
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        dateFormat.setTimeZone(tz);
        
        return new String[] {
                bundle.getString("vehicle")+": ",
            vehicleNumber,
            bundle.getString("from")+": ",
            dateFormat.format(start),
            bundle.getString("to")+": ",
            dateFormat.format(end)
        };
    }

    @Override
    public synchronized String getFileName(String vehicleNumber, long start, TimeZone tz) {
        SimpleDateFormat dateFormatFile = new SimpleDateFormat(
                "yyyy_MM_dd HH:mm:ss");
        dateFormatFile.setTimeZone(tz);
        
        return "DataPlot-export " + vehicleNumber + " "
                + dateFormatFile.format(start);
    }

    @Override
    public synchronized String[] getRow(DataSet data,
            List<ChannelConfig> channels, TimeZone tz) {
        SimpleDateFormat dateFormatMillisecond = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss.SSS");
        dateFormatMillisecond.setTimeZone(tz);
        
        String[] row = new String[channels.size() + 1];
        row[0] = dateFormatMillisecond.format(data.getTimestamp());
        boolean empty = true;
        
        for (int i = 0; i < channels.size(); i++) {
            ChannelConfig config = channels.get(i);
            Channel<?> channel = data.getChannels().getById(config.getId());
            row[i + 1] = channel != null ? channel.getStringValue() : "";
            if ((channel != null) && (channel.getStringValue() != null)) {
                empty = false;
            }
        }
        if (empty) {
            return null;
        }
        
        return row;
    }
    
    @Override
    public String getSourceNumber(String vehicleId, String fleetId) {
        return cdProviders.get(fleetId).getVehicleNumberById(vehicleId);       
    }
}