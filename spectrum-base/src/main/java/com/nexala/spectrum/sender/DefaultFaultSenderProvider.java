package com.nexala.spectrum.sender;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.FaultTransmission;
import com.nexala.spectrum.db.dao.FaultTransmissionDao;

public class DefaultFaultSenderProvider implements FaultSenderProvider {

    @Inject
    private FaultTransmissionDao faultTransmissionDao;

    @Inject
    private ConfigurationManager confManager;

    @Inject
    public DefaultFaultSenderProvider(FaultTransmissionDao faultTransmissionDao) {
        this.faultTransmissionDao = faultTransmissionDao;
    }

    @Override
    public List<FaultTransmission> searchData() {
        return faultTransmissionDao.searchData();
    }

    @Override
    public void updateFaultTransmission(Long id, String response, boolean successful) {
        int maxRetries = confManager.getConfAsInteger(Spectrum.SPECTRUM_FAULT_SENDER_MAX_RETRIES, 10);
        faultTransmissionDao.updateFaultTransmission(id, response, successful, maxRetries);
    }
}
