package com.nexala.spectrum.sender;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.FaultTransmission;

@ImplementedBy(DefaultFaultSenderProvider.class)
public interface FaultSenderProvider {

    /**
     * Returns a list of faults to be transmitted
     */
    public List<FaultTransmission> searchData();

    /**
     * Update status of transmission in FaultTransmission table
     */
    public void updateFaultTransmission(Long id, String response, boolean successful);
}
