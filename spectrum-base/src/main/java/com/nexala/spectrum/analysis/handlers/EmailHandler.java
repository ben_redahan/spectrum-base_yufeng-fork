/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.analysis.handlers;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.mail.EmailClient;
import com.nexala.spectrum.mail.EmailProvider;
import com.nexala.spectrum.mail.EmailUtilities;
import com.nexala.spectrum.mail.beans.Email;
import com.nexala.spectrum.mail.beans.Recipient;
import com.trimble.rail.security.client.HttpClient;
import com.trimble.rail.security.core.api.User;
import com.trimble.rail.security.core.api.UserManagement;

@Singleton
public class EmailHandler {
    private static final Logger LOGGER = Logger.getLogger(EmailHandler.class);

    private final TimeZone timeZone;
    private final SimpleDateFormat dateFormat;
    private final EmailClient client;
    private final EmailProvider provider;

    @Inject
    private EmailConfiguration emailConfiguration;

    @Inject
    private UserManagement userManagement;

    private ConfigurationManager confManager;

    @Inject
    public EmailHandler(ConfigurationManager confManager, HttpClient httpClient) {

        String tz = confManager.getConfAsString(Spectrum.EMAIL_TIMEZONE, Spectrum.EMAIL_DEFAULT_TIMEZONE);
        String df = confManager.getConfAsString(Spectrum.EMAIL_DATE_FORMAT, Spectrum.EMAIL_DEFAULT_DATE_FORMAT);
        String mailHost = confManager.getConfAsString(Spectrum.MAIL_HOSTNAME, Spectrum.EMAIL_DEFAULT_MAIL_HOSTNAME);
        String mailPort = confManager.getConfAsString(Spectrum.MAIL_PORT, Spectrum.EMAIL_DEFAULT_MAIL_PORT);
        Boolean starttls = confManager.getConfAsBoolean(Spectrum.MAIL_STARTTLS, Spectrum.EMAIL_DEFAULT_STARTTLS);
        String username = confManager.getConfAsString(Spectrum.MAIL_USERNAME, null);
        String password = confManager.getConfAsString(Spectrum.MAIL_PASSWORD, null);
        String protocol = confManager.getConfAsString(Spectrum.EMAIL_URL_PROTOCOL, Spectrum.EMAIL_DEFAULT_URL_PROTOCOL);
        String hostname = confManager.getConfAsString(Spectrum.EMAIL_URL_HOSTNAME, Spectrum.EMAIL_DEFAULT_URL_HOSTNAME);
        int port = confManager.getConfAsInteger(Spectrum.EMAIL_URL_PORT, Spectrum.EMAIL_DEFAULT_URL_PORT);
        String path = confManager.getConfAsString(Spectrum.EMAIL_URL_PATH, Spectrum.EMAIL_DEFAULT_URL_PATH);
        String query = confManager.getConfAsString(Spectrum.EMAIL_URL_QUERY, Spectrum.EMAIL_DEFAULT_URL_QUERY);
        provider = new EmailProvider(httpClient, protocol, hostname, port, path, query);

        LOGGER.info(String.format(
                "E-mail configured: %s = %s; %s = %s; " + "%s = %s; %s = %s; %s = %s; %s = %s; %s = %s; %s = %s; "
                        + "%s = %s",
                Spectrum.EMAIL_URL_PROTOCOL, protocol, Spectrum.EMAIL_URL_HOSTNAME, hostname, Spectrum.EMAIL_URL_PORT,
                port, Spectrum.EMAIL_URL_PATH, path, Spectrum.EMAIL_URL_QUERY, query, Spectrum.MAIL_HOSTNAME, mailHost,
                Spectrum.MAIL_PORT, mailPort, Spectrum.EMAIL_TIMEZONE, TimeZone.getTimeZone(tz).getID(),
                Spectrum.EMAIL_DATE_FORMAT, df));

        timeZone = TimeZone.getTimeZone(tz);

        SimpleDateFormat sdf;
        try {
            sdf = new SimpleDateFormat(df);
        } catch (IllegalArgumentException e) {
            LOGGER.warn(String.format("Invalid e-mail date format (%s), " + "using default date format (%s)", df,
                    Spectrum.EMAIL_DEFAULT_DATE_FORMAT));
            sdf = new SimpleDateFormat(Spectrum.EMAIL_DEFAULT_DATE_FORMAT);
        }
        sdf.setTimeZone(timeZone);
        dateFormat = sdf;

        Properties properties = new Properties();
        properties.setProperty(Spectrum.MAIL_HOSTNAME, mailHost);
        properties.setProperty(Spectrum.MAIL_PORT, mailPort);
        
        if (starttls) {
            properties.put("mail.transport.protocol", "smtps");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.starttls.required", "true");
        }
        
        if (username != null) {
            properties.setProperty("mail.smtp.auth", "true");
            
            Authenticator auth = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            };
            client = new EmailClient(properties, auth);
        } else {
            client = new EmailClient(properties);
        }
        
        this.confManager = confManager;

    }

    public void sendEmail(String emailPath, com.nexala.spectrum.rest.data.beans.Event event) {

        // Check if the emails are enabled for the rule engine
        Boolean emailsEnabled = confManager.getConfAsBoolean(Spectrum.SPECTRUM_RULE_ENGINE_EMAILS_ENABLE, true);
        if (!emailsEnabled) {
            LOGGER.debug("emails disabled for rule engine");
            return;
        }

        String template = provider.loadEmailTemplate(emailPath);
        if (template == null) {
            LOGGER.warn(String.format("E-mail template %s not found", emailPath));
            return;
        }

        Email email = EmailUtilities.emailFromXml(template);
        if (email == null) {
            LOGGER.warn("Failed to parse e-mail template");
        }
        
        String emailSender = confManager.getConfAsString(Spectrum.MAIL_EMAILSENDER);
        if (emailSender != null) {
            email.setFrom(emailSender);
        }
        
        Set<String> recipients = new HashSet<String>();
        for (Recipient recipient : email.getRecipients()) {
            switch (recipient.getType()) {
            case ROLE: {
                String roleName = recipient.getName();

                List<User> users = userManagement.getUsersByRole(roleName);
                for (User user : users) {
                    recipients.add(user.getEmail());
                }
                break;
            }
            case USER: {
                String userName = recipient.getName();
                User user = userManagement.getUserByUsername(userName);
                if (user == null) {
                    LOGGER.warn(String.format("Failed to find user %s", userName));
                    continue;
                }
                recipients.add(user.getEmail());

            }
            }
        }

        Map<String, String> variables = getVariableMap(event);

        String subject = EmailUtilities.applyTemplating(email.getSubject(), variables);
        String body = EmailUtilities.applyTemplating(email.getBody(), variables);

        client.sendEmail(email.getFrom(), subject, body, recipients.toArray(new String[recipients.size()]));
    }

    private Map<String, String> getVariableMap(com.nexala.spectrum.rest.data.beans.Event event) {
        Map<String, String> variableMap = emailConfiguration.getVariableMap(event, dateFormat);

        return variableMap;
    }
}
