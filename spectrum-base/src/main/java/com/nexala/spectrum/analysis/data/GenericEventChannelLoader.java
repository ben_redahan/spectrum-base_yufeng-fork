//*** static header file ***
package com.nexala.spectrum.analysis.data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.SpectrumProperties;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelDouble;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.rest.data.beans.DataSet;

/**
 * Load the channels collections into {@link GenericEvent}
 * @author BBaudry
 *
 */
@Singleton
public class GenericEventChannelLoader implements EventChannelDataLoader<GenericEvent> {
    private ChannelManager cm;

    @Inject
    public GenericEventChannelLoader(SpectrumProperties properties, ChannelManager cm) {
        this.cm = cm;
    }

    /**
     * @see com.nexala.spectrum.analysis.data.EventChannelDataLoader#loadDataSet(com.nexala.spectrum.analysis.AnalysisData, com.nexala.spectrum.rest.data.beans.DataSet)
     */
    public GenericEvent loadDataSet(GenericEvent vehicle, DataSet data) {
        return loadDataSet(vehicle, data, null);
    }
    

    /**
     * Load the data into the
     * @param vehicle
     * @param data
     * @param source
     * @return
     */
    public GenericEvent loadDataSet(GenericEvent vehicle, DataSet data, String source) {
        if (data == null) {
            return null;
        }

        ChannelCollection cc = data.getChannels();

        for (Channel<?> current : cc.getChannels().values()) {
            String name;
            int id = current.getId();
            if (id > 0) {
                name = "ch" + current.getId(); // FIXME remove this stupid ch from everywhere
            } else {
                // id < 0, it's an extra channel
                name = current.getName();
            }

            Class<?> channelType = cm.getChannelType(name);
            int pos = cm.getChannelPosition(source, name);
            if (channelType != null && pos >= 0) {
                // If the channel is part of the channel manager config, we copy the data.
                if (Boolean.TYPE.equals(channelType)) {
                    ChannelBoolean cb = (ChannelBoolean) current;
                    if (cb.getValue() != null) {
                        vehicle.put(pos, cb.getValue());
                    }
                } else if (Integer.TYPE.equals(channelType)) {
                    ChannelInt ci = (ChannelInt) current;
                    if (ci.getValue() != null) {
                        vehicle.put(pos, ci.getValue());
                    }

                } else if (Double.TYPE.equals(channelType)) {
                    ChannelDouble cd = (ChannelDouble) current;
                    if (cd.getValue() != null) {
                        vehicle.put(pos, cd.getValue());
                    }

                } else if (String.class.equals(channelType)) {
                    vehicle.put(pos, current.getStringValue());

                } else if (Long.TYPE.equals(channelType)) {
                    ChannelLong cl = (ChannelLong) current;
                    if (cl.getValue() != null) {
                        vehicle.put(pos, cl.getValue());
                    }
                }
            }

        }

        return vehicle;
        
    }
    

    /**
     * Load all the datasets as generic events. <br>
     * WARNING! this method assert that all the Datasets have the same list of
     * channels.
     * @param data the data to load
     * @return the generic events
     */
    public Map<Integer, List<GenericEvent>> loadDataSets(List<? extends DataSet> data) {
        List<GenericEvent> geList = new ArrayList<>();
        List<DataSet> datasets = new ArrayList<>(data);
        if (data.size() == 0) {
            return new HashMap<>();
        }

        // Create all the Generic events
        for (DataSet d : data) {
            geList.add(cm.getEventInstance(d.getTimestamp()));
        }

        //Get the first dataset to get the channel names
        DataSet first = datasets.get(0);
        ChannelCollection cc = first.getChannels();

        // Iterate over all the channels to populate the GenericEvents
        for (Entry<String, Channel<?>> entry : cc.getChannels().entrySet()) {
            Channel<?> current = entry.getValue();
            String key = entry.getKey();
            String name;
            int id = current.getId();
            if (id > 0) {
                name = "ch" + current.getId(); // FIXME remove this stupid ch from everywhere
            } else {
                // id < 0, it's an extra channel
                name = current.getName();
            }

            Class<?> channelType = cm.getChannelType(name);
            int pos = cm.getChannelPosition(null, name);
            if (channelType != null && pos >= 0) {
                // If the channel is part of the channel manager config, we copy the data.
                if (Boolean.TYPE.equals(channelType)) {
                    for (int i = 0; i < datasets.size(); i++) {
                        DataSet ds = datasets.get(i);
                        GenericEvent ge = geList.get(i);
                        
                        ChannelBoolean cb = (ChannelBoolean) ds.getChannels().getByName(key);
                        if (cb.getValue() != null) {
                            ge.put(pos, cb.getValue());
                        }
                    }
                } else if (Integer.TYPE.equals(channelType)) {
                    for (int i = 0; i < datasets.size(); i++) {
                        DataSet ds = datasets.get(i);
                        GenericEvent ge = geList.get(i);
                        
                        ChannelInt cb = (ChannelInt) ds.getChannels().getByName(key);
                        if (cb.getValue() != null) {
                            ge.put(pos, cb.getValue());
                        }
                    }
                } else if (Double.TYPE.equals(channelType)) {
                    for (int i = 0; i < datasets.size(); i++) {
                        DataSet ds = datasets.get(i);
                        GenericEvent ge = geList.get(i);
                        
                        ChannelDouble cb = (ChannelDouble) ds.getChannels().getByName(key);
                        if (cb.getValue() != null) {
                            ge.put(pos, cb.getValue());
                        }
                    }

                } else if (String.class.equals(channelType)) {
                    for (int i = 0; i < datasets.size(); i++) {
                        DataSet ds = datasets.get(i);
                        GenericEvent ge = geList.get(i);
                        
                        Channel<?> cb = (Channel<?>) ds.getChannels().getByName(key);
                        if (cb.getValue() != null) {
                            ge.put(pos, cb.getStringValue());
                        }
                    }

                } else if (Long.TYPE.equals(channelType)) {
                    for (int i = 0; i < datasets.size(); i++) {
                        DataSet ds = datasets.get(i);
                        GenericEvent ge = geList.get(i);
                        
                        ChannelLong cl = (ChannelLong) ds.getChannels().getByName(key);
                        if (cl.getValue() != null) {
                            ge.put(pos, cl.getValue());
                        }
                    }
                }
            }
        }
        
        return buildDatasetMap(geList, datasets);
    }

    /**
     * Build the map of generic event.
     * Order the data by timestamp and delete the duplicated timestamp for one vehicle.
     * @param geList the list of generic events
     * @param datasets the list of datasets
     * @return the map of generic event by id
     */
    private Map<Integer, List<GenericEvent>> buildDatasetMap(List<GenericEvent> geList,
            List<DataSet> datasets) {
        Map<Integer, List<GenericEvent>> result = new HashMap<>();
        // Build the map
        for (int i = 0; i < datasets.size(); i++) {
            DataSet ds = datasets.get(i);
            GenericEvent ge = geList.get(i);
            Integer sourceId = ds.getSourceId();
            
            List<GenericEvent> list = result.get(sourceId);
            if (list == null) {
               list = new ArrayList<>();
               result.put(sourceId, list);
            }
            
            list.add(ge);
        }
        
        TreeSet<GenericEvent> treeset = new TreeSet<>(new Comparator<GenericEvent>() {
            @Override
            public int compare(GenericEvent o1, GenericEvent o2) {
                return Long.compare(o1.getTimestamp(), o2.getTimestamp());
            }
        });
        
        //Order the events and remove duplicated
        for (Entry<Integer, List<GenericEvent>> entry :result.entrySet()) {
            treeset.clear();
            treeset.addAll(entry.getValue());
            result.put(entry.getKey(), new ArrayList<>(treeset));
        }
        
        return result;
    }
}
