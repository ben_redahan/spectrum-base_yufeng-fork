/*
s * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.web.service;

import java.util.List;

import com.nexala.spectrum.analysis.AnalysisData;
import com.nexala.spectrum.analysis.EventSource;

/**
 * Provides a project specific {@link AnalysisEngine} in order to test rules
 * in the editor. As well as methods to get the channel data for testing and
 * the available stock to test.
 * 
 * @author poriordan
 * @param <T>
 */
public interface RuleTestDataProvider<T extends AnalysisData> {

    
    /**
     * This method returns a list of available stock, e.g. Vehicles / Units, etc
     * in the system [for testing].
     */
    public abstract List<EventSource> getTestTargets();
}
