/**
 * 
 */
package com.nexala.spectrum.exportpdf;

/**
 * A configuration for the Pdf export.
 * @author brice
 *
 */
public class PdfExportConfiguration {
    
    private String title;
    
    private String fileName;
    
    private String fontSize;

    /**
     * Get the name.
     * @return the name
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the name.
     * @param name the name to set
     */
    public void setTitle(String name) {
        this.title = name;
    }

    /**
     * Get the fileName.
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Set the fileName.
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Get the fontSize.
     * @return the fontSize
     */
    public String getFontSize() {
        return fontSize;
    }

    /**
     * Set the fontSize.
     * @param fontSize the fontSize to set
     */
    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

}
