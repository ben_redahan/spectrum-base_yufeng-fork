/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.List;

import com.google.inject.ImplementedBy;

@ImplementedBy(DefaultCalculatedChannelConfiguration.class)
public interface CalculatedChannelConfiguration {
    public List<CalculatedChannel> getCalculatedChannels();
}
