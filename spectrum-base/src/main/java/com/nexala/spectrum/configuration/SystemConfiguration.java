package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.rest.data.SystemConfigProvider;

import net.sf.ehcache.CacheManager;

public class SystemConfiguration {

	@Inject
	private CacheManager cacheManager;
	
	@Inject 
	private Map<String, SystemConfigProvider> systemConfigProviderMap;
	
	public static String DATABASE_NAMES = "databaseNames";
	
	public SystemConfiguration() {
		//loadDatabaseNames();
	}
	
	public List<String> getDatabaseNames() {
		
		ArrayList<String> names = new ArrayList();
		
		//FIXME: Use Google Guava lib to replace EhCache lib in this case
		//if (cacheManager.cacheExists(DATABASE_NAMES)) {
			//return (List<String>)cacheManager.get("")
		//}
		
		//FIXME: Only load the values in the Class construction
		return loadDatabaseNames();
	}
	
	private List<String> loadDatabaseNames() {
		List<String> databaseNames = new ArrayList<String>();
		for (SystemConfigProvider systemConfigProvider : systemConfigProviderMap.values()) {
			String databaseName = systemConfigProvider.getDatabaseName();
			if (databaseName != null && 
				!databaseName.isEmpty()) {
				databaseNames.add(databaseName);
			}
		}
		return databaseNames;
	}
}
