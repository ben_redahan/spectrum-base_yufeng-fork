/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.nexala.spectrum.db.beans.ChannelConfig;

public class CalculatedChannel {
    private final ChannelConfig config;
    private final ChannelCalculationStrategy ccs;
    private final ChannelConfig[] dependencies;

    public CalculatedChannel(ChannelCalculationStrategy ccs,
            ChannelConfig config, ChannelConfig... dependencies) {

        if (config == null) {
            throw new NullPointerException("config may not be null");
        }

        if (ccs == null) {
            throw new NullPointerException(
                    "calculation strategy may not be null");
        }

        this.config = config;
        this.ccs = ccs;

        if (dependencies != null) {
            List<ChannelConfig> lstConfig = new ArrayList<ChannelConfig>(Arrays.asList(dependencies));
            lstConfig.removeAll(Collections.singleton(null));

            this.dependencies = lstConfig.toArray(new ChannelConfig[lstConfig.size()]);
        } else {
            this.dependencies = dependencies;
        }
    }

    public ChannelCalculationStrategy getStrategy() {
        return ccs;
    }

    public ChannelConfig getChannel() {
        return config;
    }

    public ChannelConfig[] getDependencies() {
        return dependencies;
    }
}