/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.ChannelVehicleTypeConfig;

public class ChannelConfigurationBean {
    private final List<ChannelConfig> channelConfigList;

    private final List<ChannelGroupConfig> channelGroupConfigList;
    
    private final List<ChannelVehicleTypeConfig> channelVehicleTypeConfigList;

    /** Maps a channel id to an index in the channel config list */
    private final Map<Integer, Integer> idToChannelMap;

    /** Maps a channel name to an index in the channel config list */
    private final Map<String, Integer> nameToChannelMap;
    
    /** Maps a channel name to an index in the channel config list */
    private final Map<Integer, List<Integer>> channelIdToVehicleTypeMap;

    /**
     * Maps a short channel name to an index in the channel config list, e.g. C1
     * or COL1 to 4
     */
    private final Map<String, Integer> shortNameToChannelMap;

    @Inject
    public ChannelConfigurationBean(
            @Assisted("channelConfigList") List<ChannelConfig> channelConfigList,
            @Assisted("channelGroupConfigList") List<ChannelGroupConfig> channelGroupConfigList,
            @Assisted("channelVehicleTypeConfigList") List<ChannelVehicleTypeConfig> channelVehicleTypeConfigList,
            @Assisted("idToChannelMap") Map<Integer, Integer> idToChannelMap,
            @Assisted("nameToChannelMap") Map<String, Integer> nameToChannelMap,
            @Assisted("shortNameToChannelMap") Map<String, Integer> shortNameToChannelMap,
            @Assisted("channelIdToVehicleTypelMap") Map<Integer, List<Integer>> channelIdToVehicleTypeMap) {

        this.channelConfigList = channelConfigList;
        this.channelGroupConfigList = channelGroupConfigList;
        this.channelVehicleTypeConfigList = channelVehicleTypeConfigList;
        this.idToChannelMap = idToChannelMap;
        this.nameToChannelMap = nameToChannelMap;
        this.shortNameToChannelMap = shortNameToChannelMap;
        this.channelIdToVehicleTypeMap = channelIdToVehicleTypeMap;
    }

    public List<ChannelConfig> getChannelConfigList() {
        return channelConfigList;
    }

    public List<ChannelGroupConfig> getChannelGroupConfigList() {
        return channelGroupConfigList;
    }
    
    public List<ChannelVehicleTypeConfig> getChannelVehicleTypeConfigList() {
        return channelVehicleTypeConfigList;
    }

    public Map<Integer, Integer> getIdToChannelMap() {
        return idToChannelMap;
    }

    public Map<String, Integer> getNameToChannelMap() {
        return nameToChannelMap;
    };

    public Map<String, Integer> getShortNameToChannelMap() {
        return shortNameToChannelMap;
    }
    
    public Map<Integer, List<Integer>> getChannelIdToVehicleTypeMap() {
        return channelIdToVehicleTypeMap;
    }
}