/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.List;
import java.util.Map;

import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.ChannelVehicleTypeConfig;

public interface ChannelConfigurationBeanFactory {
    ChannelConfigurationBean create(
            @Assisted("channelConfigList") List<ChannelConfig> channelConfigList,
            @Assisted("channelGroupConfigList") List<ChannelGroupConfig> channelGroupConfigList,
            @Assisted("channelVehicleTypeConfigList") List<ChannelVehicleTypeConfig> channelVehicleTypeConfigList,
            @Assisted("idToChannelMap") Map<Integer, Integer> idToChannelMap,
            @Assisted("nameToChannelMap") Map<String, Integer> nameToChannelMap,
            @Assisted("shortNameToChannelMap") Map<String, Integer> shortNameToChannelMap,
            @Assisted("channelIdToVehicleTypelMap") Map<Integer, List<Integer>> channelIdToVehicleTypeMap);
}