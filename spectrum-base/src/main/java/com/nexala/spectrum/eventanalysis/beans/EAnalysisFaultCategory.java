package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisFaultCategory {
    private final int id;
    private final String name;

    public EAnalysisFaultCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
