/*
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisFilterOption {
    private final String id;
    private final String name;

    public EAnalysisFilterOption(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
