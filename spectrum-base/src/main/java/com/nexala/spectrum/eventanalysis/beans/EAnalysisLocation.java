package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisLocation {
    private final int id;
    private final String tiploc;
    private final String name;
    private final String code;

    public EAnalysisLocation(int id, String tiploc, String name, String code) {
        this.id = id;
        this.tiploc = tiploc;
        this.name = name;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public String getTiploc() {
        return tiploc;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
