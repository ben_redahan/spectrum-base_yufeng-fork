package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisFaultType {
    private final int id;
    private final String name;
    private final String color;

    public EAnalysisFaultType(int id, String name, String color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
