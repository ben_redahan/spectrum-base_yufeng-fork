package com.nexala.spectrum.eventanalysis.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultCategory;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultMeta;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFilterOption;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisLocation;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisUnit;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicle;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicleType;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisBeanDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisFaultCategoryDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisFaultMetaDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisFaultTypeDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisLocationDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisUnitDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisVehicleDao;
import com.nexala.spectrum.eventanalysis.dao.EAnalysisVehicleTypeDao;
import com.typesafe.config.Config;

public class DefaultEAnalysisProvider implements EAnalysisProvider {
    private EAnalysisVehicleTypeDao vehicleTypeDao;
    private EAnalysisFaultTypeDao faultTypeDao;
    private EAnalysisFaultCategoryDao faultCategoryDao;
    private Config config;

    @Inject
    private EAnalysisFaultMetaDao faultMetaDao;

    @Inject
    private EAnalysisUnitDao eanalysisUnitDao;

    @Inject
    private EAnalysisVehicleDao vehicleDao;

    @Inject
    private EAnalysisLocationDao locationDao;

    @Inject
    private EAnalysisBeanDao beanDao;
    
    @Inject
    private UnitDao unitDao;

    @Inject
    public DefaultEAnalysisProvider(EAnalysisVehicleTypeDao vehicleTypeDao,
            EAnalysisFaultTypeDao faultTypeDao,
            EAnalysisFaultCategoryDao faultCategoryDao,
            ApplicationConfiguration applicationConfiguration) {
        this.vehicleTypeDao = vehicleTypeDao;
        this.faultTypeDao = faultTypeDao;
        this.faultCategoryDao = faultCategoryDao;
        this.config = applicationConfiguration.get();
    }

    @Override
    public List<EAnalysisFilterOption> getFilterOptions(String filterName) {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();

        // default spectrum base filters
        if (EAnalysisConstants.FILTER_UNIT.equals(filterName)) {
            result = getUnitFilterOptions();
        } else if (EAnalysisConstants.FILTER_VEHICLE_TYPE.equals(filterName)) {
            result = getVehicleTypeFilterOptions();
        } else if (EAnalysisConstants.FILTER_VEHICLE.equals(filterName)) {
            result = getVehicleFilterOptions();
        } else if (EAnalysisConstants.FILTER_FAULT_TYPE.equals(filterName)) {
            result = getFaultTypeFilterOptions();
        } else if (EAnalysisConstants.FILTER_FAULT_CATEGORY.equals(filterName)) {
            result = getFaultCategoryOptions();
        } else if (EAnalysisConstants.FILTER_FAULT_CODE.equals(filterName) ||
                EAnalysisConstants.FILTER_FAULT_META.equals(filterName)) {
            result = getFaultMetaFilterOptions();
        } else if (EAnalysisConstants.FILTER_LOCATION.equals(filterName)) {
            result = getLocationFilterOptions();
        } else if (EAnalysisConstants.FILTER_UNIT_TYPE.equals(filterName)) {
            result = getUnitTypeFilterOptions();
        } else if (EAnalysisConstants.FILTER_SERVICE_REQUEST.equals(filterName)) {
        	result = getBooleanDropdownOptions();
        } else if (EAnalysisConstants.FILTER_ACKNOWLEDGED.equals(filterName)) {
            result = getBooleanDropdownOptions();
        } else if (EAnalysisConstants.FILTER_HAS_RECOVERY.equals(filterName)) {
            result = getBooleanDropdownOptions();
        } else {
            // possible extra project-specific fields
            result = getExtraFilterOptions(filterName);
        }
        
        return result;
    }

    protected List<EAnalysisFilterOption> getVehicleTypeFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisVehicleType> items = getVehicleTypeList();
        for (EAnalysisVehicleType it : items) {
            result.add(new EAnalysisFilterOption(it.getId(), it.getName()));
        }
        return result;
    }

    protected List<EAnalysisFilterOption> getFaultTypeFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisFaultType> items = getFaultTypeList();
        for (EAnalysisFaultType it : items) {
            result.add(new EAnalysisFilterOption(String.valueOf(it.getId()), it
                    .getName()));
        }
        return result;
    }

    protected List<EAnalysisFilterOption> getFaultCategoryOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisFaultCategory> items = getFaultCategoryList();
        for (EAnalysisFaultCategory it : items) {
            result.add(new EAnalysisFilterOption(String.valueOf(it.getId()), it
                    .getName()));
        }
        return result;
    }

    protected List<EAnalysisFilterOption> getUnitFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisUnit> units = eanalysisUnitDao.allUnits();
        for (EAnalysisUnit unit : units) {
            result.add(new EAnalysisFilterOption(String.valueOf(unit.getFleetCode() + "-" + unit.getId()),
                    unit.getNumber()));
        }
        return result;
    }

    protected List<EAnalysisFilterOption> getVehicleFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisVehicle> vehicles = vehicleDao.allVehicles();
        for (EAnalysisVehicle vehicle : vehicles) {
            result.add(new EAnalysisFilterOption(String.valueOf(vehicle.getFleetCode() + "-" + vehicle.getId()),
                    vehicle.getNumber()));
        }
        return result;
    }

    protected List<EAnalysisFilterOption> getFaultMetaFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisFaultMeta> faultMeta = faultMetaDao.allFaultMeta();
        for (EAnalysisFaultMeta fm : faultMeta) {
            result.add(new EAnalysisFilterOption(String.valueOf(fm.getId()),
                    fm.getCode() + " - " + fm.getDescription()));
        }
        return result;
    }

    protected List<EAnalysisFilterOption> getLocationFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<EAnalysisLocation> locations = locationDao.allLocations();
        for (EAnalysisLocation location : locations) {
            result.add(new EAnalysisFilterOption(String.valueOf(location.getId()),
                    location.getCode() + " - " + location.getName()));
        }
        return result;
    }
    
    protected List<EAnalysisFilterOption> getUnitTypeFilterOptions() {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        List<String> unitTypeList = unitDao.getUnitTypes();
        for(String unitType : unitTypeList){
            result.add(new EAnalysisFilterOption(unitType, unitType));
        }
        return result;
    }
    
    protected List<EAnalysisFilterOption> getBooleanDropdownOptions() {
    	List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
    	
        result.add(new EAnalysisFilterOption("1", "Yes"));
        result.add(new EAnalysisFilterOption("0", "No"));
    	
        return result;
    }

    protected List<EAnalysisFilterOption> getExtraFilterOptions(String filterName) {
        List<EAnalysisFilterOption> result = new ArrayList<EAnalysisFilterOption>();
        for (Config filterConfig : config.getConfigList("r2m.eventAnalysis.filters")) {
            if (filterConfig.getString("name").equals(filterName)) {
                if (filterConfig.hasPath("values")) {
                    for (Config filterOption : filterConfig.getConfigList("values")) {
                        result.add(new EAnalysisFilterOption(filterOption.getString("value"),
                                filterOption.getString("label")));
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<EAnalysisVehicleType> getVehicleTypeList() {
        return vehicleTypeDao.getVehicleTypeList();
    }

    @Override
    public List<EAnalysisFaultType> getFaultTypeList() {
        return faultTypeDao.getFaultTypeList();
    }

    @Override
    public List<EAnalysisFaultCategory> getFaultCategoryList() {
        return faultCategoryDao.getFaultCategoryList();
    }

    @Override
    public List<EAnalysisFaultMeta> searchFaultMeta(String keyword,
            TreeMap<String, String> searchParameters) {
        return faultMetaDao.search(keyword, searchParameters);
    }

    @Override
    public List<EAnalysisUnit> searchUnit(String unitNumber) {
        return eanalysisUnitDao.search(unitNumber);
    }

    @Override
    public List<EAnalysisVehicle> searchVehicle(String vehicleType,
            String vehicleNumber, String unitIds) {
        return vehicleDao.search(vehicleType, vehicleNumber, unitIds);
    }

    @Override
    public List<EAnalysisLocation> searchLocation(String locationName) {
        return locationDao.search(locationName);
    }

    @Override
    public List<EAnalysisBean> searchData(String fleetId, Date startDate, Date endDate, TreeMap<String, String> searchParameters) {
        return beanDao.search(fleetId, startDate, endDate, searchParameters);
    }

    @Override
    public List<EAnalysisBean> searchDetailData(String fleetId, Date startDate,
            Date endDate, String grouping, String referenceKey,
            TreeMap<String, String> searchParameters) {
        String tempFleetId = null;
        if (EAnalysisConstants.EMPTY_KEY.equals(referenceKey)) {
            referenceKey = EAnalysisConstants.EMPTY_KEY_VALUE;
        } else if (referenceKey != null) {
            if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_VEHICLE)) {
                String[] keySplit = referenceKey.split("-");
                referenceKey = keySplit[1];
                tempFleetId = keySplit[0];
                searchParameters.put("vehicleId", referenceKey);
            } else if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_UNIT)) {
                String[] keySplit = referenceKey.split("-");
                referenceKey = keySplit[1];
                tempFleetId = keySplit[0];
                searchParameters.put("unitId", referenceKey);
            } else if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_FAULTCODE)) {
                String parameterId = EAnalysisConstants.FILTER_FAULT_META;
                if (config.getBoolean("r2m.eventAnalysis.faultCodeAsReference")) {
                    parameterId = EAnalysisConstants.FILTER_FAULT_CODE;
                }
                searchParameters.put(parameterId, referenceKey);
            } else if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_DATE)) {
                startDate = new Date(Long.parseLong(referenceKey));
                endDate = new Date(Long.parseLong(referenceKey) + 86400000 - 1);
            } else if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_CATEGORY)) {
                searchParameters.put("faultCategoryId", referenceKey);
            } else if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_SET)) {
                String[] keySplit = referenceKey.split("-");
                referenceKey = keySplit[1];
                tempFleetId = keySplit[0];
                searchParameters.put("setcode", referenceKey);
            }
        }
        
        if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_HEADCODE) && referenceKey != null) {
            searchParameters.put("headcode", referenceKey);
        }
        
        if (grouping.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_LOCATION) && referenceKey != null) {
            searchParameters.put("locationId", referenceKey);
        }

        if (tempFleetId == null
                || (tempFleetId != null && tempFleetId.equals(fleetId))) {
            return beanDao.search(fleetId, startDate, endDate, searchParameters);
        }

        return Collections.emptyList();
    }

}
