package com.nexala.spectrum.eventanalysis.conf;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.time.DurationFormatUtils;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisGrouping;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.conf.AnalogueColumnBuilder;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ColumnSorterBean;
import com.nexala.spectrum.view.conf.DigitalColumnBuilder;
import com.nexala.spectrum.view.conf.DurationColumnBuilder;
import com.nexala.spectrum.view.conf.Field;
import com.nexala.spectrum.view.conf.MapConfiguration;
import com.nexala.spectrum.view.conf.TextColumnBuilder;
import com.nexala.spectrum.view.conf.TimestampColumnBuilder;
import com.nexala.spectrum.view.conf.maps.MapLicencedConfiguration;
import com.nexala.spectrum.view.conf.maps.MapMarkerConfiguration;
import com.typesafe.config.Config;

@Singleton
public class EAnalysisConfigurationImpl implements EAnalysisConfiguration {
    private final List<EAnalysisGrouping> groupings;
    private final List<Column> detailColumns;
    private final ApplicationConfiguration config;
    private final List<Field> searchFields;
    private MapConfiguration mapConfiguration;
    
    private final Integer filtersPerRow;

    @Inject
    public EAnalysisConfigurationImpl(ConfigurationManager confManager, ApplicationConfiguration applicationConfiguration,
            MapConfiguration mapConfiguration) {
        this.groupings = getGroupingList(confManager);
        this.detailColumns = getDetailColumnList();
        this.config = applicationConfiguration;
        this.searchFields = getSearchFieldList();
        this.mapConfiguration = mapConfiguration;
        this.filtersPerRow = config.get().getConfig("r2m.eventAnalysis").getInt("filtersPerRow");
    }

    @Override
    public List<EAnalysisGrouping> getGroupings() {
        return groupings;
    }

    @Override
    public List<EAnalysisGrouping> getGroupings(List<EAnalysisFaultType> eaFaultTypes, Licence licence) {
        setGroupingsColumns(eaFaultTypes, licence);
        return groupings;
    }

    @Override
    public List<Column> getDetailColumns() {
        return detailColumns;
    }

    @Override
    public ColumnSorterBean getDetailDefaultSorter() {
        return new ColumnSorterBean(EAnalysisConstants.COLUMN_CREATE_TIME, true);
    }
    
    @Override
    public List<Field> getSearchFields() {
        return searchFields;
    }

    private List<EAnalysisGrouping> getGroupingList(ConfigurationManager confManager) {
        List<EAnalysisGrouping> eaGroupings = new ArrayList<EAnalysisGrouping>();

        String groupings = confManager.getConfAsString(EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS);
        if (groupings != null) {
            for (String groupingIndex : groupings.split(",")) {
                String groupingCode = confManager.getConfAsString(EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS + "."
                        + groupingIndex.trim() + "." + EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS_CODE);

                if (!isValidGrouping(groupingCode)) {
                    continue;
                }

                String groupingLabel = confManager.getConfAsString(EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS + "."
                        + groupingIndex.trim() + "." + EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS_LABEL);

                String groupingParams = confManager.getConfAsString(EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS + "."
                        + groupingIndex.trim() + "." + EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS_PARAMS);

                String groupingContentSize = confManager.getConfAsString(EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS + "."
                        + groupingIndex.trim() + "."
                        + EAnalysisConstants.PROPERTY_EVENT_ANALYSIS_GROUPINGS_CONTENT_SIZE);

                eaGroupings.add(new EAnalysisGrouping(groupingCode, groupingLabel, groupingParams, groupingContentSize));
            }
        }

        return eaGroupings;
    }

    private boolean isValidGrouping(String groupingCode) {
        if (groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_VEHICLE)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_UNIT)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_FAULTCODE)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_LOCATION)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_DATE)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_HEADCODE)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_CATEGORY)
                || groupingCode.equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_SET)) {

            return true;
        }

        return false;
    }

    protected List<Column> getDetailColumnList() {
        return Lists
                .newArrayList(new Column[] {
                        new TextColumnBuilder().displayName("").name(EAnalysisConstants.COLUMN_FAULT_TYPE_COLOR)
                                .format("<div class='eventIcon' align='center' style='background: {string}'></div>")
                                .sortable(true).sortByColumn(EAnalysisConstants.COLUMN_FAULT_TYPE_PRIORITY).width(14).build(),
                        new TimestampColumnBuilder().displayName(EAnalysisConstants.LABEL_CREATE_TIME)
                                .name(EAnalysisConstants.COLUMN_CREATE_TIME).format("{date}").width(125).sortable(true)
                                .build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_HEADCODE)
                                .name(EAnalysisConstants.COLUMN_HEADCODE).width(40).sortable(true).build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_UNIT)
                                .name(EAnalysisConstants.COLUMN_UNIT_NUMBER).width(40).sortable(true).build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_VEHICLE)
                                .name(EAnalysisConstants.COLUMN_VEHICLE_NUMBER).width(40).sortable(true).build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_LOCATION)
                                .name(EAnalysisConstants.COLUMN_LOCATION_CODE).width(45).sortable(true).build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_CODE)
                                .name(EAnalysisConstants.COLUMN_FAULT_CODE).width(120).sortable(true).build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_CATEGORY)
                                .name(EAnalysisConstants.COLUMN_CATEGORY).width(60).sortable(true).build(),
                        new TextColumnBuilder().displayName(EAnalysisConstants.LABEL_DESCRIPTION)
                                .name(EAnalysisConstants.COLUMN_DESCRIPTION).width(160).sortable(true).build(),
                        new DigitalColumnBuilder().displayName(EAnalysisConstants.LABEL_CURRENT)
                                .name(EAnalysisConstants.COLUMN_CURRENT).width(30).sortable(true)
                                .format("{boolean:Yes|No}").build(),
                        new TextColumnBuilder().name(EAnalysisConstants.COLUMN_ID).visible(false).build(),
                        new TextColumnBuilder().name(EAnalysisConstants.COLUMN_FLEET_ID).visible(false).build(),
                        new TextColumnBuilder().name(EAnalysisConstants.COLUMN_FAULT_TYPE).visible(false).build(),
                        new AnalogueColumnBuilder().name(EAnalysisConstants.COLUMN_FAULT_TYPE_PRIORITY).visible(false).build()});
    }

    private void setGroupingsColumns(List<EAnalysisFaultType> eaFaultTypes, Licence licence) {
        List<Column> counterColumns = new ArrayList<Column>();
        
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(licence.getUserLocale());
        for (EAnalysisFaultType faultType : eaFaultTypes) {
            // Use magic word feature from the i18n js plugin (https://github.com/wikimedia/jquery.i18n#magic-word-support)
            counterColumns.add(new TextColumnBuilder()
                    .displayName(faultType.getName() + " "+bundle.getString("count"))
                    .name(faultType.getName() + EAnalysisConstants.COLUMN_COUNT).width(30).sortable(true).build());
        }

        for (EAnalysisGrouping eaGrouping : groupings) {
            if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_VEHICLE)) {
                setVehicleGroupingDetails(eaGrouping, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_UNIT)) {
                setUnitGroupingDetails(eaGrouping, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_FAULTCODE)) {
                setFaultCodeGroupingDetails(eaGrouping, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_LOCATION)) {
                setLocationGroupingDetails(eaGrouping, licence, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_DATE)) {
                setDateGroupingDetails(eaGrouping, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_HEADCODE)) {
                setHeadcodeGroupingDetails(eaGrouping, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_CATEGORY)) {
                setCategoryGroupingDetails(eaGrouping, bundle);
            } else if (eaGrouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_SET)) {
                setSetcodeGroupingDetails(eaGrouping, bundle);
            } 

            if (eaGrouping.hasCountersColumns()) {
                eaGrouping.getColumns().addAll(counterColumns);
            }

            eaGrouping.getColumns().add(
                    new TextColumnBuilder().displayName(bundle.getString("totalCount"))
                            .name(EAnalysisConstants.COLUMN_TOTAL_COUNT).width(30).sortable(true).build());

            eaGrouping.getColumns().add(
                    new DurationColumnBuilder().displayName(bundle.getString("totalDuration"))
                            .name(EAnalysisConstants.COLUMN_TOTAL_DURATION).width(40).sortable(true).build());

            eaGrouping.getColumns().add(
                    new DurationColumnBuilder().displayName(bundle.getString("averageDuration"))
                            .name(EAnalysisConstants.COLUMN_AVERAGE_DURATION).width(30).sortable(true).build());

            eaGrouping.getColumns().add(
                    new TextColumnBuilder().name(EAnalysisConstants.COLUMN_REFERENCE_KEY).visible(false).build());
        }
    }

    private List<Field> getSearchFieldList() {
        List<Field> result = new ArrayList<Field>();

        List<? extends Config> fields = config.get().getConfigList("r2m.eventAnalysis.filters");
        for (Config field : fields) {
            result.add(config.buildField(field, 123));
        }
        
        return result;
    }

    private void setVehicleGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("vehicle"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(30).sortable(true).build());
    }

    private void setUnitGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("unit"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(30).sortable(true).build());
    }

    private void setFaultCodeGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setCountersColumns(false);

        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("code"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(60).sortable(true).build());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("description"))
                        .name(EAnalysisConstants.COLUMN_DESCRIPTION).width(60).sortable(true).build());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("category"))
                        .name(EAnalysisConstants.COLUMN_CATEGORY).width(30).sortable(true).build());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("type"))
                        .name(EAnalysisConstants.COLUMN_FAULT_TYPE).width(20).sortable(true).build());
    }

    private void setLocationGroupingDetails(EAnalysisGrouping eaGrouping, Licence licence, ResourceBundleUTF8 bundle) {
        eaGrouping.setRendererObject("nx.ea.MapRenderer");

        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("location"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(60).sortable(true).build());

        MapLicencedConfiguration licencedConf = mapConfiguration.getMapLicencedObjects(licence);
        MapMarkerConfiguration routeConfig = mapConfiguration.getMarkerConfiguration(Spectrum.MAP_ROUTE_MARKER);
        MapMarkerConfiguration stationConfig = mapConfiguration.getMarkerConfiguration(Spectrum.MAP_STATION_MARKER);
        eaGrouping.setExtraFields(licencedConf.getRoutes(), licencedConf.getStations(), routeConfig, stationConfig);
    }

    private void setDateGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setDefaultSorter(new ColumnSorterBean(EAnalysisConstants.COLUMN_REFERENCE_VALUE, false));

        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TimestampColumnBuilder().visible(false)
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(30).build());
        eaGrouping.getColumns().add(
                new TimestampColumnBuilder().displayName(bundle.getString("createTime"))
                        .name(EAnalysisConstants.COLUMN_CREATE_TIME).format("{date}").width(30).sortable(true).build());
        
    }

    private void setHeadcodeGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("headcode"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(30).sortable(true).build());
    }

    private void setCategoryGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("category"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(30).sortable(true).build());
    }
    
    private void setSetcodeGroupingDetails(EAnalysisGrouping eaGrouping, ResourceBundleUTF8 bundle) {
        eaGrouping.setColumns(new ArrayList<Column>());

        eaGrouping.getColumns().add(
                new TextColumnBuilder().displayName(bundle.getString("set"))
                        .name(EAnalysisConstants.COLUMN_REFERENCE_VALUE).width(30).sortable(true).build());
    }

    @Override
    public String[] getExportRow(String groupingCode, EAnalysisReport row, boolean hasCountersColumns, TimeZone tz) {
        List<String> result = new ArrayList<String>();
        Map<String, String> descriptors = row.getDescriptors();
        
        String referenceValue = descriptors.get(EAnalysisConstants.COLUMN_REFERENCE_VALUE);
        if (EAnalysisConstants.EVENT_ANALYSIS_GROUPING_DATE.equals(groupingCode) && referenceValue != null 
                && !referenceValue.isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setTimeZone(tz);
            referenceValue = String.valueOf(sdf.format(Long.valueOf(referenceValue)));
        }
        result.add(referenceValue);
        
        if (EAnalysisConstants.EVENT_ANALYSIS_GROUPING_FAULTCODE.equals(groupingCode)) {
            result.add(descriptors.get(EAnalysisConstants.COLUMN_DESCRIPTION));
            result.add(descriptors.get(EAnalysisConstants.COLUMN_CATEGORY));
            result.add(descriptors.get(EAnalysisConstants.COLUMN_FAULT_TYPE));
        }

        if (hasCountersColumns) {
            for (Map.Entry<Integer, Integer> entry : row.getFaultTypeCountMap().entrySet()) {
                result.add(String.valueOf(entry.getValue()));
            }
        }

        result.add(String.valueOf(row.getTotalCount()));
        result.add(String.valueOf(DurationFormatUtils.formatDuration(row.getTotalDuration().longValue(), "HH:mm:ss")));
        result.add(String.valueOf(DurationFormatUtils.formatDuration(row.getAverageDuration().longValue(), "HH:mm:ss")));

        return result.toArray(new String[result.size()]);
    }

    @Override
    public Plugin getParentPlugin() {
        return config.getParentPlugin(Spectrum.EVENT_ANALYSIS_ID);
    }

    @Override
    public String getScreenName() {
        return "Event Analysis";
    }

	@Override
	public List<Plugin> getPlugins() {
        return config.getPlugins(Spectrum.EVENT_ANALYSIS_ID);
	}
	
	@Override
    public Integer getFiltersPerRow() {
        return config.get().getConfig("r2m.eventAnalysis").getInt("filtersPerRow");
    }
}
