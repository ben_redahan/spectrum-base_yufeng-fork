package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBeanBuilder;

public class EAnalysisBeanDao extends GenericDao<EAnalysisBean, String> {
    @Inject
    private QueryManager queryManager;

    private static class EventAnalysisReportBeanMapper extends
            JdbcRowMapper<EAnalysisBean> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public final EAnalysisBean createRow(ResultSet resultSet)
                throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            return new EAnalysisBeanBuilder()
                    .setFleetId(rs.getString("FleetCode"))
                    .setId(rs.getInt("FaultID"))
                    .setCreateTime(rs.getTimestamp("CreateTime"))
                    .setEndTime(rs.getTimestamp("EndTime"))
                    .setCurrent(rs.getBoolean("IsCurrent"))
                    .setHeadcode(rs.getString("HeadCode"))
                    .setSetCode(rs.getString("SetCode"))
                    .setUnitId(rs.getInt("UnitID"))
                    .setUnitNumber(rs.getString("UnitNumber"))
                    .setVehicleId(rs.getInt("FaultVehicleID"))
                    .setVehicleNumber(rs.getString("FaultVehicleNumber"))
                    .setFaultMetaId(rs.getInt("FaultMetaID"))
                    .setFaultCode(rs.getString("FaultCode"))
                    .setDescription(rs.getString("Description"))
                    .setFaultTypeId(rs.getInt("FaultTypeID"))
                    .setFaultType(rs.getString("FaultType"))
                    .setFaultTypeColor(rs.getString("FaultTypeColor"))
                    .setFaultTypePriority(rs.getInt("Priority"))
                    .setCategoryId(rs.getInt("CategoryID"))
                    .setCategory(rs.getString("Category"))
                    .setLatitude(rs.getDouble("Latitude"))
                    .setLongitude(rs.getDouble("Longitude"))
                    .setLocationId(rs.getInt("LocationID"))
                    .setLocationCode(rs.getString("LocationCode")).build();
        }
    }

    @Inject
    private EventAnalysisReportBeanMapper mapper;

    public List<EAnalysisBean> search(String fleetId, Date startDate,
            Date endDate, TreeMap<String, String> searchParameters) {

        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_BEANS);
        query = query.replace("__ADDITIONAL_FIELDS__", params(searchParameters));

        ArrayList<Object> qparams = new ArrayList<Object>();
        qparams.add(fleetId);
        qparams.add(startDate);
        qparams.add(endDate);

        // parameters are sorted by parameter key from tree map
        for (String key : searchParameters.keySet()) {
            String param = searchParameters.get(key);
            if (param != null) {
                // Convert the .* character to % and . character to _ for fault code
                param = param.replaceAll("\\.\\*", "%");
                param = param.replaceAll("\\.", "_");
            }
            
            qparams.add(param);
        }

        return findByStoredProcedure(query, mapper, qparams.toArray());
    }
    
    private String params(Map<String, String> parameters) {
        StringBuilder sb = new StringBuilder();

        for (String key : parameters.keySet()) {
            sb.append(", ?");
        }

        return sb.toString();
    }
}
