package com.nexala.spectrum.eventanalysis.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;

public class EAnalysisReporterProvider {
	
	static ApplicationConfiguration appConf = new ApplicationConfiguration();
	
    public static final EAnalysisReporter CATEGORY = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, String.valueOf(bean.getCategoryId()));

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getCategory());

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };

    public static final EAnalysisReporter DATE = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setTimeZone(tz);
            
            String key = null;
            try {
                key = String.valueOf(sdf.parse(sdf.format(bean.getCreateTime())).getTime());
            } catch (ParseException e) {
            }

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, key != null ? key : EAnalysisConstants.EMPTY_KEY);
            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, key);
            
            descriptors.put(EAnalysisConstants.COLUMN_CREATE_TIME, Long.toString(bean.getCreateTime()));

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.CREATE_TIME_ASC;
        }
    };

    public static final EAnalysisReporter FAULT_META = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();
            if (appConf.get().getBoolean("r2m.eventAnalysis.faultCodeAsReference")) {
                descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, String.valueOf(bean.getFaultCode()));
            } else {
                descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, String.valueOf(bean.getFaultMetaId()));
            }

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getFaultCode());

            descriptors.put(EAnalysisConstants.COLUMN_DESCRIPTION, bean.getDescription());

            descriptors.put(EAnalysisConstants.COLUMN_CATEGORY, bean.getCategory());

            descriptors.put(EAnalysisConstants.COLUMN_FAULT_TYPE, bean.getFaultType());

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };

    public static final EAnalysisReporter HEADCODE = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, bean.getHeadcode() != null ? bean.getHeadcode()
                    : EAnalysisConstants.EMPTY_KEY);

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getHeadcode());

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };

    public static final EAnalysisReporter LOCATION = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY,
                    bean.getLocationId() != null ? String.valueOf(bean.getLocationId()) : EAnalysisConstants.EMPTY_KEY);

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getLocationCode());

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };

    public static final EAnalysisReporter UNIT = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            String key = EAnalysisConstants.EMPTY_KEY;

            if (bean.getUnitId() != null && bean.getFleetId() != null) {
                key = bean.getFleetId() + "-" + bean.getUnitId();
            }

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, key);

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getUnitNumber());

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };

    public static final EAnalysisReporter VEHICLE = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            String key = EAnalysisConstants.EMPTY_KEY;

            if (bean.getVehicleId() != null && bean.getFleetId() != null) {
                key = bean.getFleetId() + "-" + bean.getVehicleId();
            }
            
            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, key);

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getVehicleNumber());

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };
    
    public static final EAnalysisReporter SET = new EAnalysisReporter() {
        @Override
        public Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz) {
            Map<String, String> descriptors = new HashMap<String, String>();

            String key = EAnalysisConstants.EMPTY_KEY;

            if (bean.getSetCode() != null && bean.getFleetId() != null) {
                key = bean.getFleetId() + "-" + bean.getSetCode();
            }

            descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_KEY, key);
            
            if (bean.getSetCode() != null) {
                descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, bean.getSetCode());
            } else {
                descriptors.put(EAnalysisConstants.COLUMN_REFERENCE_VALUE, " ");
            }

            return descriptors;
        }

        @Override
        public Comparator<EAnalysisReport> getReportSortComparator() {
            return EAnalysisReporterComparator.TOTAL_COUNT_DESC;
        }
    };

}
