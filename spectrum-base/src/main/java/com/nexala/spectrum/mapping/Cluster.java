/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.mapping;

public abstract class Cluster<T> {

    private Coordinate coordinate;
    
    public Cluster() {}
    
    public void setCoordinate(Coordinate coordinate) {
        
        if (coordinate == null) {
            throw new IllegalArgumentException();
        }
        
        this.coordinate = coordinate;
    }
    
    public Coordinate getCoordinate() {
        return coordinate;
    }
    
    public Double getLatitude() {
        return coordinate.getLatitude();
    }
    
    public Double getLongitude() {
        return coordinate.getLongitude();
    }

    public abstract void addItem(T clusterItem);
    public abstract void init(T clusterItem);
}