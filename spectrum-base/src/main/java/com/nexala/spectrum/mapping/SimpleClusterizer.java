/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.mapping;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Paul O'Riordan
 * 
 *        Clusters a list of Coordinates (C) into a subset of Clusters (T) using
 *        a simple O(n^2) algorithm, which groups Coordinates based on their
 *        distance from each other.
 */

public final class SimpleClusterizer<T extends Cluster<C>, C extends Coordinate>
        implements Clusterizer<LinkedList<C>, T> {

    private Class<T> clazz;

    /**
     * Requires the cluster class to be passed as a parameter as the type cannot
     * be instantiated from the generic parameter.
     * 
     * @param c
     */
    public SimpleClusterizer(Class<T> c) {
        this.clazz = c;
    }

    /**
     * @param coords
     * @param clusterSize (metres)
     * @return A subset of the coordinates as Cluster objects
     */
    public List<T> cluster(LinkedList<C> coords, double clusterSize) {

        List<T> clusters = new ArrayList<T>();

        C head;

        while ((head = coords.poll()) != null) {

            T cluster;

            // if point does not have coordinates, can not be
            // part of cluster.
            if (head.getLatitude() == null || head.getLongitude() == null) {
                continue;
            }

            cluster = newCluster();
            cluster.init(head);

            Iterator<C> coordIter = coords.iterator();

            while (coordIter.hasNext()) {

                C nxt = coordIter.next();

                if (nxt.getLatitude() == null || nxt.getLongitude() == null) {
                    continue;
                }

                if (Mapping.getGreatCircleDistance(head, nxt) * 1000 < clusterSize) {
                    cluster.addItem(nxt);
                    coordIter.remove();
                }
            }

            clusters.add(cluster);
        }

        return clusters;
    }

    private T newCluster() {
        try {
            return this.clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
