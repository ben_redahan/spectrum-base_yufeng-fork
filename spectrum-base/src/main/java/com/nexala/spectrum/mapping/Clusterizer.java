/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.mapping;

import java.util.List;

public interface Clusterizer<L, T> {
    public List<T> cluster(L coords, double clusterSize);
}