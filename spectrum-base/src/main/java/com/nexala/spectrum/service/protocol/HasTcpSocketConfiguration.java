package com.nexala.spectrum.service.protocol;

public interface HasTcpSocketConfiguration extends HasSocketConfiguration {
    /**
     * The number of milliseconds to keep a client socket inactive before
     * closing it.
     * @return the number of milliseconds to keep a client socket inactive
     *         before closing it.
     */
    int getSocketTimeout();
}
