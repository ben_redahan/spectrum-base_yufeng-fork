package com.nexala.spectrum.service;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;

import org.apache.log4j.Logger;

import com.nexala.spectrum.service.protocol.HasSocketConfiguration;
import com.nexala.spectrum.service.protocol.Protocol;

/**
 * A named service that binds to a socket when started, listens for connections and accepts them while running and
 * closes the socket when stopped.
 * 
 * @author Severinas Monkevicius
 * @param <S> the type of socket this service uses.
 * @param <C> the type of {@link HasSocketConfiguration} used to configure this service.
 */
public abstract class AbstractSocketService<S, C extends HasSocketConfiguration> extends SingleThreadService<C> {
    private final Logger LOG = Logger.getLogger(AbstractSocketService.class);

    private final ThreadFactory threadFactory;
    private ExecutorService executor;

    /**
     * The list of protocols currently being executed or queued for execution. A protocol is added to this list when a
     * client connection is accepted and is removed when the connection is closed. The purpose of this list is to close
     * the client connections when the service needs to stop.
     */
    private final List<Protocol> protocols;

    /**
     * The socket used by this service.
     */
    private S socket;

    /**
     * Creates a new named service that will be configured using the specified configuration.
     * 
     * @param name the name.
     * @param config the configuration.
     */
    public AbstractSocketService(String name, C config) {
        super(name, config);

        this.threadFactory = new NumberedThreadFactory(name());
        this.protocols = new ArrayList<Protocol>(config.getMaxConnectionCount());
    }

    /**
     * Creates and returns a command that accepts connections on the service's socket and forwards them on to a protocol
     * for handling. If the command returns without the service being stopped first, the service's status is set to
     * {@link ServiceStatus#FAILED}.
     */
    @Override
    protected SingleThreadCommand getCommand() {
        return new ServiceWorker();
    }

    /**
     * Opens the socket by calling {@link #openSocket(HasSocketConfiguration)}.
     */
    @Override
    protected void onStart() throws ServiceException {
        C config = config();

        try {
            socket = openSocket(config);
        } catch (IOException e) {
            throw new ServiceException(name() + " - failed to open socket on port " + config.getPort(), e);
        }

        executor = Executors.newFixedThreadPool(config().getMaxConnectionCount(), threadFactory);
    }

    protected abstract S openSocket(C config) throws IOException;

    protected abstract Protocol accept(S socket, C config) throws IOException;

    /**
     * Closes any existing client connections and closes the socket by calling {@link #closeSocket(Object)}.
     */
    @Override
    protected void onStop() {
        executor.shutdown();

        synchronized (protocols) {
            for (Protocol p : protocols) {
                try {
                    p.close();
                } catch (Exception e) {
                    LOG.warn(name() + " - failed to close protocol", e);
                }
            }
            protocols.clear();
        }

        try {
            closeSocket(socket);
        } finally {
            // This ensures the executor is always terminated
            ServiceUtilities.terminateExecutor(executor, config().getDisconnectTimeout());
        }
    }

    protected abstract void closeSocket(S socket);

    /**
     * A service worker listens for and handles incoming client connections. See the description of
     * {@link AbstractSocketService#getCommand()} for more details.
     * 
     * @author Severinas Monkevicius
     */
    private class ServiceWorker implements SingleThreadCommand {
        @Override
        public void run() throws ServiceException {
            while (checkStatus(ServiceStatus.RUNNING)) {
                final Protocol p;

                try {
                    p = accept(socket, config());
                } catch (SocketException e) {
                    if (checkStatus(ServiceStatus.RUNNING)) {
                        throw new ServiceException(name() + " - failed to accept connection", e);
                    }
                    break;
                } catch (IOException e) {
                    LOG.warn(name() + " - failed to accept connection", e);
                    continue;
                }

                addProtocol(p);

                try {
                    executor.execute(new ProtocolWorker(p));
                } catch (RejectedExecutionException e) {
                    LOG.info(name() + " - could not accept protocol worker for execution");
                    removeProtocol(p);
                }
            }
        }
    }

    /**
     * A wrapper for protocols that calls the {@link Protocol#communicate()} method and makes sure the protocol is
     * removed from the list of active protocols after it returns.
     * 
     * @author Severinas Monkevicius
     */
    private class ProtocolWorker implements Runnable {
        private final Protocol protocol;

        public ProtocolWorker(Protocol protocol) {
            this.protocol = protocol;
        }

        @Override
        public void run() {
            try {
                protocol.communicate();
            } catch (Exception e) {
                LOG.warn(name() + " - protocol communication failed", e);
            } finally {
                removeProtocol(protocol);
            }
        }
    }

    private void addProtocol(Protocol protocol) {
        synchronized (protocols) {
            protocols.add(protocol);
            int protocolCount = protocols.size();
            int maxCount = config().getMaxConnectionCount();
            try {
                protocolAdded(protocolCount, maxCount);
            } catch (Exception e) {
                LOG.warn(name() + " - exception while notifying about added protocol", e);
            }
            if (protocolCount > maxCount) {
                LOG.warn(name() + " - maximum connection count (" + maxCount + ") exceeded (" + protocolCount + ")");
            }
        }
    }

    protected void protocolAdded(int protocolCount, int maxProtocolCount) {
    }

    private void removeProtocol(Protocol protocol) {
        synchronized (protocols) {
            protocols.remove(protocol);
            int protocolCount = protocols.size();
            int maxCount = config().getMaxConnectionCount();
            try {
                protocolRemoved(protocolCount, maxCount);
            } catch (Exception e) {
                LOG.warn(name() + " - exception while notifying about removed protocol", e);
            }
            if (protocols.size() == maxCount) {
                LOG.warn(name() + " - maximum connection count (" + maxCount + ") no longer exceeded");
            }
        }
    }

    protected void protocolRemoved(int protocolCount, int maxProtocolCount) {
    }
}
