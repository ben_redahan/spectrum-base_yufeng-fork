package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

/**
 * A base class for socket-based protocols.
 * 
 * @author Severinas Monkevicius
 */
public abstract class TcpProtocol implements Protocol {
    private static final Logger LOG = Logger.getLogger(TcpProtocol.class);

    private final Socket socket;
    private final AtomicBoolean closed;

    /**
     * Creates a new socket protocol instance that communicates using the specified {@link Socket socket}.
     * 
     * @param socket the socket to use.
     */
    public TcpProtocol(Socket socket) {
        this.socket = socket;
        this.closed = new AtomicBoolean();
    }

    /**
     * The {@code communicate(Socket)} method is where protocol implementations do their work. To reduce the amount of
     * boilerplate code, the call to {@code communicate(Socket)} is wrapped in the appropriate IO exception handlers.
     * Only streams retrieved from the socket should be closed by implementations (the socket itself is closed
     * automatically).
     * 
     * @param socket the socket to use for communication.
     * @throws IOException if an IO error occurs.
     */
    protected abstract void communicate(Socket socket) throws IOException;

    /**
     * This method wraps the actual protocol implementation in the appropriate IO exception handlers, including a
     * special case for socket exceptions that can identify whether the socket was closed unexpectedly.
     */
    @Override
    public void communicate() {
        InetAddress ia = socket.getInetAddress();
        String a = ia == null ? "unknown" : ia.getHostAddress();
        int p = socket.getPort();

        LOG.debug(String.format("Established connection (%s:%d)", a, p));
        try {
            communicate(socket);
        } catch (SocketTimeoutException e) {
            LOG.warn(String.format("Connection timed out (%s:%d)", a, p));
        } catch (SocketException e) {
            if (!closed.get()) {
                LOG.warn(String.format("Connection failed (%s:%d)", a, p), e);
            }
        } catch (IOException e) {
            LOG.warn(String.format("Connection failed (%s:%d)", a, p), e);
        } finally {
            IOUtils.closeQuietly(socket);
            LOG.debug(String.format("Closed connection (%s:%d)", a, p));
        }
    }

    /**
     * Invoking the {@code close()} method of a {@code SocketProtocol} causes the underlying socket to be closed
     * immediately.
     */
    @Override
    public synchronized void close() {
        closed.set(true);
        IOUtils.closeQuietly(socket);
    }
}
