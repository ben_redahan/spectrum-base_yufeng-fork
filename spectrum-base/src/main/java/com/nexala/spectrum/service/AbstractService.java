package com.nexala.spectrum.service;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

/**
 * A base class for services. Has synchronized service status controls and shutdown hooks to cleanly stop itself if the
 * VM needs to shut down.
 * 
 * @author Severinas Monkevicius
 */
public abstract class AbstractService<C extends ServiceConfig> implements Service<C> {
    private final Logger LOG = Logger.getLogger(AbstractService.class);

    /**
     * Lock used to synchronize access to the service status
     */
    private final ReentrantLock statusLock = new ReentrantLock();

    /**
     * Condition used to wait for the service to finish starting.
     */
    private final Condition notStarting = statusLock.newCondition();

    /**
     * The service's shutdown hook. Instantiated once when the service is created and added / removed from the
     * {@link Runtime runtime} when the service is {@link #start() started} or {@link #stop() stopped}.
     */
    private final Thread shutdownHook;

    private final String name;
    private final C config;
    private ServiceStatus serviceStatus;

    /**
     * Creates a new named service.
     * 
     * @param name the name.
     * @param config the service configuration.
     */
    public AbstractService(String name, C config) {
        this.name = name;
        this.config = config;

        this.shutdownHook = new ServiceShutdownThread(this);
        this.serviceStatus = ServiceStatus.IDLE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String name() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public C config() {
        return config;
    }

    /**
     * {@inheritDoc} If the service starts successfully, a shutdown hook is added that will shutdown the service if the
     * VM is stopped.
     */
    @Override
    public final synchronized void start() {
        if (!config.isEnabled()) {
            return;
        }

        if (!checkAndSetStatus(ServiceStatus.STARTING, ServiceStatus.IDLE, ServiceStatus.FAILED)) {
            return;
        }

        LOG.debug(name + " - starting service");

        ServiceUtilities.removeShutdownHook(shutdownHook);
        ServiceUtilities.addShutdownHook(shutdownHook);

        try {
            doStart();
        } catch (Exception e) {
            ServiceUtilities.removeShutdownHook(shutdownHook);
            setStatus(ServiceStatus.FAILED);
            LOG.error(name + " - failed to start", e);
            return;
        }

        setStatus(ServiceStatus.RUNNING);

        LOG.info(name + " - service started");
    }

    protected abstract void doStart() throws Exception;

    /**
     * {@inheritDoc} After the service is stopped, the shutdown hook added when the service was started is removed.
     */
    @Override
    public final synchronized void stop() {
        if (!checkAndSetStatus(ServiceStatus.STOPPING, ServiceStatus.RUNNING)) {
            return;
        }

        LOG.debug(name + " - stopping service");

        try {
            doStop();
        } catch (Exception e) {
            LOG.warn(name + " - stopped with an exception", e);
        }

        ServiceUtilities.removeShutdownHook(shutdownHook);

        setStatus(ServiceStatus.IDLE);

        LOG.info(name + " - service stopped");
    }

    protected abstract void doStop() throws Exception;

    /**
     * {@inheritDoc}
     */
    @Override
    public final ServiceStatus serviceStatus() {
        statusLock.lock();
        try {
            return serviceStatus;
        } finally {
            statusLock.unlock();
        }
    }

    /**
     * Checks whether the current status of the service is in a list of given statuses.
     * 
     * @param statuses the list of statuses to check the current status against.
     * @return <tt>true</tt> if the current status is in the list of given statuses, <tt>false</tt> otherwise.
     */
    protected final boolean checkStatus(ServiceStatus... statuses) {
        statusLock.lock();
        try {
            for (ServiceStatus s : statuses) {
                if (serviceStatus == s) {
                    return true;
                }
            }

            return false;
        } finally {
            statusLock.unlock();
        }
    }

    /**
     * Checks whether the current status of the service is in a list of given statuses and updates it with a new value
     * if the check passes.
     * 
     * @param serviceStatus the status to set if the check passes.
     * @param statuses the list of statuses to check the current status against.
     * @return <tt>true</tt> if the check passed and the status was updated, <tt>false</tt> otherwise.
     * @see #checkStatus(ServiceStatus...)
     */
    protected final boolean checkAndSetStatus(ServiceStatus serviceStatus, ServiceStatus... statuses) {
        statusLock.lock();
        try {
            if (checkStatus(statuses)) {
                setStatus(serviceStatus);
                return true;
            }

            return false;
        } finally {
            statusLock.unlock();
        }
    }

    /**
     * Sets the status of the service.
     * 
     * @param serviceStatus the status.
     */
    protected final void setStatus(ServiceStatus serviceStatus) {
        statusLock.lock();
        try {
            this.serviceStatus = serviceStatus;
            if (serviceStatus != ServiceStatus.STARTING) {
                notStarting.signalAll();
            }
        } finally {
            statusLock.unlock();
        }
    }

    /**
     * Returns whether the service is running after waiting for it to finish starting if it is doing so at the time of
     * the call.
     * 
     * @return <tt>true</tt> if the status of the service is {@link ServiceStatus#RUNNING}; <tt>false</tt> if it is not.
     */
    protected final boolean isStartedAndRunning() {
        statusLock.lock();
        try {
            while (serviceStatus == ServiceStatus.STARTING) {
                notStarting.await();
            }
            return serviceStatus == ServiceStatus.RUNNING;
        } catch (InterruptedException e) {
            return serviceStatus == ServiceStatus.RUNNING;
        } finally {
            statusLock.unlock();
        }
    }
}
