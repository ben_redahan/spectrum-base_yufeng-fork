package com.nexala.spectrum.service.protocol;

public abstract class AbstractSocketConfiguration implements HasSocketConfiguration {
    @Override
    public boolean isKeepAlive() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public long getInitialDelay() {
        return 0;
    }

    @Override
    public int getDisconnectTimeout() {
        return 30000;
    }

    @Override
    public int getMaxConnectionCount() {
        return 100;
    }
}
