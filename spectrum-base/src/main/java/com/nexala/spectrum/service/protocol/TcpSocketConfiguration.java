package com.nexala.spectrum.service.protocol;

public class TcpSocketConfiguration extends AbstractSocketConfiguration implements HasTcpSocketConfiguration {

    private final int port;
    
    public TcpSocketConfiguration(int port) {
        if (port < 0 || port > 65535) {
            throw new IllegalArgumentException("Invalid port number");
        }
        
        this.port = port;
    }
    
    @Override
    public int getPort() {
        return port;
    }

    @Override
    public boolean isKeepAlive() {
        return true;
    }
    
    @Override
    public int getSocketTimeout() {
        return 10000;
    }
}
