package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

public class SecureSocketProvider implements SocketProvider {
    @Override
    public ServerSocket create(HasTcpSocketConfiguration conf) throws IOException {
        ServerSocketFactory factory = SSLServerSocketFactory.getDefault();
        ServerSocket socket = factory.createServerSocket(conf.getPort());
        return socket;
    }
}
