package com.nexala.spectrum.service.protocol;


public abstract class UdpProtocol implements Protocol {
    protected final byte[] message;
    protected final ClientConfig context;

    public UdpProtocol(byte[] message, ClientConfig context) {
        this.message = message;
        this.context = context;
    }

    protected abstract void communicate(byte[] message, ClientConfig context);

    @Override
    public void communicate() {
        communicate(message, context);
    }

    @Override
    public void close() {
        // NOOP
    }
}
