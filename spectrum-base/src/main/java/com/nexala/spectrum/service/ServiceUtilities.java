package com.nexala.spectrum.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ServiceUtilities {
    public static boolean terminateExecutor(ExecutorService executor, int timeout) {
        boolean completed = true;

        if (!executor.isShutdown()) {
            executor.shutdown();
        }
        if (!executor.isTerminated()) {
            try {
                if (!executor.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
                    completed = false;
                    executor.shutdownNow();
                }
            } catch (InterruptedException e) {
                completed = false;
                executor.shutdownNow();
            }
        }

        return completed;
    }

    public static void addShutdownHook(Thread hook) {
        Runtime.getRuntime().addShutdownHook(hook);
    }

    public static void removeShutdownHook(Thread hook) {
        if (!hook.isAlive()) {
            try {
                Runtime.getRuntime().removeShutdownHook(hook);
            } catch (IllegalStateException e) {
                // Thrown if the VM is shutting down, can be ignored
            }
        }
    }

    public static void startServices(Collection<Service<?>> services) {
        Collection<Thread> threads = new ArrayList<Thread>(services.size());

        for (Service<?> service : services) {
            Thread thread = new ServiceStartupThread(service);
            threads.add(thread);
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                // NOOP
            }
        }
    }

    public static void stopServices(Collection<Service<?>> services) {
        Collection<Thread> threads = new ArrayList<Thread>(services.size());

        for (Service<?> service : services) {
            Thread thread = new ServiceShutdownThread(service);
            threads.add(thread);
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                // NOOP
            }
        }
    }
}
