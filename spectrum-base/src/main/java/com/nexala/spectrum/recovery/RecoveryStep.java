package com.nexala.spectrum.recovery;

import java.util.List;

/**
 * Represents an individual recovery step
 * @author Marcus O'Connell
 *
 */
public class RecoveryStep extends RecoveryVertex {

	private String name;
	private Object stepId;
	private String question;
	private String questionUrl;
	private List<RecoveryAnswer> answers;
	
	public RecoveryStep(Object stepId, String name, String question, String questionUrl, List<RecoveryAnswer> answers) {
		this.stepId = stepId;
		this.name = name;
		this.question = question;
		this.answers = answers;
		this.questionUrl = questionUrl;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getQuestion() {
		
		return this.question;
	}
	
	public String getQuestionUrl() {
		return this.questionUrl;
	}
	
	public Object getStepId() {
		return this.stepId;
	}
	
	public List<RecoveryAnswer> getAnswers() {
		return this.answers;
	}
	
	public String toString() {
		return new String("Step id=" + stepId + ", Question=" + question);
	}
}
