package com.nexala.spectrum.recovery;


public class RecoveryAnswer {

	private Object answerId;
	private String answer;
	private boolean hasDataCheck;	
	private String dataCheck;
	private String action;
	private String actionUrl;
	
	public RecoveryAnswer(Object answerId, String answer, boolean hasDataCheck, String dataCheck, String action, String actionUrl) {
		this.answerId = answerId;
		this.answer = answer;
		this.hasDataCheck = hasDataCheck;
		this.dataCheck = dataCheck;
		this.action = action;
		this.actionUrl = actionUrl;
	}
	
	public Object getAnswerId() {
		return answerId;
	}

	public String getAnswer() {
		return answer;
	}

	public boolean hasDataCheck() {
		return hasDataCheck;
	}

	public String getAction() {
		return action;
	}
	
	public String getActionUrl() {
		return this.actionUrl;
	}
	
	public String getDataCheck() {
	    return this.dataCheck;
	}
	
}
