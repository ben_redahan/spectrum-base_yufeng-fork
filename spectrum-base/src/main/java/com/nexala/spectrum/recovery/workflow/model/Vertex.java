package com.nexala.spectrum.recovery.workflow.model;

import java.util.LinkedList;
import java.util.List;


/**
 * Data structure for a vertex in a directed graph
 * @author Marcus O'Connell
 *
 */
public class Vertex {

	private Object id;
	private Object data;
	private List<Edge> inEdges = new LinkedList<Edge>();
	private List<Edge> outEdges = new LinkedList<Edge>();
	private boolean isStarting = false;
	private boolean isEnding = false;
	
	
	/**
	 * @return the id
	 */
	public Object getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Object id) {
		this.id = id;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}
	
	public List<Edge> getInEdges() {
		return this.inEdges;
	}
	
	public List<Edge> getOutEdges() {
		return this.outEdges;
	}
	
	public void addInEdge( Edge edge) {
		this.inEdges.add(edge);
	}
	
	public void addOutEdge(Edge edge) {
		this.outEdges.add(edge);
	}
	/**
	 * @return the isStarting
	 */
	public boolean isStarting() {
		return isStarting;
	}
	/**
	 * @param isStarting the isStarting to set
	 */
	public void setStarting(boolean isStarting) {
		this.isStarting = isStarting;
	}
	/**
	 * @return the isEnding
	 */
	public boolean isEnding() {
		return isEnding;
	}
	/**
	 * @param isEnding the isEnding to set
	 */
	public void setEnding(boolean isEnding) {
		this.isEnding = isEnding;
	}
	
	

}
