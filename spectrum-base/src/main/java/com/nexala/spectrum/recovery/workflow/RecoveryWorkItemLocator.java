package com.nexala.spectrum.recovery.workflow;

import com.nexala.workflow.WorkItemLocator;

public class RecoveryWorkItemLocator implements WorkItemLocator {
	
	private String key;
	
	public RecoveryWorkItemLocator(String key) {
		this.key = key;
	}

	public String geyKey() {
		return this.key;
	}
	
	public String toString() {
		return this.key;
	}
}
