/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.loader.beans.FileType;

public class FileTypeDao extends GenericDao<FileType, Long> {
    @Inject
    private FileTypeMapper mapper;

    private static final class FileTypeMapper extends JdbcRowMapper<FileType> {
        @Inject
        private BoxedResultSetFactory boxedRSFactory;

        @Inject
        private FileFieldDefinitionDao fieldDao;

        @Override
        public FileType createRow(ResultSet results) throws SQLException {
            BoxedResultSet rs = boxedRSFactory.create(results);

            FileType fileType = new FileType();
            fileType.setId(rs.getInt(Spectrum.ID));
            fileType.setName(rs.getString(Spectrum.NAME));
            fileType.setFileNameFieldSeparator(rs.getString(Spectrum.FILE_NAME_FIELD_SEPARATOR));
            fileType.setFileDataType(rs.getString(Spectrum.FILE_DATA_TYPE));
            fileType.setFieldSeparator(rs.getString(Spectrum.FIELD_SEPARATOR));
            fileType.setTextQualifier(rs.getString(Spectrum.TEXT_QUALIFIER));
            fileType.setProcessor(rs.getString(Spectrum.PROCESSOR));
            fileType.setStoredProcedureName(rs.getString(Spectrum.STORED_PROCEDURE_NAME));
            fileType.setHeaderRowsToSkip(rs.getInt(Spectrum.HEADER_ROWS_TO_SKIP));
            fileType.setUseBatch(rs.getBoolean(Spectrum.USE_BATCH));

            fileType.setFileFields(fieldDao.findByFileType(fileType));

            return fileType;
        }
    };

    public FileType findById(long id) throws DataAccessException {
        return findById(id, mapper);
    }

    @Override
    public String getTableName() {
        return Spectrum.FILE_TYPE_TABLE;
    }

    @Override
    public String getIdColumnName() {
        return Spectrum.ID;
    }
}
