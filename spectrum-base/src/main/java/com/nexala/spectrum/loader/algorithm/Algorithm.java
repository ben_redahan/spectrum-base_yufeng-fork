package com.nexala.spectrum.loader.algorithm;

import org.apache.log4j.Logger;

import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

public abstract class Algorithm {
    protected final Logger logger = Logger.getLogger(this.getClass());

    public FieldValue<?> getFieldValue(FileFieldDefinition fileFieldDefinition, byte[] source, DataFieldValue dependence)
            throws LoaderException {

        throw new LoaderException("Invalid algorithm implementation");
    }

    public FieldValue<?> getFieldValue(FileFieldDefinition fileFieldDefinition, String source, DataFieldValue dependence)
            throws LoaderException {

        throw new LoaderException("Invalid algorithm implementation");
    }

    private short[] getUnsigned(byte[] signed) {
        short[] unsigned = new short[signed.length];

        for (int i = 0; i < signed.length; i++) {
            unsigned[i] = (short) (signed[i] & 0xFF);
        }

        return unsigned;
    }

    protected long getUnsignedLong(byte[] data) {
        short[] unsigned = getUnsigned(data);

        long val = unsigned[0];
        if (unsigned.length > 1) {
            for (int i = 1; i < unsigned.length; i++) {
                val = val << 8 | unsigned[i];
            }
        }

        return val;
    }

    protected boolean getBit(byte[] data, int bitPos) {
        return (getUnsignedLong(data) & (1 << bitPos)) > 0;
    }

    protected byte[] getByteReversed(byte[] data) {
        byte[] reversed = new byte[data.length];

        int reversedPos = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            reversed[reversedPos++] = data[i];
        }

        return reversed;
    }

    protected byte[] getBitReversed(byte[] data) {
        byte[] reversed = new byte[data.length];

        int reversedPos = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            reversed[reversedPos++] = (byte) (Integer.reverse(data[i]) >>> (Integer.SIZE - Byte.SIZE));
        }

        return reversed;
    }
}