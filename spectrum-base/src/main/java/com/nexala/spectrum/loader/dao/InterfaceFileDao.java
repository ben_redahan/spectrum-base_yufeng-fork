/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.loader.beans.InterfaceFile;

public class InterfaceFileDao extends GenericDao<InterfaceFile, Long> {
    @Inject
    private InterfaceFileMapper iFileMapper;

    @Inject
    private QueryManager queryManager;

    private static final class InterfaceFileMapper extends JdbcRowMapper<InterfaceFile> {
        @Inject
        private BoxedResultSetFactory boxedRSFactory;

        @Inject
        private InterfaceFileDataDao iFileDataDao;

        @Override
        public InterfaceFile createRow(ResultSet results) throws SQLException {
            BoxedResultSet rs = boxedRSFactory.create(results);

            InterfaceFile iFile = new InterfaceFile();
            iFile.setId(rs.getLong(Spectrum.ID));
            iFile.setFileName(rs.getString(Spectrum.FILE_NAME));
            iFile.setTimestamp(rs.getTimestamp(Spectrum.IMPORT_TIMESTAMP));
            iFile.setStatus(rs.getString(Spectrum.STATUS));

            iFile.setInterfaceFileDataList(iFileDataDao.findByInterfaceFile(iFile));

            return iFile;
        }
    };

    public List<InterfaceFile> findByStatus(int filesBatchSize, String status) {
        String query = queryManager.getQuery(Spectrum.INTERFACE_FILE_BY_STATUS_QUERY);
        return findByQuery(query, iFileMapper, filesBatchSize, status);
    }

    public void changeStatus(long iFileId, String newStatus) throws DataAccessException {
        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(queryManager.getQuery(Spectrum.INTERFACE_FILE_CHANGE_STATUS_QUERY));
            ps.setString(1, newStatus);
            ps.setLong(2, iFileId);
            ps.execute();
        } catch (Exception ex) {
            throw new DataAccessException(String.format("Error changing interface file id %s status to '%s'", iFileId,
                    newStatus), ex);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    logger.error("Error closing 'change status' prepared statement", ex);
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.error("Error closing connection", ex);
                }
            }
        }
    }

    @Override
    public RowMapper<InterfaceFile> getRowMapper() {
        return iFileMapper;
    }

    @Override
    public String getIdColumnName() {
        return Spectrum.ID;
    }

    @Override
    public String getTableName() {
        return Spectrum.INTERFACE_FILE_TABLE;
    }
}
