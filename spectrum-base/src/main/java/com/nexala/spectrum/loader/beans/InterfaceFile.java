package com.nexala.spectrum.loader.beans;

import java.util.List;

public class InterfaceFile {
    private long id;
    private String fileName;
    private long timestamp;
    private String status;
    private List<InterfaceFileData> iFileDataList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<InterfaceFileData> getInterfaceFileDataList() {
        return iFileDataList;
    }

    public void setInterfaceFileDataList(List<InterfaceFileData> iFileDataList) {
        this.iFileDataList = iFileDataList;
    }
}
