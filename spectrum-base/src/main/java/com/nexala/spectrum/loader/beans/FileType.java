package com.nexala.spectrum.loader.beans;

import java.util.List;

public class FileType {
    private int id;
    private String name;
    private String fileNameFieldSeparator;
    private String fileDataType;
    private String fieldSeparator;
    private String textQualifier;
    private String processor;
    private String storedProcedureName;
    private int headerRowsToSkip;
    private boolean useBatch;
    private List<FileFieldDefinition> fileFields;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileNameFieldSeparator() {
        return fileNameFieldSeparator;
    }

    public void setFileNameFieldSeparator(String fileNameFieldSeparator) {
        this.fileNameFieldSeparator = fileNameFieldSeparator;
    }

    public String getFileDataType() {
        return fileDataType;
    }

    public void setFileDataType(String fileDataType) {
        this.fileDataType = fileDataType;
    }

    public String getFieldSeparator() {
        return fieldSeparator;
    }

    public void setFieldSeparator(String fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
    }

    public String getTextQualifier() {
        return textQualifier;
    }

    public void setTextQualifier(String textQualifier) {
        this.textQualifier = textQualifier;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getStoredProcedureName() {
        return storedProcedureName;
    }

    public void setStoredProcedureName(String storedProcedureName) {
        this.storedProcedureName = storedProcedureName;
    }

    public int getHeaderRowsToSkip() {
        return headerRowsToSkip;
    }

    public void setHeaderRowsToSkip(int headerRowsToSkip) {
        this.headerRowsToSkip = headerRowsToSkip;
    }

    public boolean isUseBatch() {
        return useBatch;
    }

    public void setUseBatch(boolean useBatch) {
        this.useBatch = useBatch;
    }

    public List<FileFieldDefinition> getFileFields() {
        return fileFields;
    }

    public void setFileFields(List<FileFieldDefinition> fileFields) {
        this.fileFields = fileFields;
    }

}
