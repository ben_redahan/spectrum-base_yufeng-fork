package com.nexala.spectrum.loader;

import com.google.inject.Inject;
import com.nexala.spectrum.scheduling.SimpleScheduledTask;
import com.nexala.spectrum.scheduling.SimpleTaskFactory;

public class InterfaceFileTaskFactory implements SimpleTaskFactory {
    @Inject
    InterfaceFileLoaderTaskFactory factory;

    @Override
    public SimpleScheduledTask getTask() {
        return factory.create();
    }
}
