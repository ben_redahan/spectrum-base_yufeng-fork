package com.nexala.spectrum.loader.beans;


public class DataFieldValue {
    private final FileFieldDefinition fileFieldDefinition;
    private FieldValue<?> fieldValue;

    public DataFieldValue(FileFieldDefinition fileFieldDefinition, FieldValue<?> fieldValue) {
        this.fileFieldDefinition = fileFieldDefinition;
        this.fieldValue = fieldValue;
    }

    public FileFieldDefinition getFileFieldDefinition() {
        return fileFieldDefinition;
    }

    public FieldValue<?> getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(FieldValue<?> fieldValue) {
        this.fieldValue = fieldValue;
    }
}
