/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader;

public interface InterfaceFileLoaderTaskFactory {
    InterfaceFileLoaderTask create();
}