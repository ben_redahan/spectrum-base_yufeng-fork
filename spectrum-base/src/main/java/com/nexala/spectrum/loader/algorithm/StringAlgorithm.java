package com.nexala.spectrum.loader.algorithm;

import java.io.UnsupportedEncodingException;

import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValueString;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

@Singleton
public class StringAlgorithm extends Algorithm {
    @Override
    public FieldValueString getFieldValue(FileFieldDefinition fileFieldDefinition, String source,
            DataFieldValue dependence) throws LoaderException {

        source = source != null ? source.trim() : source;

        String pattern = fileFieldDefinition.getProcessingPattern();

        if (pattern != null) {
            try {
                for (String decodePair : pattern.split(",")) {
                    String[] keyValue = decodePair.split("=");
                    if (keyValue[0].trim().equalsIgnoreCase(source)) {
                        return new FieldValueString(keyValue[1].trim());
                    }
                }

                throw new LoaderException(String.format("Error matching source '%s' on pattern '%s'", source, pattern));
            } catch (Exception ex) {
                throw new LoaderException(String.format("Error executing '%s', source '%s', "
                        + "pattern '%s', file field definition id %s", this.getClass().getName(), source, pattern,
                        fileFieldDefinition.getId()), ex);
            }
        } else {
            return new FieldValueString(source);
        }
    }

    @Override
    public FieldValueString getFieldValue(FileFieldDefinition fileFieldDefinition, byte[] source,
            DataFieldValue dependence) throws LoaderException {

        String pattern = fileFieldDefinition.getProcessingPattern();

        if (pattern != null) {
            if (pattern.equalsIgnoreCase(Spectrum.PATTERN_BYTE_STRING)
                    || pattern.equalsIgnoreCase(Spectrum.PATTERN_REVERSE_BYTE_STRING)) {

                if (pattern.equalsIgnoreCase(Spectrum.PATTERN_REVERSE_BYTE_STRING)) {
                    source = getByteReversed(source);
                }

                StringBuffer sb = new StringBuffer();
                for (byte b : source) {
                    sb.append(b);
                }

                return new FieldValueString(sb.toString());
            } else {
                throw new LoaderException(String.format("Error executing '%s', invalid pattern '%s' on file "
                        + "field definition id %s", this.getClass().getName(), pattern, fileFieldDefinition.getId()));
            }
        } else {
            try {
                return new FieldValueString(new String(source, "US-ASCII"));
            } catch (UnsupportedEncodingException ex) {
                throw new LoaderException(String.format("Error executing '%s', source '%s', "
                        + "pattern '%s', file field definition id %s", this.getClass().getName(), source, pattern,
                        fileFieldDefinition.getId()), ex);
            }
        }
    }
}
