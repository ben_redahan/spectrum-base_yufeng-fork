package com.nexala.spectrum.utils.channelmerger;

import com.nexala.spectrum.channel.Channel;

/**
 * Merge two channels into one.
 * @author brice
 *
 * @param <T> a type
 */
public interface ChannelMerger<T> {
    
    /**
     * Update the reference channel with the current channel if needed
     * @param reference the reference channel
     * @param current the current channel
     * @return reference channel updated
     */
    public Channel<T> merge(Channel<T> reference, Channel<T> current);

}
