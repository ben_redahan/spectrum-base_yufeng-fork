package com.nexala.spectrum.utils.channelmerger;

import com.nexala.spectrum.channel.Channel;

/**
 * Merge 2 channels. If the reference channel value is null then we copy the current value and category.
 * @author brice
 *
 * @param <T>
 */
public class NotNullChannelMerger<T> implements ChannelMerger<T>{

    /**
     * If the reference channel value is null then we copy the current channel value and category into reference.
     * 
     * @see com.nexala.spectrum.utils.channelmerger.ChannelMerger#merge(com.nexala.spectrum.channel.Channel, com.nexala.spectrum.channel.Channel)
     */
    @Override
    public Channel<T> merge(Channel<T> reference, Channel<T> current) {
        
        if (current != null && current.getValue() != null) {
            if (reference != null && reference.getValue() == null) {
                reference.setValue(current.getValue());
                reference.setCategory(current.getCategory());
            }
        }
        
        return reference;
    }

}
