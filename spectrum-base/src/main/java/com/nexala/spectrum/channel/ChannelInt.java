/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

public final class ChannelInt extends Channel<Integer> {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7186910637764691627L;

    private Integer value;

    public ChannelInt(String name) {
        super(name);
    }

    public ChannelInt(String name, Integer value) {
        super(name);
        this.value = value;
    }

    public ChannelInt(String name, Integer value, ChannelCategory category) {
        super(name);
        this.value = value;
        this.setCategory(category);
    }
    
    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelInt other = (ChannelInt) obj;
        if (this.value == null) {
            if (other.value != null)
                return false;
        } else if (!this.value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public Double getValueAsDouble() {
        Double doubleValue = null;
        if (this.value != null) {
            doubleValue = Double.valueOf(this.value);
        }
        return doubleValue;
    }

    public ChannelInt copy() {
        ChannelInt ci = new ChannelInt(getName());
        ci.setId(getId());
        ci.setValue(getValue());
        ci.setCategory(getCategory());
        return ci;
    }
}
