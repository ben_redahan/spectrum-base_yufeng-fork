/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.channel;

public class Binary {
    
    private static final String INT32 = "Too many params for int32";
    
    public static int binToInt(boolean msb, boolean... bits) {
        if (bits.length > 30) {
            throw new IllegalArgumentException(INT32);
        }
        
        int val = (msb == true ? 1 : 0) << bits.length;
        
        for (int i = bits.length - 1; i >= 0; i--) {
            val |= (bits[i] == true ? 1 : 0) << i;
        }
        
        return val;
    }
    
    public static int binToInt(int msb, int... bits) {
        if (bits.length > 30) {
            throw new IllegalArgumentException(INT32);
        }
        
        int val = msb << bits.length;
        
        for (int i = bits.length - 1; i >= 0; i--) {
            val |= bits[i] << i;
        }
        
        return val;
    }
}