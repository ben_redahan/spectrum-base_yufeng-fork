/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nexala.spectrum.rest.serializer.ChannelSerializer;

/**
 * This is abstract definition of a channel, a channel consists of a name, a
 * category and a value of type T.
 * 
 * @param <T>
 */
@JsonSerialize(using = ChannelSerializer.class)
public abstract class Channel<T> implements Categorizable, Serializable,
        Cloneable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8458864612752020387L;

    private int id = -1;
    private String name;
    private String columnName;
    private ChannelCategory category;

    public Channel(String name) {
        setCategory(ChannelCategory.NORMAL);
        setName(name);
        setColumnName(name);
    }

    public final void setId(int id) {
        this.id = id;
    }

    public final int getId() {
        return id;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public final void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public final String getColumnName() {
        return columnName;
    }
    
    @Override
    public final void setCategory(ChannelCategory category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((this.getCategory() == null) ? 0 : this.category.hashCode());

        result = prime * result
                + ((this.getName() == null) ? 0 : this.getName().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        @SuppressWarnings("unchecked")
        Channel<T> other = (Channel<T>) obj;

        if (this.getCategory() != other.getCategory()) {
            return false;
        }

        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }

        return true;
    }

    @JsonIgnore
    public String getStringValue() {
        if (getValue() != null) {
            return getValue().toString();
        } else {
            return null;
        }
    }

    // @Override
    public final ChannelCategory getCategory() {
        return category;
    }

    public abstract T getValue();

    public abstract void setValue(T value);
    
    public <Type> Type getValue(Class<Type> targetType) {
        return targetType.cast(getValue());
    }
    
    public <Type> Type getValue(Class<Type> targetType, Type defaultValue) {
        return getValue() == null ? defaultValue : targetType.cast(getValue());
    }

    /**
     * Creates a copy of this channel value by copying all of the fields,
     * including the value.
     * @return a copy of this channel.
     */
    public abstract Channel<T> copy();

    /**
     * Returns the value of the Channel<?> object represented as a Double. This
     * is used by the data plots as they need a numeric representation of
     * channel values to plot them.
     * @return the channel value as a double.
     */
    @JsonIgnore
    public abstract Double getValueAsDouble();

    public String toString() {
        return this.getCategory() + " - " + this.getValue();
    }
}
