package com.nexala.spectrum.db.dao;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.mappers.GenericBeanMapper;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.view.conf.DefaultCurrentEventsConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultUnitSearchConfiguration;

public class UnitSearchDao extends GenericDao<GenericBean, Long> {
    
    @Inject
    BoxedResultSetFactory factory;
           
    private final GenericBeanMapper unitSearchMapper;
    
    private final GenericBeanMapper createFaultSearchMapper;
    
    private final ApplicationConfiguration appConf;
    
    @Inject
    public UnitSearchDao(DefaultUnitSearchConfiguration defaultUnitSearchConfiguration, 
    		DefaultCurrentEventsConfiguration defaultCurrentEventsConfiguration,
            ApplicationConfiguration appConf, BoxedResultSetFactory factory) {
        List<BeanField> searchFields = defaultUnitSearchConfiguration.getSearchFields();
        this.unitSearchMapper = new GenericBeanMapper(searchFields, factory);
        this.appConf = appConf;
        
        
        List<BeanField> createFaultSearchFields = defaultCurrentEventsConfiguration.getSearchFields();
        this.createFaultSearchMapper = new GenericBeanMapper(createFaultSearchFields, factory);
    }
    
    @Override
    public JdbcRowMapper<GenericBean> getRowMapper() {
        return unitSearchMapper;
    }
    
    public List<GenericBean> getFleetUnitsByString(String fleetId, String substr) {
        String query = appConf.getQuery(Spectrum.UNIT_SEARCH_QUERY);
        return findByQuery(query, unitSearchMapper, fleetId, "%" + substr + "%");
    };
    
    public List<GenericBean> getCreateFaultFleetUnitsByString(String fleetId, String substr) {
        String query = appConf.getQuery(Spectrum.CREATE_FAULT_SEARCH_QUERY);
        return findByQuery(query, createFaultSearchMapper, fleetId, "%" + substr + "%");
    };
}
