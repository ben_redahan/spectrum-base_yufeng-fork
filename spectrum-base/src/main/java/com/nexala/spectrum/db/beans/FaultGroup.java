package com.nexala.spectrum.db.beans;

import java.io.Serializable;

public final class FaultGroup implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3302143824717117276L;

    private Integer id;
    private String name;

    /**
     * Non-argument default constructor required for GWT RPC serialization. <em>Do not use.</em>
     */
    public FaultGroup() {}

    public FaultGroup(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
