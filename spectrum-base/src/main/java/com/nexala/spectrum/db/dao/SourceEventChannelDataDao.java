package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.rest.data.beans.SourceEventChannelData;

public class SourceEventChannelDataDao extends GenericDao<SourceEventChannelData, Long> {
    
    private final SourceEventChannelDataMapper sourceEventChannelDataMapper;
    
    @Inject
    public SourceEventChannelDataDao(SourceEventChannelDataMapper sourceEventChannelDataMapper) {
        this.sourceEventChannelDataMapper = sourceEventChannelDataMapper;
    }
    
    private static class SourceEventChannelDataMapper extends JdbcRowMapper<SourceEventChannelData> {
        
        private final BoxedResultSetFactory factory;
        
        @Inject
        public SourceEventChannelDataMapper(BoxedResultSetFactory factory) {
            this.factory = factory;
        }

        @Override
        public SourceEventChannelData createRow(ResultSet resultSet) throws SQLException {
            return null;
        }
        
        @Override
        public List<SourceEventChannelData> createRows(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            
            Map<Integer, SourceEventChannelData> eventChannelBySource = new HashMap<>();

            try {
                while (resultSet.next()) {
                    Integer unitId = rs.getInt("UnitID");
                    
                    SourceEventChannelData current = eventChannelBySource.get(unitId);
                    
                    if (current == null) {
                        current = new SourceEventChannelData(unitId);
                        eventChannelBySource.put(unitId, current);
                    }
                    
                    Integer channelId = rs.getInt("ChannelID");
                    Boolean value = rs.getBoolean("Value");
                    Date timestamp = rs.getDate("TimeLastUpdated");
                    Date lastInsertTime = rs.getDate("LastInsertTime");
                    
                    current.put(timestamp, channelId, value, lastInsertTime);
                    
                }
            } catch (SQLException e) {
                throw new DataAccessException(e);
            }

            return new ArrayList<>(eventChannelBySource.values());
        }
        
    }
    
    public List<SourceEventChannelData> getAllEventChannelValues() {
        String allEventChannelValueLatestQuery = new QueryManager().getQuery(Spectrum.ALL_EVENT_CHANNEL_VALUE_LATEST_QUERY);
        return findByQuery(allEventChannelValueLatestQuery, sourceEventChannelDataMapper);
    }

}