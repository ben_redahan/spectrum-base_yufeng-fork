/*
+ * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.E2mFaultCode;
import com.nexala.spectrum.db.beans.FaultMeta;

public class FaultMetaDao extends GenericDao<FaultMeta, Integer> {
    private FaultMetaCategoryOptionalDao dao;
    private E2mFaultCodeDao e2mFaultCodeDao;
    private final boolean optionalCategoriesEnabled;
    private final boolean optionalE2mFaultCodeEnabled;
    private final boolean optionalFaultSourceEnabled;
    
    private ApplicationConfiguration appConf;

    @Inject
    public FaultMetaDao(FaultMetaCategoryOptionalDao dao, E2mFaultCodeDao e2mFaultCodeDao,
            ConfigurationManager confManager, ApplicationConfiguration appConf) {
        this.dao = dao;
        Boolean enabled = confManager
                .getConfAsBoolean(Spectrum.SPECTRUM_RULE_EDITOR_ENABLE_OPTIONAL_CATEGORIES);
        this.optionalCategoriesEnabled = enabled != null ? enabled : false;
        this.e2mFaultCodeDao = e2mFaultCodeDao;
        Boolean e2mFaultCodeEnabled = confManager
                .getConfAsBoolean(Spectrum.SPECTRUM_FAULT_SENDER_ENABLE);
        this.optionalE2mFaultCodeEnabled = e2mFaultCodeEnabled != null ? e2mFaultCodeEnabled : false;
        Boolean faultSourceEnabled = confManager
                .getConfAsBoolean(Spectrum.SPECTRUM_RULE_EDITOR_ENABLE_FAULT_SOURCE);
        this.optionalFaultSourceEnabled = faultSourceEnabled != null ? faultSourceEnabled : false;
        this.appConf = appConf;
    }
    
    private class FaultMetaMapper extends JdbcRowMapper<FaultMeta> {
        @Override
        public FaultMeta createRow(ResultSet resultSet) throws SQLException {
            FaultMeta fm = new FaultMeta();

            fm.setId(resultSet.getInt("ID"));
            fm.setFaultCode(resultSet.getString("FaultCode"));
            fm.setDescription(resultSet.getString("Description"));
            fm.setSummary(resultSet.getString("Summary"));
            fm.setAdditionalInfo(resultSet.getString("AdditionalInfo"));
            fm.setUrl(resultSet.getString("Url"));
            fm.setCategoryId(resultSet.getInt("CategoryID"));
            fm.setGroupId(resultSet.getInt("GroupID"));
            fm.setTypeId(resultSet.getInt("FaultTypeID"));
            fm.setHasRecovery(resultSet.getBoolean("HasRecovery"));
            fm.setOptionalCategoryIdList(dao.getOptionalCategoryList(fm.getId()));
            E2mFaultCode e2mFaultCode = e2mFaultCodeDao.getE2mFaultCode(fm.getId());
            fm.setE2mFaultCode(e2mFaultCode.getCode());
            fm.setE2mFaultDescription(e2mFaultCode.getDescription());
            fm.setE2mPriorityCode(e2mFaultCode.getPriorityCode());
            fm.setE2mPriority(e2mFaultCode.getPriority());
            fm.setFromDate(resultSet.getTimestamp("FromDate"));
            fm.setToDate(resultSet.getTimestamp("ToDate"));
            fm.setParentId(resultSet.getInt("ParentID"));
            
            // FIXME implement proper checks for column presence
            try {
                fm.setRecoveryTypeId(resultSet.getInt("RecoveryTypeId"));
            } catch (SQLException e) {
            }

            try {
                fm.setRecoveryProcessPath(resultSet.getString("RecoveryProcessPath"));
            } catch (SQLException e) {
            }
            
            try {
                fm.setSourceId(resultSet.getInt("SourceID"));
            } catch (SQLException e) {
            }
            
            if (DbUtil.hasColumn(resultSet, "FleetID")) {
            	fm.setFleetId(resultSet.getInt("FleetID"));
            }

            return fm;
        }
    }

    private class FaultMetaLiteMapper extends JdbcRowMapper<FaultMeta> {
        @Override
        public FaultMeta createRow(ResultSet resultSet) throws SQLException {
            FaultMeta fm = new FaultMeta();

            fm.setId(resultSet.getInt("InsertedID"));

            return fm;
        }
    }

    /**
     * Gets a list of all FaultMeta records, ordered by the faultCode
     * 
     * @return
     */
    public List<FaultMeta> getFaultMetaList() throws DataAccessException {
        List<FaultMeta> result = findByQuery("SELECT * FROM FaultMeta fm "
                + "LEFT JOIN FaultMetaE2M fme ON fm.ID = fme.FaultMetaID ORDER BY FaultCode", new FaultMetaMapper());
        Collections.sort(result);
        return result;
    }

    /**
     * Gets the fault meta summary for the specified faultCode. Returns null if no summary or if faultCode doesn't exist.
     * 
     * @param faultCode
     * @return
     */
    public String getFaultMetaSummary(String faultCode) throws DataAccessException {

        List<FaultMeta> faults = findByQuery("SELECT * FROM FaultMeta WHERE FaultCode = ?", new FaultMetaMapper(),
                faultCode);

        if (faults.size() > 0) {
            return faults.get(0).getSummary();
        } else {
            return null;
        }
    }

    public List<FaultMeta> getFaultMetaList(Integer icategory, String faultCode) throws DataAccessException {

        String query = "SELECT * FROM FaultMeta " + "WHERE (? = -1 OR CategoryID = ?) "
                + "AND (? IS NULL OR (faultCode LIKE ?) OR description LIKE ?) " + "ORDER BY FaultCode";

        List<FaultMeta> result = findByQuery(query, new FaultMetaMapper(), icategory == null ? -1 
                : icategory, icategory == null ? -1
                : icategory, faultCode, faultCode == null ? "%" 
                : faultCode + "%", faultCode == null ? "%" 
                : "%" + faultCode + "%");
        
        Collections.sort(result);
        return result;
    }
    
    /**
     * Gets a faultMeta record by its faultCode, returns null if no faultMeta record with the specified faultCode exists
     * 
     * @param faultCode
     * @return
     */
    public FaultMeta findByFaultCode(String faultCode) throws DataAccessException {
    	return findByFaultCode(faultCode, null);
    }

    /**
     * Gets a faultMeta record by its faultCode and fleetId, returns null if no faultMeta record with the specified faultCode exists
     * 
     * @param faultCode
     * @return
     */
    public FaultMeta findByFaultCode(String faultCode, Integer fleetId) throws DataAccessException {
        List<FaultMeta> faults;
        if (fleetId != null) {
        	faults = findByQuery("SELECT * FROM FaultMeta fm "
                    + "LEFT JOIN FaultMetaE2M fme ON fm.ID = fme.FaultMetaID WHERE faultCode = ? AND fleetID = ?", new FaultMetaMapper(),
                    faultCode, fleetId);
        } else {
        	faults = findByQuery("SELECT * FROM FaultMeta fm "
                + "LEFT JOIN FaultMetaE2M fme ON fm.ID = fme.FaultMetaID WHERE faultCode = ?", new FaultMetaMapper(),
                faultCode);
        }
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        String currentDate = format.format(new Date());
        currentDate = currentDate.replace(currentDate.substring(0, 4), "1970");
        
        Date fromDateO;
        Date toDateO;
        Date currentDateO;
        
        for (int i=0; i<faults.size(); i++) {
            try {
                fromDateO = faults.get(i).getFromDate();
                toDateO = faults.get(i).getToDate();
                currentDateO = format.parse(currentDate);
                
                if ((faults.get(i).getFromDate() != null) && (faults.get(i).getToDate() != null)) {
                        if (currentDateO.before(fromDateO)) {
                            c.setTime(currentDateO);
                            c.add(Calendar.YEAR, 1);
                            currentDateO = c.getTime();
                            if ((currentDateO.after(fromDateO)) && (currentDateO.before(toDateO))) {
                                return faults.get(i);
                            } else if ((i == 0) && (faults.get(i + 1) != null)) { //
                                return faults.get(i + 1);
                            } else if ((i == 1) && (faults.get(i - 1) != null)) { //
                                return faults.get(i - 1);
                            }
                        }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (faults.size() > 0) {
            return faults.get(0);
        } else {
            return null;
        }
    }

    /**
     * Get a list of faultmeta records by category ID.
     * 
     * @param categoryId The category ID
     * @return
     */
    public List<FaultMeta> findByCategoryId(Integer categoryId) throws DataAccessException {
        List<FaultMeta> result = findByQuery("SELECT * FROM FaultMeta fm "
                + "LEFT JOIN FaultMetaE2M fme ON fm.ID = fme.FaultMetaID WHERE CategoryID = ?", new FaultMetaMapper(), categoryId);
        
        Collections.sort(result);
        return result;
    }

    public List<FaultMeta> findAllPrimaryFields() throws DataAccessException {
    	return findAllPrimaryFields("");
    }
    
    public List<FaultMeta> findAllPrimaryFields(String fleetCode) throws DataAccessException {
        String query;
        List<FaultMeta> result;
        
        if (fleetCode != null &&
        	!fleetCode.isEmpty()) {
        	query = appConf.getQuery(Spectrum.FAULT_META_PARENT_QUERY_BY_FLEET_CODE);
        	result = findByQuery(query, new FaultMetaMapper(), fleetCode);
        } else {
        	query = appConf.getQuery(Spectrum.FAULT_META_PARENT_QUERY);
        	result = findByQuery(query, new FaultMetaMapper());
        }
        
        Collections.sort(result);
		return result;
    }
    
    public List<FaultMeta> findAllSecondaryFields() throws DataAccessException {
    	return findAllSecondaryFields("");
    }
    
    public List<FaultMeta> findAllSecondaryFields(String fleetCode) throws DataAccessException {
        String query;
        List<FaultMeta> result;
        
        if (fleetCode != null &&
        	!fleetCode.isEmpty()) {
        	query = appConf.getQuery(Spectrum.FAULT_META_CHILD_QUERY_BY_FLEET_CODE);
        	result = findByQuery(query, new FaultMetaMapper(), fleetCode);
        } else {
        	query = appConf.getQuery(Spectrum.FAULT_META_CHILD_QUERY);
        	result = findByQuery(query, new FaultMetaMapper());
        }
        return result;
    }
    
    public FaultMeta findSecondaryFieldByPrimaryId(int parentId) throws DataAccessException {
    	return findSecondaryFieldByPrimaryId(parentId, null);
    }
    
    public FaultMeta findSecondaryFieldByPrimaryId(int parentId, Integer fleetId) throws DataAccessException {
        String query;
        List<FaultMeta> childFaultMeta;
        
    	query = appConf.getQuery(Spectrum.FAULT_META_CHILD_BY_PARENT_QUERY);
    	childFaultMeta = findByQuery(query, new FaultMetaMapper(), parentId, fleetId);
        
        if (childFaultMeta.size() > 0) {
            return childFaultMeta.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Inserts a new FaultMeta record into the database and returns the ID of the newly created record
     * 
     * @deprecated Use create(FaultMeta t) instead.
     * @param t FaultMeta object to create
     * @return
     */
    public Integer insert(FaultMeta t) {
        return create(t, null);
    }
    
    /**
     * Inserts a new FaultMeta record into the database and returns the ID of the newly created record
     * 
     * @param t FaultMeta object to create
     * @param username the username of who created the faultmeta
     * @return
     */
    public Integer create(FaultMeta t, String username) {
    	String query = "{ call NSP_InsertFaultMeta(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?__ADDITIONAL_FIELDS__) }";

        Integer parentId = 0;
        
        ArrayList<Object> qparams = new ArrayList<Object>();
        qparams.add(t.getId());
        qparams.add(username);
        qparams.add(t.getFaultCode());
        qparams.add(t.getDescription());
        qparams.add(t.getSummary());
        qparams.add(t.getAdditionalInfo());
        qparams.add(t.getUrl());
        qparams.add(t.getCategoryId());
        qparams.add(t.getHasRecovery());
        qparams.add(t.getRecoveryProcessPath());
        qparams.add(t.getTypeId());
        qparams.add(t.getGroupId());
        qparams.add(t.getFleetId());

        if (optionalFaultSourceEnabled) {
            query = query.replace("__ADDITIONAL_FIELDS__", ", ?");
            qparams.add(t.getSourceId());
        } else {
            query = query.replace("__ADDITIONAL_FIELDS__", "");
        }
        
        List<FaultMeta> liteFaultMeta = this.findByStoredProcedure(query, new FaultMetaLiteMapper(), qparams.toArray());
        
        Integer id = liteFaultMeta.get(0).getId();
        
        updateOptionalCategoryList(id, t.getOptionalCategoryIdList());
        
        if (t.getE2mFaultCode() != null && !t.getE2mFaultCode().isEmpty() && optionalE2mFaultCodeEnabled) {
            updateFaultMetaE2m(id, t.getE2mFaultCode(), t.getE2mFaultDescription(), t.getE2mPriorityCode(), t.getE2mPriority());
        }
        
        //override fields require a typeId to be saved
        if (t.getTypeIdOverride() != null && t.getTypeIdOverride() > 0) {
            String query2 = "{ call NSP_InsertFaultMeta(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?__ADDITIONAL_FIELDS__) }";

            parentId = liteFaultMeta.get(0).getId(); 
            qparams.clear();
            
            qparams.add(null); 
            qparams.add(username);
            qparams.add(t.getFaultCode());
            qparams.add(t.getDescription());
            qparams.add(t.getSummary());
            qparams.add(t.getAdditionalInfo());
            qparams.add(t.getUrl());
            qparams.add(t.getCategoryId());
            qparams.add(t.getHasRecovery());
            qparams.add(t.getRecoveryProcessPath());
            qparams.add(t.getTypeIdOverride());
            qparams.add(t.getGroupId());
            qparams.add(t.getFleetId());
            qparams.add(t.getFromDate());
            qparams.add(t.getToDate());
            qparams.add(parentId);
            if (optionalFaultSourceEnabled) {
                query2 = query2.replace("__ADDITIONAL_FIELDS__", ", ?");
                qparams.add(t.getSourceId());
            } else {
                query2 = query2.replace("__ADDITIONAL_FIELDS__", "");
            }
            
            liteFaultMeta = this.findByStoredProcedure(query2, new FaultMetaLiteMapper(), qparams.toArray());
            
            Integer childId = liteFaultMeta.get(0).getId();
            
            updateOptionalCategoryList(childId, t.getOptionalCategoryIdList());
            
            if (t.getE2mFaultCode() != null && !t.getE2mFaultCode().isEmpty() && optionalE2mFaultCodeEnabled) {
                updateFaultMetaE2m(childId, t.getE2mFaultCode(), t.getE2mFaultDescription(), t.getE2mPriorityCode(), t.getE2mPriority());
            }
        }
        return id;
    }
    
    @Override
    public void update(Integer id, FaultMeta b) throws DataAccessException {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates a record by its ID
     * 
     * @param id The id to update
     * @param t The new data to assign to the id
     * @param username the username of the user who changed the fault meta
     */
    public void update(Integer id, FaultMeta t, String username) throws DataAccessException {
        String query = "{ call NSP_UpdateFaultMeta(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?__ADDITIONAL_FIELDS__) }";
        
        ArrayList<Object> qparams = new ArrayList<Object>();
        qparams.add(id);
        qparams.add(username);
        qparams.add(t.getFaultCode());
        qparams.add(t.getDescription());
        qparams.add(t.getSummary());
        qparams.add(t.getAdditionalInfo());
        qparams.add(t.getUrl());
        qparams.add(t.getCategoryId());
        qparams.add(t.getHasRecovery());
        qparams.add(t.getRecoveryProcessPath());
        qparams.add(t.getTypeId());
        qparams.add(t.getGroupId());
        
        if (optionalFaultSourceEnabled) {
            query = query.replace("__ADDITIONAL_FIELDS__", ", ?");
            qparams.add(t.getSourceId());
        } else {
            query = query.replace("__ADDITIONAL_FIELDS__", "");
        }

        this.executeStoredProcedure(query, qparams.toArray());
        
        updateOptionalCategoryList(id, t.getOptionalCategoryIdList());
        
        if (t.getE2mFaultCode() != null && optionalE2mFaultCodeEnabled) {
            updateFaultMetaE2m(id, t.getE2mFaultCode(), t.getE2mFaultDescription(), t.getE2mPriorityCode(), t.getE2mPriority());
        }
        
        //override fields require a typeId to be saved
        if (t.getTypeIdOverride() != null && t.getTypeIdOverride() > 0) {
            //need to check if there is already an overriding field
        	Integer faultMetaFleetId = t.getFleetId();
        	
        	FaultMeta overrideField = findSecondaryFieldByPrimaryId(id, faultMetaFleetId);
            qparams.clear();
            
            if(overrideField != null) {
                //ie if there already exists an overriding field
                String updateQuery = "{ call NSP_UpdateFaultMeta(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?__ADDITIONAL_FIELDS__) }";
                
                qparams.add(overrideField.getId());
                qparams.add(username);
                qparams.add(t.getFaultCode());
                qparams.add(t.getDescription());
                qparams.add(t.getSummary());
                qparams.add(t.getAdditionalInfo());
                qparams.add(t.getUrl());
                qparams.add(t.getCategoryId());
                qparams.add(t.getHasRecovery());
                qparams.add(t.getRecoveryProcessPath());
                qparams.add(t.getTypeIdOverride());
                qparams.add(t.getGroupId());
                qparams.add(t.getFromDate());
                qparams.add(t.getToDate());                

                if (optionalFaultSourceEnabled) {
                    updateQuery = updateQuery.replace("__ADDITIONAL_FIELDS__", ", ?");
                    qparams.add(t.getSourceId());
                } else {
                    updateQuery = updateQuery.replace("__ADDITIONAL_FIELDS__", "");
                }

                this.executeStoredProcedure(updateQuery, qparams.toArray());
                
                updateOptionalCategoryList(overrideField.getId(), t.getOptionalCategoryIdList());
                
                if (t.getE2mFaultCode() != null && optionalE2mFaultCodeEnabled) {
                    updateFaultMetaE2m(overrideField.getId(), t.getE2mFaultCode(), t.getE2mFaultDescription(), t.getE2mPriorityCode(), t.getE2mPriority());
                }
                
            } else {
                //if there isn't an overriding field for this fault, we need to create one
            	String insertQuery = "{ call NSP_InsertFaultMeta(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?__ADDITIONAL_FIELDS__) }";
                
                qparams.add(null); 
                qparams.add(username);
                qparams.add(t.getFaultCode());
                qparams.add(t.getDescription());
                qparams.add(t.getSummary());
                qparams.add(t.getAdditionalInfo());
                qparams.add(t.getUrl());
                qparams.add(t.getCategoryId());
                qparams.add(t.getHasRecovery());
                qparams.add(t.getRecoveryProcessPath());
                qparams.add(t.getTypeIdOverride());
                qparams.add(t.getGroupId());
                qparams.add(t.getFleetId());
                
                qparams.add(t.getFromDate());
                qparams.add(t.getToDate());
                qparams.add(id);
                
                if (optionalFaultSourceEnabled) {
                    insertQuery = insertQuery.replace("__ADDITIONAL_FIELDS__", ", ?");
                    qparams.add(t.getSourceId());
                } else {
                    insertQuery = insertQuery.replace("__ADDITIONAL_FIELDS__", "");
                }
                
                List<FaultMeta> liteFaultMeta = this.findByStoredProcedure(insertQuery, new FaultMetaLiteMapper(), qparams.toArray());
                
                Integer childId = liteFaultMeta.get(0).getId();
                
                updateOptionalCategoryList(childId, t.getOptionalCategoryIdList());
                
                if (t.getE2mFaultCode() != null && !t.getE2mFaultCode().isEmpty() && optionalE2mFaultCodeEnabled) {
                    updateFaultMetaE2m(childId, t.getE2mFaultCode(), t.getE2mFaultDescription(), t.getE2mPriorityCode(), t.getE2mPriority());
                }
                
            }
        }
    }
    
    private void updateOptionalCategoryList(Integer id, List<Integer> catIdList) {
        if (optionalCategoriesEnabled) {
            String queryInsert = "{ call NSP_InsertFaultMetaCategoryOptional(?, ?) }";
            
            Set<Integer> distinctCatIds = new HashSet<Integer>();
            distinctCatIds.addAll(catIdList);
            
            deleteOptionalCategoryList(id);
            
            for (Integer catId : distinctCatIds) {
                this.executeStoredProcedure(queryInsert, id, catId);
            }
        }
    }

    /**
     * Deletes a record by its ID
     */
    public void delete(Integer id, String e2mFaultCode) throws DataAccessException {
        String query = "{ call NSP_DeleteFaultMeta(?) }";
        
        deleteOptionalCategoryList(id);

        this.executeStoredProcedure(query, id);

        if (e2mFaultCode != null && optionalE2mFaultCodeEnabled) {
            deleteFaultMetaE2m(id);
        }    
    }
    
    public void fillDeleteQueryAndParam(Integer id, String e2mFaultCode, List<String> queries, List<Object> parameters) {
    	if (this.optionalCategoriesEnabled) {
    		queries.add("EXEC [DB_NAME].dbo.NSP_DeleteFaultMetaCategoryOptional ?");
    		parameters.add(id);
    	}
    	
    	queries.add("EXEC [DB_NAME].dbo.NSP_DeleteFaultMeta ?");
    	parameters.add(id);
    	
    	if (e2mFaultCode != null && optionalE2mFaultCodeEnabled) {
            queries.add("EXEC [DB_NAME].dbo.NSP_DeleteFaultMetaE2M ?");
        	parameters.add(id);
        }  
    }
    
    private void deleteOptionalCategoryList(Integer id) {
        if (this.optionalCategoriesEnabled) {
            String queryDelete = "{ call NSP_DeleteFaultMetaCategoryOptional(?) }";
            
            this.executeStoredProcedure(queryDelete, id);
        }
    }

    private void updateFaultMetaE2m(Integer id, String e2mFaultCode, String e2mFaultDescription, String e2mPriorityCode, String e2mPriority) {
        String query = "{ call NSP_UpdateFaultMetaE2M(?, ?, ?, ?, ?) }";
        this.executeStoredProcedure(query, id, e2mFaultCode, e2mFaultDescription, e2mPriorityCode, e2mPriority);
    }
    
    private void deleteFaultMetaE2m(Integer id) {
        String query = "{ call NSP_DeleteFaultMetaE2M(?) }";
        this.executeStoredProcedure(query, id);
    }
    
    @Override
    public String getIdColumnName() {
        return "ID";
    }

    @Override
    public String getTableName() {
        return "FaultMeta";
    }
    
    private final FaultMetaMapper faultMetaMapper = new FaultMetaMapper();
    
    @Override
    public JdbcRowMapper<FaultMeta> getRowMapper() {
        return faultMetaMapper;
    }
}
