/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * @author dclerkin
 * @date 15/11/2013
 */
package com.nexala.spectrum.db.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.google.inject.Singleton;

@Singleton
public class TomcatConnectionProperties {
    private final Logger logger = Logger.getLogger(TomcatConnectionProperties.class);

    private static final String PROPERTIES = "WEB-INF/conf/ncu_ts.properties";

    private Properties properties = new Properties();

    public TomcatConnectionProperties() {
        try {
            ClassLoader cl = TomcatConnectionProperties.class.getClassLoader();
            InputStream is = cl.getResourceAsStream(PROPERTIES);

            if (is == null) {
                logger.warn(String.format("TomcatConnection properties file not found (%s).", PROPERTIES));
            } else {
                properties.load(is);
                logger.warn(String.format("TomcatConnection properties file loaded (%s).", PROPERTIES));
            }
        } catch (IOException e) {
            logger.warn(String.format("Failed to load TomcatConnection properties file (%s).", PROPERTIES), e);
        }
    }
    
    public String get(String name) {
        return new Properties(properties).getProperty(name);
    }
    
    public String get(String name, String defaultValue) {
        return new Properties(properties).getProperty(name, defaultValue);
    }
}
