package com.nexala.spectrum.db.beans;

import org.apache.commons.lang.StringEscapeUtils;

public class ChannelDefinitionRuleValidationParam {
    private String channelId;
    private String minValue;
    private String maxValue;
    private String minInclusive;
    private String maxInclusive;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getMinInclusive() {
        return minInclusive;
    }

    public void setMinInclusive(String minInclusive) {
        this.minInclusive = minInclusive;
    }

    public String getMaxInclusive() {
        return maxInclusive;
    }

    public void setMaxInclusive(String maxInclusive) {
        this.maxInclusive = maxInclusive;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("<validation ");
        sb.append("channelid=\"" + StringEscapeUtils.escapeXml(channelId) + "\" ");
        sb.append("minvalue=\"" + StringEscapeUtils.escapeXml(minValue) + "\" ");
        sb.append("maxvalue=\"" + StringEscapeUtils.escapeXml(maxValue) + "\" ");
        sb.append("mininclusive=\"" + StringEscapeUtils.escapeXml(minInclusive) + "\" ");
        sb.append("maxinclusive=\"" + StringEscapeUtils.escapeXml(maxInclusive) + "\" ");
        sb.append(" />");

        return sb.toString();
    }
}
