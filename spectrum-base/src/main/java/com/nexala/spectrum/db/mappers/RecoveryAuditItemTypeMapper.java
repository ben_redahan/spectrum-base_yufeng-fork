package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.RecoveryAuditItemType;

public class RecoveryAuditItemTypeMapper extends JdbcRowMapper<RecoveryAuditItemType> {
    public RecoveryAuditItemType createRow(ResultSet resultSet) throws SQLException {
        RecoveryAuditItemType r = new RecoveryAuditItemType();
        r.setId(resultSet.getLong("id"));
        r.setQuestion(resultSet.getLong("Question") == 1L ? true : false);
        r.setItemText(resultSet.getString("ItemText"));
        r.setAnswer(resultSet.getString("Answer"));
        r.setUserAction(resultSet.getLong("UserAction") == 1L ? true : false);
        r.setReversion(resultSet.getLong("Reversion") == 1L ? true : false);
        r.setExecution(resultSet.getLong("Execution") == 1L ? true : false);
        r.setOutcome(resultSet.getLong("Outcome") == 1L ? true : false);
        r.setNextStep(resultSet.getLong("NextStep") == 1L ? true : false);
        
        return r;
    }
}
