package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.Location;

public class LocationMapper extends JdbcRowMapper<Location> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Location createRow(ResultSet rs) throws SQLException {
        Location location = new Location();
        location.setId(rs.getInt("LocationID"));
        location.setName(rs.getString("LocationName"));
        location.setTiploc(rs.getString("Tiploc"));

        return location;
    }

}
