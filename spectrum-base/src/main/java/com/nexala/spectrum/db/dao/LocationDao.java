/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.Location;

public class LocationDao extends GenericDao<Location, Long> {

    private static final JdbcRowMapper<Location> rowMapper = new JdbcRowMapper<Location>() {
        @Override
        public final Location createRow(ResultSet rs) throws SQLException {
            Location location = new Location();
            location.setId(rs.getInt("ID"));
            location.setCode(rs.getString("LocationCode"));
            location.setName(rs.getString("LocationName"));
            location.setTiploc(rs.getString("Tiploc"));
            location.setLatitude(rs.getDouble("Lat"));
            location.setLongitude(rs.getDouble("Lng"));
            location.setType(rs.getString("Type"));

            return location;
        }
    };

    public Location getLocation(String code) {
        String query = new QueryManager().getQuery(Spectrum.LOCATION_BY_CODE);

        List<Location> locations = findByQuery(query, rowMapper, code);

        if (locations != null && locations.size() == 1) {
            return locations.get(0);
        }

        return null;
    }
    
    public List<Location> getLocationList(String location) {
    	String query = new QueryManager().getQuery(Spectrum.LOCATION_LIST);

        List<Location> locations = findByQuery(query, rowMapper, location);

        return locations;
    }
    
}
