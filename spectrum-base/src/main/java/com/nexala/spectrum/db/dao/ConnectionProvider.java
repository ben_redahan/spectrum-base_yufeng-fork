/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * @author dclerkin
 * @date 15/11/2013
 */
package com.nexala.spectrum.db.dao;

import java.sql.Connection;

public interface ConnectionProvider {
    public Connection getConnection() throws DataAccessException;
}
