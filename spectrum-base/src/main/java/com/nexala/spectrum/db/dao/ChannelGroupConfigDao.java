/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;

public class ChannelGroupConfigDao extends GenericDao<ChannelGroupConfig, Long> {
    private static class ChannelGroupConfigMapper extends JdbcRowMapper<ChannelGroupConfig> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public ChannelGroupConfig createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            ChannelGroupConfig record = new ChannelGroupConfig(rs.getInt("ID"), rs.getString("Name"),
                    rs.getString("Description"), rs.getBoolean("IsVisible"));

            return record;
        }
    }

    private final ChannelGroupConfigMapper mapper;

    @Inject
    public ChannelGroupConfigDao(ChannelGroupConfigMapper mapper) {
        this.mapper = mapper;
    }

    public List<ChannelGroupConfig> findAll() throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_GROUP_CONFIG_QUERY);
        return this.findByQuery(query, mapper);
    }

    public ChannelGroupConfig findById(int groupId) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_GROUP_CONFIG_ID_QUERY);

        List<ChannelGroupConfig> list = this.findByQuery(query, mapper, groupId);

        return list != null && list.size() == 1 ? list.get(0) : null;
    }
}
