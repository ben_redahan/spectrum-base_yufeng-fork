package com.nexala.spectrum.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelDouble;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.utils.DateUtils;

public class ChannelCollectionMapper extends JdbcRowMapper<ChannelCollection> {
    private final Logger logger = Logger
            .getLogger(ChannelCollectionMapper.class);

    private ChannelConfiguration channelConf;
    
    @Inject
    private DateUtils dateUtils;
    
    @Inject
    public ChannelCollectionMapper(@Assisted ChannelConfiguration channelConf) {
        this.channelConf = channelConf;
    }

    @Override
    public ChannelCollection createRow(ResultSet rs) throws SQLException {
        ChannelCollection cc = new ChannelCollection();

        ResultSetMetaData meta = rs.getMetaData();

        for (int i = 1; i <= meta.getColumnCount(); i++) {
            String columnName = meta.getColumnName(i);
            ChannelConfig config = channelConf.getConfigByShortName(columnName);
            Integer channelId;
            String channelName;
            
            if (config != null) {
                channelName = config.getName();
                channelId = config.getId();
                columnName = config.getShortName(); // same reference than the cached conf (less memory used) 
            } else {
                channelName = columnName;
                channelId = -1;
            }

            Channel<?> channel = null;

            int columnType = meta.getColumnType(i);
            switch (columnType) {
                case Types.BIT:
                    Boolean booleanVal = rs.getBoolean(i);
                    
                    if (rs.wasNull()) {
                        booleanVal = null;
                    }

                    channel = new ChannelBoolean(channelName, booleanVal);
                    channel.setColumnName(columnName);
                    break;
                case Types.DECIMAL:
                case Types.FLOAT:
                case Types.DOUBLE:
                    Double doubleVal = rs.getDouble(i);

                    if (rs.wasNull()) {
                        doubleVal = null;
                    }

                    channel = new ChannelDouble(channelName, doubleVal);
                    channel.setColumnName(columnName);
                    break;
                case Types.TINYINT:
                case Types.SMALLINT:
                case Types.INTEGER:
                    Integer intVal = rs.getInt(i);

                    if (rs.wasNull()) {
                        intVal = null;
                    }

                    channel = new ChannelInt(channelName, intVal);
                    channel.setColumnName(columnName);
                    break;
                case Types.BIGINT:
                    Long longVal = rs.getLong(i);

                    if (rs.wasNull()) {
                        longVal = null;
                    }

                    channel = new ChannelLong(channelName, longVal);
                    channel.setColumnName(columnName);
                    break;
                case Types.TIMESTAMP:
                    Timestamp timestamp = rs.getTimestamp(i, dateUtils.getDatabaseCalendar());

                    channel = new ChannelLong(channelName,
                            timestamp != null ? timestamp.getTime() : null);
                    channel.setColumnName(columnName);
                    break;
                case Types.VARCHAR:
                case Types.CHAR:
                    channel = new ChannelString(channelName, rs.getString(i));
                    channel.setColumnName(columnName);
                    break;
                default:
                    logger.warn(String.format("Unexpected column type "
                            + "when mapping channel %s (%d) ", channelName,
                            columnType));
            }

            if (channel != null) {
                channel.setId(channelId);
                cc.put(channel);
            }
        }
        return cc;
    }
}
