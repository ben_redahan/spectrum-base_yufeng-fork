package com.nexala.spectrum.db.beans;

import java.util.List;
import java.util.Map;

public final class ChannelDefinitionParam {
    
    private Map<String, Object> parameters;

    private List<ChannelDefinitionRuleParam> rules;

    public List<ChannelDefinitionRuleParam> getRules() {
        return rules;
    }

    public void setRules(List<ChannelDefinitionRuleParam> rules) {
        this.rules = rules;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}