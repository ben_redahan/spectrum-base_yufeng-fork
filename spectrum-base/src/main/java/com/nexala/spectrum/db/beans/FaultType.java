package com.nexala.spectrum.db.beans;

import java.io.Serializable;

public final class FaultType implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3302143824717117276L;

    private Integer id;
    private String name;
    private String displayColor;

    /** The Priority of the faultType. Lower priority numbers are the most serious. */
    private Integer priority;

    /**
     * Non-argument default constructor required for GWT RPC serialization. <em>Do not use.</em>
     */
    public FaultType() {}

    public FaultType(Integer id, Integer priority, String name, String displayColor) {
        this.id = id;
        this.priority = priority;
        this.name = name;
        this.displayColor = displayColor;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDisplayColor() {
        return displayColor;
    }

    public Integer getPriority() {
        return priority;
    }
}
