package com.nexala.spectrum.db.dao.charting;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;

public class ChartConfigurationMapper extends JdbcRowMapper<DataViewConfiguration> {
    @Override
    public final DataViewConfiguration createRow(ResultSet resultSet)
            throws SQLException {
        DataViewConfiguration configuration = new DataViewConfiguration();
        configuration.setId(resultSet.getLong("ID"));
        configuration.setName(resultSet.getString("Name"));
        configuration.setUserName(resultSet.getString("UserName"));
        configuration.setSystem(resultSet.getBoolean("IsSystem"));
        configuration.setTicks(resultSet.getInt("Ticks"));
        return configuration;
    }
}
