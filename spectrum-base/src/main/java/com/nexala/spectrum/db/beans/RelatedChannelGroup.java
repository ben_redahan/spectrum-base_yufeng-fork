package com.nexala.spectrum.db.beans;

public class RelatedChannelGroup {

    private Integer id;
    
    private String displayName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    public String toString() {
        return this.displayName;
    }
}
