package com.nexala.spectrum.db.beans;

public class FaultGroupInputParam {

    private Long faultId;
    
    private Integer faultGroupId;
    
    private boolean isFaultGroupLead;
    
    private Long faultRowVersion;

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public Integer getFaultGroupId() {
        return faultGroupId;
    }

    public void setFaultGroupId(Integer faultGroupId) {
        this.faultGroupId = faultGroupId;
    }

    public boolean getIsFaultGroupLead() {
        return isFaultGroupLead;
    }

    public void setIsFaultGroupLead(boolean isFaultGroupLead) {
        this.isFaultGroupLead = isFaultGroupLead;
    }

    public Long getFaultRowVersion() {
        return faultRowVersion;
    }

    public void setFaultRowVersion(Long faultRowVersion) {
        this.faultRowVersion = faultRowVersion;
    }
    
}
