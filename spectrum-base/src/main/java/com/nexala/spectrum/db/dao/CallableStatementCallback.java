package com.nexala.spectrum.db.dao;

import java.sql.CallableStatement;

public interface CallableStatementCallback<T> {
	T execute(CallableStatement cs);
}
