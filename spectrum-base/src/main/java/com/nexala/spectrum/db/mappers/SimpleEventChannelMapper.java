package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.EventChannelValue;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;

public class SimpleEventChannelMapper extends JdbcRowMapper<EventChannelValue> {

    private final BoxedResultSetFactory factory;
    
    @Inject
    public SimpleEventChannelMapper(
            BoxedResultSetFactory factory) {
        this.factory = factory;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public EventChannelValue createRow(ResultSet resultSet) throws SQLException {
        BoxedResultSet rs = factory.create(resultSet);
        
        Long id;
        try {
            id = rs.getLong("ID");
        } catch (java.sql.SQLException e) {
            id = null;
        }
        
        Long timestamp;
        try {
            timestamp = rs.getTimestamp("TimeStamp");
        } catch (java.sql.SQLException e) {
            timestamp = null;
        }
        
        Integer unitId;
        try {
            unitId = rs.getInt("UnitID");
        } catch (java.sql.SQLException e) {
            unitId = null;
        }
        
        Integer channelId;
        try {
            channelId = rs.getInt("ChannelID");
        } catch (java.sql.SQLException e) {
            channelId = null;
        }
        
        Boolean value;
        try {
            value = rs.getBoolean("Value");
        } catch (java.sql.SQLException e) {
            value = null;
        }
        
        return new EventChannelValue(id, timestamp, unitId, channelId, value);
    }
}
