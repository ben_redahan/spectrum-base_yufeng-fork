/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.mappers.StockDescMapper;
import com.nexala.spectrum.rest.data.beans.StockDesc;

public class StockDao extends GenericDao<StockDesc, Integer> {
    
    @Inject
    private StockDescMapper stockMapper;
    
    @Inject
    private ApplicationConfiguration configuration;
    
    public List<StockDesc> getSiblings(int stockId) {
        QueryManager qm = new QueryManager();
        return findByStoredProcedure(qm.getQuery(Spectrum.STOCK_SIBLINGS), stockId);
    }
    
    public List<StockDesc> getStocksByEvent(int eventId) {
        String query = configuration.getQuery(Spectrum.GET_STOCKS_BY_EVENT);
        return findByQuery(query, eventId);
    }
    
    @Override
    public RowMapper<StockDesc> getRowMapper() {
        return stockMapper;
    }
}
