package com.nexala.spectrum.db.beans;

import com.nexala.spectrum.db.dao.Record;

public class RecoveryAuditItemType implements Record {
    private long id;
    private boolean question;
    private String itemText;
    private String answer;
    private boolean userAction;
    private boolean reversion;
    private boolean outcome;
    private boolean execution;
    private boolean nextStep;
    
    public RecoveryAuditItemType() {
        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }

    public String getItemText() {
        return itemText;
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isUserAction() {
        return userAction;
    }

    public void setUserAction(boolean userAction) {
        this.userAction = userAction;
    }

    public boolean isReversion() {
        return reversion;
    }

    public void setReversion(boolean reversion) {
        this.reversion = reversion;
    }

    public boolean isOutcome() {
        return outcome;
    }

    public void setOutcome(boolean outcome) {
        this.outcome = outcome;
    }

    public boolean isExecution() {
        return execution;
    }

    public void setExecution(boolean execution) {
        this.execution = execution;
    }

    public boolean isNextStep() {
        return nextStep;
    }

    public void setNextStep(boolean nextStep) {
        this.nextStep = nextStep;
    }
}
