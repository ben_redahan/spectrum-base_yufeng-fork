package com.nexala.spectrum.db.beans;

import java.util.Date;
import java.util.List;

/**
 * Bean for fleet formation table.
 * @author BBaudry
 *
 */
public class FleetFormation {

    private Integer id;

    private List<Integer> unitId;

    private List<String> unitNumber;

    private Date validFrom;

    private Date validTo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getUnitId() {
        return unitId;
    }

    public void setUnitId(List<Integer> unitId) {
        this.unitId = unitId;
    }

    public List<String> getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(List<String> unitNumber) {
        this.unitNumber = unitNumber;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
}
