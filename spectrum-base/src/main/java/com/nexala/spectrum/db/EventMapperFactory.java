/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.db;

import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.db.mappers.EventMapper;

/**
 * Factory to instanciate a {@link EventMapper}
 * 
 * @author Fulvio
 *
 */
public interface EventMapperFactory {
    EventMapper create(@Assisted String screen);
}
