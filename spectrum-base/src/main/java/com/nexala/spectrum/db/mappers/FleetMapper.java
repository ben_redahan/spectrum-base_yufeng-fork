package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.Fleet;

public class FleetMapper extends JdbcRowMapper<Fleet> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Fleet createRow(ResultSet rs) throws SQLException {
        return new Fleet(
        		rs.getInt("ID"),
                rs.getString("Name"),
                rs.getString("Code"));
    }
}
