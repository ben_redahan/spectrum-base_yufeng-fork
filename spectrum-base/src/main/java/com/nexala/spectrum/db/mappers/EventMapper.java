/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.configuration.EventConfig;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventField;

public class EventMapper extends JdbcRowMapper<Event> {

    private final String screen;
    
    @Inject
    BoxedResultSetFactory factory;

    @Inject
    EventConfig config;

    @Inject
    public EventMapper(@Assisted String screen) {
        this.screen = screen;
    }

    @Override
    public Event createRow(ResultSet r) throws SQLException {

        BoxedResultSet rs = factory.create(r);
        Event record = new Event();

        List<EventField> eventFields = config.getFields();

        String fleetId = rs.getString("FleetCode");
        record.setField("fleetId", fleetId);

        for (EventField field : eventFields) {
            List<String> fleets = field.getFleets();
            List<String> screens = field.getScreens();
            if ((fleetId != null && fleets.size() > 0 && !fleets.contains(fleetId)) 
                    || (screens.size() > 0 && !screens.contains(this.screen))) {
                continue;
            }
            String fieldId = field.getId();
            String dbName = field.getDbName();
            String type = field.getType();
            record.setField(fieldId, extractField(rs, dbName, type));
        }

        return record;
    }
}
