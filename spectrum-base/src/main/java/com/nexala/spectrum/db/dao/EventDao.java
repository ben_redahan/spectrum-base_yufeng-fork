/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.OptimisticLockException;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.EventMapperFactory;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.beans.FaultGroupInputParam;
import com.nexala.spectrum.db.mappers.EventMapper;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;
import com.nexala.spectrum.utils.DateUtils;
import com.typesafe.config.Config;

public class EventDao extends GenericDao<Event, Long> {
    
    /** To be used with stored procedures that return a single integer in its result set */
    private static class CountDao extends GenericDao<Integer, Integer> {
        class CountMapper extends JdbcRowMapper<Integer> {
            @Override
            public Integer createRow(ResultSet resultSet) throws SQLException {
                return resultSet.getInt(1);
            }
        }
        
        public List<Integer> getCount(String query, Object... parameters) {
            return findByStoredProcedure(query, new CountMapper(), parameters);
        }
        
    }
    
    @Inject
    private CountDao countDao;
    
    @Inject
    private TimestampDao timestampDao;
    
    @Inject
    private ApplicationConfiguration appConf;

    @Inject
    protected DateUtils dateUtils;
    
    protected final EventMapper genericEventMapper;
    
    private final EventMapper eventDetailMapper;
    
    private final EventMapper eventHistoryMapper;

    private final EventMapper unitEventMapper;

    private final EventMapper eventNotificationMapper;

    private final EventMapper liveAndRecentEventMapper;
    
    private FaultMetaCategoryOptionalDao faultMetaCategoryOptionalDao;
    
    @Inject
    public EventDao(EventMapperFactory eventMapperFactory, FaultMetaCategoryOptionalDao faultMetaCategoryOptionalDao) {
        this.genericEventMapper = eventMapperFactory.create("generic");
        this.eventDetailMapper = eventMapperFactory.create("eventDetail");
        this.eventHistoryMapper = eventMapperFactory.create("eventHistory");
        this.unitEventMapper = eventMapperFactory.create("unitEvent");
        this.eventNotificationMapper = eventMapperFactory.create("eventNotification");
        this.liveAndRecentEventMapper = eventMapperFactory.create("liveAndRecentEvents");
        this.faultMetaCategoryOptionalDao = faultMetaCategoryOptionalDao;
    }
    
    @Override
    public RowMapper<Event> getRowMapper() {
        return genericEventMapper;
    }

    public void update(Integer eventId, String action, String comment, String userId, Long timestamp, String method) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_UPDATE);

        executeStoredProcedure(query, eventId, comment, userId, action, method,
                (timestamp != null ? new java.sql.Timestamp(timestamp) : null));
    }
    
    /**
     * Sets a fault to Acknowledged in the Fault table
     * @param eventId - the ID on the Fault table of the event
     * @param rowVersion - the version number
     */
    public void acknowledge(int eventId, long rowVersion) {
    	String query = appConf.getQuery(Spectrum.EVENT_ACKNOWLEDGE);
    	Date acknowledgeTime = new Date();
    	java.sql.Timestamp sqlAcknowledgeTime = new java.sql.Timestamp(acknowledgeTime.getTime());
        Connection c = getConnection();

        PreparedStatement ps = null;

        try {
            ps = c.prepareStatement(query);
            ps.setTimestamp(1, sqlAcknowledgeTime);
            ps.setInt(2, eventId);
            ps.setLong(3, rowVersion);
            int executeUpdate = ps.executeUpdate();
            
            if (executeUpdate == 0) {
                // The row wasn't updated because of concurrent change
                throw new OptimisticLockException("The event was modified by another user");
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    /**
     * Sets a fault group to Acknowledged in the Fault table
     * @param eventGroupId - the ID of the group the event is in on the Fault table 
     */
    public void acknowledgeGroup(Integer eventGroupId) {
        String query = appConf.getQuery(Spectrum.EVENT_GROUP_ACKNOWLEDGE);
        Date acknowledgeTime = new Date();
        java.sql.Timestamp sqlAcknowledgeTime = new java.sql.Timestamp(acknowledgeTime.getTime());
        Connection c = getConnection();

        PreparedStatement ps = null;

        try {
            ps = c.prepareStatement(query);
            ps.setTimestamp(1, sqlAcknowledgeTime);
            ps.setInt(2, eventGroupId);
            int executeUpdate = ps.executeUpdate();
            
            if (executeUpdate == 0) {
                // The row wasn't updated because of concurrent change
                throw new OptimisticLockException("The event was modified by another user");
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    /**
     * Sets a fault to Deactivated in the Fault table
     * @param eventId - the ID on the Fault table of the event
     * @param userName 
     * @param rowVersion the version number
     */
    public void deactivate(int eventId, String userName, Boolean isCurrent, long rowVersion) {
    	String query = new QueryManager().getQuery(Spectrum.EVENT_DEACTIVATE);
    	
    	Date deactivateTime = new Date();
    	
    	Integer updatedRows = getIntByStoredProcedure(query, eventId, isCurrent, userName, rowVersion, deactivateTime);
    	
    	if (updatedRows == 0) {
    	    // The row wasn't updated because of concurrent change
    	    throw new OptimisticLockException("The event was modified by another user");
    	}
    }

    /**
     * Returns an event from an event Id. An event can be a fault, warning or
     * event.
     * 
     * @param Long eventId
     * @return Event
     */
    public Event getEventById(Long eventId) {
        Event event = new Event();
        String query = new QueryManager()
                .getQuery(Spectrum.EVENT_DETAIL);
        List<Event> events = findByStoredProcedure(query, eventDetailMapper,
                eventId);
        if(events != null && events.size() > 0) {
            event = events.get(0);
        }
        return event;
    }
    
    public List<Event> getCurrent(Integer limit, List<Object> searchCriteria) {
        String query = new QueryManager().getQuery(Spectrum.CURRENT_EVENTS);
        
        List<Object> params = new ArrayList<Object>();
        params.add(limit);
        
        for(Object criteria : searchCriteria) {
            params.add(criteria);
        }

        List<Event> events = findByStoredProcedure(query, liveAndRecentEventMapper, params.toArray());
        
        return events;
    };

    public List<Event> getEventsSince(long timestamp) {
        String query = new QueryManager().getQuery(Spectrum.EVENTS_SINCE);
        List<Event> events = findByStoredProcedure(query, eventNotificationMapper, new Date(
                timestamp));
        return events;
    }
    
    public Integer getEventCountLive(List<Object> searchCriteria) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_COUNT_LIVE);        
        
        List<Integer> counts = countDao.getCount(query, searchCriteria.toArray());

        if (counts.size() > 0) {
            return counts.get(0);
        } else {
            return 0;
        }
    }
    
    public Integer getEventCountSince(long timestamp) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_COUNT_LIVE);
        List<Integer> counts = countDao.getCount(query, new Date(timestamp));
        
        if (counts.size() > 0) {
            return counts.get(0);
        } else {
            return 0;
        }
    }
    
    /**
     * Returns a list of ALL LIVE events. An event can be a fault, warning or
     * event.
     * 
     * @param stockId
     * @return List<Event>
     */
    public List<Event> getAllLive() {
        String query = new QueryManager().getQuery(Spectrum.LIVE_EVENTS_QUERY);
        List<Event> events = findByQuery(query, genericEventMapper);
        
        return events;
    }

    /**
     * Returns a list of ALL events for a unit. An event can be a fault, warning or
     * event.
     * 
     * @param stockId
     * @return List<Event>
     */
    public List<Event> allByUnit(Integer stockId) {
        String query = new QueryManager().getQuery(Spectrum.ALL_EVENTS_UNIT_QUERY);
        List<Event> events = findByQuery(query, unitEventMapper, stockId);
        
        return findOptionalCategories(events);
    }

    /**
     * Returns a list of ALL LIVE events. An event can be a fault, warning or
     * event.
     * 
     * @param stockId
     * @return List<Event>
     */
    public List<Event> liveByUnit(Integer stockId) {
        String query = new QueryManager().getQuery(Spectrum.LIVE_EVENTS_UNIT_QUERY);
        List<Event> events = findByQuery(query, unitEventMapper, stockId);
        
        return findOptionalCategories(events);
    }

    /**
     * Returns a list of NOT LIVE events. An event can be a fault, warning or
     * event.
     * 
     * @param stockId
     * @param count
     *            number of events to return
     * @return List<Event>
     */
    public List<Event> notLiveByUnit(Integer stockId, int count) {
        String query = new QueryManager().getQuery(Spectrum.NOT_LIVE_EVENTS_UNIT_QUERY);
        List<Event> events = findByStoredProcedure(query, unitEventMapper, count, stockId);
        
        return findOptionalCategories(events);
    }
    
    /**
     * Returns a list of ALL LIVE & NOT ACKNOWLEDGED events. An event can be a fault, warning or
     * event.
     * 
     * @param stockId
     * @return List<Event>
     */
    public List<Event> notAcknowledgedByUnit(Integer stockId) {
        String query = new QueryManager().getQuery(Spectrum.NOT_ACKNOWLEDGED_EVENTS_UNIT_QUERY);
        List<Event> events = findByStoredProcedure(query, unitEventMapper, stockId);
        
        return findOptionalCategories(events);
    }
    
    /**
     * Returns a list of ALL LIVE & ACKNOWLEDGED events. An event can be a fault, warning or
     * event.
     * 
     * @param stockId
     * @return List<Event>
     */
    public List<Event> acknowledgedByUnit(Integer stockId) {
        String query = new QueryManager().getQuery(Spectrum.ACKNOWLEDGED_EVENTS_UNIT_QUERY);
        List<Event> events = findByStoredProcedure(query, unitEventMapper, stockId);
        
        return findOptionalCategories(events);
    }

    public List<Event> searchEvents(Integer limit, Date dateFrom, Date dateTo, String live, String keyword) {
        return searchEvents(eventHistoryMapper, limit, dateFrom, dateTo, live, keyword);
    }
    
    protected List<Event> searchEvents(RowMapper<Event> mapper, Integer limit, Date dateFrom, 
            Date dateTo, String live, String keyword) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_HISTORY_QUERY);
        return findByStoredProcedure(query, mapper, limit, dateFrom, dateTo, live, keyword);
    }
    
    public List<Event> searchEvents(Integer limit, Date dateFrom, Date dateTo, String live, String code, String type,
            String description, String category, List<EventSearchParameter> parameters) {
        
        // Convert the .* character to % and . character to _ for fault code
        code = code.replaceAll("\\.\\*", "%");
        code = code.replaceAll("\\.", "_");
        
        for (EventSearchParameter i : parameters) {
            if (i.getField().equals("unitNumber")) {
                String unitNumber = i.getSearchString();
                unitNumber = unitNumber.replaceAll("\\.\\*", "%");
                unitNumber = unitNumber.replaceAll("\\.", "_");
                i.setSearchString(unitNumber);
            }
        }
        
        return searchEvents(eventHistoryMapper, limit, dateFrom, dateTo, live, code, type,
                description, category, parameters);
    }

    protected List<Event> searchEvents(RowMapper<Event> mapper, Integer limit, Date dateFrom, Date dateTo, String live, String code, String type,
            String description, String category, List<EventSearchParameter> parameters) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_HISTORY_ADVANCED_QUERY);
        query = query.replace("__ADDITIONAL_FIELDS__", params(parameters));
        
        ArrayList<Object> qparams = new ArrayList<Object>();
        qparams.add(limit);
        qparams.add(dateFrom);
        qparams.add(dateTo);
        qparams.add(live);
        qparams.add(code);
        qparams.add(type);
        qparams.add(category);
        qparams.add(formatStringForSearch(description));
        
        for (EventSearchParameter parameter : parameters) {
            qparams.add(parameter.getSearchString().trim().equals("") ? null
                    : formatStringForSearch(parameter.getSearchString()));
        }
        
        return findByStoredProcedure(query,mapper, qparams.toArray());
        
    }

    private String params(List<EventSearchParameter> parameters) {
        StringBuilder sb = new StringBuilder();
        
        for (EventSearchParameter param : parameters) {
            sb.append(", ?");
        }
        
        return sb.toString();
    }

    private String formatStringForSearch(String original) {
        String formattedString = "";
        Config config = appConf.get();
        if (config.hasPath("r2m.eventHistory.useWildcardsInSearchFields")
                && config.getBoolean("r2m.eventHistory.useWildcardsInSearchFields")) {
            formattedString = original.replaceAll("\\.\\*", "%");
            formattedString = formattedString.replaceAll("\\.", "_");
            return formattedString;
        } else {
            return original;
        }
    }

    /**
     * createFault
     * 
     * @param faultCode
     * @param timestamp
     * @param vehicleId
     * @throws SqlRuntimeException
     *             if execution fails for any reason. This happen if the fault
     *             is a duplicate, the vehicle info is invalid or if ChannelData
     *             doesn't exist for the vehicle...
     * @return
     */
	public long createFault(String faultCode, Long timestamp, Integer sourceId, String ruleName, String contextId, String fleetId) {
        String query = new QueryManager().getQuery(Spectrum.CREATE_EVENT_STMT);
        Connection c = getConnection();

        CallableStatement ps = null;

        long faultId = -1;

        try {
            ps = c.prepareCall(query);
            ps.registerOutParameter(1, Types.INTEGER);
            ps.setString(2, faultCode);
            ps.setString(3, fleetId);
            // FIXME - should use GenericDao to insert fault, rather than using PreparedStatement directly.
            ps.setTimestamp(4, new Timestamp(timestamp), dateUtils.getDatabaseCalendar());
            ps.setInt(5, sourceId);
            ps.setString(6, ruleName);
            ps.setString(7, contextId);
            ps.execute();

            faultId = ps.getInt(1);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        if (faultId < 1) {
            String info = faultId + " : Failed to insert fault: " + faultCode + " @ " + timestamp + " source: "
                    + sourceId;

            throw new DataAccessException(info);
        } else {
            logger.debug("Created fault with id: " + faultId);
        }

        return faultId;
    }
    
    /**
     * createNewFault - manually created fault from fleet summary live.recent events panel, passes end timestamp too
     * 
     * @param faultCode
     * @param timestamp
     * @param vehicleId
     * @throws SqlRuntimeException
     *             if execution fails for any reason. This happen if the fault
     *             is a duplicate, the vehicle info is invalid or if ChannelData
     *             doesn't exist for the vehicle...
     * @return
     */
    public long createNewFault(String faultCode, String fleetId, Long timestamp, Integer sourceId, String contextId) {
        String query = new QueryManager().getQuery(Spectrum.CREATE_EVENT_STMT_NOTCURRENT);
        Connection c = getConnection();

        CallableStatement ps = null;

        long faultId = -1;

        try {
            ps = c.prepareCall(query);
            ps.registerOutParameter(1, Types.INTEGER);
            ps.setString(2, faultCode);
            ps.setString(3, fleetId);
            // FIXME - should use GenericDao to insert fault, rather than using PreparedStatement directly.
            ps.setTimestamp(4, new Timestamp(timestamp), dateUtils.getDatabaseCalendar()); // create time
            ps.setTimestamp(5, new Timestamp(timestamp), dateUtils.getDatabaseCalendar()); // end time
            ps.setInt(6, sourceId);
            ps.setString(7,  "Manually Created");
            ps.setString(8, contextId);
            ps.execute();

            faultId = ps.getInt(1);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        if (faultId < 1) {
            String info = faultId + " : Failed to insert fault: " + faultCode + " @ " + timestamp + " source: "
                    + sourceId;

            throw new DataAccessException(info);
        } else {
            logger.debug("Created fault with id: " + faultId);
        }

        return faultId;
    }

    /**
     * Creates a new delayed fault in the database.
     * 
     * @param faultCode
     *            the fault code.
     * @param createTime
     *            the time the fault was created.
     * @param endTime
     *            the time the fault was retracted.
     * @param sourceId
     *            the number of the fault vehicle.
     * @throws SqlRuntimeException
     *             if execution fails for any reason. This happen if the fault
     *             is a duplicate, the vehicle info is invalid or if ChannelData
     *             doesn't exist for the vehicle...
     * @return the ID of the new fault.
     */
    /*public long createDelayedFault(String faultCode, Long createTime,
            Long endTime, Integer sourceId) {
        String query = new QueryManager()
                .getQuery(Spectrum.CREATE_DELAYED_FAULT_STMT);
        Connection c = getConnection();

        CallableStatement ps = null;

        long faultId = -1;

        try {
            ps = c.prepareCall(query);
            ps.registerOutParameter(1, Types.INTEGER);
            ps.setString(2, faultCode);
            ps.setTimestamp(3, new Timestamp(createTime),
                    Calendar.getInstance(TimeZone.getTimeZone(props
                            .get("db.timezone"))));
            ps.setTimestamp(4, new Timestamp(endTime),
                    Calendar.getInstance(TimeZone.getTimeZone(props
                            .get("db.timezone"))));
            ps.setInt(5, sourceId);
            ps.execute();

            faultId = ps.getInt(1);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        if (faultId < 1) {
            String errorMsg = "Error inserting fault, not inserted";

            if (faultId == -1) {
                errorMsg = "Duplicate fault, not inserted";
            } else if (faultId == -3) {
                errorMsg = "Live fault overlapping, not inserted";
            } else if (faultId == -4) {
                errorMsg = "Incorrect @FaultLevelType (V, U or X), not inserted";
            }

            logger.debug(errorMsg);
        }

        return faultId;
    }*/

    public void retractFault(long faultId, long timestamp) {
        logger.debug("Retracting fault with id: " + faultId);

        String query = new QueryManager().getQuery(Spectrum.RETRACT_EVENT_STMT);
        Connection c = getConnection();

        PreparedStatement ps = null;

        int modCount = 0;

        try {
            ps = c.prepareStatement(query);
            ps.setTimestamp(1, new Timestamp(timestamp), dateUtils.getDatabaseCalendar());
            ps.setLong(2, faultId);
            modCount = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        if (modCount != 1) {
            throw new DataAccessException("Error. RetractFault modified " + modCount + " records");
        }
    }

    public void retractFaults() {
        String query = new QueryManager().getQuery(Spectrum.RETRACT_EVENTS_STMT);
        Connection c = getConnection();

        PreparedStatement ps = null;

        int modCount = 0;

        try {
            ps = c.prepareStatement(query);
            modCount = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        logger.debug(modCount + " faults retracted");
    }

    public void retractFaults(String type) {
        String query = new QueryManager().getQuery(Spectrum.RETRACT_EVENTS_TYPE_STMT);
        Connection c = getConnection();

        PreparedStatement ps = null;

        int modCount = 0;

        try {
            ps = c.prepareStatement(query);
            ps.setString(1, type);
            modCount = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        logger.debug(modCount + " faults retracted");
    }
    
    public boolean areFaultsRetracted(Long timestamp){
        Connection c = getConnection();
        CallableStatement proc;

        int result = 0;

        try {
            proc = c.prepareCall(new QueryManager().getQuery(Spectrum.FAULT_RETRACTED_QUERY_STMT));
            proc.setTimestamp(1, new java.sql.Timestamp(timestamp));
            ResultSet rs = proc.executeQuery();
            if(rs.next()){
                result = rs.getInt("Flag");
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeConnection(c);
        }

        return result > 0;
    }
    
    public Long getLastLiveEventTimestamp() {
        String query = new QueryManager().getQuery(Spectrum.LIVE_EVENTS_TIMESTAMP);
        List<Long> timestamp = timestampDao.getTimestampByQuery(query);
        return timestamp.size() > 0 ? timestamp.get(0) : null;
    }

    public Long getLastRecentEventTimestamp() {
        String query = new QueryManager().getQuery(Spectrum.RECENT_EVENTS_TIMESTAMP);
        List<Long> timestamp = timestampDao.getTimestampByQuery(query);
        return timestamp.size() > 0 ? timestamp.get(0) : null;
    }

    private List<Integer> getEventOptionalCategories(Integer faultMetaId) {
        return faultMetaCategoryOptionalDao
                .getOptionalCategoryList(faultMetaId);
    }

    protected List<Event> findOptionalCategories(List<Event> events) {
        for (Event e : events) {
            e.setField("optionalCategoryIdList", getEventOptionalCategories((Integer) e.getField(Spectrum.EVENT_FAULT_META)));
        }
        return events;
    }
    
    public void groupFaults(String username, List<FaultGroupInputParam> params, Long leadEventId) {
        Connection c = null;
        PreparedStatement ps = null;

        try {
            c = this.getConnection();

            // transaction start
            c.setAutoCommit(false);
            
            // create new fault group
            Long newFaultGroupId = null;
            
            ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_CREATE_FAULT_GROUP), Statement.RETURN_GENERATED_KEYS);
            this.addParameterToPreparedStatement(ps, 1, username);
            
            Integer rowsUpdated = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rowsUpdated.equals(1) && rs.next()) {
                newFaultGroupId = rs.getLong(1);
            } else {
                throw new SQLException("Creating Fault Group failed, no ID obtained.");
            }

            List<Long> recordsAlreadyUpdated = new ArrayList<>();
            for (FaultGroupInputParam param : params) {
                // if the event is the lead of another group, delete the old group
                if (param.getIsFaultGroupLead()) {
                    Integer faultGroupId = param.getFaultGroupId();
                    
                    ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_REMOVE_ALL_FROM_GROUP));
                    this.addParameterToPreparedStatement(ps, 1, faultGroupId);
                    ps.executeUpdate();
                    
                    ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_DELETE_FAULT_GROUP));
                    this.addParameterToPreparedStatement(ps, 1, faultGroupId);
                    ps.executeUpdate();
                    
                    // check if any of the other records were part of the destroyed group
                    // since they were updated here, we need to get the new rowversion for the optimistic locking check
                    for (FaultGroupInputParam p : params) {
                        if (faultGroupId.equals(p.getFaultGroupId())) {
                            recordsAlreadyUpdated.add(p.getFaultId());
                        }
                    }
                }
                
                boolean isLead = false;
                if (param.getFaultId().equals(leadEventId)) {
                    isLead = true;
                }

                if (recordsAlreadyUpdated.contains(param.getFaultId())) {
                    // We updated this fault from destroying the group
                    // so do not use optimistic locking here
                    ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_ADD_FAULT_TO_GROUP));
                    this.addParameterToPreparedStatement(ps, 1, newFaultGroupId);
                    this.addParameterToPreparedStatement(ps, 2, isLead);
                    this.addParameterToPreparedStatement(ps, 3, param.getFaultId());
                    
                    ps.executeUpdate();
                } else {
                    ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_ADD_FAULT_TO_GROUP_WITH_ROWVERSION));
                    this.addParameterToPreparedStatement(ps, 1, newFaultGroupId);
                    this.addParameterToPreparedStatement(ps, 2, isLead);
                    this.addParameterToPreparedStatement(ps, 3, param.getFaultId());
                    this.addParameterToPreparedStatement(ps, 4, param.getFaultRowVersion());
                    
                    int rowCount = ps.executeUpdate();
                    
                    if (rowCount == 0) {
                        // The row wasn't updated because of concurrent change, so we rollback
                        c.rollback();
                        
                        throw new OptimisticLockException("An event in this group was modified by another user");
                    }
                }
            }
            
            // transaction ok
            c.commit();
        } catch (SQLException e) {
            if (c != null) {
                try {
                    c.rollback();
                } catch (SQLException ex1) {
                    logger.error("Error rolling back database changes", ex1);
                }
            }
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    public void updateGroup(String username, List<FaultGroupInputParam> params, Long faultGroupId) {
    	Connection c = null;
        PreparedStatement ps = null;

        try {
            c = this.getConnection();

            // transaction start
            c.setAutoCommit(false);

            for (FaultGroupInputParam param : params) {
            	
            	if (param.getFaultGroupId() == null) {
            		ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_ADD_FAULT_TO_GROUP_WITH_ROWVERSION));
                    this.addParameterToPreparedStatement(ps, 1, faultGroupId);
                    this.addParameterToPreparedStatement(ps, 2, param.getIsFaultGroupLead());
                    this.addParameterToPreparedStatement(ps, 3, param.getFaultId());
                    this.addParameterToPreparedStatement(ps, 4, param.getFaultRowVersion());
                    
                    int rowCount = ps.executeUpdate();
                    
                    if (rowCount == 0) {
                        // The row wasn't updated because of concurrent change, so we rollback
                        c.rollback();
                        
                        throw new OptimisticLockException("An event in this group was modified by another user");
                    }
            	}
            	                
            }
            
            // transaction ok
            c.commit();
        } catch (SQLException e) {
            if (c != null) {
                try {
                    c.rollback();
                } catch (SQLException ex1) {
                    logger.error("Error rolling back database changes", ex1);
                }
            }
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    public void updateGroupWithNewLead(String username, List<FaultGroupInputParam> params, Long faultGroupId) {
        Connection c = null;
        PreparedStatement ps = null;

        try {
            c = this.getConnection();

            // transaction start
            c.setAutoCommit(false);
            
            Boolean newServiceRequest = false;
            outer:
            for (FaultGroupInputParam param : params) {
                if ((param.getFaultGroupId() == null) && (param.getIsFaultGroupLead() == true)) {
                    newServiceRequest = true;
                    break outer;
                }
            }

            for (FaultGroupInputParam param : params) {
                ps = c.prepareStatement(appConf.getQuery(Spectrum.EVENT_ADD_FAULT_TO_GROUP));
                this.addParameterToPreparedStatement(ps, 1, faultGroupId);
                this.addParameterToPreparedStatement(ps, 2, param.getIsFaultGroupLead());
                this.addParameterToPreparedStatement(ps, 3, param.getFaultId());
                    
                int rowCount = ps.executeUpdate();
            }
            
            // transaction ok
            c.commit();
        } catch (SQLException e) {
            if (c != null) {
                try {
                    c.rollback();
                } catch (SQLException ex1) {
                    logger.error("Error rolling back database changes", ex1);
                }
            }
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    public List<Event> getEventsByGroup(Integer groupId) {
        String query = new QueryManager().getQuery(Spectrum.GET_EVENTS_BY_GROUP_ID);
        return findByStoredProcedure(query, eventHistoryMapper, groupId); 
    }
    
    public void insertFaultExternalReference(Integer eventId, String systemName, String code) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_INSERT_EXTERNAL_REFERENCE);
        
        executeStoredProcedure(query, eventId, systemName, code);
    }
    
    /**
     * Updates the LastUpdateTime column in the Fault table for a certain event
     * @param eventId - the ID on the Fault table of the event
     * @param rowVersion - the version number
     */
    public void updateLastUpdateTime(int eventId, long rowVersion) {
        String query = appConf.getQuery(Spectrum.EVENT_UPDATE_LAST_UPDATE_TIME);
        Connection c = getConnection();

        PreparedStatement ps = null;

        try {
            ps = c.prepareStatement(query);
            ps.setInt(1, eventId);
            ps.setLong(2, rowVersion);
            int executeUpdate = ps.executeUpdate();
            
            if (executeUpdate == 0) {
                // The row wasn't updated because of concurrent change
                throw new OptimisticLockException("The event was modified by another user");
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    /**
     * Returns the end time of the event in question
     * event.
     * 
     * @param eventId
     * @return endTime
     */
    public Date getEndTime(Integer eventId) {
        String query = appConf.getQuery(Spectrum.EVENT_GET_ENDTIME);
        Date endTime = getDateByQuery(query, eventId);
        return endTime;
    }
    
    public List<Event> liveEventsFormationByUnit(Integer stockId) {
        String query = appConf.getQuery(Spectrum.LIVE_EVENTS_FORMATION_QUERY);
        List<Event> events = findByQuery(query, unitEventMapper, stockId);
        
        return findOptionalCategories(events);
    }

    public List<Event> notLiveEventsFormationByUnit(Integer stockId, int count) {
        String query = appConf.getQuery(Spectrum.NOT_LIVE_EVENTS_FORMATION_QUERY);
        List<Event> events = findByQuery(query, unitEventMapper, stockId, count);
        
        return findOptionalCategories(events);
    }
    
    public void updateSelectedPeriodValue(Integer eventId, Integer vehicleId, Integer period, Long timeStamp) {
    	String query = new QueryManager().getQuery(Spectrum.UPDATE_CHANNEL_VALUE_AND_FAULT);
    	executeStoredProcedure(query, vehicleId,period/1000, new Timestamp (timeStamp),eventId);
  }
  
}
