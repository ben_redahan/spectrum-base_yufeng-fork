/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.db.mappers.UserConfigurationMapper;

public class UserConfigurationDao extends GenericDao<UserConfiguration, Integer> {
    
    @Inject
    private UserConfigurationMapper userConfigMapper;
    
    @Inject
    private QueryManager qm;
    
    /**
     * Get the user configuration in database, for an user and variable name
     * If the user configuration don't exist, it's created
     * @param username the username
     * @param variable the variable
     * @return the user configuration
     */
    public UserConfiguration getUserConfiguration(String username, String variable) {
        UserConfiguration result = null;
        
        List<UserConfiguration> userConfigs = findByQuery(qm.getQuery(Spectrum.GET_USER_CONFIG_QUERY), username, variable);
        
        if(userConfigs.size() == 1) {
            result = userConfigs.get(0);
        } else {
            // Create the configuration in db
            executeUpdateByQuery(qm.getQuery(Spectrum.INSERT_USER_CONFIG_QUERY), username, variable);
            result = findByQuery(qm.getQuery(Spectrum.GET_USER_CONFIG_QUERY), username, variable).get(0);
        }
        
        return result;
    }
    
    public void updateUserConfiguration(String username, String variable, String data) {
        QueryManager qm = new QueryManager();
        executeUpdateByQuery(qm.getQuery(Spectrum.UPDATE_USER_CONFIG_QUERY), data, username, variable);
    }
    
    
    @Override
    public RowMapper<UserConfiguration> getRowMapper() {
        return userConfigMapper;
    }
}
