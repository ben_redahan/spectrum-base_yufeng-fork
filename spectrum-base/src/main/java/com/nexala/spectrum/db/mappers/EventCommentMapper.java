/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;

public class EventCommentMapper extends JdbcRowMapper<EventComment> {
    @Inject
    private BoxedResultSetFactory factory;

    @Override
    public EventComment createRow(ResultSet r) throws SQLException {
        BoxedResultSet rs = factory.create(r);

        return new EventComment(
                rs.getInt("FaultID"),
                rs.getTimestamp("Timestamp"),
                rs.getString("UserName"),
                rs.getString("Comment"),
                rs.getString("Action")
        );
    }
}
