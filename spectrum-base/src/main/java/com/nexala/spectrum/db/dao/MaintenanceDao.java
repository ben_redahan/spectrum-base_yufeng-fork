package com.nexala.spectrum.db.dao;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.mappers.GenericBeanMapper;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;

public class MaintenanceDao extends GenericDao<GenericBean, Long> {
    
    private final GenericBeanMapper restrictionsMapper;
    
    private final GenericBeanMapper workOrderMapper;
    
    private final ApplicationConfiguration appConf;
    
    @Inject
    public MaintenanceDao(UnitDetailConfiguration unitDetailConfiguration, 
            ApplicationConfiguration appConf, BoxedResultSetFactory factory) {
        List<BeanField> workOrderFields = unitDetailConfiguration.getWorkOrderFields();
        this.workOrderMapper = new GenericBeanMapper(workOrderFields, factory);
        
        List<BeanField> restrictionsFields = unitDetailConfiguration.getRestrictionFields();
        this.restrictionsMapper = new GenericBeanMapper(restrictionsFields, factory);
        
        this.appConf = appConf;
    }
    
    @Override
    public JdbcRowMapper<GenericBean> getRowMapper() {
        return workOrderMapper;
    }
    
    public List<GenericBean> getWorkOrdersByUnitId(int unitId) {
        String query = appConf.getQuery(Spectrum.GET_WORK_ORDERS);
        return findByQuery(query, workOrderMapper, unitId);
    };
    
    public List<GenericBean> getWorkOrdersByUnitId(String unitId) {
        String query = appConf.getQuery(Spectrum.GET_WORK_ORDERS);
        return findByQuery(query, workOrderMapper, unitId);
    };
    
    public List<String> getWorkOrderCommentByWONumber(int woNumber) {
        String query = appConf.getQuery(Spectrum.GET_WORK_ORDER_COMMENT);
        return getStringListByQuery(query, woNumber);
    };
    
    public List<GenericBean> getRestrictionsByUnitId(int unitId) {
        String query = appConf.getQuery(Spectrum.GET_RESTRICTIONS);
        return findByQuery(query, restrictionsMapper, unitId);
    };
    
    public List<GenericBean> getRestrictionsByUnitId(String unitId) {
        String query = appConf.getQuery(Spectrum.GET_RESTRICTIONS);
        return findByQuery(query, restrictionsMapper, unitId);
    };
}
