/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.db.RowMapper;

public interface HasCrudOperations<T, ID extends Serializable> {
    ID create(Map<String, Object> columnValues) throws DataAccessException;

    ID create(T b) throws DataAccessException;

    void delete(ID id) throws DataAccessException;

    List<T> findAll(RowMapper<T> mapper) throws DataAccessException;

    T findById(ID id, RowMapper<T> mapper) throws DataAccessException;

    List<T> findByQuery(PreparedStatement ps, RowMapper<T> mapper) throws DataAccessException;

    List<T> findByQuery(String query, RowMapper<T> mapper, Object... params) throws DataAccessException;
    
    Integer executeUpdateByQuery(String query, Object... parameters) throws DataAccessException;

    List<T> findByStoredProcedure(String query, RowMapper<T> rowMapper, Object... parameters)
            throws DataAccessException;

    Connection getConnection() throws DataAccessException;

    String getIdColumnName() throws DataAccessException;

    String getTableName() throws DataAccessException;

    void update(ID id, Map<String, Object> columnValues) throws DataAccessException;

    void update(ID id, T b) throws DataAccessException;
}
