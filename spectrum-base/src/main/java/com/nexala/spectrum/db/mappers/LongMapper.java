/**
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 */
package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;

/**
 * A mapper for long.
 * 
 * @author bbaudry
 *
 */
public class LongMapper extends JdbcRowMapper<Long> {

    /**
     * @see com.nexala.spectrum.db.JdbcRowMapper#createRow(java.sql.ResultSet)
     */
    @Override
    public Long createRow(ResultSet resultSet) throws SQLException {
        return resultSet.getLong("Value");
    }

}
