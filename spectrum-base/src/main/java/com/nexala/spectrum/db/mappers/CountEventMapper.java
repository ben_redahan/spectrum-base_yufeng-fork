package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.DataAccessException;

public class CountEventMapper extends JdbcRowMapper<Integer> {

    @Override
    public Integer createRow(ResultSet resultSet) throws SQLException {
        Integer count = 0;

        try {
            while (resultSet.next()) {
                count++;
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }

        return count;
    }
}
