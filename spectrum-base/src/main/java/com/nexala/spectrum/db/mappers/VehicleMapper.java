package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.Vehicle;

public class VehicleMapper extends JdbcRowMapper<Vehicle> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Vehicle createRow(ResultSet rs) throws SQLException {
        return new Vehicle(rs.getInt("VehicleID"), rs.getString("VehicleNumber"));
    }
}
