package com.nexala.spectrum.db.dao;

import java.util.List;

import javax.inject.Inject;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FleetFormation;
import com.nexala.spectrum.db.mappers.FleetFormationMapper;

/**
 * dao for fleet formation table.
 * @author BBaudry
 *
 */
public class FleetFormationDao extends GenericDao<FleetFormation, String> {
    
    @Inject
    private FleetFormationMapper mapper;
    
    @Inject
    private ApplicationConfiguration appConf;

    public List<FleetFormation> getValidFormations() {
        String query = appConf.getQuery(Spectrum.GET_VALID_FLEET_FORMATIONS);
        return findByQuery(query, mapper);
    }

    @Override
    public JdbcRowMapper<FleetFormation> getRowMapper() {
        return mapper;
    }
    
    public String getTableName() {
        return "FleetFormation";
    }

    public String getIdColumnName() {
        return "ID";
    }
    
   
    

}
