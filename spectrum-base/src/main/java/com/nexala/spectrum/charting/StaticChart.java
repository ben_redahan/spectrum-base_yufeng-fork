/*
 * 
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.charting;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.VerticalAlignment;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.charting.DigitalTimeSeries;
import com.nexala.spectrum.charting.TimeChart;
import com.nexala.spectrum.charting.XYLineAndShapeGapRenderer;
import com.nexala.spectrum.charting.XYStepGapRenderer;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class StaticChart extends TimeChart {
    private static final Logger LOGGER = Logger.getLogger(StaticChart.class);

    private final int width;
    private final int height;

    private static final int MARGIN_TOP = 0;
    private static final int MARGIN_LEFT = 10;
    private static final int MARGIN_BOTTOM = 0;
    private static final int MARGIN_RIGHT = 0;

    public static enum ChartType {
        DIGITAL, ANALOGUE
    };

    private final List<ChannelConfig> analogue = new ArrayList<ChannelConfig>();
    private final List<ChannelConfig> digital = new ArrayList<ChannelConfig>();
    private StaticChartConfig config = null;
    private final Locale locale = Locale.UK; // FIXME

    @Inject
    public StaticChart(ConfigurationManager conf, ApplicationConfiguration appConfig,
            @Assisted("width") Integer width,
            @Assisted("height") Integer height,
            @Assisted List<ChannelConfig> channels,
            @Assisted TimeZone timezone, @Assisted StaticChartConfig config) {

        super(channels, timezone);

        this.config = config;
        this.height = height;
        this.style = new StaticChartStyle();

        slotInMilliseconds = appConfig.get().getInt("r2m.dataView.dataPlotSlotInSeconds") * 1000;

        for (int i = 0; i < channels.size(); i++) {
            ChannelConfig channel = channels.get(i);

            final String name = channel.getName();
            TimeSeries timeserie = null;

            if (channel.getType() == ChannelType.DIGITAL) {
                if (ChartType.DIGITAL.equals(config.getType())) {
                    // Graph with lowest database order is highest
                    timeserie = new DigitalTimeSeries(name, i);
                    digital.add(channel);
                } else if (ChartType.ANALOGUE.equals(config.getType())) {
                    // Display the digital channel on the static charts
                    timeserie = getAnalogDataset(config, channel, name);
                    analogue.add(channel);
                }

            } else if (channel.getType() == ChannelType.ANALOG) {

                timeserie = getAnalogDataset(config, channel, name);
                analogue.add(channel);

            }
            if (timeserie != null) {
                channelIdToDataset.put(name, timeserie);
            }

        }

        this.width = width;
    }

    private TimeSeries getAnalogDataset(StaticChartConfig config,
            ChannelConfig channel, final String name) {
        final TimeSeries dataset;
        BigDecimal scale = config.getScaleByChannelsId().get(channel.getId());
        if (scale != null && scale.compareTo(BigDecimal.ONE) != 0) {
            dataset = new ScaledValueTimeSeries(name,
                    ScaledValueTimeSeries.OPERATION_MULTIPLY,
                    scale.doubleValue());
        } else {
            dataset = new DefaultTimeSeries(name);
        }
        return dataset;
    }

    private XYPlot createDigitalPlot() {
        if (digital.size() < 1) {
            return null;
        }

        DateAxis dateAxis = new DateAxis();
        dateAxis.setTimeZone(tz);

        // XXX styleXYPlot(plot)
        XYPlot plot = new XYPlot();
        plot.setDomainAxis(dateAxis);
        plot.setRangeGridlinePaint(style.getGridlineColor());
        plot.setDomainGridlinePaint(style.getGridlineColor());
        plot.setRenderer(new StandardXYItemRenderer());

        XYStepRenderer renderer = slotInMilliseconds != null ? new XYStepGapRenderer(
                slotInMilliseconds) : new XYStepRenderer();
        renderer.setUseOutlinePaint(true);
        renderer.setUseFillPaint(false);
        renderer.setUseOutlinePaint(false);

        String[] axisLabels = new String[digital.size()];

        TimeSeriesCollection dataset = new TimeSeriesCollection(tz);

        for (int i = 0; i < digital.size(); i++) {
            ChannelConfig channel = digital.get(i);
            Color color = colorGenerator.next();
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesPaint(i, color);
            renderer.setSeriesOutlinePaint(i, color);
            renderer.setSeriesStroke(i, new BasicStroke(2.0f));

            TimeSeries series = channelIdToDataset.get(channel.getName());

            dataset.addSeries(series);

            plot.setRenderer(i, renderer);

            axisLabels[i] = "";

        }

        ValueAxis axis = new SymbolAxis(null, axisLabels);

        axis.setFixedDimension(50);
        axis.setLabelFont(style.getLegendFont());
        axis.setLowerBound(-0.5);
        axis.setUpperBound(digital.size());

        plot.setRangeAxis(axis);
        plot.setDataset(dataset);
        plot.setAxisOffset(new RectangleInsets(MARGIN_TOP, MARGIN_LEFT,
                MARGIN_BOTTOM, MARGIN_RIGHT));

        return plot;
    }

    private XYPlot createAnaloguePlots() {
        DateAxis dateAxis = new DateAxis();
        dateAxis.setTimeZone(tz);

        XYPlot plot = new XYPlot();
        plot.setDomainAxis(dateAxis);
        plot.setRangeGridlinePaint(style.getGridlineColor());
        plot.setDomainGridlinePaint(style.getGridlineColor());

        final AxisLocation location = AxisLocation.BOTTOM_OR_LEFT;
        plot.setRangeAxisLocation(location);

        // Add the axis
        NumberAxis axis = new NumberAxis();

        BigDecimal minValue = config.getLowerBound();
        BigDecimal maxValue = config.getUpperBound();

        double maxValueDbl = maxValue == null ? MAX_VALUE : maxValue
                .doubleValue();
        axis.setRange(minValue == null ? MIN_VALUE : minValue.doubleValue(),
                maxValueDbl);
        axis.setLabelFont(style.getLegendFont());
        axis.setAxisLineStroke(new BasicStroke(1.0f));
        axis.setFixedDimension(50);
        axis.setTickUnit(new NumberTickUnit(maxValueDbl / config.getTicks()));// Number
                                                                              // of
                                                                              // ticks
                                                                              // for
                                                                              // the
                                                                              // chart
        plot.setRangeAxis(axis);
        plot.setAxisOffset(new RectangleInsets(MARGIN_TOP, MARGIN_LEFT,
                MARGIN_BOTTOM, MARGIN_RIGHT));

        for (int i = 0; i < analogue.size(); i++) {
            ChannelConfig channel = analogue.get(i);
            Color color = colorGenerator.next();

            boolean isDisplayedAsDifference = channel.isDisplayedAsDifference();

            TimeSeriesCollection collection = new TimeSeriesCollection(
                    channelIdToDataset.get(channel.getName()), tz);

            // Each series also requires its own renderer, or colours
            // will not render correctly.
            XYLineAndShapeRenderer renderer = slotInMilliseconds != null ? new XYLineAndShapeGapRenderer(
                    slotInMilliseconds) : new XYLineAndShapeRenderer();
            renderer.setUseFillPaint(false);
            renderer.setUseOutlinePaint(false);
            renderer.setSeriesLinesVisible(0, true);
            renderer.setSeriesShapesVisible(0, false);
            renderer.setSeriesPaint(0, color);
            renderer.setSeriesOutlinePaint(0, color);
            renderer.setSeriesStroke(0, new BasicStroke(2.0f));
            renderer.setSeriesToolTipGenerator(0, null);
            renderer.setUseOutlinePaint(true);

            plot.setDataset(i, collection);
            plot.setRenderer(i, renderer);

        }

        return plot;
    }
    
    public void plot(List<DataSet> data) {
        plotData(data, locale, height);
    }

    public byte[] render(int interval, Long endTime)
            throws IOException {
        
        if (endTime == null) {
            endTime = Calendar.getInstance(tz).getTimeInMillis();
        }

        XYPlot plot = null;
        

        List<LegendItemSource> renderers = new ArrayList<LegendItemSource>();

        if (config.getType().equals(ChartType.ANALOGUE)) {
            plot = createAnaloguePlots();

            // Add legend
            int rendererLength = plot.getRendererCount();
            for (int i = 0; i < rendererLength; i++) {
                renderers.add(plot.getRenderer(i));
            }
        } else if (config.getType().equals(ChartType.DIGITAL)) {
            plot = createDigitalPlot();

            // Add legend
            renderers.add(plot.getRenderer());
        }

        plot.getDomainAxis().setRange(endTime - interval, endTime);

        JFreeChart chart = new JFreeChart(null, null, plot, false);
        chart.setBackgroundPaint(Color.white);

        StaticLegendTitle tdsLegend = new StaticLegendTitle(renderers, config,
                height, config.getType().equals(ChartType.DIGITAL));
        chart.addSubtitle(tdsLegend);

        // Add title
        TextTitle tt = new VerticalTextTitle(config.getChartName(), new Font(
                "Tahoma", Font.PLAIN, 12));
        tt.setPosition(RectangleEdge.LEFT);
        tt.setVerticalAlignment(VerticalAlignment.CENTER);
        tt.setPadding(0, 2, 2, 2);
        tt.setWidth(50);

        chart.setTitle(tt);

        return renderChart(height, width, chart);
    }

    public StaticChartConfig getConfig() {
        return config;
    }

}