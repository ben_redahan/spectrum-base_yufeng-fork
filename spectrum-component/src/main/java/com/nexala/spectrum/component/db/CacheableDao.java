package com.nexala.spectrum.component.db;

import java.util.List;

/**
 * Interface to implement on the doas with cacheable objects.
 * @author BBaudry
 *
 * @param <T> type of the cacheable object
 */
public interface CacheableDao<T> {

    public List<T> getAll();
    
}
