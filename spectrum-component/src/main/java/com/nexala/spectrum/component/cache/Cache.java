package com.nexala.spectrum.component.cache;

import java.util.List;

import com.nexala.spectrum.component.db.CacheableDao;

/**
 * A cache of object. Cache all the objects at startup and never refresh it.
 * @author BBaudry
 *
 * @param <T> type of the object that are cached.
 */
public abstract class Cache<T> {
    
    protected CacheableDao<T> cacheable;
    
    protected List<T> data;
    
    public Cache(CacheableDao<T> cacheable) {
        this.cacheable = cacheable;
        data = cacheable.getAll();
        init();
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
    
    /**
     * All extra initialization need to be done on this method 
     * (for example a cache per id)
     */
    protected abstract void init();

}
