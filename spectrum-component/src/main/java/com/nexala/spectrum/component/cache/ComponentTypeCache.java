package com.nexala.spectrum.component.cache;

import javax.inject.Inject;

import com.nexala.spectrum.component.db.ComponentTypeDao;
import com.nexala.spectrum.component.db.beans.ComponentType;

/**
 * Cache of components.
 * @author BBaudry
 *
 */
public class ComponentTypeCache extends Cache<ComponentType> {

    @Inject
    public ComponentTypeCache(ComponentTypeDao dao) {
        super(dao);
    }
    
    @Override
    protected void init() {
        
    }

}
