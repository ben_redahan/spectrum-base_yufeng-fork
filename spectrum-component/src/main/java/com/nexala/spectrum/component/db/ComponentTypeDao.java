package com.nexala.spectrum.component.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.nexala.spectrum.component.ComponentConstants;
import com.nexala.spectrum.component.Configuration;
import com.nexala.spectrum.component.db.beans.ComponentType;

public class ComponentTypeDao extends NamedParameterJdbcDaoSupport implements CacheableDao<ComponentType> {
    
    private final Configuration conf;
    
    @Inject
    public ComponentTypeDao (Configuration conf) {
        this.conf = conf;
    }
    
    private ResultSetExtractor<List<ComponentType>> componentTypeExtractor = new ResultSetExtractor<List<ComponentType>>() {
        @Override
        public List<ComponentType> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<ComponentType> result = new ArrayList<>();
            while (rs.next()) {
                ComponentType ct = new ComponentType();
                ct.setId(rs.getInt("ID"));
                ct.setLevel(rs.getInt("Level"));
                ct.setCode(rs.getString("Code"));
                ct.setType(rs.getString("Type"));

                result.add(ct);
            }
            return result;
        }
    };
    
    /**
     * Returns an active FleetStatus object for a specific unit number
     * @param unitNumber
     * @return
     */
    @Override
    public List<ComponentType> getAll() {
        List<ComponentType> result = new ArrayList<>();
        
        String query = conf.getQuery(ComponentConstants.COMPONENT_TYPE_GET_ALL);
        result = this.getNamedParameterJdbcTemplate().query(query, componentTypeExtractor);
        
        return result;
    }

}
