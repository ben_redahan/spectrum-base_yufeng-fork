package com.nexala.spectrum.formation.db.beans;

public class FleetStatus {
    
    private int id;
    private String headcode;
    private String setcode;
    private String diagram;
    private int unitId;
    private int unitPosition;
    private String unitNumberList;
    private int formationId;
    
    /**
     * Default constructor
     */
    public FleetStatus() {
        
    }
    
    public FleetStatus(int id, String headcode, String setcode, String diagram, int unitId, int unitPosition, String unitNumberList, int formationId) {
        this.id = id;
        this.headcode = headcode;
        this.setcode = setcode;
        this.diagram = diagram;
        this.unitId = unitId;
        this.unitPosition = unitPosition;
        this.unitNumberList = unitNumberList;
        this.formationId = formationId;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeadcode() {
        return headcode;
    }

    public void setHeadcode(String headcode) {
        this.headcode = headcode;
    }

    public String getSetcode() {
        return setcode;
    }

    public void setSetcode(String setcode) {
        this.setcode = setcode;
    }

    public String getDiagram() {
        return diagram;
    }

    public void setDiagram(String diagram) {
        this.diagram = diagram;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getUnitPosition() {
        return unitPosition;
    }

    public void setUnitPosition(int unitPosition) {
        this.unitPosition = unitPosition;
    }

    public String getUnitNumberList() {
        return unitNumberList;
    }

    public void setUnitNumberList(String unitNumberList) {
        this.unitNumberList = unitNumberList;
    }

    public int getFormationId() {
        return formationId;
    }

    public void setFormationId(int formationId) {
        this.formationId = formationId;
    }

}
