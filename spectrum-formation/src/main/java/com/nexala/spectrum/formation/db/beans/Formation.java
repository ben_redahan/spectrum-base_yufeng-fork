package com.nexala.spectrum.formation.db.beans;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Formation {
    private Integer id;
    
    private Date validFrom;
    private Date validTo;
    
    private List<Unit> unitList;
    
    private List<String> unitNumberList;
    private List<Integer> unitIdList;
    
    private String headcode;
    private String activeCab;

    
    /**
     * Default constructor
     */
    public Formation() {
        
    }
    
    public Formation(FormationBuilder builder) {
        this.id = builder.id;
        this.unitNumberList = builder.unitNumberList;
        this.unitIdList = builder.unitIdList;
        this.validFrom = builder.validFrom;
        this.validTo = builder.validTo;
        this.headcode = builder.headcode;
        this.activeCab = builder.activeCab;
    }
        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
    
    public List<String> getUnitNumberList() {
        return unitNumberList;
    }

    public void setUnitNumberList(List<String> unitNumberList) {
        this.unitNumberList = unitNumberList;
    }
    
    public List<Integer> getUnitIdList() {
        return unitIdList;
    }
    
    public void setUnitIdList(List<Integer> unitIdList) {
        this.unitIdList = unitIdList;
    }
    
    /**
     * Returns the Unit IDs from the lists as a comma-separated string
     * @return
     */
    public String getUnitNumberListAsString() {
        return String.join(",", this.unitNumberList);
    }
    
    /**
     * Returns the UnitIDs from the list as a comma-separated string
     * @return 
     */
    public String getUnitIdListAsString() {
        return String.join(",", this.unitIdList.stream().map(s -> s.toString()).collect(Collectors.toList()));
    }

    public String getHeadcode() {
        return headcode;
    }

    public void setHeadcode(String headcode) {
        this.headcode = headcode;
    }
    
    public String getActiveCab() {
        return activeCab;
    }

    public void setActiveCab(String activeCab) {
        this.activeCab = activeCab;
    }

    public List<Unit> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }
        
}
