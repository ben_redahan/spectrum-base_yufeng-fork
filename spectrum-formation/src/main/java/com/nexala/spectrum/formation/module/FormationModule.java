package com.nexala.spectrum.formation.module;

import com.google.inject.AbstractModule;
import com.nexala.spectrum.formation.Configuration;

public class FormationModule extends AbstractModule {

    @Override
    protected void configure() {
        binder().bind(Configuration.class);

    }

}
